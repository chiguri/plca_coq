Require Import List.
Require Import PLCAobject.
Require Import PLCAconstraints PLCAconsistency PLCAplanar PLCAequivalence.

Require Import MergeArea.
Require Import MAconsistency MAconnected MAeuler.


Theorem MA_planar :
 forall {Pl Pr : list Point} {Ll Lr : list Line} {Cl Cr : list Circuit} {Al Ar : list Area} {ol or : Circuit},
  MA_Relation Pl Ll Cl Al ol Pr Lr Cr Ar or ->
   PLCAplanarity_condition Pl Ll Cl Al ol ->
   PLCAplanarity_condition Pr Lr Cr Ar or.
intros.
unfold PLCAplanarity_condition in H0.
destruct H0 as [constraints [consistency [euler connected]]].
assert (const := MAconsistency H consistency constraints).
destruct const.
unfold PLCAplanarity_condition.
split_n 3.
 now auto.
 now auto.
 apply (MAconnected H); now auto.
 apply (MAeuler H); now auto.
Qed.
