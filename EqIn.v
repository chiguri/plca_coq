Require Import List Permutation.


Section EqIn.
Context {A : Type}.

Variable EQ : A -> A -> Prop.
Variable eq_reflexive  : forall(a : A), EQ a a.
Variable eq_symmetric  : forall(a b : A), EQ a b -> EQ b a.
Variable eq_transitive : forall(a b c : A), EQ a b -> EQ b c -> EQ a c.
Variable eq_dec        : forall(a b : A), { EQ a b } + { ~EQ a b }.



Fixpoint EqIn (a : A) (l : list A) :=
match l with
| nil => False
| a' :: l' => EQ a a' \/ EqIn a l'
end.


Lemma eqin_nil :
forall{a : A},
 ~EqIn a nil.
Proof.
intros a.
exact id.
Qed.

Lemma eqin_eq :
forall{a b : A}{l : list A},
 EQ a b -> EqIn a (b::l).
Proof.
intros a b l AB.
simpl.
left; exact AB.
Qed.


Lemma eqin_cons :
forall{a b : A}{l : list A},
 EqIn a l -> EqIn a (b :: l).
Proof.
intros a b l IN.
simpl.
right; exact IN.
Qed.


Lemma In_EqIn :
forall(a : A)(l : list A),
 In a l -> EqIn a l.
Proof.
intros a l IN.
induction l.
 exact IN.
 simpl in IN.
 destruct IN as [IN | IN]; [ subst | ].
  exact (eqin_eq (eq_reflexive _)).
  exact (eqin_cons (IHl IN)).
Qed.


Theorem Permutation_eqin :
 forall (l l' : list A) (x : A),
 Permutation l l' -> EqIn x l -> EqIn x l'.
Proof.
intros l l' x PR IN.
induction PR; simpl in *.
 exact IN.
 destruct IN; auto.
 destruct IN as [IN | [IN | IN ]]; auto.
 auto.
Qed.


(* Decidability *)
Lemma eqin_dec :
forall(a : A)(l : list A),
 { EqIn a l } + { ~EqIn a l }.
Proof.
intros a l.
induction l; simpl.
 right.
 exact id.
 destruct (eq_dec a a0).
  left; left; exact e.
  destruct IHl.
   left; right; exact e.
    right.
    intros D.
    destruct D as [D | D].
     exact (n D).
     exact (n0 D).
Qed.


Lemma EqIn_In :
forall(a : A)(l : list A),
 EqIn a l -> exists b : A, EQ a b /\ In b l.
Proof.
intros a l EIN.
induction l.
 elimtype False; exact (eqin_nil EIN).
 simpl in EIN.
 destruct EIN as [EIN | EIN].
  exists a0; split; [ exact EIN | exact (in_eq _ _) ].
  destruct (IHl EIN) as [b [AB IBL]].
  exists b; split; [ exact AB | exact (in_cons _ _ _ IBL) ].
Qed.


Lemma eqin_inv :
forall (a b : A) (l: list A),
 EqIn a (b :: l) -> EQ a b \/ EqIn a l.
Proof.
intros a b l AB.
simpl in AB.
exact AB.
Qed.


Lemma eqin_app_or :
forall(a : A)(l1 l2 : list A),
 EqIn a (l1 ++ l2) -> EqIn a l1 \/ EqIn a l2.
Proof.
intros a l1 l2 AL.
induction l1; simpl in AL.
 right; exact AL.
 destruct AL as [AL | AL].
 left; exact (eqin_eq AL).
 destruct (IHl1 AL) as [L1 | L2].
  left; simpl.
  right; exact L1.
  right; exact L2.
Qed.


Lemma eqin_or_app :
forall(a : A)(l1 l2 : list A),
 EqIn a l1 \/ EqIn a l2 -> EqIn a (l1 ++ l2).
Proof.
intros a l1 l2 OR.
destruct OR as [L1 | L2].
 induction l1.
  simpl in L1.
  elimtype False; exact L1.
  simpl in *.
  destruct L1 as [L1 | L1].
   left; exact L1.
   right; exact (IHl1 L1).
 induction l1.
  simpl in L2.
  exact L2.
  simpl.
  right; exact IHl1.
Qed.



Lemma eqin_app_iff :
forall(a : A)(l1 l2 : list A),
 EqIn a (l1 ++ l2) <-> EqIn a l1 \/ EqIn a l2.
Proof.
intros a l1 l2.
split; [ exact (eqin_app_or _ _ _) | exact (eqin_or_app _ _ _) ].
Qed.



Lemma eq_eqin_elem :
forall(a b : A)(l : list A),
   EQ a b
-> EqIn a l
-> EqIn b l.
Proof.
intros a b l AB EIN.
induction l; simpl in *.
 exact EIN.
 destruct EIN as [EIN | EIN].
  left; exact (eq_transitive _ _ _  (eq_symmetric _ _ AB) EIN).
  right; exact (IHl EIN).
Qed.


End EqIn.
