Require Import List Permutation Arith.
Require Import EqIn Rotate SetRemove PLCAobject PLCAtrail PLCApath.
Require Import PLCAconsistency PLCAconstraints.

Inductive I : list Point -> list Line -> list Circuit -> list Area -> Circuit -> Prop :=
| single_loop : forall(pl : list Point)(ll : list Line)(x y : Point),
   simplepath x y pl ll
-> x <> y
-> 2 <= length ll
-> I pl ((y,x)::ll) (((y,x)::ll)::(reverse_linelist ((y,x)::ll))::nil) ((((y,x)::ll)::nil)::nil) ((reverse_linelist ((y,x)::ll)))

| add_inline : forall(P : list Point)(L : list Line)(C : list Circuit)(A : list Area)(o : Circuit)
             (a : Area)(x y z : Point)(pxy pyz pzx : list Point)(lxy lyz lzx : list Line),
   I P L C A o
-> In a A
-> In (lxy ++ lyz ++ lzx) a
-> trail x y pxy lxy
-> trail y z pyz lyz
-> trail z x pzx lzx
-> ~LIn (y, z) L
-> y <> z
-> 2 <= length lyz
-> 2 <= length (lxy ++ lzx)
-> I P
     ((y,z)::L)
     (((z,y)::lyz)::(lxy++(y,z)::lzx)::(set_remove eq_listline_dec (lxy ++ lyz ++ lzx) C))
     ((((z,y)::lyz)::nil)
      ::((lxy++(y,z)::lzx)::(set_remove eq_listline_dec (lxy ++ lyz ++ lzx) a))
      ::(set_remove eq_listcircuit_dec a A))
     o


| add_inpath : forall(P : list Point)(L : list Line)(C : list Circuit)(A : list Area)(o : Circuit)
             (a : Area)(x y z s e : Point)(ap pxy pyz pzx : list Point)(al lxy lyz lzx : list Line),
   I P L C A o
-> In a A
-> In (lxy ++ lyz ++ lzx) a
-> trail x y pxy lxy
-> trail y z pyz lyz
-> trail z x pzx lzx
-> simplepath s e ap al
-> 1 <= length (al ++ lyz)
-> 1 <= length (lxy ++ lzx)
-> y <> z \/ s <> e
-> (forall(p : Point), In p ap -> ~In p P)
-> I (ap++P)
     ((y,s)::(e,z)::al++L)
     ((((s,y)::lyz)++((z,e)::(reverse_linelist al)))::(lxy++((y,s)::al)++((e,z)::lzx))::(set_remove eq_listline_dec (lxy ++ lyz ++ lzx) C))
     (((((s,y)::lyz)++((z,e)::(reverse_linelist al)))::nil)
      ::((lxy++((y,s)::al)++((e,z)::lzx))::(set_remove eq_listline_dec (lxy ++ lyz ++ lzx) a))
      ::(set_remove eq_listcircuit_dec a A))
     o

| add_outpath : forall(P : list Point)(L : list Line)(C : list Circuit)(A : list Area)(o : Circuit)
             (x y z s e : Point)(ap pxy pyz pzx : list Point)(al lxy lyz lzx : list Line),
   I P L C A o
-> In (lxy ++ lyz ++ lzx) C
-> o = (lxy ++ lyz ++ lzx)
-> trail x y pxy lxy
-> trail y z pyz lyz
-> trail z x pzx lzx
-> simplepath s e ap al
-> 1 <= length (al ++ lyz)
-> 1 <= length (lxy ++ lzx)
-> y <> z \/ s <> e
-> (forall(p : Point), In p ap -> ~In p P)
-> I (ap++P)
     ((y,s)::(e,z)::al++L)
     ((((s,y)::lyz)++((z,e)::(reverse_linelist al)))::(lxy++((y,s)::al)++((e,z)::lzx))::(set_remove eq_listline_dec (lxy ++ lyz ++ lzx) C))
     (((((s,y)::lyz)++((z,e)::(reverse_linelist al)))::nil)::A)
     (lxy++((y,s)::al)++((e,z)::lzx))

| add_loop : forall(P : list Point)(L : list Line)(C : list Circuit)(A : list Area)(o : Circuit)
             (a : Area)(x y : Point)(ap : list Point)(al : list Line),
   I P L C A o
-> In a A
-> simplepath x y ap al
-> x <> y
-> 2 <= length al
-> (forall(p : Point), In p ap -> ~In p P)
-> I (ap++P)
     ((y,x)::al++L)
     (((y,x)::al)::(reverse_linelist ((y,x)::al))::C)
     ((((y,x)::al)::nil)::((reverse_linelist ((y,x)::al))::a)::(set_remove eq_listcircuit_dec a A))
     o.

Lemma add_pathpoint :
forall{P : list Point}{L : list Line}{C : list Circuit}{A : list Area}{o : Circuit}(a : Area)
(x y z : Point)(pxy pyz pzx : list Point)(lxy lyz lzx : list Line),
   I P L C A o
-> PLCAconstraints L C A o
-> PLCAconsistency P L C A o
-> In a A
-> In (lxy ++ lyz ++ lzx) a
-> trail x y pxy lxy
-> trail y z pyz lyz
-> trail z x pzx lzx
-> In x P /\ In y P /\ In z P.
Proof.
intros P L C A o a x y z pxy pyz pzx lxy lyz lzx i CSS CST INA IAC tl1 tl2 tl3.
destruct CST as [PL [LP [LC [CL [CA [AC CST]]]]]].
apply (AC _ _ INA) in IAC.
inversion tl1; subst.
inversion tl2; subst.
inversion tl3; subst.
elimtype False.
simpl in IAC.
destruct CSS as [LCT [CCT [ACT OCT]]].
exact (C_has_no_nil _ CCT IAC).
simpl in IAC.
split; [ | split ];exact (CPconsistency z _ _ LP CL IAC (in_eq _ _) (InPL_left _ _)).
rewrite <- and_assoc; split.
split; exact (CPconsistency y _ _ LP CL IAC (in_eq _ _) (InPL_left _ _)).
inversion tl3; subst.
exact (CPconsistency y _ _ LP CL IAC (in_eq _ _) (InPL_left _ _)).
refine (CPconsistency z (z, p0) _ LP CL IAC _ (InPL_left _ _)).
right; apply in_app_iff; right; exact (in_eq _ _).
split.
 exact (CPconsistency x _ _ LP CL IAC (in_eq _ _) (InPL_left _ _)).
inversion tl2; subst.
inversion tl3; subst.
split; exact (CPconsistency x _ _ LP CL IAC (in_eq _ _) (InPL_left _ _)).
split; refine (CPconsistency z (z, p0) _ LP CL IAC _ (InPL_left _ _)).
apply in_app_iff; right; exact (in_eq _ _).
apply in_app_iff; right; exact (in_eq _ _).
split.
 refine (CPconsistency y (y, p0) _ LP CL IAC _ (InPL_left _ _)).
apply in_app_iff; right; exact (in_eq _ _).
inversion tl3; subst.
exact (CPconsistency x _ _ LP CL IAC (in_eq _ _) (InPL_left _ _)).
refine (CPconsistency z (z, p3) _ LP CL IAC _ (InPL_left _ _)).
apply in_app_iff; right.
apply in_app_iff; right.
exact (in_eq _ _).
Qed.


Lemma add_pathpoint' :
forall{P : list Point}{L : list Line}{C : list Circuit}{A : list Area}{o : Circuit}
(x y z : Point)(pxy pyz pzx : list Point)(lxy lyz lzx : list Line),
   I P L C A o
-> PLCAconstraints L C A o
-> PLCAconsistency P L C A o
-> In (lxy ++ lyz ++ lzx) C
-> trail x y pxy lxy
-> trail y z pyz lyz
-> trail z x pzx lzx
-> In x P /\ In y P /\ In z P.
Proof.
intros P L C A o x y z pxy pyz pzx lxy lyz lzx i CSS CST INC tl1 tl2 tl3.
destruct CST as [PL [LP [LC [CL [CA [AC CST]]]]]].
inversion tl1; subst.
inversion tl2; subst.
inversion tl3; subst.
elimtype False.
simpl in INC.
destruct CSS as [LCT [CCT [ACT OCT]]].
exact (C_has_no_nil _ CCT INC).
simpl in INC.
split; [ | split ];exact (CPconsistency z _ _ LP CL INC (in_eq _ _) (InPL_left _ _)).
rewrite <- and_assoc; split.
split; exact (CPconsistency y _ _ LP CL INC (in_eq _ _) (InPL_left _ _)).
inversion tl3; subst.
exact (CPconsistency y _ _ LP CL INC (in_eq _ _) (InPL_left _ _)).
refine (CPconsistency z (z, p0) _ LP CL INC _ (InPL_left _ _)).
right; apply in_app_iff; right; exact (in_eq _ _).
split.
 exact (CPconsistency x _ _ LP CL INC (in_eq _ _) (InPL_left _ _)).
inversion tl2; subst.
inversion tl3; subst.
split; exact (CPconsistency x _ _ LP CL INC (in_eq _ _) (InPL_left _ _)).
split; refine (CPconsistency z (z, p0) _ LP CL INC _ (InPL_left _ _)).
apply in_app_iff; right; exact (in_eq _ _).
apply in_app_iff; right; exact (in_eq _ _).
split.
 refine (CPconsistency y (y, p0) _ LP CL INC _ (InPL_left _ _)).
apply in_app_iff; right; exact (in_eq _ _).
inversion tl3; subst.
exact (CPconsistency x _ _ LP CL INC (in_eq _ _) (InPL_left _ _)).
refine (CPconsistency z (z, p3) _ LP CL INC _ (InPL_left _ _)).
apply in_app_iff; right.
apply in_app_iff; right.
exact (in_eq _ _).
Qed.

