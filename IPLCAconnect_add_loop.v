Require Import List Arith Omega.
Require Import EqIn Rotate SetRemove PLCAobject PLCAtrail PLCApath InductivePLCA.
Require Import PLCAconsistency PLCAplanar.


Lemma IPLCAconnect_add_loop :
forall{P : list Point}{L : list Line}{C : list Circuit}{A : list Area}{o : Circuit}(i : I P L C A o)
(o1 o2 : object)(a : Area)(x y : Point)(ap : list Point)(al : list Line),
   In a A
-> DistinctA A
-> PLCAconnect P L C A o1 o2 
->
 (PLCAconnect (ap++P) (((y,x)::al)++L)
 (((y,x)::al)::(reverse_linelist ((y,x)::al))::C)
 ((((y,x)::al)::nil)::((reverse_linelist ((y,x)::al))::a)::(set_remove eq_listcircuit_dec a A))
 o1 o2 /\ o1 <> o_area a /\ o2 <> o_area a)
\/
 (PLCAconnect (ap++P) (((y,x)::al)++L)
 (((y,x)::al)::(reverse_linelist ((y,x)::al))::C)
 ((((y,x)::al)::nil)::((reverse_linelist ((y,x)::al))::a)::(set_remove eq_listcircuit_dec a A))
 (o_area ((reverse_linelist ((y,x)::al))::a)) o2 /\ o1 = o_area a /\ o2 <> o_area a)
\/
 (PLCAconnect (ap++P) (((y,x)::al)++L)
 (((y,x)::al)::(reverse_linelist ((y,x)::al))::C)
 ((((y,x)::al)::nil)::((reverse_linelist ((y,x)::al))::a)::(set_remove eq_listcircuit_dec a A))
 o1 (o_area ((reverse_linelist ((y,x)::al))::a)) /\ o1 <> o_area a /\ o2 = o_area a)
\/
 (PLCAconnect (ap++P) (((y,x)::al)++L)
 (((y,x)::al)::(reverse_linelist ((y,x)::al))::C)
 ((((y,x)::al)::nil)::((reverse_linelist ((y,x)::al))::a)::(set_remove eq_listcircuit_dec a A))
 (o_area ((reverse_linelist ((y,x)::al))::a)) (o_area ((reverse_linelist ((y,x)::al))::a))
  /\o1 = o_area a /\ o2 = o_area a).
Proof.
intros P L C A o I o1 o2 a x y ap al INA DA PC.
induction PC as [ p l INP LIN IPL | l c LIN INC ILC | c a1 INC INA1 ICA | | ].
 left; split; [ | split; discriminate ].
  apply PLcon; [ apply in_app_iff; right; exact INP | apply in_or_app; right; exact LIN | exact IPL ].
 left; split; [ | split; discriminate ].
  apply LCcon; [ apply in_or_app; right; exact LIN | exact (in_cons _ _ _ (in_cons _ _ _ INC)) | exact ILC ].
 destruct (eq_listcircuit_dec a a1) as [EQ | NEQ].
  right; right; left.
   split; [ | split; [ discriminate | simpl; subst; exact (eq_refl _) ]].
    apply CAcon; [ exact (in_cons _ _ _ (in_cons _ _ _ INC)) | exact (in_cons _ _ _ (in_eq _ _)) |  ].
     subst; exact (in_cons _ _ _ ICA).
  left.
   split; [ | split; [ discriminate | intros D; inversion D; subst; exact (NEQ (eq_refl _)) ]].
    apply CAcon; [ exact (in_cons _ _ _ (in_cons _ _ _ INC)) | | exact ICA ].
     right; right.
      refine (in_set_remove _ _ _ _ _ INA1).
       intros D.
        exact (NEQ D).
 destruct IHPC as [[IND [O1 O2]] | [[IND [O1 O2]] | [[IND [O1 O2]] | [IND [O1 O2]]]]].
  left; split; [ exact (SYMME _ _ _ _ _ _ IND) | split; [ exact O2 | exact O1 ]].
  do 2 right; left; split; [ exact (SYMME _ _ _ _ _ _ IND) | split; [ exact O2 | exact O1 ] ].
  right; left; split; [ exact (SYMME _ _ _ _ _ _ IND) | split; [ exact O2 | exact O1 ] ].
  do 3 right; split; [ exact (SYMME _ _ _ _ _ _ IND) | split; [ exact O2 | exact O1 ] ].
 destruct IHPC1 as [[IND1 [O11 O12]] | [[IND1 [O11 O12]] | [[IND1 [O11 O12]] | [IND1 [O11 O12]]]]].
  destruct IHPC2 as [[IND2 [O21 O22]] | [[IND2 [O21 O22]] | [[IND2 [O21 O22]] | [IND2 [O21 O22]]]]].
   left; split; [ exact (TRANS _ _ _ _ _ _ _ IND1 IND2) | split; [ exact O11 | exact O22 ]].
   elimtype False; exact (O12 O21).
   do 2 right; left; split; [ exact (TRANS _ _ _ _ _ _ _ IND1 IND2) | split; [exact O11 | exact O22]].
   elimtype False; exact (O12 O21).
  destruct IHPC2 as [[IND2 [O21 O22]] | [[IND2 [O21 O22]] | [[IND2 [O21 O22]] | [IND2 [O21 O22]]]]].
   right; left; split; [ exact (TRANS _ _ _ _ _ _ _ IND1 IND2) | split; [exact O11 | exact O22]].
   elimtype False; exact (O12 O21).
   do 3 right; split; [ exact (TRANS _ _ _ _ _ _ _ IND1 IND2) | split; [exact O11 | exact O22]].
   elimtype False; exact (O12 O21).
  destruct IHPC2 as [[IND2 [O21 O22]] | [[IND2 [O21 O22]] | [[IND2 [O21 O22]] | [IND2 [O21 O22]]]]].
   elimtype False; exact (O21 O12).
   left; split; [ exact (TRANS _ _ _ _ _ _ _ IND1 IND2) | split; [exact O11 | exact O22]].
   elimtype False; exact (O21 O12).
   do 2 right; left; split; [ exact (TRANS _ _ _ _ _ _ _ IND1 IND2) | split; [exact O11 | exact O22]].
  destruct IHPC2 as [[IND2 [O21 O22]] | [[IND2 [O21 O22]] | [[IND2 [O21 O22]] | [IND2 [O21 O22]]]]].
   elimtype False; exact (O21 O12).
   right; left; split; [ exact (TRANS _ _ _ _ _ _ _ IND1 IND2) | split; [exact O11 | exact O22]].
   elimtype False; exact (O21 O12).
   do 3 right; split; [ exact (TRANS _ _ _ _ _ _ _ IND1 IND2) | split; [exact O11 | exact O22]].
Qed.
