Require Import List Arith Omega.
Require Import EqIn Rotate SetRemove PLCAobject PLCApath PLCAtrail.

(* l = (a, b), a <> b *)
Definition Line_constraint(l : Line) : Prop := (fst l) <> (snd l).

(* c = [(a,b),(b,c),(c,d),(d,e),......,(z,a)]  *)
Definition Circuit_constraints(c : Circuit) : Prop :=
 (exists x : Point, exists pl : list Point, trail x x pl c) /\ 3 <= length c.

Definition Area_constraints(a : Area) : Prop :=
 (forall(c1 c2 : Circuit), In c1 a -> In c2 a -> c1 <> c2 -> ~point_connect c1 c2 /\ ~line_connect c1 c2)
 /\
 (forall(c : Circuit), In c a -> ~RIn c (set_remove eq_listline_dec c a))
 /\
 1 <= length a.

(* P has no constraints *)
Definition Lconstraint(L : list Line)    : Prop := forall(l : Line), In l L -> Line_constraint l.

Definition Cconstraint(C : list Circuit) : Prop := forall(c : Circuit), In c C -> Circuit_constraints c.

Definition Aconstraint(A : list Area)    : Prop := forall(a : Area), In a A -> Area_constraints a.

(* Don't care Point(P) *)
(* Line(L), Circuit(C), Area(A) and outermost have contraint *)
Definition PLCAconstraints(L : list Line)(C : list Circuit)(A : list Area)(o : Circuit) : Prop :=
 Lconstraint L /\ Cconstraint C /\ Aconstraint A /\ Circuit_constraints o.


Lemma trailline_Lcontraint :
forall{x y : Point}{pl : list Point}{ll : list Line},
 trail x y pl ll -> Lconstraint ll.
Proof.
unfold Lconstraint, Line_constraint.
intros x y pl ll tl l LIN D.
destruct l as [lx ly].
simpl in D.
subst ly.
induction tl; simpl in *.
 exact LIN.
 destruct LIN as [LIN1 | LIN2].
  inversion LIN1; subst.
  exact (H (eq_refl _)).
  exact (IHtl LIN2).
Qed.


Lemma simplepathline_Lconstraint :
forall{x y : Point}{pl : list Point}{ll : list Line},
 simplepath x y pl ll -> Lconstraint ll.
Proof.
intros x y pl ll ph.
exact (trailline_Lcontraint (simplepath_onlyif_trail ph)).
Qed.


Lemma C_has_no_nil :
forall(C : list Circuit),
 Cconstraint C -> ~In nil C.
Proof.
intros C CT D.
destruct (CT _ D) as [[c CT1] CT2].
simpl in CT2.
omega.
Qed.


Lemma A_has_no_nil :
forall(A : list Area),
 Aconstraint A -> ~ In nil A.
Proof.
intros A AT D.
destruct (AT _ D) as [AT1 [AT2 AT3]].
simpl in AT3.
omega.
Qed.


Lemma Rot_reverse_linelist :
forall(c : Circuit),
   Circuit_constraints c
-> ~Rot c (reverse_linelist c).
Proof.
intros c CC ROT.
remember (reverse_linelist c) as c1.
destruct CC as [[x [pl TL]] LG].
destruct c.
simpl in LG; omega.
unfold reverse_linelist in Heqc1.
simpl in Heqc1.
 cut(In (reverse l) c1).
 intros.
 apply (RotIn _ _ (l :: c)) in H; [ | exact (Rot_sym _ _ ROT) ].
simpl in H.
destruct H.
inversion TL; subst.
unfold reverse in H; simpl in H.
inversion H; subst.
exact (H6 H3).
inversion TL; subst.
exact (H7 (LIn_reverse _ _ (in_lin _ _ H))).
rewrite Heqc1.
apply in_app_iff.
right; exact (in_eq _ _).
Qed.


Lemma Rot_Circuit_constraints :
forall(c1 c2 : Circuit),
   Rot c1 c2
-> Circuit_constraints c1
-> Circuit_constraints c2.
Proof.
intros c1 c2 ROT CC.
induction ROT.
 exact CC.
 destruct (IHROT CC) as [[x [pl TAL]] CC2].
 split.
  inversion TAL; subst.
  exists p2; exists (pl0 ++ (p2 :: nil)).
  exact (reverse_step_trail H1 H6 H5).
  rewrite app_length.
  simpl in *.
  omega.
Qed.
