Require Import List Permutation Arith Omega.
Require Import EqIn Rotate SetRemove PLCAobject PLCAtrail PLCApath.
Require Import InductivePLCA PLCAconsistency PLCAconstraints.


Theorem IPLCA_is_satisfied_with_consistency_and_constraints :
forall(P : list Point)(L : list Line)(C : list Circuit)(A : list Area)(o : Circuit), 
 I P L C A o -> PLCAconsistency P L C A o /\ PLCAconstraints L C A o.
Proof.
intros P L C A o I.
induction I as
[ pl ll x y ph XY lg |
  P L C A o a x y z pxy pyz pzx lxy lyz lzx I IND INA INC tl1 tl2 tl3 F1 F2 lg1 lg2 |
  P L C A o a x y z s e ap pxy pyz pzx al lxy lyz lzx I IND INA INC tl1 tl2 tl3 ph lg1 lg2 NS F |
  P L C A o x y z s e ap pxy pyz pzx al lxy lyz lzx I IND INC OT tl1 tl2 tl3 ph lg1 lg2 NS F |
  P L C A o a x y ap al I IND INA ph XY lg F ].
 (* simple_loopline *)
+split.
  (* PLCAconsistency *)
  unfold PLCAconsistency.
  split.
   (* PLconsistency *)
   unfold PLconsistency.
   intros p0 PL.
   apply (path_pl_ll ph) in PL; [ | omega ].
   destruct PL as [l [PP1 PP2]].
   exists l.
   simpl; split; [ right; exact PP2 | exact PP1 ].
  split.
   (* LPconsistency *)
   unfold LPconsistency.
   intros p0 l LL PL.
   simpl in LL.
   destruct LL as [LL | LL]; [ subst | ].
   unfold InPL in PL; simpl in PL.
   destruct PL as [PL1 | PL2]; subst; [ exact (path_end_pl ph) | exact (path_start_pl ph) ].
   exact (path_ll_pl ph p0 _ PL LL).
  split.
   (* LCconsistency *)
   unfold LCconsistency.
   intros l LL.
   simpl in LL.
   destruct LL as [LL | LL].
    inversion LL; subst.
     exists ((y, x) :: ll).
     split; exact (in_eq _ _).
     exists (reverse_linelist ((y, x) :: ll)).
     unfold reverse_linelist; simpl.
     apply f_equal with(f:=reverse) in H.
     rewrite reverse_reverse in H.
     rewrite <- H.
     split.
      apply in_app_iff.
      right; exact (in_eq _ _).
      right; left; exact (eq_refl _).
   destruct (LIn_In _ _ LL) as [LL1 | LL2].
    exists ((y,x)::ll).
    split; [ exact (in_cons _ _ _ LL1) | exact (in_eq _ _) ].
    exists (reverse_linelist ((y, x)::ll)).
    split; [ | right; left; exact (eq_refl _) ].
    unfold reverse_linelist; simpl.
    apply in_app_iff; left.
    rewrite <- in_rev.
    apply map_reverse, LL2.
  split.
   (* CLconsistency *)
   unfold CLconsistency.
   intros l c CC CL.
   simpl in CC.
   destruct CC as [CC | [CC | CC]].
    subst; exact (in_lin _ _ CL).
    subst.
    unfold reverse_linelist in CL.
    rewrite <- in_rev in CL.
    apply map_reverse in CL.
    exact (In_LIn_r _ _ CL).
    elimtype False; exact CC.
  split.
   (* CAconsistency *)
   unfold CAconsistency.
   intros c CC CO.
   exists (((y, x) :: ll) :: nil).
   split; [ left | exact (in_eq _ _) ].
   simpl in CC.
   destruct CC as [CC | [CC | CC]].
    exact CC.
    elim CO; symmetry; exact CC.
    elim CC.
  split.
   (* ACconsistency *)
   unfold ACconsistency.
   intros c a AA CA.
   simpl in AA.
   destruct AA as [AA1 | AA2];
   [ subst
   | elimtype False; exact AA2 ].
   simpl in CA.
   destruct CA as [CA | CA].
    subst; exact (in_eq _ _).
    elimtype False; exact CA.
  split.
   (* Outermostconsistency1 *)
   unfold Outermostconsistency1.
   right; left; reflexivity.
  split.
   (* Outermostconsistency2 *)
   unfold Outermostconsistency2.
   intros a AA D.
   simpl in AA.
   destruct AA as [AA | AA]; [ subst | exact AA ].
   simpl in D.
   destruct D as [D | D]; [ | exact D ].
   refine (Rot_reverse_linelist _ _ (Rot_sym _ _ D)).
   unfold Circuit_constraints.
   split; [ exists y; exists (y::pl) | simpl; omega].
   apply step_trail.
   exact (simplepath_onlyif_trail ph).
   exact (not_eq_sym XY).
   intros DL.
   destruct (LIn_In _ _ DL) as [DL1 | DL2].
    inversion ph; subst.
     exact DL1.
     simpl in DL1; destruct DL1.
     inversion H1; subst.
     exact (XY (eq_refl _)).
     exact (H0 (path_ll_pl H x _ (InPL_right _ _) H1)).
    inversion ph; subst.
     exact DL2.
     simpl in DL2; destruct DL2.
     unfold reverse in H1; simpl in H1.
     inversion H1; subst.
     inversion H; subst.
      simpl in lg; omega.
      exact (not_simplepath H).
     exact (H0 (path_ll_pl H x _ (InPL_left _ _) H1)).
  split.
   (* Circuitconsistency *)
   unfold Circuitconsistency.
   intros l c1 c2 C1 C2 LC1 LC2.
   simpl in C1, C2.
   destruct C1 as [C1 | [C1 | C1]];
   [ subst
   | subst
   | elimtype False; exact C1].
   destruct C2 as [C2 | [C2 | C2]];
   [ subst; exact eq_refl
   | elimtype False; subst
   | elimtype False; exact C2].
   simpl in LC1, LC2.
   destruct LC1 as [LC1 | LC1]; [ subst | ].
   unfold reverse_linelist in LC2.
   rewrite <- in_rev, map_reverse in LC2.
   unfold reverse in LC2; simpl in LC2.
   destruct LC2 as [LC2 | LC2].
   inversion LC2; subst y.
   exact (XY (eq_refl _)).
   inversion ph; subst.
    simpl in lg; omega.
    simpl in LC2.
    destruct LC2 as [LC2 | LC2].
    inversion LC2; subst.
    inversion H; subst.
     simpl in lg; omega.
     exact (not_simplepath H).
    apply H0.
    apply (path_ll_pl H x (x, y) (InPL_left _ _) LC2).
   unfold reverse_linelist in LC2.
   simpl in LC2.
   apply in_app_iff in LC2.
   destruct LC2 as [LC2 | LC2].
   rewrite <- in_rev, map_reverse in LC2.
   clear lg XY.
   induction ph; subst.
    simpl in LC1; exact LC1.
    simpl in LC1, LC2.
    destruct LC1 as [LC1 | LC1]; [ subst | ].
    destruct LC2 as [LC2 | LC2].
    unfold reverse in LC2; simpl in LC2.
    inversion LC2; subst.
    exact (H (path_start_pl ph)).
    apply H.
    exact (path_ll_pl ph p3 (p2, p3) (InPL_right _ _) LC2).
    destruct LC2 as [LC2 | LC2].
    unfold reverse in LC2; destruct l; simpl in LC2.
    inversion LC2; subst.
    exact (H (path_ll_pl ph p0 _ (InPL_right _ _) LC1)).
    exact (IHph LC1 LC2).
   simpl in LC2.
   destruct LC2 as [LC2 | LC2]; [ subst | exact LC2].
   unfold reverse in LC1; simpl in LC1.
   inversion ph; subst.
    exact LC1.
    simpl in LC1.
   destruct LC1 as [LC1 | LC1].
   inversion LC1; subst.
   inversion H; subst.
    simpl in lg; omega.
    exact (not_simplepath H).
   exact (H0 (path_ll_pl H x _ (InPL_left _ _) LC1)).
   destruct C2 as [C2 | [C2 | C2]]; 
    [ subst; elimtype False |
      subst; exact eq_refl |
      elimtype False; exact C2].
   simpl in LC1, LC2.
   destruct LC2 as [LC2 | LC2]; [subst | ].
   unfold reverse_linelist in LC1.
   rewrite <- in_rev, map_reverse in LC1.
   unfold reverse in LC1; simpl in LC1.
   destruct LC1 as [LC1 | LC1].
   inversion LC1; subst.
   exact (XY (eq_refl _)).
   inversion ph; subst.
    exact (XY (eq_refl _)).
    simpl in LC1.
    destruct LC1 as [LC1 | LC1].
    inversion LC1; subst.
    inversion H; subst.
     simpl in lg; omega.
     exact (not_simplepath H).
    exact (H0 (path_ll_pl H x _ (InPL_left _ _) LC1)).
  unfold reverse_linelist in LC1.
  rewrite <- in_rev, map_reverse in LC1.
  unfold reverse in LC1; destruct l; simpl in LC1.
  destruct LC1 as [LC1 | LC1].
  inversion LC1; subst.
  inversion ph; subst.
  exact LC2.
  simpl in LC2.
  destruct LC2 as [LC2 | LC2].
  inversion LC2; subst.
  inversion H; subst.
   simpl in lg; omega.
   exact (not_simplepath H).
  exact (H0 (path_ll_pl H p _ (InPL_left _ _) LC2)).
  clear lg XY.
  induction ph; subst.
   simpl in LC1; exact LC1.
   simpl in LC1, LC2.
   destruct LC1 as [LC1 | LC1]; [ subst | ].
   inversion LC1; subst.
   destruct LC2 as [LC2 | LC2]; [ subst | ].
   inversion LC2; subst.
    exact (H (path_start_pl ph)).
    exact (H (path_ll_pl ph p0 _ (InPL_right _ _) LC2)).
   destruct LC2 as [LC2 | LC2]; [ subst | ].
   inversion LC2; subst.
    exact (H (path_ll_pl ph _ _ (InPL_right _ _) LC1)).
    exact (IHph LC1 LC2).
  split.
   (* Areaconsistency *)
   unfold Areaconsistency.
   intros c a1 a2 A1 A2 CA1 CA2.
   simpl in A1, A2.
   destruct A1 as [A1 | A1];
   [ subst
   | elimtype False; exact A1 ].
   destruct A2 as [A2 | A2];
   [ exact A2
   | elimtype False; exact A2 ].
  split.
   (* DistinctP *)
   unfold DistinctP.
   intros p0 pi D.
   clear lg XY.
   induction ph; simpl in D.
   destruct (eq_point_dec p0 p); [ exact D | ].
   simpl in D.
   destruct D as [D | D];
   [ subst; exact (n (eq_refl _))
   | elimtype False; exact D ].
   destruct (eq_point_dec p0 p3); [ subst; exact (H D) | ].
   simpl in D.
   destruct pi.
   elim n; subst; reflexivity.
   destruct D as [D | D];
   [ subst; exact (n (eq_refl _))
   | exact (IHph H0 D)].
  split.
   (* Distinct L *)
   unfold DistinctL.
   intros l li D.
   simpl in D.
   destruct (eq_dline_dec l (y, x)).
   inversion ph; subst.
   simpl in D; exact D.
   inversion H; subst.
    simpl in lg; omega.
    clear li lg; simpl in D; destruct D as [D | [D | D]].
     inversion D; subst.
      apply XY; reflexivity.
      cbv in H3; inversion H3; clear H3; subst.
       exact (not_simplepath H).
     inversion D; subst.
      exact (not_simplepath H).
      cbv in H3; inversion H3; clear H3; subst.
       apply H0; left; reflexivity.
     clear H ph XY; revert p0 pl H0 H1 H2; induction ll; intros p0 pl H0 H1 H2.
      exact D.
      destruct D as [D | D].
       inversion D; subst.
        inversion H1; subst.
         exact (not_simplepath H1).
        cbv in H1; inversion H1; subst.
         apply H0; right; left; reflexivity.
       inversion H1; subst.
        apply IHll with (p0 := p3) (pl := pl0).
         auto.
         intro H3; apply H0; simpl; destruct H3 as [H3 | H3]; auto.
         auto.
         intro H3; apply H2; simpl; auto.
   simpl in D; destruct D as [D | D].
    inversion D; subst.
     apply n; reflexivity.
     unfold reverse in H; destruct l; simpl in H; inversion H; clear H; subst.
      clear n D.
      simpl in li; destruct li as [H | li].
       inversion H; subst; apply XY; reflexivity.
       inversion ph; subst.
        exact li.
        inversion H; subst.
         simpl in lg; omega.
         clear lg; simpl in li; destruct li as [li | [li | li]].
          inversion li; subst.
           exact (not_simplepath H).
          inversion li; subst.
           apply H0; left; reflexivity.
          clear H ph XY; revert p0 pl H0 H1 H2; induction ll; intros p0 pl H0 H1 H2.
           exact li.
           destruct li as [D | D].
            inversion D; subst.
             inversion H1; subst.
              apply H0; right; left; reflexivity.
            inversion H1; subst.
             apply IHll with (p0 := p3) (pl := pl0).
              auto.
              intro H3; apply H0; simpl; destruct H3 as [H3 | H3]; auto.
              auto.
              intro H3; apply H2; simpl; auto.
    destruct li as [li | li].
     apply n; symmetry; exact li.
     clear -ph li D.
      induction ph; simpl in D; [ exact D | ].
       destruct (eq_dline_dec l (p3, p2)); [ subst | ].
        destruct (LIn_In _ _ D) as [D1 | D2].
         exact (H (path_ll_pl ph p3 _ (InPL_left _ _) D1)).
         exact (H (path_ll_pl ph p3 _ (InPL_right _ _) D2)).
        destruct li as [li | li].
         apply n; symmetry; exact li.
         destruct D as [D | D].
          inversion D; subst.
           apply n; reflexivity.
           destruct l; cbv in H0; inversion H0; clear H0 D; subst.
            apply H; apply (path_ll_pl ph _ (p2, p3)).
             right; reflexivity.
             exact li.
          apply IHph; [exact li | exact D].
  split.
   (* DistinctC *)
   unfold DistinctC.
   intros c ci D.
   simpl in ci, D.
   destruct (eq_listline_dec c ((y, x) :: ll)).
   simpl in D.
   destruct D as [D | D]; [| exact D ].
   refine (Rot_reverse_linelist _ _ (Rot_trans _ _ _ (Rot_sym _ _ _) D) ).
   unfold Circuit_constraints.
   split; [exists y; exists (y::pl) | simpl; omega ].
   apply step_trail.
   exact (simplepath_onlyif_trail ph).
   exact (not_eq_sym XY).
   intros DL.
   destruct (LIn_In _ _ DL) as [DL1 | DL2].
    inversion ph; subst.
     exact DL1.
     simpl in DL1; destruct DL1.
     inversion H1; subst.
     exact (XY (eq_refl _)).
     exact (H0 (path_ll_pl H x _ (InPL_right _ _) H1)).
    inversion ph; subst.
     exact DL2.
     simpl in DL2; destruct DL2.
     unfold reverse in H1; simpl in H1.
     inversion H1; subst.
     inversion H; subst.
      simpl in lg; omega.
      exact (not_simplepath H).
     exact (H0 (path_ll_pl H x _ (InPL_left _ _) H1)).
    rewrite e; apply rotSame.
    destruct ci as [ci | [ci | ci]]; [ apply n; symmetry; exact ci | | exact ci].
    destruct (eq_listline_dec c (reverse_linelist ((y, x) :: ll))).
    simpl in D.
    destruct D as [D | D]; [ | exact D].
    clear ci n.
    unfold reverse_linelist in e; simpl in e.
    unfold reverse at 2 in e; simpl in e.
    assert (In (x, y) ((y, x) :: ll)).
    apply RotIn with c.
    rewrite e; apply in_or_app.
    simpl; auto.
    auto.
    destruct H as [H | H].
    inversion H; subst; apply XY; reflexivity.
    inversion ph; subst.
    apply XY; reflexivity.
    destruct H as [H | H].
    inversion H; subst.
    destruct ll0; [simpl in lg; omega | exact (not_simplepath H0) ].
    apply H1; apply (path_ll_pl H0) with (l:=(x, y)).
    simpl; auto.
    auto.
    apply n0; symmetry; exact ci.
  (* DistinctA *)
   unfold DistinctA.
   intros a ai D.
   simpl in D.
   destruct (eq_listcircuit_dec a (((y, x) :: ll) :: nil)); simpl in D.
   exact D.
   destruct ai as [ai | ai]; [apply n; symmetry; exact ai | exact ai].
 (* PLCAconstraints *)
 unfold PLCAconstraints.
 split.
  (* Lconstraints *)
  unfold Lconstraint, Line_constraint.
  intros l IN D.
  destruct l; simpl in D; subst.
  simpl in IN.
  destruct IN as [IN | IN].
  inversion IN; subst.
  exact (XY (eq_refl _)).
  exact ((simplepathline_Lconstraint ph _ IN) (eq_refl _)).
 split.
  (* Cconstraint *)
  unfold Cconstraint, Circuit_constraints.
  intros c CC.
  simpl in CC.
  destruct CC as [CC | CC]; [ subst; simpl | ].
  split; [exists y; exists (y::pl) | omega ].
  apply step_trail; [ exact (simplepath_onlyif_trail ph) | exact (not_eq_sym XY) | ].
  intros D.
  inversion ph; subst.
   simpl in lg; omega.
   simpl in D.
   destruct D as [D | D].
   inversion D; subst.
    exact (XY (eq_refl _)).
    inversion H1; subst.
    inversion H; subst.
     simpl in lg; omega.
     exact (not_simplepath H).
   destruct (LIn_In _ _ D) as [D1 | D2].
    exact (H0 (path_ll_pl H x _ (InPL_right _ _) D1)).
    exact (H0 (path_ll_pl H x _ (InPL_left _ _) D2)).
  destruct CC as [CC | CC];
  [ subst; simpl
  | elimtype False; exact CC ].
  split; [exists y; exists (rev (y::pl)) |].
  apply rev_trail.
  apply (step_trail _ _ _ _ _ (simplepath_onlyif_trail ph) (not_eq_sym XY)).
  intros D.
  destruct (LIn_In _ _ D) as [D1 | D2].
  inversion ph; subst.
   simpl in lg; omega.
   simpl in D1.
   destruct D1 as [D1 | D1].
   inversion D1; subst.
    exact (XY (eq_refl _)).
    exact (H0 (path_ll_pl H x _ (InPL_right _ _) D1)).
  inversion ph; subst.
   simpl in lg; omega.
   simpl in D2.
   destruct D2 as [D2 | D2].
    inversion D2; subst.
    inversion H; subst.
     simpl in lg; omega.
     exact (not_simplepath H).
    exact (H0 (path_ll_pl H x _ (InPL_left _ _) D2)).
  unfold reverse_linelist.
  rewrite <- map_rev, map_length, rev_length.
  exact (le_n_S _ _ lg).
 split.
  (* Aconstraint *)
  unfold Aconstraint.
  intros a AA.
  unfold Area_constraints.
  split.
   intros c1 c2 CA1 CA2 NC.
   cut(~ point_connect c1 c2).
   intros D.
   split; [ exact D | ].
   intros D1.
   exact (D (lc_only_if_pc _ _ D1)).
   intros D.
   unfold point_connect in D.
   destruct D as [l1 [l2 [p0 [D1 [D2 [D3 D4]]]]]].
   simpl in AA.
   destruct AA as [AA | AA]; [ subst | elimtype False; exact AA ].
   simpl in *.
   destruct CA1 as [CA1 | CA1]; [ subst | elimtype False; exact CA1 ].
   destruct CA2 as [CA2 | CA2]; [ subst; exact (NC (eq_refl _)) | elimtype False; exact CA2 ].
  split.
   intros c Ic D.
   simpl in AA.
   destruct AA as [AA | AA]; [ subst | elimtype False; exact AA ].
   simpl in D.
   destruct (eq_listline_dec c ((y, x) :: ll)); simpl in D; [ subst; exact D | ].
   destruct Ic as [Ic | Ic]; [ subst; exact (n (eq_refl _)) | exact Ic ].
   simpl in AA.
   destruct AA as [AA | AA]; [ subst; exact (le_n 1) | elimtype False; exact AA ].
  (* Outermost_constraints *)
  unfold Circuit_constraints.
  split; [exists y; exists (rev (y::pl)) | ].
  apply rev_trail.
  apply step_trail; [ exact (simplepath_onlyif_trail ph) | exact (not_eq_sym XY) | ].
  intros D.
  destruct (LIn_In _ _ D) as [D1 | D2].
  inversion ph; subst.
   simpl in lg; omega.
   simpl in D1.
   destruct D1 as [D1 | D1].
   inversion D1; subst.
    exact (XY (eq_refl _)).
    exact (H0 (path_ll_pl H x _ (InPL_right _ _) D1)).
   inversion ph; subst.
    simpl in lg; omega.
    simpl in D2.
    destruct D2 as [D2 | D2].
     inversion D2; subst.
     inversion H; subst.
      simpl in lg; omega.
      exact (not_simplepath H).
     exact (H0 (path_ll_pl H x _ (InPL_left _ _) D2)).
    unfold reverse_linelist.
    destruct ll; simpl in *; [ omega | ].
    destruct ll; simpl in *; [ omega | ].
    repeat rewrite app_length.
  simpl; omega.
 (* add_inline *)
+destruct IND as [CSS CST].
 destruct (add_pathpoint a x y z pxy pyz pzx lxy lyz lzx I CST CSS INA INC
 tl1 tl2 tl3) as [IX [IY IZ]].
 destruct CSS as [PL [LP [LC [CL [CA [AC [O1 [O2 [CC [AA [DP [DL [DC DA]]]]]]]]]]]]].
 destruct CST as [LT [CT [AT OT]]].
 split.
  (* PLCAconsistency *)
  unfold PLCAconsistency.
  split.
   (* PLconsistency *)
   unfold PLconsistency.
   intros p INP.
   destruct (PL p INP) as [lx [LIN IPL]].
   exists lx; split; [ exact (in_cons _ _ _ LIN) | exact IPL ].
  split.
   (* LPconsistency *)
   unfold LPconsistency.
   intros p l LIN IPL.
   simpl in LIN.
   destruct LIN as [LIN | LIN]; subst.
   cbv in IPL.
   destruct IPL; subst; [ exact IY | exact IZ ].
   exact (LP p l LIN IPL).
  split.
   (* LCconsistency *)
   unfold LCconsistency.
   intros l LIN.
   simpl in LIN.
   destruct LIN as [LIN | LIN].
   inversion LIN; subst.
   exists (lxy ++ (y, z) :: lzx); split; [ | exact (in_cons _ _ _ (in_eq _ _))].
   apply in_or_app; right.
   exact (in_eq _ _).
   destruct l; cbv in H; inversion H; subst.
   exists ((z, y) :: lyz); split; [ exact (in_eq _ _) | exact (in_eq _ _) ].
   destruct (LC l LIN) as [ec [ILC RIN]].
   destruct (eq_circuit_dec ec (lxy ++ lyz ++ lzx)) as [ROT | NROT].
   assert(In l (lxy ++ lyz ++ lzx)).
    exact (RotIn _ _ _ ILC ROT).
   apply in_app_or in H.
   destruct H as [H | H].
   exists (lxy ++ (y, z) :: lzx); split; [ | exact (in_cons _ _ _ (in_eq _ _)) ].
   apply in_or_app; left; exact H.
   apply in_app_or in H.
   destruct H as [H | H].
   exists ((z, y) :: lyz); split; [ exact (in_cons _ _ _ H) | exact (in_eq _ _) ].
   exists (lxy ++ (y, z) :: lzx); split; [ | exact (in_cons _ _ _ (in_eq _ _)) ].
   apply in_or_app; right; right.
   exact H.
   exists ec; split; [ exact ILC | ].
   right; right.
   apply in_set_remove.
   intros D; apply NROT.
   rewrite D; now apply rotSame.
   exact RIN.
  split.
   (* CLconsistency *)
   unfold CLconsistency.
   intros l c ICC ILC.
   simpl in ICC.
   apply (AC _ _ INA) in INC.
   destruct ICC as [ICC | [ICC | ICC]]; subst.
   simpl in ILC.
   destruct ILC; subst.
   left; exact (reverse_ul _ _ (eq_refl _)).
   right.
   refine (CL l (lxy ++ lyz ++ lzx) INC _).
   apply in_or_app; right.
   apply in_or_app; left; exact H.
   apply in_app_or in ILC.
   destruct ILC as [ILC | ILC].
   right.
   refine (CL l (lxy ++ lyz ++ lzx) INC _).
   apply in_or_app; left; exact ILC.
   simpl in ILC.
   destruct ILC as [ILC | ILC]; subst.
   left; exact (ul_refl _).
   right.
   refine (CL l (lxy ++ lyz ++ lzx) INC _).
   apply in_or_app; right.
   apply in_or_app; right; exact ILC.
   apply set_remove_in in ICC.
   right; exact (CL l c ICC ILC).
  split.
   (* CAconsistency *)
   unfold CAconsistency.
   intros c ICC COT.
   simpl in ICC.
   destruct ICC as [ICC | [ICC | ICC]]; subst.
   exists (((z, y) :: lyz) :: nil); split; [ exact (in_eq _ _) | exact (in_eq _ _) ].
   exists ((lxy ++ (y, z) :: lzx) :: set_remove eq_listline_dec (lxy ++ lyz ++ lzx) a).
   split; [ exact (in_eq _ _) | exact (in_cons _ _ _ (in_eq _ _)) ].
   destruct (eq_listline_dec c (lxy ++ lyz ++ lzx)) as [ROT | NROT].
   elim (DC (lxy ++ lyz ++ lzx)).
   exact (AC _ _ INA INC).
   rewrite ROT in ICC; apply in_rin; now auto.
   apply set_remove_in in ICC.
   destruct (CA c ICC COT) as [ea [IAC IAA]].
   destruct (eq_listcircuit_dec ea a) as [EQ | NEQ]; subst.
   exists ((lxy ++ (y, z) :: lzx) :: set_remove eq_listline_dec (lxy ++ lyz ++ lzx) a); split; [ | exact (in_cons _ _ _ (in_eq _ _)) ].
   right.
   apply in_set_remove.
   intros D; apply NROT.
   rewrite D; now auto.
   exact IAC.
   destruct (eq_listcircuit_dec ea a) as [EQA | NEQA].
   elim (DA _ INA).
   apply (in_set_remove eq_listcircuit_dec _ a) in IAA.
   apply in_ain in IAA.
   rewrite EQA in IAA.
   exact IAA.
   exact (not_eq_sym NEQ).
   exists ea; split; [ exact IAC | right; right ].
   apply in_set_remove.
   intros D; apply NEQA.
   rewrite D.
   now auto.
   exact IAA.
  split.
   (* ACconsistency *)
   unfold ACconsistency.
   intros c a0 IAA ICA.
   simpl in IAA.
   destruct IAA as [IAA | [IAA | IAA]]; subst.
   simpl in ICA.
   destruct ICA as [ICA | ICA]; [ subst | elim ICA ].
   exact (in_eq _ _).
   simpl in ICA.
   destruct ICA as [ICA | ICA]; [ subst | ].
   exact (in_cons _ _ _ (in_eq _ _)).
   destruct (eq_circuit_dec c (lxy ++ lyz ++ lzx)) as [ROT | NROT].
   destruct (AT _ INA) as [AT1 [AT2 AT3]].
   elim (AT2 (lxy ++ lyz ++ lzx) INC).
   apply (eq_rin_elem _ _ _ ROT).
   apply in_rin.
   exact ICA.
   apply set_remove_in in ICA.
   right; right.
   apply in_set_remove.
   intros D; apply NROT.
   rewrite D.
   now apply rotSame.
   exact (AC _ _ INA ICA).
   right; right.
   assert (c <> (lxy ++ lyz ++ lzx)).
    intro; subst.
    assert (a = a0).
     apply AA with (lxy ++ lyz ++ lzx).
     exact INA.
     apply set_remove_in in IAA.
     exact IAA.
     exact INC.
     exact ICA.
     subst.
     apply DA with a0.
      exact INA.
      apply in_ain.
      exact IAA.
   apply in_set_remove.
    intro; subst; apply H; reflexivity.
    apply AC with a0.
    apply set_remove_in in IAA.
    exact IAA.
    exact ICA.
  split.
   (* Outermostconsistency1 *)
   unfold Outermostconsistency1.
   right; right.
   destruct (eq_listline_dec o (lxy ++ lyz ++ lzx)) as [ROT | NROT].
   elim (O2 a INA).
   rewrite ROT.
   apply in_rin.
   exact INC.
   apply in_set_remove.
   intros D; apply NROT.
   symmetry; exact D.
   exact O1.
  split.
   (* Outermostconsistency2 *)
   unfold Outermostconsistency2.
   intros a0 IAA NRO.
   simpl in IAA.
   apply (AC _ _ INA) in INC.
   destruct IAA as [IAA | [IAA | IAA]]; subst.
   simpl in NRO.
   destruct NRO as [NRO | NRO]; [ | elim NRO ].
   apply (CL (z, y)) in O1.
   apply LIn_reverse in O1.
   exact (F1 O1).
   exact(RotIn _ _ _ (in_eq _ _) (Rot_sym _ _ NRO)).
   simpl in NRO.
   destruct NRO as [NRO | NRO].
   apply (CL (y, z)) in O1.
   exact (F1 O1).
   refine(RotIn _ _ _ _ (Rot_sym _ _ NRO)).
   apply in_or_app; right.
   exact (in_eq _ _).
   apply O2 with a.
    exact INA.
    apply set_remove_in_r in NRO.
    exact NRO.
   apply set_remove_in in IAA.
   exact (O2 a0 IAA NRO).
  split.
   (* Circuitconsistency *)
   unfold Circuitconsistency.
   intros l c1 c2 IC1 IC2 LC1 LC2.
   simpl in IC1, IC2.
   apply (AC _ _ INA) in INC.
   destruct IC1 as [IC1 | [IC1 | IC1]]; subst.
   destruct IC2 as [IC2 | [IC2 | IC2]]; subst; [ reflexivity | elimtype False | elimtype False ].
   simpl in LC1, LC2.
   destruct LC1 as [LC1 | LC1]; subst.
   apply in_app_or in LC2.
   destruct LC2 as [LC2 | LC2].
   elim F1.
   apply (CL (z, y)) in INC.
   apply LIn_reverse in INC.
   exact INC.
   apply in_or_app; left; exact LC2.
   simpl in LC2.
   destruct LC2 as [LC2 | LC2].
   inversion LC2; subst.
   elim (F2 (eq_refl _)).
   apply (CL (z, y)) in INC.
   apply LIn_reverse in INC.
   exact (F1 INC).
   apply in_or_app; right.
   apply in_or_app; right; exact LC2.
   apply in_app_or in LC2.
   destruct LC2 as [LC2 | LC2].
   destruct CT with (lxy ++ lyz ++ lzx).
    exact INC.
   destruct H as [p [pl H]].
   apply (trail_line_dup l lxy (lyz ++ lzx) H).
    exact LC2.
    apply in_or_app; left; exact LC1.
   simpl in *.
   destruct LC2 as [LC2 | LC2]; subst.
   apply (CL (y, z)) in INC.
   exact (F1 INC).
   apply in_or_app; right.
   apply in_or_app; left; exact LC1.
   rewrite app_assoc in INC.
   destruct CT with ((lxy ++ lyz) ++ lzx).
    exact INC.
   destruct H as [p [pl H]].
   apply (trail_line_dup l (lxy ++ lyz) lzx H).
    apply in_or_app; right; exact LC1.
    exact LC2.
   simpl in LC1.
   destruct LC1 as [LC1 | LC1]; subst.
   apply set_remove_in in IC2.
   apply (CL (z, y) _ IC2) in LC2.
   apply LIn_reverse in LC2.
   exact (F1 LC2).
   assert (c2 = (lxy ++ lyz ++ lzx)).
    apply CC with l.
    apply set_remove_in in IC2.
    exact IC2.
    exact INC.
    exact LC2.
    apply in_or_app.
    right; apply in_or_app.
    left; exact LC1.
   apply DC with c2.
    apply set_remove_in in IC2.
    exact IC2.
    apply in_rin.
    subst.
    exact IC2.
   destruct IC2 as [IC2 | [IC2 | IC2]]; subst; [ elimtype False | reflexivity | elimtype False ].
   apply in_app_or in LC1.
   destruct LC1 as [LC1 | LC1].
   simpl in LC2.
   destruct LC2 as [LC2 | LC2]; subst.
   apply (CL (z, y)) in INC.
   apply LIn_reverse in INC.
   exact (F1 INC).
   apply in_or_app; left; exact LC1.
   destruct CT with (lxy ++ lyz ++ lzx).
    exact INC.
   destruct H as [p [pl H]].
   apply (trail_line_dup l lxy (lyz ++ lzx) H).
    exact LC1.
    apply in_or_app; left; exact LC2.
   simpl in LC1, LC2.
   destruct LC1 as [LC1 | LC1]; subst.
   destruct LC2 as [LC2 | LC2].
   inversion LC2; subst.
   elim (F2 (eq_refl _)).
   apply (CL (y, z)) in INC.
   exact (F1 INC).
   apply in_or_app; right.
   apply in_or_app; left; exact LC2.
   destruct LC2 as [LC2 | LC2]; subst.
   apply (CL (z, y)) in INC.
   apply LIn_reverse in INC.
   exact (F1 INC).
   apply in_or_app; right.
   apply in_or_app; right; exact LC1.
   rewrite app_assoc in INC.
   destruct CT with ((lxy ++ lyz) ++ lzx).
    exact INC.
   destruct H as [p [pl H]].
   apply (trail_line_dup l (lxy ++ lyz) lzx H).
    apply in_or_app; right; exact LC2.
    exact LC1.
   apply in_app_or in LC1.
   destruct LC1 as [LC1 | LC1].
   assert (c2 = (lxy ++ lyz ++ lzx)).
    apply CC with l.
    apply set_remove_in in IC2.
    exact IC2.
    exact INC.
    exact LC2.
    apply in_or_app.
    left; exact LC1.
   apply DC with c2.
    apply set_remove_in in IC2.
    exact IC2.
    apply in_rin.
    subst.
    exact IC2.
   simpl in LC1.
   destruct LC1 as [LC1 | LC1]; subst.
   apply set_remove_in in IC2.
   apply (CL (y, z) _ IC2) in LC2.
   exact (F1 LC2).
   assert (c2 = (lxy ++ lyz ++ lzx)).
    apply CC with l.
    apply set_remove_in in IC2.
    exact IC2.
    exact INC.
    exact LC2.
    apply in_or_app.
    right; apply in_or_app.
    right; exact LC1.
   apply DC with c2.
    apply set_remove_in in IC2.
    exact IC2.
    apply in_rin.
    subst.
    exact IC2.
   destruct IC2 as [IC2 | [IC2 | IC2]]; subst; [ elimtype False | elimtype False | ].
   simpl in LC2.
   destruct LC2 as [LC2 | LC2]; subst.
   apply set_remove_in in IC1.
   apply (CL (z, y) _ IC1) in LC1.
   apply LIn_reverse in LC1.
   exact (F1 LC1).
   assert (c1 = (lxy ++ lyz ++ lzx)).
    apply CC with l.
    apply set_remove_in in IC1.
    exact IC1.
    exact INC.
    exact LC1.
    apply in_or_app.
    right; apply in_or_app.
    left; exact LC2.
   apply DC with c1.
    apply set_remove_in in IC1.
    exact IC1.
    apply in_rin.
    subst.
    exact IC1.
   apply in_app_or in LC2.
   destruct LC2 as [LC2 | LC2].
   assert (c1 = (lxy ++ lyz ++ lzx)).
    apply CC with l.
    apply set_remove_in in IC1.
    exact IC1.
    exact INC.
    exact LC1.
    apply in_or_app.
    left; exact LC2.
   apply DC with c1.
    apply set_remove_in in IC1.
    exact IC1.
    apply in_rin.
    subst.
    exact IC1.
   simpl in LC2.
   destruct LC2 as [LC2 | LC2]; subst.
   apply set_remove_in in IC1.
   apply (CL (y, z) _ IC1) in LC1.
   exact (F1 LC1).
   assert (c1 = (lxy ++ lyz ++ lzx)).
    apply CC with l.
    apply set_remove_in in IC1.
    exact IC1.
    exact INC.
    exact LC1.
    apply in_or_app.
    right; apply in_or_app.
    right; exact LC2.
   apply DC with c1.
    apply set_remove_in in IC1.
    exact IC1.
    apply in_rin.
    subst.
    exact IC1.
   exact (CC l c1 c2 (set_remove_in _ _ _ _ IC1) (set_remove_in _ _ _ _ IC2) LC1 LC2).
  split.
   (* Areaconsistency *)
   unfold Areaconsistency.
   intros c a1 a2 IA1 IA2 CA1 CA2.
   simpl in IA1, IA2.
   apply (AC _ _ INA) in INC.
   destruct IA1 as [IA1 | [IA1 | IA1]]; subst.
   destruct IA2 as [IA2 | [IA2 | IA2]]; subst; [ reflexivity | elimtype False | elimtype False ].
   simpl in CA1, CA2.
   destruct CA1 as [CA1 | CA1]; [ subst | elim CA1].
   destruct CA2 as [CA2 | CA2].
   assert(In (y, z) (lxy ++ (y, z) :: lzx)).
    apply in_or_app; right.
    exact (in_eq _ _).
   rewrite CA2 in H.
   simpl in H.
   destruct H as [H | H]; subst.
   inversion H; subst.
   elim (F2 (eq_refl _)).
   apply (CL (y, z)) in INC.
   exact (F1 INC).
   apply in_or_app; right.
   apply in_or_app; left.
   exact H.
   apply set_remove_in in CA2.
   apply (AC _ _ INA) in CA2.
   apply (CL (z, y)) in CA2.
   apply LIn_reverse in CA2.
   exact (F1 CA2).
   exact (in_eq _ _).
   simpl in CA1.
   destruct CA1 as [CA1 | CA1]; [ subst | elim CA1].
   apply set_remove_in in IA2.
   apply (AC _ _ IA2) in CA2.
   apply (CL (z, y)) in CA2.
   apply LIn_reverse in CA2.
   exact (F1 CA2).
   exact (in_eq _ _).
   destruct IA2 as [IA2 | [IA2 | IA2]]; subst; [ elimtype False | reflexivity | elimtype False ].
   simpl in CA1, CA2.
   destruct CA1 as [CA1 | CA1]; subst.
   destruct CA2 as [CA2 | CA2]; [ | elim CA2].
   assert(In (y, z) (lxy ++ (y, z) :: lzx)).
    apply in_or_app; right.
    exact (in_eq _ _).
   rewrite <- CA2 in H.
   simpl in H.
   destruct H as [H | H].
   inversion H; subst.
   exact (F2 (eq_refl _)).
   apply (CL (y, z)) in INC.
   exact (F1 INC).
   apply in_or_app; right.
   apply in_or_app; left.
   exact H.
   destruct CA2 as [CA2 | CA2]; [ subst | elim CA2].
   apply set_remove_in in CA1.
   apply (AC _ _ INA) in CA1.
   apply (CL (z, y)) in CA1.
   apply LIn_reverse in CA1.
   exact (F1 CA1).
   exact (in_eq _ _).
   simpl in CA1.
   destruct CA1 as [CA1 | CA1]; [ subst | ].
   apply set_remove_in in IA2.
   apply (AC _ _ IA2) in CA2.
   apply (CL (y, z)) in CA2.
   exact (F1 CA2).
   apply in_or_app; right.
   exact (in_eq _ _).
   rewrite (AA c a a2) in IA2.
   elim (DA a2).
   apply set_remove_in in IA2.
   exact IA2.
   apply in_ain.
   exact IA2.
   exact INA.
   apply set_remove_in in IA2.
   exact IA2.
   apply set_remove_in in CA1.
   exact CA1.
   exact CA2.
   destruct IA2 as [IA2 | [IA2 | IA2]]; subst; [ elimtype False | elimtype False | ].
   simpl in CA1, CA2.
   destruct CA2 as [CA2 | CA2]; [ subst | elim CA2].
   apply set_remove_in in IA1.
   apply (AC _ _ IA1) in CA1.
   apply (CL (z, y)) in CA1.
   apply LIn_reverse in CA1.
   exact (F1 CA1).
   exact (in_eq _ _).
   simpl in CA2.
   destruct CA2 as [CA2 | CA2]; subst.
   apply set_remove_in in IA1.
   apply (AC _ _ IA1) in CA1.
   apply (CL (y, z)) in CA1.
   exact (F1 CA1).
   apply in_or_app; right.
   exact (in_eq _ _).
   rewrite (AA c a a1) in IA1.
   elim (DA a1).
   apply set_remove_in in IA1.
   exact IA1.
   apply in_ain.
   exact IA1.
   exact INA.
   apply set_remove_in in IA1.
   exact IA1.
   apply set_remove_in in CA2.
   exact CA2.
   exact CA1.
   exact (AA c a1 a2 (set_remove_in _ _ _ _ IA1) (set_remove_in _ _ _ _ IA2) CA1 CA2).
  split.
   (* DistinctP *)
   unfold DistinctP.
   exact DP.
  split.
   (* DistinctL *)
   unfold DistinctL.
   intros l LIN D.
   simpl in LIN.
   destruct LIN as [LIN | LIN]; subst.
   simpl in D.
   destruct (eq_dline_dec (y, z) (y, z)); subst.
   elim (F1 D).
   simpl in D.
   elim (n (eq_refl _)).
   simpl in D.
   destruct (eq_dline_dec l (y, z)); subst.
   elim (F1 D).
   simpl in D.
   destruct D as [D | D].
   apply F1, (eq_lin_elem _ _ _ D).
   exact (in_lin _ _ LIN).
   exact (DL l LIN D).
  split.
   (* DistinctC *)
   unfold DistinctC.
   intros c RIN D.
   simpl in RIN.
   destruct RIN as [RIN | [RIN | RIN]]; subst.
   simpl in D.
   destruct (eq_listline_dec ((z, y) :: lyz) ((z, y) :: lyz)) as [EQ1 | NEQ1].
   simpl in D.
   destruct D as [D | D].
   assert(In (y, z) ((z, y) :: lyz)).
    refine (RotIn _ _ _ _ (Rot_sym _ _ D)).
    apply in_or_app; right.
    exact (in_eq _ _).
   simpl in H.
   destruct H.
   inversion H; subst.
   elim (F2 (eq_refl _)).
   apply (AC _ _ INA) in INC.
   apply (CL (y, z)) in INC.
   exact (F1 INC).
   apply in_or_app; right.
   apply in_or_app; left.
   exact H.
   apply rin_in in D.
   destruct D as [ec [ROT ICC]].
   apply eq_eqin_in in ICC.
   apply set_remove_eqin in ICC.
   apply eq_eqin_in in ICC.
   apply (CL (z, y)) in ICC.
   apply LIn_reverse in ICC.
   exact (F1 ICC).
   exact (RotIn _ _ _ (in_eq _ _) ROT).
   elim (NEQ1 (eq_refl _)).
   simpl in D.
   destruct (eq_listline_dec (lxy ++ (y, z) :: lzx) ((z, y) :: lyz)) as [EQ2 | NEQ2].
   assert(In (y, z) ((z, y) :: lyz)).
    rewrite <- EQ2.
    apply in_or_app; right.
    exact (in_eq _ _).
   simpl in H.
   destruct H.
   inversion H; subst.
   elim (F2 (eq_refl _)).
   apply (AC _ _ INA) in INC.
   apply (CL (y, z)) in INC.
   exact (F1 INC).
   apply in_or_app; right.
   apply in_or_app; left.
   exact H.
   simpl in D.
   destruct D as [D | D].
   assert(In (y, z) ((z, y) :: lyz)).
    refine (RotIn _ _ _ _ D).
    apply in_or_app; right.
    exact (in_eq _ _).
   simpl in H.
   destruct H.
   inversion H; subst.
   elim (F2 (eq_refl _)).
   apply (AC _ _ INA) in INC.
   apply (CL (y, z)) in INC.
   exact (F1 INC).
   apply in_or_app; right.
   apply in_or_app; left.
   exact H.
   destruct (eq_listline_dec (lxy ++ (y, z) :: lzx) (lxy ++ (y, z) :: lzx)) as [EQ3 | NEQ3].
   apply rin_in in D.
   destruct D as [ec [ROT ICC]].
   apply eq_eqin_in in ICC.
   apply set_remove_eqin in ICC.
   apply eq_eqin_in in ICC.
   apply (CL (y, z)) in ICC.
   exact (F1 ICC).
   refine (RotIn _ _ _ _ ROT).
   apply in_or_app; right.
   exact (in_eq _ _).
   elim (NEQ3 (eq_refl _)).
   simpl in D.
   destruct (eq_listline_dec c ((z, y) :: lyz)) as [EQ4 | NEQ4]; subst.
   apply set_remove_in in RIN.
   apply (CL (z, y)) in RIN.
   apply LIn_reverse in RIN.
   exact (F1 RIN).
   exact (in_eq _ _).
   simpl in D.
   destruct D as [D | D].
   apply set_remove_in in RIN.
   apply in_rin in RIN.
   apply (eq_rin_elem _ _ _ D) in RIN.
   apply rin_in in RIN.
   destruct RIN as [ec [ROT ICC]].
   apply (CL (z, y)) in ICC.
   apply LIn_reverse in ICC.
   exact (F1 ICC).
   exact (RotIn _ _ _ (in_eq _ _) ROT).
   simpl in D.
   destruct (eq_listline_dec c (lxy ++ (y, z) :: lzx)) as [EQ5 | NEQ5]; subst.
   apply set_remove_in in RIN.
   apply (CL (y, z)) in RIN.
   exact (F1 RIN).
   apply in_or_app; right.
   exact (in_eq _ _).
   simpl in D.
   destruct D as [D | D].
   apply set_remove_in in RIN.
   apply in_rin in RIN.
   apply (eq_rin_elem _ _ _ D) in RIN.
   apply rin_in in RIN.
   destruct RIN as [ec [ROT ICC]].
   apply (CL (y, z)) in ICC.
   exact (F1 ICC).
   refine (RotIn _ _ _ _ ROT).
   apply in_or_app; right.
   exact (in_eq _ _).
   elim DC with c.
   apply set_remove_in in RIN.
   exact RIN.
   clear -D.
   induction C.
   simpl in D; elim D.
   simpl in *.
   destruct eq_listline_dec with (lxy ++ lyz ++ lzx) a.
   destruct eq_listline_dec with c a.
   apply set_remove_in_r in D.
   exact D.
   right.
   exact D.
   simpl in D; destruct eq_listline_dec with c a.
   apply set_remove_in_r in D.
   exact D.
   destruct D.
   left; exact H.
   right; apply IHC.
   exact H.
   (* DistinctA *)
   unfold DistinctA.
   intros a0 AIN D.
   simpl in AIN.
   destruct AIN as [AIN | AIN]; subst.
   simpl in D.
   destruct (eq_listcircuit_dec (((z, y) :: lyz) :: nil) (((z, y) :: lyz) :: nil)).
   simpl in D.
   destruct D as [D | D].
   assert (set_remove eq_listline_dec (lxy ++ lyz ++ lzx) a = nil).
    apply eq_area_length in D; simpl in D.
     inversion D.
      destruct (set_remove eq_listline_dec (lxy ++ lyz ++ lzx) a); [auto | inversion H0].
   rewrite H in *; clear H.
   apply eq_area_length_1 in D.
   assert (In (y, z) ((z, y) :: lyz)).
    apply RotIn with (lxy ++ (y, z) :: lzx).
     apply in_or_app; simpl; now auto.
     apply Rot_sym; now auto.
   destruct H.
    inversion H; subst.
     apply F2; now auto.
    apply F1.
     apply CL with (lxy ++ lyz ++ lzx).
      apply AC with a; now auto.
      apply in_or_app; right; apply in_or_app; now auto.
   apply in_split in INA.
   apply F1.
    apply set_remove_in_a in D; apply ain_in in D.
     destruct D as [a' [D1 D2]].
      apply eq_area_length_1_inv in D1; destruct D1 as [c [D1 ?]]; subst.
       apply LIn_reverse_inv; unfold reverse; simpl.
        apply CL with c.
         apply AC with (c :: nil).
          now auto.
          left; now auto.
         apply RotIn with ((z, y) :: lyz).
          left; now auto.
          now auto.
   elim (n (eq_refl _)).
   destruct AIN as [AIN | AIN]; subst.
   simpl in D.
   destruct (eq_listcircuit_dec ((lxy ++ (y, z) :: lzx)
             :: set_remove eq_listline_dec (lxy ++ lyz ++ lzx) a) (((z, y) :: lyz) :: nil)).
   inversion e; subst.
   destruct lxy; simpl in H0.
    inversion H0; subst; apply F2; now auto.
    inversion H0; subst.
    apply F1.
     apply LIn_reverse_inv; unfold reverse; simpl.
      apply CL with (((z, y) :: lxy) ++ (lxy ++ (y, z) :: lzx) ++ lzx).
       apply AC with a; now auto.
       left; now auto.
   destruct D as [D | D].
   apply eq_area_sym in D; apply eq_area_length_1_inv in D; destruct D as [c [D ?]]; subst.
    inversion H; subst.
   assert (In (y, z) ((z, y) :: lyz)).
    apply RotIn with (lxy ++ (y, z) :: lzx).
     apply in_or_app; simpl; now auto.
     apply Rot_sym; now auto.
   destruct H0.
    inversion H0; subst; apply F2; now auto.
    apply F1.
     apply CL with (lxy ++ lyz ++ lzx).
      apply AC with a; now auto.
      apply in_or_app; right; apply in_or_app; now auto.
   destruct (eq_listcircuit_dec ((lxy ++ (y, z) :: lzx)
             :: set_remove eq_listline_dec (lxy ++ lyz ++ lzx) a)).
   apply set_remove_in_a in D.
   apply ain_in in D; destruct D as [a' [D1 D2]].
   apply eq_area_rin with _ _ (lxy ++ (y, z) :: lzx) in D1.
    apply F1.
     apply rin_in in D1; destruct D1 as [c [D0 D1]].
     apply CL with c.
      apply AC with a'; now auto.
      apply RotIn with (lxy ++ (y, z) :: lzx).
       apply in_or_app; simpl; now auto.
       now auto.
    left; now apply rotSame.
   elim (n0 (eq_refl _)).
   simpl in D.
   destruct (eq_listcircuit_dec a0 (((z, y) :: lyz) :: nil)); subst.
   apply set_remove_in in AIN.
   apply F1.
   apply LIn_reverse_inv; unfold reverse; simpl.
    apply CL with ((z, y) :: lyz).
     apply AC with (((z, y) :: lyz) :: nil); simpl; now auto.
     left; now auto.
   destruct D as [D | D].
   apply eq_area_sym in D; apply eq_area_length_1_inv in D; destruct D as [c [D ?]]; subst.
    apply set_remove_in in AIN.
    apply F1.
    apply LIn_reverse_inv; unfold reverse; simpl.
     apply CL with c.
      apply AC with (c :: nil); simpl; now auto.
     apply RotIn with ((z, y) :: lyz).
      left; now auto.
      now auto.
   destruct (eq_listcircuit_dec a0 ((lxy ++ (y, z) :: lzx)
             :: set_remove eq_listline_dec (lxy ++ lyz ++ lzx) a)); subst.
   apply ain_in in D; destruct D as [a' [D1 D2]].
   apply eq_area_rin with _ _ (lxy ++ (y, z) :: lzx) in D1.
    apply F1.
     apply rin_in in D1; destruct D1 as [c [D0 D1]].
     apply CL with c.
      apply set_remove_in in D2.
       apply AC with a'; now auto.
      apply RotIn with (lxy ++ (y, z) :: lzx).
       apply in_or_app; simpl; now auto.
       now auto.
    left; now apply rotSame.
   destruct D as [D | D].
   apply eq_area_sym in D.
   apply eq_area_rin with _ _ (lxy ++ (y, z) :: lzx) in D.
    apply F1.
     apply rin_in in D; destruct D as [c [D0 D1]].
     apply CL with c.
      apply set_remove_in in AIN.
       apply AC with a0; now auto.
      apply RotIn with (lxy ++ (y, z) :: lzx).
       apply in_or_app; simpl; now auto.
       now auto.
    left; now apply rotSame.
   apply set_remove_in in AIN.
   elim (DA a0 AIN).
   clear -D.
   induction A; simpl in *.
    now auto.
    destruct eq_listcircuit_dec with a a1.
     subst.
      destruct eq_listcircuit_dec with a0 a1.
       apply set_remove_in_a in D.
        now auto.
       right; now auto.
     simpl in D; destruct eq_listcircuit_dec with a0 a1.
      apply set_remove_in_a in D.
       now auto.
      destruct D.
       left; now auto.
       right; now auto.
  (* PLCAconstraints *)
  unfold PLCAconstraints.
  split.
   (* Lconstraint *)
   unfold Lconstraint.
   intros l LIN.
   unfold Line_constraint.
   simpl in LIN.
   destruct LIN as [LIN | LIN]; subst.
   cbv; intros; subst.
   exact (F2 (eq_refl _)).
   exact (LT l LIN).
  split.
   (* Cconstraint *)
   unfold Cconstraint.
   intros c ICC.
   unfold Circuit_constraints.
   simpl in ICC.
   destruct ICC as [ICC | [ICC | ICC]]; subst.
   split; [ exists z; exists (z::pyz) | simpl; omega ].
   apply step_trail.
   exact tl2.
   exact (not_eq_sym F2).
   intros D.
   apply (AC _ _ INA) in INC.
   destruct (LIn_In _ _ D) as [D1 | D1].
   apply (CL (z, y)) in INC.
   apply LIn_reverse in INC.
   exact (F1 INC).
   apply in_or_app; right.
   apply in_or_app; left.
   exact D1.
   apply (CL (y, z)) in INC.
   exact (F1 INC).
   apply in_or_app; right.
   apply in_or_app; left.
   exact D1.
   split; [ exists x; exists (pxy++pzx) | ].
   apply app_trail.
   exact tl1.
   exact tl3.
   intros l Lxy Lzx.
   destruct CT with (lxy ++ lyz ++ lzx).
    apply AC with a.
     exact INA.
     exact INC.
   destruct H as [p [pl H]].
   apply (trail_line_dup' l lxy (lyz ++ lzx) H).
    exact Lxy.
    apply lin_or_app; right; exact Lzx.
   exact F2.
   intros D.
   apply (AC _ _ INA) in INC.
   destruct (LIn_In _ _ D) as [D1 | D1].
   apply (CL (y, z)) in INC.
   exact (F1 INC).
   apply in_or_app; left.
   exact D1.
   apply (CL (z, y)) in INC.
   apply LIn_reverse in INC.
   exact (F1 INC).
   apply in_or_app; left.
   exact D1.
   intros D.
   apply (AC _ _ INA) in INC.
   destruct (LIn_In _ _ D) as [D1 | D1].
   apply (CL (y, z)) in INC.
   exact (F1 INC).
   apply in_or_app; right.
   apply in_or_app; right.
   exact D1.
   apply (CL (z, y)) in INC.
   apply LIn_reverse in INC.
   exact (F1 INC).
   apply in_or_app; right.
   apply in_or_app; right.
   exact D1.
   rewrite app_length in *.
   simpl; omega.
   apply set_remove_in in ICC.
   exact (CT _ ICC).
  split.
   (* Aconstraint *)
   unfold Aconstraint.
   intros a0 IAA.
   unfold Area_constraints.
   simpl in IAA.
   destruct IAA as [IAA | [IAA | IAA]]; subst.
   split.
    intros c1 c2 IC1 IC2 NEQ.
    simpl in IC1, IC2.
    destruct IC1 as [IC1 | IC1]; [ subst | elim IC1].
    destruct IC2 as [IC2 | IC2]; [ subst | elim IC2].
    elim (NEQ (eq_refl _)).
   split.
    intros c IC D.
    simpl in IC, D.
    destruct IC as [IC | IC]; [ subst | elim IC].
    destruct (eq_listline_dec ((z, y) :: lyz) ((z, y) :: lyz)).
     elim D.
     elim (n (eq_refl _)).
    simpl; omega.
   split.
    intros c1 c2 IC1 IC2 NEQ.
    simpl in IC1, IC2.
    destruct IC1 as [IC1 | IC1]; [ subst | ].
    destruct IC2 as [IC2 | IC2]; [ subst | ].
    elim (NEQ (eq_refl _)).
    apply not_pc_only_if_not_lc_pc.
    intros D.
    unfold point_connect in D.
    destruct D as [l1 [l2 [p [L1 [L2 [PL1 PL2]]]]]].
    assert(~point_connect (lxy ++ lyz ++ lzx) c2).
     destruct (AT a INA) as [AT1 [AT2 AT3]].
     destruct (eq_listline_dec (lxy ++ lyz ++ lzx) c2) as [EQ1 | NEQ1]; subst.
     elim (AT2 (lxy ++ lyz ++ lzx)).
     apply set_remove_in in IC2.
     exact IC2.
     apply in_rin.
     exact IC2.
     apply set_remove_in in IC2.
     destruct (AT1 (lxy ++ lyz ++ lzx) c2 INC IC2 NEQ1) as [NPC NLC].
     exact NPC.
    elim H.
    unfold point_connect.
    apply in_app_or in L1.
    destruct L1 as [L1 | L1].
    exists l1; exists l2; exists p.
    split.
    apply in_or_app; left; exact L1.
    split; [ exact L2 | ].
    split; [ exact PL1 | exact PL2 ].
    simpl in L1.
    destruct L1 as [L1 | L1]; subst.
    cbv in PL1.
    destruct PL1; subst.
    assert(In p pyz) by exact (trail_start_pl tl2).
    apply (trail_pl_ll tl2) in H0.
    destruct H0 as [el [IPL LIN]].
    exists el; exists l2; exists p.
    split.
     apply in_or_app; right.
     apply in_or_app; left.
     exact LIN.
    split; [ exact L2 | ].
    split; [ exact IPL | exact PL2 ].
    inversion tl2; subst.
    elim (F2 (eq_refl _)).
    simpl; omega.
    assert(In p pyz) by exact (trail_end_pl tl2).
    apply (trail_pl_ll tl2) in H0.
    destruct H0 as [el [IPL LIN]].
    exists el; exists l2; exists p.
    split.
     apply in_or_app; right.
     apply in_or_app; left.
     exact LIN.
    split; [ exact L2 | ].
    split; [ exact IPL | exact PL2 ].
    inversion tl2; subst.
    elim (F2 (eq_refl _)).
    simpl; omega.
    exists l1; exists l2; exists p.
    split.
     apply in_or_app; right.
     apply in_or_app; right.
     exact L1.
    split; [ exact L2 | ].
    split; [ exact PL1 | exact PL2].
    destruct IC2 as [IC2 | IC2]; [ subst | ].
    apply not_pc_only_if_not_lc_pc.
    intros D.
    unfold point_connect in D.
    destruct D as [l1 [l2 [p [D1 [D2 [D3 D4]]]]]].
    assert (c1 <> (lxy ++ lyz ++ lzx)).
     intro.
     destruct AT with a.
      now auto.
      destruct H1.
       apply H1 with c1.
        apply set_remove_in in IC1.
         now auto.
       subst; apply in_rin; now auto.
    apply set_remove_in in IC1.
    apply in_app_or in D2; destruct D2 as [D2 | [D2 | D2]].
     destruct AT with a.
      now auto.
      destruct H0 with c1 (lxy ++ lyz ++ lzx).
       now auto.
       now auto.
       now auto.
       apply H2; unfold point_connect.
        exists l1, l2, p.
        split.
         now auto.
         split.
          apply in_or_app; now auto.
          split; now auto.
     assert (exists l : Line, In l (lxy ++ lyz ++ lzx) /\ InPL p l).
     {
      inversion D2; subst.
      destruct D4 as [D4 | D4]; simpl in D4; subst.
       inversion tl2; subst.
        elim F2; now auto.
        exists (p, p2); split.
         apply in_or_app; simpl; now auto.
         simpl; now auto.
       inversion tl3; subst.
        inversion tl1; subst.
         simpl in lg2; omega.
         exists (x, p2); split; simpl; now auto.
        exists (p, p2); split; simpl.
         apply in_or_app; right; apply in_or_app; simpl; now auto.
         now auto.
     }
     destruct AT with a.
      now auto.
      destruct H1 with c1 (lxy ++ lyz ++ lzx); auto.
       apply H3; unfold point_connect.
        destruct H0 as [l [Hi Hp]].
        exists l1, l, p.
         split.
          now auto.
         split.
          now auto.
         split.
          now auto.
          now auto.
     destruct AT with a.
      now auto.
      destruct H0 with c1 (lxy ++ lyz ++ lzx).
       now auto.
       now auto.
       now auto.
       apply H2; unfold point_connect.
        exists l1, l2, p.
        split.
         now auto.
         split.
          apply in_or_app; right; apply in_or_app; now auto.
          split; now auto.
    apply set_remove_in in IC1.
    apply set_remove_in in IC2.
    destruct (AT a INA) as [AT1 [AT2 AT3]].
    exact (AT1 c1 c2 IC1 IC2 NEQ).
   split.
    intros c ICC D.
    simpl in ICC, D.
    destruct ICC as [ICC | ICC]; [subst | ].
    destruct (eq_listline_dec (lxy ++ (y, z) :: lzx) (lxy ++ (y, z) :: lzx)).
    apply rin_in in D.
    destruct D as [a1 [ROT IAA]].
    apply set_remove_in in IAA.
    apply (AC a1 a INA) in IAA.
    apply (CL (y, z) a1) in IAA.
    elim (F1 IAA).
    refine (RotIn _ _ _ _ ROT).
    apply in_or_app; right.
    exact (in_eq _ _).
    elim (n (eq_refl _)).
   destruct (eq_listline_dec c (lxy ++ (y, z) :: lzx)); subst.
    apply set_remove_in in ICC.
    assert (X := AC (lxy ++ (y, z) :: lzx) a INA ICC).
    apply (CL (y, z) (lxy ++ (y, z) :: lzx)) in X.
    apply F1.
    exact X.
    apply in_or_app; right; now apply in_eq.
    simpl in D.
    destruct D as [D | D].
    apply set_remove_in in ICC.
    apply (AC c a INA) in ICC.
    apply (CL (y, z) c) in ICC.
    elim (F1 ICC).
    refine (RotIn _ _ _ _ (Rot_sym _ _ D)).
    apply in_or_app; right.
    exact (in_eq _ _).
    apply set_remove_in in ICC.
    destruct (AT a INA) as [AT1 [AT2 AT3]].
    apply (AT2 c ICC).
    clear -D.
    induction a; simpl in *.
     exact D.
     destruct eq_listline_dec with (lxy ++ lyz ++ lzx) a.
      destruct eq_listline_dec with c a.
       apply set_remove_in_r in D; now auto.
       right; now auto.
      simpl in D; destruct eq_listline_dec with c a.
       apply set_remove_in_r in D; now auto.
       destruct D as [D | D].
        left; now auto.
        right; now auto.
    simpl; omega.
   apply set_remove_in in IAA.
   exact (AT a0 IAA).
   (* Outermost constraints *)
   exact OT.
 (* add_inpath *)
+destruct IND as [CSS CST].
 destruct (add_pathpoint a x y z pxy pyz pzx lxy lyz lzx I CST CSS INA INC
 tl1 tl2 tl3) as [IX [IY IZ]].
 destruct CSS as [PL [LP [LC [CL [CA [AC [O1 [O2 [CC [AA [DP [DL [DC DA]]]]]]]]]]]]].
 destruct CST as [LT [CT [AT OT]]].
 split.
  (* PLCAconsistency *)
  unfold PLCAconsistency.
  split.
   (* PLconsistency *)
   unfold PLconsistency.
   intros p INP.
   destruct (in_app_or ap P p INP) as [INP1 | INP2].
   destruct al as [ | [l' al]].
    inversion ph; subst.
    simpl in INP1; destruct INP1.
     exists (y, e); subst.
     split; compute; auto.
     elim H.
   apply (path_pl_ll ph) in INP1; [ | simpl; omega ].
   destruct INP1 as [l [AL1 AL2]].
   exists l; split; [ | exact AL1].
   simpl; right; right.
   destruct AL2 as [AL2 | AL2].
    left; exact AL2.
    right; apply in_or_app; left.
    exact AL2.
   destruct (PL _ INP2) as [l [IN IPL]].
   exists l; split; [ | exact IPL ].
   right; right.
   apply in_or_app; right.
   exact IN.
  split.
   (* LPconsistency *)
   unfold LPconsistency.
   intros p l LIN IPL.
   simpl in LIN.
   destruct LIN as [LIN | [LIN | LIN]]; [ subst | subst | ].
   unfold InPL in IPL; simpl in IPL.
   destruct IPL as [IPL | IPL]; subst.
   apply in_app_iff; right; exact IY.
   apply in_app_iff; left; exact (path_start_pl ph).
   unfold InPL in IPL; simpl in IPL.
   destruct IPL as [IPL | IPL]; subst.
   apply in_app_iff; left; exact (path_end_pl ph).
   apply in_app_iff; right; exact IZ.
   apply in_app_iff in LIN.
   destruct LIN as [LIN | LIN].
    apply in_app_iff; left.
    exact (path_ll_pl ph p _ IPL LIN).
    apply in_app_iff; right.
    exact (LP p l LIN IPL).
  split.
   (* LCconsistency *)
   unfold LCconsistency.
   intros l LIN.
   simpl in LIN.
   destruct LIN as [LIN | [LIN | LIN]].
    simpl.
    inversion LIN; subst.
     exists (lxy ++ (y, s) :: al ++ (e, z) :: lzx).
     split; [ | exact (in_cons _ _ _ (in_eq _ _)) ].
     apply in_app_iff; right; exact (in_eq _ _).
     exists (((s, y) :: lyz ++ (z, e) :: reverse_linelist al)).
     split; [ | left; exact (eq_refl _) ].
     apply f_equal with(f:=reverse) in H.
     rewrite reverse_reverse in H.
     simpl.
     left; symmetry; exact H.
    inversion LIN; subst.
     exists (lxy ++ ((y, s) :: al) ++ (e, z) :: lzx).
     split; [ | exact (in_cons _ _ _ (in_eq _ _)) ].
     apply in_app_iff; right; right.
     apply in_app_iff; right; exact (in_eq _ _).
     apply f_equal with(f:=reverse) in H.
     rewrite reverse_reverse in H.
     exists (((s, y) :: lyz) ++ (z, e) :: reverse_linelist al).
     split; [ | exact (in_eq _ _) ].
     apply in_app_iff; right.
     rewrite H.
     exact (in_eq _ _).
    apply lin_app_or in LIN.
    destruct LIN as [LIN | LIN].
     destruct (LIn_In _ _ LIN).
      exists (lxy ++ ((y, s) :: al) ++ (e, z) :: lzx).
      split; [ | exact (in_cons _ _ _ (in_eq _ _)) ].
      apply in_app_iff; right.
      apply in_app_iff; left.
      exact (in_cons _ _ _ H).
      exists (((s, y) :: lyz) ++ (z, e) :: reverse_linelist al).
      split; [ | exact (in_eq _ _) ].
      apply in_app_iff; right; right.
      unfold reverse_linelist.
      rewrite <- in_rev.
      apply map_reverse.
      exact H.
   apply LC in LIN.
   destruct LIN as [c [LIC IC]].
   destruct (eq_circuit_dec (lxy ++ lyz ++ lzx) c).
   apply Rot_sym, (RotIn _ _ _ LIC) in r.
   apply in_app_iff in r.
   destruct r as [R | R].
   exists (lxy ++ ((y, s) :: al) ++ (e, z) :: lzx).
   split; [ | exact (in_cons _ _ _ (in_eq _ _)) ].
   apply in_app_iff; now left.
   apply in_app_iff in R.
   destruct R as [R | R].
   exists (((s, y) :: lyz) ++ (z, e) :: reverse_linelist al).
   split; [ | exact (in_eq _ _) ].
   simpl; right; apply in_app_iff; left; exact R.
   exists (lxy ++ ((y, s) :: al) ++ (e, z) :: lzx).
   split; [ | exact (in_cons _ _ _ (in_eq _ _)) ].
   apply in_app_iff; right; simpl; right.
   apply in_app_iff; right; simpl; right; exact R.
   exists c; split; [ exact LIC | simpl; right; right ].
   assert (lxy ++ lyz ++ lzx <> c) as n'.
    intro; apply n; subst; apply rotSame.
   exact (in_set_remove eq_listline_dec _ _ _ n' IC).
  split.
   (* CLconsistency *)
   unfold CLconsistency.
   intros l c ICC ILC.
   simpl in ICC.
   destruct ICC as [ICC | [ICC | ICC]]; [ subst | subst | ].
   simpl in ILC.
   destruct ILC as [ILC | ILC]; [ subst | ].
   left; exact (reverse_ul _ _(eq_refl _)).
   apply in_app_iff in ILC.
   destruct ILC as [ILC | ILC].
   simpl; right; right.
   apply lin_or_app; right.
   apply (AC _ _ INA) in INC.
   apply (CL l) in INC; [ exact INC | ].
   apply in_app_iff; right.
   apply in_app_iff; left; exact ILC.
   simpl in ILC.
   destruct ILC as [ILC | ILC]; [ subst | ].
   right; left; exact (reverse_ul _ _ (eq_refl _)).
   unfold reverse_linelist in ILC.
   rewrite <- in_rev, map_reverse in ILC.
   simpl; right; right.
   apply lin_or_app; left.
   exact (In_LIn_r _ _ ILC).
   apply in_app_iff in ILC.
   destruct ILC as [ILC | ILC].
   apply (AC _ _ INA) in INC.
   simpl; right; right.
   apply lin_or_app; right.
   apply (CL l) in INC; [ exact INC | ].
   apply in_app_iff; left; exact ILC.
   simpl in ILC.
   destruct ILC as [ILC | ILC]; [ subst | ].
   left; exact (ul_refl _ ).
   apply in_app_iff in ILC.
   destruct ILC as [ILC | ILC].
   simpl; right; right.
   apply lin_or_app; left; exact (in_lin _ _ ILC).
   simpl in ILC.
   destruct ILC as [ILC | ILC]; [ subst | ].
   simpl; right; left; exact (ul_refl _).
   apply (AC _ _ INA) in INC.
   simpl; right; right.
   apply lin_or_app; right.
   apply (CL l) in INC; [ exact INC | ].
   apply in_app_iff; right.
   apply in_app_iff; right; exact ILC.
   simpl; right; right.
   apply lin_or_app; right.
   exact (CL _ _ (set_remove_in eq_listline_dec _ _ _ ICC) ILC).
  split.
   (* CAconsistency *)
   unfold CAconsistency.
   intros c ICC ICN.
   simpl in ICC.
   destruct ICC as [ICC | [ICC | ICC]]; [ subst | subst | ].
   exists (((s, y) :: lyz ++ (z, e) :: reverse_linelist al) :: nil).
   split; [ exact (in_eq _ _) | exact (in_eq _ _) ].
   exists ((lxy ++ ((y, s) :: al) ++ (e, z) :: lzx)
      :: set_remove eq_listline_dec (lxy ++ lyz ++ lzx) a).
   split; [ exact (in_eq _ _) | exact (in_cons _ _ _ (in_eq _ _)) ].
   destruct (eq_listline_dec c (lxy ++ lyz ++ lzx)).
   subst.
   elimtype False.
   apply (DC (lxy ++ lyz ++ lzx)).
   exact (AC _ _ INA INC).
   destruct (in_split _ C (set_remove_in _ _ _ _ ICC)) as [C1 [C2 IC]]; subst.
   apply in_rin.
   exact ICC.
   apply (CA _ (set_remove_in eq_listline_dec _ _ _ ICC)) in ICN.
   destruct ICN as [a0 [ICN1 ICN2]].
   destruct (eq_listcircuit_dec a a0) as [EQ | NEQ]; [ subst | ].
   exists ((lxy ++ ((y, s) :: al) ++ (e, z) :: lzx)
         :: set_remove eq_listline_dec (lxy ++ lyz ++ lzx) a0).
   split; [ | exact (in_cons _ _ _ (in_eq _ _)) ].
   right; refine (in_set_remove _ _ _ _ _ ICN1).
   intro D; apply Rot_not_eq with C (lxy ++ lyz ++ lzx) c.
   exact DC.
   exact (AC _ _ ICN2 INC).
   exact (AC _ _ ICN2 ICN1).
   intro H0; apply n; symmetry; exact H0.
   subst; now apply rotSame.
   destruct (eq_area_dec a a0) as [EQ1 | NEQ2].
    elimtype False; apply (DA a).
    exact INA.
    assert (In a0 (set_remove eq_listcircuit_dec a A)).
    apply in_set_remove.
    exact NEQ.
    exact ICN2.
    destruct (in_split _ _ H) as [a1 [a2 Hs]]; rewrite Hs.
    apply eqin_or_app; right; left; exact EQ1.
   exists a0; split; [exact ICN1 | right; right ].
   exact (in_set_remove _ _ _ _ NEQ ICN2).
  split.
   (* ACconsistency *)
   unfold ACconsistency.
   intros c a0 IAA IAC.
   simpl in IAA.
   destruct IAA as [IAA | [IAA | IAA]]; subst.
   simpl in IAC.
   destruct IAC as [IAC | IAC];
   [ subst; exact (in_eq _ _)
   | elim IAC ].
   simpl in IAC.
   destruct IAC as [IAC | IAC];
   [ subst; exact (in_cons _ _ _ (in_eq _ _))
   | simpl; right; right ].
   destruct (eq_listline_dec (lxy ++ lyz ++ lzx) c); [ subst | ].
   elimtype False.
   destruct (AT _ INA) as [AT1 [AT2 AT3]].
   apply (AT2 (lxy ++ lyz ++ lzx)).
   exact INC.
   apply in_rin.
   exact IAC.
   apply (in_set_remove _ _ _ _ n).
   exact (AC _ _ INA (set_remove_in _ _ _ _ IAC)).
   right; right.
   destruct (eq_listline_dec (lxy ++ lyz ++ lzx) c).
   elimtype False; subst; apply (DA a0).
   apply (set_remove_in _ _ _ _ IAA).
   subst; apply (AA _ _ _ (set_remove_in _ _ _ _ IAA) INA IAC) in INC.
   subst; apply in_ain in IAA.
   clear -IAA.
   apply ain_in in IAA.
   destruct IAA as [ea [EQA IAA]].
   apply eq_ain_elem with ea.
    apply eq_area_sym.
    exact EQA.
   apply in_ain.
   exact IAA.
   apply (in_set_remove _ _ _ _ n).
   exact (AC _ _ (set_remove_in _ _ _ _ IAA) IAC).
  split.
   (* Outermostconsistency1 *)
   unfold Outermostconsistency1.
   simpl; right; right.
   destruct (eq_listline_dec (lxy ++ lyz ++ lzx) o);
   [ subst; elimtype False
   | ].
   apply (O2 a INA).
   apply in_rin.
   exact INC.
   apply in_set_remove.
   exact n.
   exact O1.
  split.
   (* Outermostconsistency2 *)
   unfold Outermostconsistency2.
   intros a0 IAA D.
   simpl in IAA.
   destruct IAA as [IAA | [IAA | IAA]]; [ subst | subst | ].
   simpl in D.
   destruct D as [D | D]; [ | exact D].
   assert (O1' := eq_rin_elem o ((s, y) :: lyz ++ (z, e) :: reverse_linelist al) C D (in_rin _ _ O1)).
   apply (eq_rin_elem _ _ _ (Rot_sym _ _ D)) in O1'.
   apply (F s (path_start_pl ph)).
   apply (eq_rin_elem _ _ _ D) in O1'.
   apply (CPconsistency s (s, y) o LP CL).
   exact O1.
   eapply RotIn.
   eapply (in_eq _ _).
   apply Rot_sym; exact D.
   exact (InPL_left _ _).
   simpl in D.
   destruct D as [D | D].
   assert (O1' := eq_rin_elem _ _ _ D (in_rin _ _ O1)).
   apply (F s (path_start_pl ph)).
   refine (CPconsistency s (y, s) _ LP CL O1 _ (InPL_right _ _)).
   eapply RotIn.
   eapply in_or_app.
   right; eapply in_eq.
   apply Rot_sym; exact D.
   apply O2 with a.
    exact INA.
    apply set_remove_in_r in D; now auto.
   apply set_remove_in in IAA.
   exact (O2 a0 IAA D).
  split.
   (* Circuitconsistency *)
   unfold Circuitconsistency.
   intros l c1 c2 IC1 IC2 LC1 LC2.
   simpl in IC1, IC2.
   destruct IC1 as [IC1 | [IC1 | IC1]];[ subst | subst| ].
   destruct IC2 as [IC2 | [IC2 | IC2]];
   [ subst; reflexivity
   | elimtype False; subst
   | elimtype False ].
   simpl in LC1, LC2.
   destruct LC1 as [LC1 | LC1]; [ subst | ].
   apply in_app_iff in LC2.
   destruct LC2 as [LC2 | LC2].
   apply (AC _ a INA) in INC.
   apply (CL (s, y)) in INC; [ | apply in_app_iff; left; exact LC2 ].
   apply (F s (path_start_pl ph)).
   apply LIn_In in INC.
   destruct INC as [INC | INC].
   exact (LP s _ INC (InPL_left _ _)).
   unfold reverse in INC; simpl in INC; exact (LP s _ INC (InPL_right _ _)).
   simpl in LC2.
   destruct LC2 as [LC2 | LC2].
   inversion LC2; subst.
   exact (F s (path_start_pl ph) IY).
   apply in_app_iff in LC2.
   destruct LC2 as [LC2 | LC2].
   exact (F y (path_ll_pl ph y (s, y) (or_intror _ (eq_refl y)) LC2) IY).
   simpl in LC2.
   destruct LC2 as [LC2 | LC2].
   inversion LC2; subst.
   inversion ph; subst.
    destruct NS as [NS | NS]; apply NS; reflexivity.
    exact (H0 (path_end_pl H)).
   apply (AC _ _ INA) in INC.
   apply (CL (s, y)) in INC.
   apply (F s (path_start_pl ph)).
   apply LIn_In in INC.
   destruct INC as [INC | INC].
   exact (LP s _ INC (InPL_left _ _)).
   unfold reverse in INC; simpl in INC; exact (LP s _ INC (InPL_right _ _)).
   apply in_app_iff; right; apply in_app_iff; right; exact LC2.
   apply in_app_iff in LC1.
   destruct LC1 as [LC1 | LC1].
   apply in_app_iff in LC2.
   destruct LC2 as [LC2 | LC2].
   apply (AC _ _ INA) in INC.
   destruct (CT _ INC) as [[tx [ll INC1]] INC2].
   clear -LC1 LC2 INC1.
   destruct lxy; simpl in LC2, INC1; [ exact LC2 | ].
   destruct LC2 as [LC2 | LC2]; [ subst | ].
   destruct l; simpl in INC1.
   inversion INC1; subst.
   apply H7.
   apply lin_or_app; right.
   apply lin_or_app; left.
   exact (in_lin _ _ LC1).
   destruct l0; simpl in INC1.
   inversion INC1; subst.
   clear -LC1 LC2 H5.
   revert p0 p pl H5.
   induction lxy; intros; simpl in *.
    exact LC2.
    destruct LC2 as [LC2 | LC2]; [ subst | ].
     inversion H5; subst.
     apply H7.
     apply lin_or_app; right.
     apply lin_or_app; left.
     exact (in_lin _ _ LC1).
     inversion H5; subst.
     exact (IHlxy LC2 _ _ _ H1).
   simpl in LC2.
   destruct LC2 as [LC2 | LC2]; [ subst | ].
   apply (AC _ _ INA) in INC.
   apply (CL (y, s)) in INC.
   apply (F s (path_start_pl ph)).
   apply LIn_In in INC.
   destruct INC as [INC | INC].
   exact (LP s _ INC (InPL_right _ _)).
   unfold reverse in INC; simpl in INC; exact (LP s _ INC (InPL_left _ _)).
   apply in_app_iff; right.
   apply in_app_iff; left; exact LC1.
   apply in_app_iff in LC2.
   destruct LC2 as [LC2 | LC2].
   apply (AC _ _ INA) in INC.
   apply (CL l) in INC.
   destruct l.
   apply (path_ll_pl ph p) in LC2; [ | exact (InPL_left _ _) ].
   apply LIn_In in INC.
   destruct INC as [INC | INC].
   exact (F p LC2 (LP p _ INC (InPL_left _ _))).
   unfold reverse in INC; simpl in INC; exact (F p LC2 (LP p _ INC (InPL_right _ _))).
   apply in_app_iff; right.
   apply in_app_iff; left; exact LC1.
   simpl in LC2.
   destruct LC2 as [LC2 | LC2]; [ subst | ].
   apply (AC _ _ INA) in INC.
   apply (CL (e, z)) in INC.
   apply LIn_In in INC; destruct INC as [INC | INC].
   exact (F e (path_end_pl ph) (LP e _ INC (InPL_left _ _))).
   exact (F e (path_end_pl ph) (LP e _ INC (InPL_right _ _))).
   apply in_app_iff; right.
   apply in_app_iff; left; exact LC1.
   apply (AC _ _ INA) in INC.
   destruct (CT _ INC) as [[tx [ll INC1]] INC2].
   clear -LC1 LC2 INC1 INC2.
   destruct lxy.
   destruct lyz; simpl in LC1; [ exact LC1 | ].
   destruct LC1 as [LC1 | LC1]; [ subst | ].
   simpl in INC1.
   destruct l; simpl in INC1.
   inversion INC1; subst.
   apply H7.
   apply lin_or_app; right.
   exact (in_lin _ _ LC2).
   simpl in INC1.
   destruct l0; simpl in INC1.
   clear INC2.
   inversion INC1; subst.
   clear - LC1 LC2 H5.
   revert p0 p pl lzx H5 LC2.
   induction lyz; intros; simpl in *.
    exact LC1.
    destruct LC1 as [LC1 | LC1]; [ subst | ].
     inversion H5; subst.
     apply H7.
     apply lin_or_app; right.
     exact (in_lin _ _ LC2).
     inversion H5; subst.
     exact (IHlyz LC1 _ _ _ _ H1 LC2).
   simpl in INC1.
   destruct l0; simpl in INC1.
   clear INC2.
   inversion INC1; subst.
   clear -LC1 LC2 H5.
   revert p0 p pl H5.
   induction lxy; intros.
   revert p0 p pl H5.
   induction lyz; intros; simpl in *.
    exact LC1.
    destruct LC1 as [LC1 | LC1]; [ subst | ].
     inversion H5; subst.
     apply H7.
     apply lin_or_app; right.
     exact (in_lin _ _ LC2).
     inversion H5; subst.
     exact (IHlyz LC1 _ _ _ H1).
    inversion H5; subst.
    exact (IHlxy _ _ _ H1).
   simpl in LC1.
   destruct LC1 as [LC1 | LC1]; [ subst | ].
   apply in_app_iff in LC2.
   destruct LC2 as [LC2 | LC2].
   apply (AC _ _ INA) in INC.
   apply (CL (z, e)) in INC; [ | apply in_app_iff; left; exact LC2 ].
   apply LIn_In in INC; destruct INC as [INC | INC].
   exact (F e (path_end_pl ph) (LP e _ INC (InPL_right _ _))).
   exact (F e (path_end_pl ph) (LP e _ INC (InPL_left _ _))).
   simpl in LC2.
   destruct LC2 as [LC2 | LC2].
   inversion LC2; subst.
   inversion ph; subst.
    destruct NS as [NS | NS]; apply NS; reflexivity.
    exact (H0 (path_end_pl H)).
   apply in_app_iff in LC2.
   destruct LC2 as [LC2 | LC2].
   exact (F z (path_ll_pl ph z (z, e) (InPL_left _ _) LC2) IZ).
   simpl in LC2.
   destruct LC2 as [LC2 | LC2].
   inversion LC2; subst.
   exact (F z (path_end_pl ph) IZ).
   apply (AC _ _ INA) in INC.
   apply (CL (z, e)) in INC; [ | apply in_app_iff; right; apply in_app_iff; right; exact LC2 ].
   apply LIn_In in INC; destruct INC as [INC | INC].
   exact (F e (path_end_pl ph) (LP e _ INC (InPL_right _ _))).
   exact (F e (path_end_pl ph) (LP e _ INC (InPL_left _ _))).
   unfold reverse_linelist in LC1.
   rewrite <- in_rev, map_reverse in LC1.
   destruct l; unfold reverse in LC1; simpl in LC1.
   apply in_app_or in LC2.
   destruct LC2 as [LC2 | LC2].
   apply (AC _ _ INA) in INC.
   apply (CL (p, p0)) in INC; [ | apply in_app_iff; left; exact LC2 ].
   apply (path_ll_pl ph p _ (InPL_right _ _)) in LC1.
   apply LIn_In in INC; destruct INC as [INC | INC].
   exact (F p LC1 (LP p _ INC (InPL_left _ _))).
   exact (F p LC1 (LP p _ INC (InPL_right _ _))).
   simpl in LC2.
   destruct LC2 as [LC2 | LC2].
   inversion LC2; subst.
   exact (F p (path_ll_pl ph p _ (InPL_right _ _) LC1) IY).
   apply in_app_iff in LC2.
   destruct LC2 as [LC2 | LC2].
   clear -ph LC1 LC2.
   induction ph.
    exact LC1.
    simpl in LC1.
    destruct LC1 as [LC1 | LC1].
    inversion LC1; subst.
    simpl in LC2.
    destruct LC2 as [LC2 | LC2].
    inversion LC2; subst.
    exact (H (path_start_pl ph)).
    exact (H (path_ll_pl ph p0 (p, p0) (InPL_right _ _) LC2)).
    simpl in LC2.
    destruct LC2 as [LC2 | LC2].
    inversion LC2; subst.
    exact (H (path_ll_pl ph p _ (InPL_right _ _) LC1)).
    exact (IHph LC1 LC2).
   simpl in LC2.
   destruct LC2 as [LC2 | LC2].
   inversion LC2; subst.
   exact (F p0 (path_ll_pl ph p0 (p0, p) (InPL_left _ _) LC1) IZ).
   apply (AC _ _ INA) in INC.
   apply (CL (p, p0)) in INC; [ | apply in_app_iff; right; apply in_app_iff; right; exact LC2 ].
   apply LIn_In in INC; destruct INC as [INC | INC].
   exact (F p (path_ll_pl ph p (p0, p) (InPL_right _ _) LC1) (LP p _ INC (InPL_left _ _))).
   exact (F p (path_ll_pl ph p (p0, p) (InPL_right _ _) LC1) (LP p _ INC (InPL_right _ _))).
   simpl in LC1.
   destruct LC1 as [LC1 | LC1]; [ subst | ].
   apply set_remove_in in IC2.
   destruct (LIn_In _ _ (CL (s, y) c2 IC2 LC2)) as [H | H].
   exact (F s (path_start_pl ph)
              (LP s  _ H (InPL_left _ _))).
   exact (F s (path_start_pl ph)
              (LP s  _ H (InPL_right _ _))).
   apply in_app_iff in LC1.
   destruct LC1 as [LC1 | LC1].
   destruct (eq_circuit_dec (lxy ++ lyz ++ lzx) c2) as [ROT | NROT].
    apply (DC _ (AC _ _ INA INC)).
   apply eq_rin_elem with c2.
    apply Rot_sym; exact ROT.
   apply in_rin; exact IC2.
    apply NROT, Rot_sym.
    assert (c2 = lxy ++ lyz ++ lzx).
    refine (CC l _ _ (set_remove_in _ _ _ _ IC2) (AC _ _ INA INC) LC2 _ ).
    apply in_or_app; right; apply in_or_app; left; exact LC1.
    rewrite H; exact (rotSame _).
   simpl in LC1.
   destruct LC1 as [LC1 | LC1]; [ subst | ].
   apply set_remove_in in IC2.
   destruct (LIn_In _ _ (CL (z, e) c2 IC2 LC2)) as [H | H].
   exact (F e (path_end_pl ph)
              (LP e  _ H (InPL_right _ _))).
   exact (F e (path_end_pl ph)
              (LP e  _ H (InPL_left _ _))).
   unfold reverse_linelist in LC1.
   rewrite <- in_rev, map_reverse in LC1.
   apply set_remove_in in IC2.
   unfold reverse in LC1; destruct l; simpl in LC1.
   destruct (LIn_In _ _ (CL (p, p0) c2 IC2 LC2)) as [H | H].
   exact (F p (path_ll_pl ph p (p0, p) (InPL_right _ _) LC1)
              (LP p  _ H (InPL_left _ _))).
   exact (F p (path_ll_pl ph p (p0, p) (InPL_right _ _) LC1)
              (LP p  _ H (InPL_right _ _))).
   destruct IC2 as [IC2 | [IC2 | IC2]];
   [ elimtype False; subst
   | subst; reflexivity
   | elimtype False ].
   apply in_app_iff in LC1.
   destruct LC1 as [LC1 | LC1].
   simpl in LC2.
   destruct LC2 as [LC2 | LC2]; [ subst | ].
   apply (AC _ _ INA) in INC.
   apply (CL (s, y)) in INC; [ | apply in_app_iff; left; exact LC1 ].
   apply LIn_In in INC; destruct INC as [INC | INC].
   exact (F s (path_start_pl ph)
              (LP _  _ INC (InPL_left _ _))).
   exact (F s (path_start_pl ph)
              (LP _  _ INC (InPL_right _ _))).
   apply in_app_iff in LC2.
   destruct LC2 as [LC2 | LC2].
   destruct (CT _ (AC _ _ INA INC)) as [[tx [c CT1]] CT2].
   clear -CT1 LC1 LC2.
   destruct lxy.
   exact LC1.
   simpl in LC1.
   destruct LC1 as [LC1 | LC1]; [ subst | ].
   destruct l.
   simpl in CT1.
   inversion CT1; subst.
   apply H7.
   apply lin_or_app; right.
   apply lin_or_app; left.
   exact (in_lin _ _ LC2).
   destruct l0.
   simpl in CT1.
   inversion CT1; subst.
   clear -LC1 LC2 H5.
   revert p0 p pl lyz lzx LC1 LC2 H5.
   induction lxy; intros.
    exact LC1.
    simpl in LC1, H5.
    destruct LC1 as [LC1 | LC1]; [ subst | ].
    inversion H5; subst.
    apply H7.
    apply lin_or_app; right.
    apply lin_or_app; left.
    exact (in_lin _ _ LC2).
    inversion H5; subst.
    exact (IHlxy _ _ _ _ _ LC1 LC2 H1).
   simpl in LC2.
   destruct LC2 as [LC2 | LC2]; [ subst | ].
   apply (AC _ _ INA) in INC.
   apply (CL (z, e)) in INC; [ | apply in_app_iff; left; exact LC1 ].
   apply LIn_In in INC; destruct INC as [INC | INC].
   exact (F e (path_end_pl ph)
              (LP _  _ INC (InPL_right _ _))).
   exact (F e (path_end_pl ph)
              (LP _  _ INC (InPL_left _ _))).
   unfold reverse_linelist in LC2.
   rewrite <- in_rev, map_reverse in LC2.
   unfold reverse in LC2.
   destruct l; simpl in *.
   apply (AC _ _ INA) in INC.
   apply (CL (p, p0)) in INC; [ | apply in_app_iff; left; exact LC1 ].
   apply LIn_In in INC; destruct INC as [INC | INC].
   exact (F p (path_ll_pl ph p _ (InPL_right _ _) LC2)
              (LP _  _ INC (InPL_left _ _))).
   exact (F p (path_ll_pl ph p _ (InPL_right _ _) LC2)
              (LP _ _ INC (InPL_right _ _))).
   simpl in LC1.
   destruct LC1 as [LC1 | LC1]; [ subst | ].
   simpl in LC2.
   destruct LC2 as [LC2 | LC2].
   inversion LC2; subst.
   exact (F y (path_start_pl ph) IY).
   apply in_app_iff in LC2.
   destruct LC2 as [LC2 | LC2].
   apply (AC _ _ INA) in INC.
   apply (CL (y, s)) in INC; [ | apply in_app_iff; right; apply in_app_iff; left; exact LC2 ].
   apply LIn_In in INC; destruct INC as [INC | INC].
   exact (F s (path_start_pl ph)
              (LP _ _ INC (InPL_right _ _))).
   exact (F s (path_start_pl ph)
              (LP _ _ INC (InPL_left _ _))).
   simpl in LC2.
   destruct LC2 as [LC2 | LC2].
   inversion LC2; subst.
   inversion ph; subst.
    destruct NS as [NS | NS]; apply NS; reflexivity.
    exact (not_simplepath ph).
   unfold reverse_linelist in LC2.
   rewrite <- in_rev, map_reverse in LC2.
   unfold reverse in LC2; simpl in LC2.
   exact (F y (path_ll_pl ph y _ (InPL_right _ _) LC2) IY).
   apply in_app_iff in LC1.
   destruct LC1 as [LC1 | LC1].
   simpl in LC2.
   destruct LC2 as [LC2 | LC2]; [ subst | ].
   exact (F y (path_ll_pl ph y _ (InPL_right _ _) LC1) IY).
   apply in_app_iff in LC2.
   destruct LC2 as [LC2 | LC2].
   destruct l.
   apply (AC _ _ INA) in INC.
   apply (CL (p, p0)) in INC; [ | apply in_app_iff; right; apply in_app_iff; left; exact LC2 ].
   apply LIn_In in INC; destruct INC as [INC | INC].
   exact (F p (path_ll_pl ph p (p, p0) (InPL_left _ _) LC1)
              (LP _ _ INC (InPL_left _ _))).
   exact (F p (path_ll_pl ph p (p, p0) (InPL_left _ _) LC1)
              (LP _ _ INC (InPL_right _ _))).
   simpl in LC2.
   destruct LC2 as [LC2 | LC2]; [ subst | ].
   exact (F z (path_ll_pl ph z _ (InPL_left _ _) LC1) IZ).
   unfold reverse_linelist in LC2.
   rewrite <- in_rev, map_reverse in LC2.
   unfold reverse in LC2.
   destruct l; simpl in LC2.
   clear -ph LC1 LC2.
   revert p p0 LC1 LC2.
   induction ph; intros.
    exact LC1.
    simpl in LC1, LC2.
    destruct LC1 as [LC1 | LC1].
    inversion LC1; subst.
    destruct LC2 as [LC2 | LC2].
    inversion LC2; subst.
    exact (H (path_start_pl ph)).
    exact (H (path_ll_pl ph p _ (InPL_right _ _) LC2)).
    destruct LC2 as [LC2 | LC2].
    inversion LC2; subst.
    exact (H (path_ll_pl ph p0 _ (InPL_right _ _) LC1)).
    exact (IHph _ _ LC1 LC2).
   simpl in LC1.
   destruct LC1 as [LC1 | LC1]; [ subst | ].
   simpl in LC2.
   destruct LC2 as [LC2 | LC2].
   inversion LC2; subst.
   inversion ph; subst.
    destruct NS as [NS | NS]; apply NS; reflexivity.
    exact (not_simplepath ph).
   apply in_app_iff in LC2.
   destruct LC2 as [LC2 | LC2].
   apply (AC _ _ INA) in INC.
   apply (CL (e, z)) in INC; [ | apply in_app_iff; right; apply in_app_iff; left; exact LC2 ].
   apply LIn_In in INC; destruct INC as [INC | INC].
   exact (F e (path_end_pl ph)
              (LP _ _ INC (InPL_left _ _))).
   exact (F e (path_end_pl ph)
              (LP _ _ INC (InPL_right _ _))).
   simpl in LC2.
   destruct LC2 as [LC2 | LC2].
   inversion LC2; subst.
   exact (F e (path_end_pl ph) IZ).
   unfold reverse_linelist in LC2.
   rewrite <- in_rev, map_reverse in LC2.
   unfold reverse in LC2; simpl in LC2.
   exact (F z (path_ll_pl ph z _ (InPL_left _ _) LC2) IZ).
   simpl in LC2.
   destruct LC2 as [LC2 | LC2]; [ subst | ].
   apply (AC _ _ INA) in INC.
   apply (CL (s, y)) in INC; [ | apply in_app_iff; right; apply in_app_iff; right; exact LC1 ]. 
   apply LIn_In in INC; destruct INC as [INC | INC].
   exact (F s (path_start_pl ph)
              (LP _ _ INC (InPL_left _ _))).
   exact (F s (path_start_pl ph)
              (LP _ _ INC (InPL_right _ _))).
   apply in_app_iff in LC2.
   destruct LC2 as [LC2 | LC2].
   destruct (CT _ (AC _ _ INA INC)) as [[tx [c CT1]] CT2].
   clear -CT1 LC1 LC2.
   destruct lxy.
   destruct lyz; [ exact LC2 | ].
   destruct l0.
   simpl in LC2, CT1.
   destruct LC2 as [LC2 | LC2]; [ subst | ].
   inversion CT1; subst.
   apply H7.
   apply lin_or_app; right.
   exact (in_lin _ _ LC1).
   inversion CT1; subst.
   clear -LC1 LC2 H5.
   revert p0 p pl LC1 LC2 H5.
   induction lyz; intros; simpl in H5.
    exact LC2.
    simpl in LC2.
    destruct LC2 as [LC2 | LC2]; [ subst | ].
    inversion H5; subst.
    apply H7.
    apply lin_or_app; right.
    exact (in_lin _ _ LC1).
    inversion H5; subst.
    exact (IHlyz _ _ _ LC1 LC2 H1).
   destruct l0.
   simpl in CT1.
   inversion CT1; subst.
   clear -LC1 LC2 H5.
   revert p0 p pl H5.
   induction lxy; intros; simpl in H5.
    revert p0 p pl LC1 LC2 H5.
    induction lyz; intros; simpl in H5.
     exact LC2.
     simpl in LC2.
     destruct LC2 as [LC2 | LC2]; [ subst | ].
     inversion H5; subst.
     apply H7.
     apply lin_or_app; right.
     exact (in_lin _ _ LC1).
     inversion H5; subst.
     exact (IHlyz _ _ _ LC1 LC2 H1).
   inversion H5; subst.
   exact (IHlxy _ _ _ H1).
   simpl in LC2.
   destruct LC2 as [LC2 | LC2]; [ subst | ].
   apply (AC _ _ INA) in INC.
   apply (CL (z, e)) in INC; [ | apply in_app_iff; right; apply in_app_iff; right; exact LC1 ].
   apply LIn_In in INC; destruct INC as [INC | INC].
   exact (F e (path_end_pl ph)
              (LP _  _ INC (InPL_right _ _))).
   exact (F e (path_end_pl ph)
              (LP _  _ INC (InPL_left _ _))).
   unfold reverse_linelist in LC2.
   rewrite <- in_rev, map_reverse in LC2.
   unfold reverse in LC2.
   destruct l; simpl in LC2.
   apply (AC _ _ INA) in INC.
   apply (CL (p, p0)) in INC; [ | apply in_app_iff; right; apply in_app_iff; right; exact LC1 ].
   apply LIn_In in INC; destruct INC as [INC | INC].
   exact (F p (path_ll_pl ph p _ (InPL_right _ _) LC2)
              (LP _  _ INC (InPL_left _ _))).
   exact (F p (path_ll_pl ph p _ (InPL_right _ _) LC2)
              (LP _  _ INC (InPL_right _ _))).
   apply in_app_iff in LC1.
   destruct LC1 as [LC1 | LC1].
   destruct (eq_listline_dec (lxy ++ lyz ++ lzx) c2) as [ROT | NROT].
   apply (DC (lxy ++ lyz ++ lzx)); subst.
   exact (AC _ _ INA INC).
   apply in_rin.
   exact IC2.
   apply set_remove_in in IC2.
   apply (AC _ _ INA) in INC.
   refine (NROT (CC l (lxy ++ lyz ++ lzx) c2 INC IC2 _ LC2)).
   apply in_app_iff; left; exact LC1.
   simpl in LC1.
   destruct LC1 as [LC1 | LC1]; [ subst | ].
   apply set_remove_in in IC2.
   apply (CL (y, s)) in IC2; [ | exact LC2].
   apply LIn_In in IC2; destruct IC2 as [IC2 | IC2].
   exact (F s (path_start_pl ph)
              (LP _  _  IC2 (InPL_right _ _))).
   exact (F s (path_start_pl ph)
              (LP _  _  IC2 (InPL_left _ _))).
   apply in_app_iff in LC1.
   destruct LC1 as [LC1 | LC1].
   destruct l.
   apply set_remove_in in IC2.
   apply (CL (p, p0) _ IC2) in LC2.
   apply LIn_In in LC2; destruct LC2 as [LC2 | LC2].
   exact (F p (path_ll_pl ph p _ (InPL_left _ _) LC1)
              (LP _  _ LC2 (InPL_left _ _))).
   exact (F p (path_ll_pl ph p _ (InPL_left _ _) LC1)
              (LP _  _ LC2 (InPL_right _ _))).
   simpl in LC1.
   destruct LC1 as [LC1 | LC1]; [ subst | ].
   apply set_remove_in in IC2.
   apply (CL (e, z) _ IC2) in LC2.
   apply LIn_In in LC2; destruct LC2 as [LC2 | LC2].
   exact (F e (path_end_pl ph)
              (LP _  _  LC2 (InPL_left _ _))).
   exact (F e (path_end_pl ph)
              (LP _  _ LC2 (InPL_right _ _))).
   destruct (eq_listline_dec c2 (lxy ++ lyz ++ lzx)) as [ROT | NROT].
   apply (DC (lxy ++ lyz ++ lzx)); subst.
   exact (AC _ _ INA INC).
   apply in_rin.
   exact IC2.
   apply set_remove_in in IC2.
   apply (AC _ _ INA) in INC.
   refine (NROT (CC l c2 (lxy ++ lyz ++ lzx) IC2 INC LC2 _)).
   apply in_app_iff; right; apply in_app_iff; right; exact LC1.
   destruct IC2 as [IC2 | [IC2 | IC2]]; [ subst; elimtype False | subst; elimtype False | ].
   simpl in LC2.
   destruct LC2 as [LC2 | LC2]; [ subst | ].
   apply set_remove_in in IC1.
   destruct (LIn_In _ _ (CL (s, y) _ IC1 LC1)) as [H | H].
   exact (F s (path_start_pl ph)
              (LP _  _ H (InPL_left _ _))).
   exact (F s (path_start_pl ph)
              (LP _  _ H (InPL_right _ _))).
   apply in_app_iff in LC2.
   destruct LC2 as [LC2 | LC2].
   destruct (eq_listline_dec c1 (lxy ++ lyz ++ lzx)) as [ROT | NROT].
   apply (DC (lxy ++ lyz ++ lzx)); subst.
   exact (set_remove_in _ _ _ _ IC1).
   apply in_rin.
   exact IC1.
   apply set_remove_in in IC1.
   apply (AC _ _ INA) in INC.
   refine (NROT (CC l c1 (lxy ++ lyz ++ lzx) IC1 INC LC1 _)).
   apply in_app_iff; right.
   apply in_app_iff; left; exact LC2.
   apply set_remove_in in IC1.
   simpl in LC2.
   destruct LC2 as [LC2 | LC2]; [ subst | ].
   destruct (LIn_In _ _ (CL (z, e) _ IC1 LC1)) as [H | H].
   exact (F e (path_end_pl ph)
              (LP _  _ H (InPL_right _ _))).
   exact (F e (path_end_pl ph)
              (LP _  _ H (InPL_left _ _))).
   unfold reverse_linelist in LC2.
   rewrite <- in_rev, map_reverse in LC2.
   destruct l; unfold reverse in LC2; simpl in LC2.
   destruct (LIn_In _ _ (CL (p, p0) _ IC1 LC1)) as [H | H].
   exact (F p (path_ll_pl ph p _ (InPL_right _ _) LC2)
              (LP _  _ H (InPL_left _ _))).
   exact (F p (path_ll_pl ph p _ (InPL_right _ _) LC2)
              (LP _  _ H (InPL_right _ _))).
   apply in_app_iff in LC2.
   destruct LC2 as [LC2 | LC2].
   destruct (eq_listline_dec c1 (lxy ++ lyz ++ lzx)) as [ROT | NROT].
   apply (DC (lxy ++ lyz ++ lzx)); subst.
   exact (set_remove_in _ _ _ _ IC1).
   apply in_rin.
   exact IC1.
   apply set_remove_in in IC1.
   apply (AC _ _ INA) in INC.
   refine (NROT (CC l c1 (lxy ++ lyz ++ lzx) IC1 INC LC1 _)).
   apply in_app_iff; left; exact LC2.
   simpl in LC2.
   destruct LC2 as [LC2 | LC2]; [ subst | ].
   apply set_remove_in in IC1.
   destruct (LIn_In _ _ (CL (y, s) _ IC1 LC1)) as [H | H].
   exact (F s (path_start_pl ph)
              (LP _  _ H (InPL_right _ _))).
   exact (F s (path_start_pl ph)
              (LP _  _ H (InPL_left _ _))).
   apply in_app_iff in LC2.
   destruct LC2 as [LC2 | LC2].
   apply set_remove_in in IC1.
   destruct l.
   destruct (LIn_In _ _ (CL (p, p0) _ IC1 LC1)) as [H | H].
   exact (F p (path_ll_pl ph p _ (InPL_left _ _) LC2)
              (LP _  _ H (InPL_left _ _))).
   exact (F p (path_ll_pl ph p _ (InPL_left _ _) LC2)
              (LP _  _ H (InPL_right _ _))).
   simpl in LC2.
   destruct LC2 as [LC2 | LC2]; [ subst | ].
   apply set_remove_in in IC1.
   destruct (LIn_In _ _ (CL _ _ IC1 LC1)) as [H | H].
   exact (F e (path_end_pl ph)
              (LP _  _ H (InPL_left _ _))).
   exact (F e (path_end_pl ph)
              (LP _  _ H (InPL_right _ _))).
   destruct (eq_listline_dec c1 (lxy ++ lyz ++ lzx)) as [ROT | NROT].
   apply (DC (lxy ++ lyz ++ lzx)); subst.
   exact (set_remove_in _ _ _ _ IC1).
   apply in_rin.
   exact IC1.
   apply set_remove_in in IC1.
   apply (AC _ _ INA) in INC.
   refine (NROT (CC l c1 (lxy ++ lyz ++ lzx) IC1 INC LC1 _)).
   apply in_app_iff; right; apply in_app_iff; right; exact LC2.
   unfold Circuitconsistency in CC.
   exact (CC l c1 c2 (set_remove_in _ _ _ _ IC1) (set_remove_in _ _ _ _ IC2) LC1 LC2).
  split.
   (* Areaconsistency *)
   unfold Areaconsistency.
   intros c a1 a2 IA1 IA2 CA1 CA2.
   simpl in IA1, IA2.
   destruct IA1 as [IA1 | [IA1 | IA1]]; [ subst | subst | ].
   simpl in CA1.
   destruct CA1 as [CA1 | CA1]; [subst | elim CA1].
   destruct IA2 as [IA2 | [IA2 | IA2]];
   [ subst; exact (eq_refl _)
   | elimtype False; subst
   | elimtype False ].
   simpl in CA2.
   destruct CA2 as [CA2 | CA2]; [ subst | ].
   apply (F s (path_start_pl ph)).
   apply (AC _ _ INA) in INC.
   destruct lxy; simpl in CA2; inversion CA2; subst.
   exact IY.
   apply (CL (s, y)) in INC.
   apply LIn_In in INC; destruct INC as [INC | INC].
   exact (LP _  _ INC (InPL_left _ _)).
   exact (LP _  _ INC (InPL_right _ _)).
   simpl; left; reflexivity.
   apply set_remove_in in CA2.
   apply (AC _ _ INA) in CA2.
   apply (F s (path_start_pl ph)).
   exact (CPconsistency s (s, y) _ LP CL CA2 (in_eq _ _) (InPL_left _ _)).
   apply set_remove_in in IA2.
   apply (AC _ _ IA2) in CA2.
   apply (F s (path_start_pl ph)).
   exact (CPconsistency s (s, y) _ LP CL CA2 (in_eq _ _) (InPL_left _ _)).
   destruct IA2 as [IA2 | [IA2 | IA2]]; [ subst | exact IA2 | ].
   simpl in CA1, CA2.
   destruct CA2 as [CA2 | CA2]; elimtype False; [subst | exact CA2].
   destruct CA1 as [CA1 | CA1]; [ subst | ].
   apply (F s (path_start_pl ph)).
   apply (AC _ _ INA) in INC.
   destruct lxy; simpl in CA1; inversion CA1; subst.
   exact IY.
   apply (CL (s, y)) in INC.
   apply LIn_In in INC; destruct INC as [INC | INC].
   exact (LP _  _ INC (InPL_left _ _)).
   exact (LP _  _ INC (InPL_right _ _)).
   simpl; left; reflexivity.
   apply (F s (path_start_pl ph)).
   apply set_remove_in in CA1.
   apply (AC _ _ INA) in CA1.
   exact (CPconsistency s (s, y) _ LP CL CA1 (in_eq _ _) (InPL_left _ _)).
   simpl in CA1; destruct CA1 as [CA1 | CA1].
   elimtype False; subst.
   apply set_remove_in in IA2.
   apply (AC _ _ IA2) in CA2.
   apply (F s (path_start_pl ph)).
   refine (CPconsistency s (y, s) _ LP CL CA2 _ (InPL_right _ _)).
   apply in_or_app; right; now apply in_eq.
   elimtype False.
   assert (IA2' := set_remove_in _ _ _ _ IA2).
   apply set_remove_in in CA1.
   apply (DA a2 IA2').
   assert (a2 = a).
   exact (AA _ _ _ IA2' INA CA2 CA1).
   subst.
   apply in_ain.
   exact IA2.
   assert (IA1' := set_remove_in _ _ _ _ IA1).
   destruct IA2 as [IA2 | [IA2 | IA2]]; subst.
   elimtype False; simpl in CA2; destruct CA2 as [CA2 | CA2]; [subst; simpl in CA1 | exact CA2].
   apply (F s (path_start_pl ph)).
   apply (AC _ _ IA1') in CA1.
   exact (CPconsistency _ _ _ LP CL CA1 (in_eq _ _) (InPL_left _ _)).
   elimtype False; simpl in CA2; destruct CA2 as [CA2 | CA2].
   apply (F s (path_start_pl ph)).
   apply (AC _ _ IA1') in CA1.
   refine (CPconsistency _ _ _ LP CL CA1 _ (InPL_right y _)).
   subst; apply in_or_app; right; now apply in_eq.
   apply set_remove_in in CA2.
   apply (DA a1 IA1').
   assert (a1 = a).
   exact (AA _ _ _ IA1' INA CA1 CA2).
   subst.
   apply in_ain.
   exact IA1.
   apply set_remove_in in IA2.
   exact (AA _ _ _ IA1' IA2 CA1 CA2).
  split.
   (* DistinctP *)
   unfold DistinctP.
   intros p Ip D.
   clear -ph D DP F.
   revert s e al ph.
   induction ap; intros; simpl in D.
    exact (DP p (set_remove_in _ _ _ _ D) D).
    destruct (eq_point_dec p a); [subst | ].
     apply in_app_iff in D.
     destruct D as [D | D].
     inversion ph; subst.
     exact D.
     exact (H5 D).
     exact (F a (in_eq _ _) D).
     simpl in D.
     destruct D as [D | D]; [ subst; exact (n (eq_refl _)) | ].
      inversion ph; subst.
      exact (DP p (set_remove_in _ _ _ _ D) D).
      apply IHap with(s:=p2)(e:=e)(al:=ll); [ | exact D | exact H3 ].
      intros p0 INP DIP.
      exact (F p0 (or_intror _ INP) DIP).
  split.
   (* DistinctL *)
   unfold DistinctL.
   intros l Il D.
   simpl in D.
   destruct (eq_dline_dec l (y, s)) as [EQ1 | NEQ1].
   subst; simpl in D.
   destruct D as [D | D].
   inversion D; subst.
    exact (F _ (path_end_pl ph) IY).
    cbv in H; inversion H; subst.
     inversion ph; subst.
      destruct NS as [NS | NS]; apply NS; reflexivity.
      exact (not_simplepath ph).
   apply lin_app_or in D.
    destruct D as [D | D].
     destruct (LIn_In _ _ D) as [LIN1 | LIN2].
      exact (F y (path_ll_pl ph y _ (InPL_left _ _) LIN1) IY).
      exact (F y (path_ll_pl ph y _ (InPL_right _ _) LIN2) IY).
     apply LIn_In in D.
      destruct D as [D | D].
       exact (F s (path_start_pl ph)
                  (LP _ _ D (InPL_right _ _))).
       exact (F s (path_start_pl ph)
                  (LP _ _ D (InPL_left _ _))).
   destruct Il as [Il | Il]; [symmetry in Il; exact (NEQ1 Il) | ].
   destruct D as [D | D].
   inversion D; subst.
   apply NEQ1; reflexivity.
   destruct l; inversion H; subst; clear H D.
   destruct Il as [Il | Il].
   inversion Il; subst; clear Il.
   inversion ph; subst.
    destruct NS as [NS | NS]; apply NS; reflexivity.
    exact (not_simplepath ph).
   apply in_app_or in Il; destruct Il as [Il | Il].
   exact (F _ (path_ll_pl ph y _ (InPL_right _ _) Il) IY).
   exact (F _ (path_start_pl ph) (LP _ _ Il (InPL_left _ _))).
   destruct (eq_dline_dec l (e, z)) as [EQ2 | NEQ2].
   subst.
   apply lin_app_or in D.
   destruct D as [D | D].
    destruct (LIn_In _ _ D) as [LIN1 | LIN2].
     exact (F z (path_ll_pl ph z _ (InPL_right _ _) LIN1) IZ).
     exact (F z (path_ll_pl ph z _ (InPL_left _ _) LIN2) IZ).
    apply LIn_In in D; destruct D as [D | D].
     exact (F e (path_end_pl ph)
                (LP _ _ D (InPL_left _ _))).
     exact (F e (path_end_pl ph)
                (LP _ _ D (InPL_right _ _))).
   destruct Il as [Il | Il]; [symmetry in Il; exact (NEQ2 Il) | ].
   simpl in D; destruct D as [D | D].
   inversion D; subst.
   apply NEQ2; reflexivity.
   destruct l; inversion H; clear H; subst.
   apply in_app_or in Il.
   destruct Il as [Il | Il].
    exact (F z (path_ll_pl ph z _ (InPL_left _ _) Il) IZ).
    exact (F e (path_end_pl ph)
               (LP _ _ Il (InPL_right _ _))).
   cut (~LIn l (set_remove eq_dline_dec l al)); [ intros AL | ].
   cut (forall(l : Line), In l al -> ~LIn l L); [ intros FL | ].
   clear -D Il AL FL DL.
   induction al; simpl in *.
    exact (DL l Il D).
    destruct (eq_dline_dec l a) as [EQ | NEQ].
    apply lin_app_or in D.
    destruct D as [LIN | LIN].
    exact (AL LIN).
    inversion EQ; subst.
    exact (FL _ (or_introl _ (eq_refl _)) LIN).
    simpl in D, AL.
    destruct Il as [Il | Il]; [ symmetry in Il; exact (NEQ Il) | ].
    destruct D as [D | D]; [ subst; exact (AL (or_introl D)) | ].
    apply (IHal Il D).
    exact (fun D => AL (or_intror _ D)).
    exact (fun l0 LAL D => FL l0 (or_intror _ LAL) D).
   intros l0 LAL INL.
   destruct l0.
   destruct (LIn_In _ _ INL) as [LIN | LIN].
   exact (F p (path_ll_pl ph p (p, p0) (InPL_left _ _) LAL) (LP p _ LIN (InPL_left _ _))).
   exact (F p (path_ll_pl ph p (p, p0) (InPL_left _ _) LAL) (LP p _ LIN (InPL_right _ _))).
   clear -F LP Il ph.
   intro D.
   induction ph; simpl in D; [ exact D | ].
   destruct (eq_dline_dec l (p3, p2)) as [EQ | NEQ].
   subst.
   destruct (LIn_In _ _ D) as [D1 | D2].
    exact (H (path_ll_pl ph p3 _ (InPL_left _ _) D1)).
    exact (H (path_ll_pl ph p3 _ (InPL_right _ _) D2)).
   simpl in D.
   destruct Il as [Il | Il]; [ symmetry in Il; exact (NEQ Il) | ].
   assert (forall p : Point, In p pl -> ~ In p P) as F'.
   intros p0 H0; apply F.
   right; exact H0.
   destruct D as [D | D]; [ clear IHph | exact (IHph F' Il D) ].
   inversion D; clear D; subst.
    apply NEQ; reflexivity.
    destruct l; inversion H0; clear H0; subst.
    apply in_app_or in Il; destruct Il as [Il | Il].
    exact (H (path_ll_pl ph p3 _ (InPL_right _ _) Il)).
    apply (F p3).
     now apply in_eq.
     exact (LP _ _ Il (InPL_right _ _)).
  split.
   (* DistinctC *)
   unfold DistinctC.
   remember (((s, y) :: lyz) ++ (z, e) :: reverse_linelist al) as c1.
   remember (lxy ++ ((y, s) :: al) ++ (e, z) :: lzx) as c2.
   intros c Ic D.
   simpl in D.
   destruct (eq_listline_dec c c1) as [EQ1 | NEQ1].
   destruct D as [D | D].
   subst.
   assert(In (s, y) (lxy ++ ((y, s) :: al) ++ (e, z) :: lzx)).
    exact (RotIn _ _ _ (in_eq _ _) D).
   apply in_app_iff in H.
   apply (AC _ _ INA) in INC.
   destruct H as [D1 | D1].
   apply (CL (s, y)) in INC.
   apply LIn_In in INC; destruct INC as [INC | INC].
   exact (F s (path_start_pl ph)
              (LP _ _ INC (InPL_left _ _))).
   exact (F s (path_start_pl ph)
              (LP _ _ INC (InPL_right _ _))).
   apply in_app_iff; left.
   exact D1.
   simpl in D1.
   destruct D1 as [D1 | D1].
   inversion D1; subst.
   exact (F s (path_start_pl ph) IY).
   apply in_app_iff in D1.
   destruct D1 as [D1 | D1].
   exact (F y (path_ll_pl ph y _ (InPL_right _ _) D1) IY).
   simpl in D1.
   destruct D1; subst.
   inversion H; subst.
    inversion ph; subst.
     destruct NS as [NS | NS]; apply NS; reflexivity.
     exact (not_simplepath ph).
   apply (CL (s, y)) in INC.
   apply LIn_In in INC; destruct INC as [INC | INC].
   exact (F s (path_start_pl ph)
              (LP _ _ INC (InPL_left _ _))).
   exact (F s (path_start_pl ph)
              (LP _ _ INC (InPL_right _ _))).
   apply in_app_iff; right.
   apply in_app_iff; right.
   exact H.
   apply set_remove_in_r in D.
   subst.
   apply (F s (path_start_pl ph)).
   apply EqIn_In in D.
   destruct D as [b [D1  D2]].
   refine (CPconsistency s (s, y) b LP CL D2 _ (InPL_left _ _)).
   exact (RotIn _ _ _ (in_eq _ _) D1).
   destruct Ic as [Ic | Ic]; [ symmetry in Ic; exact (NEQ1 Ic) | ].
   destruct D as [D | D].
   destruct Ic as [Ic | Ic].
   subst. subst.
   assert (In (s, y) (lxy ++ lyz ++ lzx) \/ s = y \/ (y = z /\ s = e) \/ In (s, y) al).
   apply Rot_sym in D.
   apply (RotIn _ _ _ (in_eq _ _)) in D.
   apply in_app_or in D; destruct D as [D | [D | D]].
   left; apply in_or_app; left; exact D.
   inversion D; subst; right; left; reflexivity.
   apply in_app_or in D; destruct D as [D | [D | D]].
   right; right; right; exact D.
   inversion D; right; right; left; subst; split; reflexivity.
   left; apply in_or_app; right; apply in_or_app; right; exact D.
   assert (Is := path_start_pl ph).
   destruct H as [H | [H | [[Hy Hs] | H]]].
   apply (AC _ _ INA) in INC.
   exact (F _ Is (CPconsistency _ _ _ LP CL INC H (InPL_left _ _))).
   subst; exact (F _ Is IY).
   subst; inversion ph; subst.
    destruct NS as [NS | NS]; apply NS; reflexivity.
    exact (not_simplepath ph).
   exact (F _ (path_ll_pl ph _ _ (InPL_right _ _) H) IY).
   apply set_remove_in in Ic.
   subst.
   assert (Is := path_start_pl ph).
   apply Rot_sym in D; apply (RotIn _ _ _ (in_eq _ _)) in D.
   exact (F _ Is (CPconsistency _ _ _ LP CL Ic D (InPL_left _ _))).
   destruct (eq_listline_dec c c2) as [EQ2 | NEQ2].
   apply set_remove_in_r in D.
   subst.
   apply EqIn_In in D.
   destruct D as [c' [D1 D2]].
   apply (F _ (path_start_pl ph)).
   apply (CPconsistency _ (y, s) _ LP CL D2).
   refine (RotIn _ _ _ _ D1).
   apply in_or_app; right; left; reflexivity.
   now apply InPL_right.
   simpl in Ic; destruct Ic as [Ic | Ic]; [ symmetry in Ic; exact (NEQ2 Ic) |].
   simpl in D; destruct D as [D | D].
   apply (F _ (path_start_pl ph)).
   refine (CPconsistency _ _ c LP CL _ _ (InPL_right y s)).
   exact (set_remove_in _ _ _ _ Ic).
   rewrite Heqc2 in D; refine (RotIn_r _ _ _ _ D).
   apply in_or_app; right; left; reflexivity.
   apply (DC c).
   exact (set_remove_in _ _ _ _ Ic).
   clear -D.
   induction C; simpl in *.
    elim D.
    destruct (eq_listline_dec c a) as [EQ1 | NEQ1]; subst.
    destruct (eq_listline_dec (lxy ++ lyz ++ lzx) a) as [ROT | NROT].
    apply rin_in in D.
    destruct D as [ec [CRT ICC]].
    apply set_remove_in in ICC.
    apply (eq_rin_elem _ _ _ (Rot_sym _ _ CRT)).
    exact (in_rin _ _ ICC).
    simpl in D.
    destruct (eq_listline_dec a a) as [EQ1 | EQ1]; subst.
     exact (set_remove_in_r _ _ _ D).
     elim (EQ1 (eq_refl _)).
    destruct (eq_listline_dec (lxy ++ lyz ++ lzx) a) as [ROT | NROT].
     right; exact D.
     simpl in D.
    destruct (eq_listline_dec c a) as [EQ2 | NEQ2]; subst.
     elim (NEQ1 (eq_refl _)).
     simpl in D.
    destruct D as [D | D].
     left; exact D.
     right; exact (IHC D).
   (* DistinctA *)
   unfold DistinctA.
   intros a0 Ia D.
   remember (lxy ++ ((y, s) :: al) ++ (e, z) :: lzx) as a1 in D.
   remember ((((s, y) :: lyz) ++ (z, e) :: reverse_linelist al) :: nil) as a2 in D.
   simpl in D.
   destruct (eq_listcircuit_dec a0 a2) as [EQ1 | NEQ1].
   simpl in D.
   destruct D as [D | D].
   subst.
   apply eq_area_sym in D.
   apply (eq_area_rin _ _ _ (in_rin _ _ (in_eq _ _))) in D.
   destruct D as [EQ1 | EQ1]; [ | exact EQ1].
   assert(In (s, y) (lxy ++ (y, s) :: al ++ (e, z) :: lzx)).
    exact (RotIn _ _ _ (in_eq _ _) (Rot_sym _ _ EQ1)).
   assert (In (s, y) (lxy ++ lyz ++ lzx) \/ s = y \/ (y = z /\ s = e) \/ In (s, y) al).
   apply in_app_or in H.
   destruct H as [H | [H | H]].
   left; apply in_or_app; left; exact H.
   right; left; inversion H; reflexivity.
   apply in_app_or in H; destruct H as [H | [H | H]].
   repeat right; exact H.
   right; right; left; inversion H; split; reflexivity.
   left; apply in_or_app; right; apply in_or_app; right; exact H.
   apply (AC _ _ INA) in INC.
   destruct H0 as [H0 | [H0 | [[Hy Hs] | H0]]].
   apply (F s (path_start_pl ph)).
   exact (CPconsistency s (s, y) _ LP CL INC H0 (InPL_left _ _)).
   apply (F s (path_start_pl ph)); subst.
   exact IY.
   subst; inversion ph; subst.
    destruct NS as [NS | NS]; apply NS; reflexivity.
    exact (not_simplepath ph).
   exact (F y (path_ll_pl ph y _ (InPL_right _ _) H0) IY).
   subst.
   apply set_remove_in_a in D.
   apply ain_in in D.
   destruct D as [a' [D1 D2]].
   apply (eq_area_rin _ _ _ (in_rin _ _ (in_eq _ _))) in D1.
   apply rin_in in D1.
   destruct D1 as [c' [D3 D4]].
   apply (AC c' _ D2) in D4.
   apply (F s (path_start_pl ph)).
   refine (CPconsistency s (s, y) _ LP CL D4 _ (InPL_left _ _)).
   exact (RotIn _ _ _ (in_eq _ _) D3).
   simpl in D; destruct D as [D | D].
   destruct Ia as [Ia | Ia]; [ subst; apply NEQ1; reflexivity | ].
   subst; apply eq_area_sym in D; apply (eq_area_rin _ _ _ (in_rin _ _ (in_eq _ _))) in D.
   apply rin_in in D.
   destruct D as [c' [D1 D2]].
   apply (RotIn _ _ _ (in_eq _ _)) in D1.
   destruct Ia as [Ia | Ia].
   subst.
   destruct D2 as [D2 | D2].
   subst; assert (In (s, y) (lxy ++ lyz ++ lzx) \/ s = y \/ (y = z /\ s = e) \/ In (s, y) al).
   apply in_app_or in D1.
   destruct D1 as [D1 | [D1 | D1]].
   left; apply in_or_app; left; exact D1.
   right; left; inversion D1; reflexivity.
   apply in_app_or in D1; destruct D1 as [D1 | [D1 | D1]].
   repeat right; exact D1.
   right; right; left; inversion D1; split; reflexivity.
   left; apply in_or_app; right; apply in_or_app; right; exact D1.
   apply (AC _ _ INA) in INC.
   destruct H as [H | [H | [[Hy Hs] | H]]].
   apply (F s (path_start_pl ph)).
   exact (CPconsistency s (s, y) _ LP CL INC H (InPL_left _ _)).
   apply (F s (path_start_pl ph)); subst.
   exact IY.
   subst; inversion ph; subst.
    destruct NS as [NS | NS]; apply NS; reflexivity.
    exact (not_simplepath ph).
   exact (F y (path_ll_pl ph y _ (InPL_right _ _) H) IY).
   apply set_remove_in in D2.
   apply (AC c' _ INA) in D2.
   apply (F s (path_start_pl ph)).
   exact (CPconsistency s (s, y) _ LP CL D2 D1 (InPL_left _ _)).
   apply set_remove_in in Ia.
   apply (AC c' _ Ia) in D2.
   apply (F s (path_start_pl ph)).
   exact (CPconsistency s (s, y) _ LP CL D2 D1 (InPL_left _ _)).
   destruct Ia as [Ia | Ia]; subst; [ apply NEQ1; reflexivity | ].
   destruct (eq_listcircuit_dec a0 ((lxy ++ ((y, s) :: al) ++ (e, z) :: lzx) :: set_remove eq_listline_dec (lxy ++ lyz ++ lzx) a)) as [EQ2 | NEQ2].
   apply set_remove_in_a in D.
   apply ain_in in D.
   destruct D as [a' [D1 D2]].
   assert (RIn (lxy ++ ((y, s) :: al) ++ (e, z) :: lzx) a0); subst.
   left; now apply rotSame.
   apply (eq_area_rin _ _ _ H) in D1.
   apply rin_in in D1.
   destruct D1 as [c' [D3 D4]].
   apply (F s (path_start_pl ph)).
   apply (AC c' _ D2) in D4.
   apply (RotIn (y, s)) in D3.
   exact (CPconsistency _ _ _ LP CL D4 D3 (InPL_right _ _)).
   apply in_or_app; right; left; reflexivity.
   destruct Ia as [Ia | Ia]; [ symmetry in Ia; exact (NEQ2 Ia) | ].
   apply set_remove_in in Ia; destruct D as [D | D].
   assert (RIn (lxy ++ ((y, s) :: al) ++ (e, z) :: lzx) a0).
   refine (eq_area_rin _ _ _ _ (eq_area_sym _ _ D)).
   left; now apply rotSame.
   apply rin_in in H.
   destruct H as [c' [D1 D2]].
   apply (F s (path_start_pl ph)).
   apply (AC c' _ Ia) in D2.
   refine (CPconsistency _ (y, s) _ LP CL D2 _ (InPL_right _ _)).
   refine (RotIn (y, s) _ _ _ D1).
   apply in_or_app; right; left; reflexivity.
   apply (DA _ Ia).
   clear -D.
   induction A; simpl in *.
    elim D.
    destruct (eq_listcircuit_dec a0 a1) as [EQ1 | NEQ1]; subst.
    destruct (eq_listcircuit_dec a a1) as [EQA | NEQA].
     apply ain_in in D.
     destruct D as [ea [EQAA IAA]].
     apply set_remove_in in IAA.
     apply (eq_ain_elem _ _ _ (eq_area_sym _ _ EQAA)).
     exact (in_ain _ _ IAA).
    simpl in D.
    destruct (eq_listcircuit_dec a1 a1) as [EQ1 | NEQ1]; subst.
     exact (set_remove_in_a _ _ _ D).
     elim (NEQ1 (eq_refl _)).
    destruct (eq_listcircuit_dec a a1) as [EQA | NEQA].
     right; exact D.
    simpl in D.
    destruct (eq_listcircuit_dec a0 a1) as [EQ2 | NEQ2]; subst.
     elim (NEQ1 (eq_refl _)).
    simpl in D.
    destruct D as [D | D].
     left; exact D.
     right; exact (IHA D).
  (* PLCAconstraints *)
  split.
   (* Lconstraint *)
   unfold Lconstraint, Line_constraint.
   intros l LIN D.
   simpl in LIN.
   destruct LIN as [LIN | [LIN | LIN]].
   destruct l as [lx ly]; simpl in D; subst.
   inversion LIN; subst.
   exact (F ly (path_start_pl ph) IY).
   destruct l as [lx ly]; simpl in D; subst.
   inversion LIN; subst.
   exact (F ly (path_end_pl ph) IZ).
   apply in_app_or in LIN.
   destruct LIN as [LIN | LIN].
   exact (((simplepathline_Lconstraint ph) _ LIN) D).
   exact ((LT _ LIN) D).
  split.
   (* Cconstraint *)
   unfold Cconstraint.
   intros c ICC.
   unfold Circuit_constraints.
   simpl in ICC.
   destruct ICC as [ICC | [ICC | ICC]]; [ subst; simpl | subst | ].
   split; [ exists s; exists (s::pyz ++ (rev ap)) | ].
   apply step_trail.
   apply (app_trail tl2 (simplepath_onlyif_trail (rev_path ph))).
   intros l IYZ D.
   apply (AC _ _ INA) in INC.
   destruct l as [lx ly].
   unfold reverse_linelist in D.
   destruct (LIn_In _ _ IYZ) as [CYZ | CYZ].
    apply (CPconsistency lx (lx, ly) _ LP CL) in INC.
    refine (F lx  _ INC).
    destruct (LIn_In _ _ D) as [D1 | D1].
     rewrite <- in_rev, map_reverse in D1.
     exact (path_ll_pl ph lx _ (InPL_right _ _) D1).
     rewrite <- in_rev, map_reverse, reverse_reverse in D1.
     exact (path_ll_pl ph lx _ (InPL_left _ _) D1).
    apply in_app_iff; right.
    apply in_app_iff; left; exact CYZ.
    exact (InPL_left _ _).
    apply (CPconsistency lx (ly, lx) _ LP CL) in INC.
    refine (F lx  _ INC).
    destruct (LIn_In _ _ D) as [D1 | D1].
     rewrite <- in_rev, map_reverse in D1.
     exact (path_ll_pl ph lx _ (InPL_right _ _) D1).
     rewrite <- in_rev, map_reverse, reverse_reverse in D1.
     exact (path_ll_pl ph lx _ (InPL_left _ _) D1).
    apply in_app_iff; right.
    apply in_app_iff; left; exact CYZ.
    exact (InPL_right _ _).
   intro D; subst.
   exact (F e (path_end_pl ph) IZ).
   intro D.
   apply (AC _ _ INA) in INC.
   apply (F e (path_end_pl ph)).
   destruct (LIn_In _ _ D) as [D1 | D1].
    apply (CL (z, e)) in INC.
    apply LIn_In in INC; destruct INC as [INC | INC].
    exact (LP e _ INC (InPL_right _ _)).
    exact (LP e _ INC (InPL_left _ _)).
    apply in_app_iff; right.
    apply in_app_iff; left; exact D1.
    apply (CL (e, z)) in INC.
    apply LIn_In in INC; destruct INC as [INC | INC].
    exact (LP e _ INC (InPL_left _ _)).
    exact (LP e _ INC (InPL_right _ _)).
    apply in_app_iff; right.
    apply in_app_iff; left; exact D1.
   intro D.
   unfold reverse_linelist in D.
   refine (F z _ IZ).
   destruct (LIn_In _ _ D) as [D1 | D1].
    rewrite <- in_rev, map_reverse in D1.
    exact (path_ll_pl ph z _ (InPL_right _ _) D1).
    rewrite <- in_rev, map_reverse, reverse_reverse in D1.
    exact (path_ll_pl ph z _ (InPL_left _ _) D1).
   intro D; subst.
   exact (F y (path_start_pl ph) IY).
   intro D.
   apply lin_app_or in D.
   destruct D as [D | D].
   apply (AC _ _ INA) in INC.
   apply (F s (path_start_pl ph)).
   destruct (LIn_In _ _ D) as [D1 | D1].
    apply (CL (s, y)) in INC.
    apply LIn_In in INC; destruct INC as [INC | INC].
    exact (LP s _ INC (InPL_left _ _)).
    exact (LP s _ INC (InPL_right _ _)).
    apply in_app_iff; right.
    apply in_app_iff; left; exact D1.
    apply (CL (y, s)) in INC.
    apply LIn_In in INC; destruct INC as [INC | INC].
    exact (LP s _ INC (InPL_right _ _)).
    exact (LP s _ INC (InPL_left _ _)).
    apply in_app_iff; right.
    apply in_app_iff; left; exact D1.
   simpl in D.
   destruct D as [D | D].
   inversion D; subst.
    exact (F z (path_start_pl ph) IZ).
    inversion H; subst.
    inversion ph; subst.
     destruct NS as [NS | NS]; apply NS; reflexivity.
     exact (not_simplepath ph).
   unfold reverse_linelist in D.
   refine (F y _ IY).
   destruct (LIn_In _ _ D) as [D1 | D1].
    rewrite <- in_rev, map_reverse in D1.
    exact (path_ll_pl ph y _ (InPL_left _ _) D1).
    rewrite <- in_rev, map_reverse, reverse_reverse in D1.
    exact (path_ll_pl ph y _ (InPL_right _ _) D1).
   unfold reverse_linelist.
   rewrite app_length; simpl.
   rewrite rev_length, map_length.
   rewrite plus_comm; simpl.
   apply le_n_S, le_n_S; rewrite app_length in lg1; exact lg1.
   split; [ exists x; exists (pxy ++ ap ++ pzx) | ].
   apply (app_trail tl1).
   apply (app_trail (simplepath_onlyif_trail ph) tl3).
   intros l LIN LZX.
   destruct l as [lx ly].
   apply (AC _ _ INA) in INC.
   destruct (LIn_In _ _ LZX) as [LZX1 | LZX2].
    apply (CPconsistency lx (lx, ly) _ LP CL) in INC.
    refine (F lx _ INC).
    destruct (LIn_In _ _ LIN) as [LIN1 | LIN2].
     exact (path_ll_pl ph lx _ (InPL_left _ _) LIN1).
     exact (path_ll_pl ph lx _ (InPL_right _ _) LIN2). 
    apply in_app_iff; right.
    apply in_app_iff; right; exact LZX1.
    exact (InPL_left _ _).
    apply (CPconsistency lx (ly, lx) _ LP CL) in INC.
    refine (F lx _ INC).
    destruct (LIn_In _ _ LIN) as [LIN1 | LIN2].
     exact (path_ll_pl ph lx _ (InPL_left _ _) LIN1).
     exact (path_ll_pl ph lx _ (InPL_right _ _) LIN2). 
    apply in_app_iff; right.
    apply in_app_iff; right; exact LZX2.
    exact (InPL_right _ _).
   intros D; subst.
   exact (F z (path_end_pl ph) IZ).
   intros D.
   refine (F z _ IZ).
   destruct (LIn_In _ _ D) as [D1 | D1].
    exact (path_ll_pl ph z _ (InPL_right _ _) D1).
    exact (path_ll_pl ph z _ (InPL_left _ _) D1).
   intros D.
   apply (F e (path_end_pl ph)).
   apply (AC _ _ INA) in INC.
   destruct (LIn_In _ _ D) as [D1 | D1].
    refine (CPconsistency e (e, z) _ LP CL INC _ (InPL_left _ _)).
    apply in_app_iff; right.
    apply in_app_iff; right; exact D1.
    refine (CPconsistency e (z, e) _ LP CL INC _ (InPL_right _ _)).
    apply in_app_iff; right.
    apply in_app_iff; right; exact D1.
   intros l LXY LIN.
   apply lin_app_or in LIN.
   destruct LIN as [LIN | LIN].
   destruct l as [lx ly].
   apply (AC _ _ INA) in INC.
   destruct (LIn_In _ _ LXY) as [LXY1 | LXY1].
    apply (CPconsistency lx (lx, ly) _ LP CL) in INC.
    refine (F lx _ INC).
    destruct (LIn_In _ _ LIN) as [LIN1 | LIN1].
     exact (path_ll_pl ph lx _ (InPL_left _ _) LIN1).
     exact (path_ll_pl ph lx _ (InPL_right _ _) LIN1).
    apply in_app_iff; left; exact LXY1.
    exact (InPL_left _ _).
    apply (CPconsistency lx (ly, lx) _ LP CL) in INC.
    refine (F lx _ INC).
    destruct (LIn_In _ _ LIN) as [LIN1 | LIN1].
     exact (path_ll_pl ph lx _ (InPL_left _ _) LIN1).
     exact (path_ll_pl ph lx _ (InPL_right _ _) LIN1).
    apply in_app_iff; left; exact LXY1.
    exact (InPL_right _ _).
   simpl in LIN.
   destruct LIN as [LIN | LIN].
   inversion LIN; subst.
   apply (F e (path_end_pl ph)).
   apply (AC _ _ INA) in INC.
   destruct (LIn_In _ _ LXY) as [LXY1 | LXY1].
    refine (CPconsistency e (e, z) _ LP CL INC _ (InPL_left _ _ )).
    apply in_app_iff; left; exact LXY1.
    refine (CPconsistency e (z, e) _ LP CL INC _ (InPL_right _ _ )).
    apply in_app_iff; left; exact LXY1.
   apply f_equal with(f:=reverse) in H.
   rewrite reverse_reverse in H.
   subst.
   apply (F e (path_end_pl ph)).
   apply (AC _ _ INA) in INC.
   destruct (LIn_In _ _ LXY) as [LXY1 | LXY1].
    refine (CPconsistency e (z, e) _ LP CL INC _ (InPL_right _ _ )).
    apply in_app_iff; left; exact LXY1.
    refine (CPconsistency e (e, z) _ LP CL INC _ (InPL_left _ _ )).
    apply in_app_iff; left; exact LXY1.
   apply (AC _ _ INA) in INC.
   apply CT in INC.
   destruct INC as [[ex [epl INC1]] INC2].
   clear -INC1 LXY LIN.
   cut (exists e : Point, trail e ex epl (lxy ++ lyz ++ lzx)); intros.
   destruct H as [e TL].
   clear INC1.
   revert e epl TL.
   induction lxy; intros; simpl in *.
    exact LXY.
    destruct LXY as [LXY | LXY].
     inversion TL; subst.
     apply H6.
     apply (eq_lin_elem _ _ _ LXY).
     apply lin_or_app; right.
     apply lin_or_app; right; exact LIN.
    inversion TL; subst.
    exact (IHlxy LXY _ _ H1).
   exists ex; exact INC1.
   intros D; subst.
   exact (F s (path_start_pl ph) IY).
   intros D.
   apply (F s (path_start_pl ph)).
   apply (AC _ _ INA) in INC.
   destruct (LIn_In _ _ D) as [D1 | D1].
    refine (CPconsistency s (y, s) _ LP CL INC _ (InPL_right _ _)).
    apply in_app_iff; left; exact D1.
    refine (CPconsistency s (s, y) _ LP CL INC _ (InPL_left _ _)).
    apply in_app_iff; left; exact D1.
   intros D.
   apply lin_app_or in D.
   apply (AC _ _ INA) in INC.
   destruct D as [D | D].
    refine (F y _ IY).
    destruct (LIn_In _ _ D) as [D1 | D1].
     exact (path_ll_pl ph y (y, s) (InPL_left _ _) D1).
     exact (path_ll_pl ph y (s, y) (InPL_right _ _) D1).
   simpl in D.
   destruct D as [D | D].
   inversion D; subst.
   exact (F z (path_start_pl ph) IZ).
   inversion H; subst.
   inversion ph; subst.
    destruct NS as [NS | NS]; apply NS; reflexivity.
    exact (not_simplepath ph).
   apply (F s (path_start_pl ph)).
   destruct (LIn_In _ _ D) as [D1 | D1].
    refine (CPconsistency s (y, s) _ LP CL INC _ (InPL_right _ _)).
    apply in_app_iff; right.
    apply in_app_iff; right; exact D1.
    refine (CPconsistency s (s, y) _ LP CL INC _ (InPL_left _ _)).
    apply in_app_iff; right.
    apply in_app_iff; right; exact D1.
   do 2 (rewrite app_length; simpl).
   rewrite plus_comm; simpl.
   apply le_n_S; rewrite <- plus_assoc.
   apply le_trans with (S (length lzx) + length lxy).
    apply le_n_S.
    rewrite plus_comm; rewrite <- app_length; exact lg2.
    now apply le_plus_r.
   exact (CT c (set_remove_in _ _ _ _ ICC)).
   (* Aconstraint *)
   unfold Aconstraint.
   split.
   remember (((s, y) :: lyz) ++ (z, e) :: reverse_linelist al) as c1.
   remember (lxy ++ ((y, s) :: al) ++ (e, z) :: lzx) as c2.
   intros a0 IAA.
   unfold Area_constraints.
   simpl in IAA.
   destruct IAA as [IAA | [IAA | IAA]].
    split; [ subst; intros c1 c2 IC1 IC2 NC | ].
    simpl in IC1, IC2.
    cut(~point_connect c1 c2); intros PC.
    split; [ exact PC | ].
    intros D; apply PC.
    exact (lc_only_if_pc _ _ D).
    destruct IC1 as [IC1 | IC1]; [ subst | exact IC1 ].
    destruct IC2 as [IC2 | IC2]; [ subst; exact (NC (eq_refl _)) | exact IC2 ].
    split; subst a0; simpl; [ intros c Ic D | exact (le_n _) ].
    destruct Ic as [Ic | Ic]; [ | exact Ic].
    destruct (eq_listline_dec c c1) as [ROT | NROT]; [ exact D | symmetry in Ic; exact (NROT Ic)].
    split; [ subst; intros c1 c2 IC1 IC2 NC | ].
    simpl in IC1, IC2.
    cut(~point_connect c1 c2); intros PC.
    split; [ exact PC | ].
    intros D; apply PC.
    exact (lc_only_if_pc _ _ D).
    unfold point_connect in PC.
    destruct PC as [l3 [l4 [p [L3 [L4 [IPL1 IPL2]]]]]].
    destruct IC1 as [IC1 | IC1]; [ subst | ].
    destruct IC2 as [IC2 | IC2]; [ subst; exact (NC (eq_refl _)) | ].
    destruct (eq_listline_dec c2 (lxy ++ lyz ++ lzx)) as [EQ | NEQ]; [ subst | ].
    destruct (AT a INA) as [AT1 [AT2 AT3]].
    apply (AT2 _ (set_remove_in _ _ _ _ IC2)).
    apply in_rin.
    exact IC2.
    apply set_remove_in in IC2.
    apply in_app_iff in L3.
    destruct L3 as [L3 | L3].
    destruct (AT a INA) as [AT1 AT2].
    destruct (AT1 c2 _ IC2 INC NEQ) as [NPC NLC].
    apply NPC.
    unfold point_connect.
    exists l4; exists l3; exists p; split; [ exact L4 | ].
    split; [ apply in_app_iff; left; exact L3 | ].
    split; [ exact IPL2 | exact IPL1 ].
    simpl in L3.
    destruct L3 as [L3 | L3]; [ subst | ].
    unfold InPL in IPL1; simpl in IPL1.
    destruct IPL1; subst.
    destruct (AT a INA) as [AA1 AA2].
    destruct (AA1 c2 (lxy ++ lyz ++ lzx) IC2 INC NEQ) as [NPC NLC].
    apply NPC.
    unfold point_connect.
    inversion tl2; subst.
    inversion tl3; subst.
    inversion tl1; subst.
    simpl in INC.
    destruct (CT _ (AC _ _ INA INC)) as [[ll CC1] CC2].
    simpl in CC2.
    elimtype False; omega.
    exists l4; exists (x, p2); exists x; split; [ exact L4 | ].
    split; [ exact (in_eq _ _) | split; [ exact IPL2 | exact (InPL_left _ _)] ].
    exists l4; exists (z, p2); exists z; split; [ exact L4 | ].
    split; [ | split; [ exact IPL2 | exact (InPL_left _ _)] ].
    apply in_app_iff; right; exact (in_eq _ _).
    exists l4; exists (p, p2); exists p; split; [ exact L4 | ].
    split; [ | split; [ exact IPL2 | exact (InPL_left _ _)] ].
    apply in_app_iff; right; exact (in_eq _ _).
    exact (F p (path_start_pl ph) (CPconsistency p l4 c2 LP CL (AC _ _ INA IC2) L4 IPL2)).
    apply in_app_iff in L3.
    destruct L3 as [L3 | L3].
    exact (F p (path_ll_pl ph p _ IPL1 L3) (CPconsistency p l4 c2 LP CL (AC _ _ INA IC2) L4 IPL2)).
    simpl in L3.
    destruct L3 as [L3 | L3]; [ subst | ].
    unfold InPL in IPL1; simpl in IPL1.
    destruct IPL1; subst.
    exact (F p (path_end_pl ph) (CPconsistency p l4 c2 LP CL (AC _ _ INA IC2) L4 IPL2)).
    destruct (AT a INA) as [AA1 AA2].
    destruct (AA1 c2 (lxy ++ lyz ++ lzx) IC2 INC NEQ) as [NPC NLC].
    apply NPC.
    unfold point_connect.
    inversion tl3; subst.
    inversion tl1; subst.
    inversion tl2; subst.
    simpl in INC.
    destruct (CT _ (AC _ _ INA INC)) as [[ll CC1] CC2].
    simpl in CC2.
    elimtype False; omega.
    exists l4; exists (y, p2); exists y; split; [ exact L4 | ].
    split; [ exact (in_eq _ _) | split; [ exact IPL2 | exact (InPL_left _ _)] ].
    exists l4; exists (x, p2); exists x; split; [ exact L4 | ].
    split; [ exact (in_eq _ _) | split; [ exact IPL2 | exact (InPL_left _ _)] ].
    exists l4; exists (p, p2); exists p; split; [ exact L4 | ].
    split; [ | split; [ exact IPL2 | exact (InPL_left _ _)] ].
    apply in_app_iff; right; apply in_app_iff; right; exact (in_eq _ _).
    destruct (AT a INA) as [AA1 AA2].
    destruct (AA1 c2 (lxy ++ lyz ++ lzx) IC2 INC NEQ) as [NPC NLC].
    apply NPC.
    unfold point_connect.
    exists l4; exists l3; exists p; split; [ exact L4 | ].
    split; [ | split; [ exact IPL2 | exact IPL1 ] ].
    apply in_app_iff; right; apply in_app_iff; right; exact L3.
    destruct IC2 as [IC2 | IC2]; [ subst | ].
    apply in_app_iff in L4.
    destruct L4 as [L4 | L4].
    destruct (AT a INA) as [AA1 [AA2 AA3]].
    destruct (eq_listline_dec c1 (lxy ++ lyz ++ lzx)) as [EQ | NEQ]; [ subst | ].
    apply (AA2 _ (set_remove_in _ _ _ _ IC1)).
    apply in_rin.
    exact IC1.
    apply set_remove_in in IC1.
    destruct (AA1 c1 (lxy ++ lyz ++ lzx) IC1 INC NEQ) as [NPC NLC].
    apply NPC; unfold point_connect.
    exists l3; exists l4; exists p.
    split; [ exact L3 | split ].
    apply in_app_iff; left; exact L4.
    split; [ exact IPL1 | exact IPL2 ].
    simpl in L4.
    destruct L4 as [L4 | L4]; [ subst | ].
    unfold InPL in IPL2; simpl in IPL2.
    destruct IPL2; subst.
    destruct (AT _ INA) as [AT1 [AT2 AT3]].
    destruct (eq_listline_dec c1 (lxy ++ lyz ++ lzx)) as [EQ | NEQ]; [ subst | ].
    apply (AT2 _ (set_remove_in _ _ _ _ IC1)).
    apply in_rin.
    exact IC1.
    apply set_remove_in in IC1.
    destruct (AT1 c1 (lxy ++ lyz ++ lzx) IC1 INC NEQ) as [NPC NLC].
    apply NPC; unfold point_connect.
    inversion tl2; subst.
    inversion tl3; subst.
    inversion tl1; subst.
    destruct (CT _ (AC _ _ INA INC)) as [[c CT1] CT2].
    elimtype False; simpl in CT2; omega.
    exists l3; exists (x, p2); exists x.
    split; [ exact L3 | split ].
    exact (in_eq _ _).
    split; [ exact IPL1 | exact (InPL_left _ _) ].
    exists l3; exists (z, p2); exists z.
    split; [ exact L3 | split ].
    apply in_app_iff; right; exact (in_eq _ _).
    split; [ exact IPL1 | exact (InPL_left _ _) ].
    exists l3; exists (p, p2); exists p.
    split; [ exact L3 | split ].
    apply in_app_iff; right; exact (in_eq _ _).
    split; [ exact IPL1 | exact (InPL_left _ _) ].
    apply set_remove_in in IC1.
    apply (AC _ _ INA) in IC1.
    exact (F p (path_start_pl ph) (CPconsistency p l3 c1 LP CL IC1 L3 IPL1)).
    apply in_app_iff in L4.
    destruct L4 as [L4 | L4].
    apply set_remove_in in IC1.
    apply (AC _ _ INA) in IC1.
    refine (F p _ (CPconsistency p l3 c1 LP CL IC1 L3 IPL1)).
    unfold InPL in IPL2; destruct l4; simpl in IPL2.
    destruct IPL2; subst.
    exact (path_ll_pl ph p _ (InPL_left _ _) L4).
    exact (path_ll_pl ph p _ (InPL_right _ _) L4).
    simpl in L4.
    destruct L4 as [L4 | L4]; [ subst | ].
    unfold InPL in IPL2; simpl in IPL2.
    destruct IPL2; subst.
    apply set_remove_in in IC1.
    apply (AC _ _ INA) in IC1.
    exact (F p (path_end_pl ph) (CPconsistency p l3 c1 LP CL IC1 L3 IPL1)).
    destruct (AT _ INA) as [AT1 [AT2 AT3]].
    destruct (eq_listline_dec c1 (lxy ++ lyz ++ lzx)) as [EQ | NEQ]; [ subst | ].
    apply (AT2 _ (set_remove_in _ _ _ _ IC1)).
    apply in_rin.
    exact IC1.
    apply set_remove_in in IC1.
    destruct (AT1 c1 (lxy ++ lyz ++ lzx) IC1 INC NEQ) as [NPC NLC].
    apply NPC; unfold point_connect.
    inversion tl3; subst.
    inversion tl1; subst.
    inversion tl2; subst.
    destruct (CT _ (AC _ _ INA INC)) as [[c CT1] CT2].
    elimtype False; simpl in CT2; omega.
    exists l3; exists (y, p2); exists y.
    split; [ exact L3 | split ].
    exact (in_eq _ _).
    split; [ exact IPL1 | exact (InPL_left _ _) ].
    exists l3; exists (x, p2); exists x.
    split; [ exact L3 | split ].
    exact (in_eq _ _).
    split; [ exact IPL1 | exact (InPL_left _ _) ].
    exists l3; exists (p, p2); exists p.
    split; [ exact L3 | split ].
    apply in_app_iff; right; apply in_app_iff; right; exact (in_eq _ _).
    split; [ exact IPL1 | exact (InPL_left _ _) ].
    destruct (AT _ INA) as [AT1 [AT2 AT3]].
    destruct (eq_listline_dec c1 (lxy ++ lyz ++ lzx)) as [EQ | NEQ]; [ subst | ].
    apply (AT2 _ (set_remove_in _ _ _ _ IC1)).
    apply in_rin.
    exact IC1.
    apply set_remove_in in IC1.
    destruct (AT1 c1 (lxy ++ lyz ++ lzx) IC1 INC NEQ) as [NPC NLC].
    apply NPC; unfold point_connect.
    exists l3; exists l4; exists p.
    split; [ exact L3 | split ].
    apply in_app_iff; right; apply in_app_iff; right; exact L4.
    split; [ exact IPL1 | exact IPL2 ].
    destruct (AT a INA) as [AT1 AT2].
    destruct (AT1 c1 c2 (set_remove_in _ _ _ _ IC1) (set_remove_in _ _ _ _ IC2) NC) as [NPC NLC].
    apply NPC.
    exists l3; exists l4; exists p.
    split; [exact L3 | ].
    split; [exact L4 | ].
    split; [exact IPL1 | exact IPL2].
    split; subst a0; [intros c Ic D | ].
    simpl in D.
    destruct (eq_listline_dec c c2) as [EQ | NEQ]; [ subst | ].
    apply set_remove_in_r in D.
    apply (F s (path_start_pl ph)).
    apply rin_in in D.
    destruct D as [c' [D1 D2]].
    apply (AC _ _ INA) in D2.
    refine (CPconsistency s (y, s) _ LP CL D2 _ (InPL_right _ _)).
    refine (RotIn _ _ _ _ D1).
    apply in_or_app; right.
    exact (in_eq _ _).
    destruct Ic as [Ic | Ic]; [ symmetry in Ic; exact (NEQ Ic) | ].
    simpl in D.
    destruct D as [D | D].
    apply (F s).
    apply (path_start_pl ph).
    apply set_remove_in in Ic.
    apply (AC _ _ INA) in Ic.
    refine (CPconsistency _ _ _ LP CL Ic _ (InPL_right y s)).
    apply RotIn with c2.
    subst; apply in_or_app; right; now apply in_eq.
    apply Rot_sym; exact D.
    destruct (AT a INA) as [AT1 [AT2 AT3]].
    apply (AT2 c).
    clear -Ic.
    induction a; [ exact Ic | ].
    simpl in *.
    destruct (eq_listline_dec (lxy ++ lyz ++ lzx) a) as [EQ1 | NEQ1].
    right; exact Ic.
    destruct Ic as [Ic | Ic]; [ left | right; apply IHa]; exact Ic.
    clear -D.
    induction a; simpl in *.
     elim D.
     destruct (eq_listline_dec c a) as [EQ1 | NEQ1]; subst.
     destruct (eq_listline_dec (lxy ++ lyz ++ lzx) a) as [ROT | NROT].
      apply rin_in in D.
      destruct D as [ec [CRT ICC]].
      apply set_remove_in in ICC.
      apply (eq_rin_elem _ _ _ (Rot_sym _ _ CRT)).
      exact (in_rin _ _ ICC).
     simpl in D.
     destruct (eq_listline_dec a a) as [EQ1 | NEQ1]; subst.
      exact (set_remove_in_r _ _ _ D).
      elim (NEQ1 (eq_refl _)).
     destruct (eq_listline_dec (lxy ++ lyz ++ lzx) a) as [ROT | NROT].
      right; exact D.
     simpl in D.
     destruct (eq_listline_dec c a) as [EQ2 | NEQ2]; subst.
      elim (NEQ1 (eq_refl _)).
     simpl in D.
     destruct D as [D | D].
      left; exact D.
      right; exact (IHa D).
    simpl; omega.
    destruct (AT a0 (set_remove_in _ _ _ _ IAA)) as [AT1 [AT2 AT3]].
    split; [ subst; intros c1 c2 IC1 IC2 NC | ].
    exact (AT1 c1 c2 IC1 IC2 NC).
    split; [ intros c D; exact (AT2 c D) | exact AT3 ].
  (* Outermost_constraints *)
  exact OT.
 (* add_outpath *)
+destruct IND as [CSS CST].
 destruct (add_pathpoint' x y z pxy pyz pzx lxy lyz lzx I CST CSS INC
 tl1 tl2 tl3) as [IX [IY IZ]].
 destruct CSS as [PL [LP [LC [CL [CA [AC [O1 [O2 [CC [AA [DP [DL [DC DA]]]]]]]]]]]]].
 subst; destruct CST as [LT [CT [AT OT]]].
 split.
  (* PLCAconsistency *)
  unfold PLCAconsistency.
  split.
   (* PLconsistency *)
   unfold PLconsistency.
   intros p INP.
   destruct (in_app_or ap P p INP) as [INP1 | INP2].
   destruct al as [ | [l' al]].
    inversion ph; subst.
    simpl in INP1; destruct INP1.
     exists (y, e); subst.
     split; compute; auto.
     elim H.
   apply (path_pl_ll ph) in INP1; [ | simpl; omega ].
   destruct INP1 as [l [AL1 AL2]].
   exists l; split; [ | exact AL1].
   simpl; right; right.
   destruct AL2 as [AL2 | AL2].
    left; exact AL2.
    right; apply in_or_app; left.
    exact AL2.
   destruct (PL _ INP2) as [l [IN IPL]].
   exists l; split; [ | exact IPL ].
   right; right.
   apply in_or_app; right.
   exact IN.
  split.
   (* LPconsistency *)
   unfold LPconsistency.
   intros p l LIN IPL.
   simpl in LIN.
   destruct LIN as [LIN | [LIN | LIN]]; [ subst | subst | ].
   unfold InPL in IPL; simpl in IPL.
   destruct IPL as [IPL | IPL]; subst.
   apply in_app_iff; right; exact IY.
   apply in_app_iff; left; exact (path_start_pl ph).
   unfold InPL in IPL; simpl in IPL.
   destruct IPL as [IPL | IPL]; subst.
   apply in_app_iff; left; exact (path_end_pl ph).
   apply in_app_iff; right; exact IZ.
   apply in_app_iff in LIN.
   destruct LIN as [LIN | LIN].
    apply in_app_iff; left.
    exact (path_ll_pl ph p _ IPL LIN).
    apply in_app_iff; right.
    exact (LP p l LIN IPL).
  split.
   (* LCconsistency *)
   unfold LCconsistency.
   intros l LIN.
   simpl in LIN.
   destruct LIN as [LIN | [LIN | LIN]].
    simpl.
    inversion LIN; subst.
     exists (lxy ++ (y, s) :: al ++ (e, z) :: lzx).
     split; [ | exact (in_cons _ _ _ (in_eq _ _)) ].
     apply in_app_iff; right; exact (in_eq _ _).
     exists (((s, y) :: lyz ++ (z, e) :: reverse_linelist al)).
     split; [ | left; exact (eq_refl _) ].
     apply f_equal with(f:=reverse) in H.
     rewrite reverse_reverse in H.
     simpl.
     left; symmetry; exact H.
    inversion LIN; subst.
     exists (lxy ++ ((y, s) :: al) ++ (e, z) :: lzx).
     split; [ | exact (in_cons _ _ _ (in_eq _ _)) ].
     apply in_app_iff; right; right.
     apply in_app_iff; right; exact (in_eq _ _).
     apply f_equal with(f:=reverse) in H.
     rewrite reverse_reverse in H.
     exists (((s, y) :: lyz) ++ (z, e) :: reverse_linelist al).
     split; [ | exact (in_eq _ _) ].
     apply in_app_iff; right.
     rewrite H.
     exact (in_eq _ _).
    apply lin_app_or in LIN.
    destruct LIN as [LIN | LIN].
     destruct (LIn_In _ _ LIN).
      exists (lxy ++ ((y, s) :: al) ++ (e, z) :: lzx).
      split; [ | exact (in_cons _ _ _ (in_eq _ _)) ].
      apply in_app_iff; right.
      apply in_app_iff; left.
      exact (in_cons _ _ _ H).
      exists (((s, y) :: lyz) ++ (z, e) :: reverse_linelist al).
      split; [ | exact (in_eq _ _) ].
      apply in_app_iff; right; right.
      unfold reverse_linelist.
      rewrite <- in_rev.
      apply map_reverse.
      exact H.
   apply LC in LIN.
   destruct LIN as [c [LIC IC]].
   destruct (eq_circuit_dec (lxy ++ lyz ++ lzx) c).
   apply Rot_sym, (RotIn _ _ _ LIC) in r.
   apply in_app_iff in r.
   destruct r as [R | R].
   exists (lxy ++ ((y, s) :: al) ++ (e, z) :: lzx).
   split; [ | exact (in_cons _ _ _ (in_eq _ _)) ].
   apply in_app_iff; now left.
   apply in_app_iff in R.
   destruct R as [R | R].
   exists (((s, y) :: lyz) ++ (z, e) :: reverse_linelist al).
   split; [ | exact (in_eq _ _) ].
   simpl; right; apply in_app_iff; left; exact R.
   exists (lxy ++ ((y, s) :: al) ++ (e, z) :: lzx).
   split; [ | exact (in_cons _ _ _ (in_eq _ _)) ].
   apply in_app_iff; right; simpl; right.
   apply in_app_iff; right; simpl; right; exact R.
   exists c; split; [ exact LIC | simpl; right; right ].
   assert (lxy ++ lyz ++ lzx <> c) as n'.
    intro; apply n; subst; apply rotSame.
   exact (in_set_remove eq_listline_dec _ _ _ n' IC).
  split.
   (* CLconsistency *)
   unfold CLconsistency.
   intros l c ICC ILC.
   simpl in ICC.
   destruct ICC as [ICC | [ICC | ICC]]; [ subst | subst | ].
   simpl in ILC.
   destruct ILC as [ILC | ILC]; [ subst | ].
   left; exact (reverse_ul _ _(eq_refl _)).
   apply in_app_iff in ILC.
   destruct ILC as [ILC | ILC].
   simpl; right; right.
   apply lin_or_app; right.
   apply (CL l) in INC; [ exact INC | ].
   apply in_app_iff; right.
   apply in_app_iff; left; exact ILC.
   simpl in ILC.
   destruct ILC as [ILC | ILC]; [ subst | ].
   right; left; exact (reverse_ul _ _ (eq_refl _)).
   unfold reverse_linelist in ILC.
   rewrite <- in_rev, map_reverse in ILC.
   simpl; right; right.
   apply lin_or_app; left.
   exact (In_LIn_r _ _ ILC).
   apply in_app_iff in ILC.
   destruct ILC as [ILC | ILC].
   simpl; right; right.
   apply lin_or_app; right.
   apply (CL l) in INC; [ exact INC | ].
   apply in_app_iff; left; exact ILC.
   simpl in ILC.
   destruct ILC as [ILC | ILC]; [ subst | ].
   left; exact (ul_refl _ ).
   apply in_app_iff in ILC.
   destruct ILC as [ILC | ILC].
   simpl; right; right.
   apply lin_or_app; left; exact (in_lin _ _ ILC).
   simpl in ILC.
   destruct ILC as [ILC | ILC]; [ subst | ].
   simpl; right; left; exact (ul_refl _).
   simpl; right; right.
   apply lin_or_app; right.
   apply (CL l) in INC; [ exact INC | ].
   apply in_app_iff; right.
   apply in_app_iff; right; exact ILC.
   simpl; right; right.
   apply lin_or_app; right.
   exact (CL _ _ (set_remove_in eq_listline_dec _ _ _ ICC) ILC).
  split.
   (* CAconsistency *)
   unfold CAconsistency.
   intros c ICC ICN.
   simpl in ICC.
   destruct ICC as [ICC | [ICC | ICC]]; [ subst | subst | ].
   exists (((s, y) :: lyz ++ (z, e) :: reverse_linelist al) :: nil).
   split; [ exact (in_eq _ _) | exact (in_eq _ _) ].
   elim ICN; now auto.
   destruct (eq_listline_dec c (lxy ++ lyz ++ lzx)).
   subst.
   elimtype False.
   apply (DC (lxy ++ lyz ++ lzx)).
   exact INC.
   destruct (in_split _ C (set_remove_in _ _ _ _ ICC)) as [C1 [C2 IC]]; subst.
   apply in_rin.
   exact ICC.
   apply (CA _ (set_remove_in eq_listline_dec _ _ _ ICC)) in n.
   destruct n as [a [ICN1 ICN2]].
   exists a.
   split; [ exact ICN1 | right; exact ICN2 ].
  split.
   (* ACconsistency *)
   unfold ACconsistency.
   intros c a IAA IAC.
   simpl in IAA.
   destruct IAA as [IAA | IAA]; subst.
   simpl in IAC.
   destruct IAC as [IAC | []]; subst; exact (in_eq _ _).
   destruct (eq_listline_dec (lxy ++ lyz ++ lzx) c); [ subst | ].
   elim O2 with a.
   exact IAA.
   apply in_rin.
   exact IAC.
   right; right.
   apply (in_set_remove _ _ _ _ n).
   exact (AC _ _ IAA IAC).
  split.
   (* Outermostconsistency1 *)
   unfold Outermostconsistency1.
   right; left; now auto.
  split.
   (* Outermostconsistency2 *)
   unfold Outermostconsistency2.
   intros a IAA D.
   simpl in IAA.
   destruct IAA as [IAA | IAA]; [ subst | ].
   simpl in D.
   destruct D as [D | D]; [ | exact D].
   apply Rot_sym, (RotIn (s, y)) in D.
   apply in_app_or in D.
   destruct D as [D | [D | D]].
   assert (LIn (s, y) L) as D'.
    apply CL with (lxy ++ lyz ++ lzx).
     exact INC.
     apply in_or_app; left; exact D.
   apply F with s.
    inversion ph; subst; simpl; now auto.
    apply LIn_In in D'; destruct D'.
     apply LP with (s, y).
     exact H.
     simpl; now auto.
     unfold reverse in H; simpl in H.
     apply LP with (y, s).
     exact H.
     simpl; now auto.
   inversion D; subst.
   apply F with s.
    inversion ph; subst; simpl; now auto.
    exact IY.
   apply in_app_or in D.
   destruct D as [D | [D | D]].
   apply F with y.
    apply (path_ll_pl ph y (s, y)).
     simpl; now auto.
     exact D.
    exact IY.
   inversion D; subst.
   destruct NS; now auto.
   assert (LIn (s, y) L) as D'.
    apply CL with (lxy ++ lyz ++ lzx).
     exact INC.
     apply in_or_app; right; apply in_or_app; right; exact D.
   apply F with s.
    inversion ph; subst; simpl; now auto.
    apply LIn_In in D'; destruct D'.
     apply LP with (s, y).
     exact H.
     simpl; now auto.
     unfold reverse in H; simpl in H.
     apply LP with (y, s).
     exact H.
     simpl; now auto.
   left; now auto.
   apply rin_in in D.
   destruct D as [c [D1 D2]].
   apply F with s.
    inversion ph; subst; simpl; now auto.
    apply (AC c a IAA) in D2.
    apply (RotIn (y, s)) in D1.
    apply (CL _ _ D2) in D1.
    apply LIn_In in D1.
    destruct D1 as [D1 | D1].
     apply LP with (y, s).
      exact D1.
      simpl; now auto.
     unfold reverse in D1; simpl in D1; apply LP with (s, y).
      exact D1.
      simpl; now auto.
   apply in_or_app; right; left; now auto.
  split.
   (* Circuitconsistency *)
   unfold Circuitconsistency.
   intros l c1 c2 IC1 IC2 LC1 LC2.
   simpl in IC1, IC2.
   destruct IC1 as [IC1 | [IC1 | IC1]];[ subst | subst| ].
   destruct IC2 as [IC2 | [IC2 | IC2]];
   [ subst; reflexivity
   | elimtype False; subst
   | elimtype False ].
   simpl in LC1, LC2.
   destruct LC1 as [LC1 | LC1]; [ subst | ].
   apply in_app_iff in LC2.
   destruct LC2 as [LC2 | LC2].
   apply (CL (s, y)) in INC; [ | apply in_app_iff; left; exact LC2 ].
   apply (F s (path_start_pl ph)).
   apply LIn_In in INC.
   destruct INC as [INC | INC].
   exact (LP s _ INC (InPL_left _ _)).
   unfold reverse in INC; simpl in INC; exact (LP s _ INC (InPL_right _ _)).
   simpl in LC2.
   destruct LC2 as [LC2 | LC2].
   inversion LC2; subst.
   exact (F s (path_start_pl ph) IY).
   apply in_app_iff in LC2.
   destruct LC2 as [LC2 | LC2].
   exact (F y (path_ll_pl ph y (s, y) (or_intror _ (eq_refl y)) LC2) IY).
   simpl in LC2.
   destruct LC2 as [LC2 | LC2].
   inversion LC2; subst.
   inversion ph; subst.
    destruct NS as [NS | NS]; apply NS; reflexivity.
    exact (H0 (path_end_pl H)).
   apply (CL (s, y)) in INC.
   apply (F s (path_start_pl ph)).
   apply LIn_In in INC.
   destruct INC as [INC | INC].
   exact (LP s _ INC (InPL_left _ _)).
   unfold reverse in INC; simpl in INC; exact (LP s _ INC (InPL_right _ _)).
   apply in_app_iff; right; apply in_app_iff; right; exact LC2.
   apply in_app_iff in LC1.
   destruct LC1 as [LC1 | LC1].
   apply in_app_iff in LC2.
   destruct LC2 as [LC2 | LC2].
   destruct (CT _ INC) as [[tx [ll INC1]] INC2].
   clear -LC1 LC2 INC1.
   destruct lxy; simpl in LC2, INC1; [ exact LC2 | ].
   destruct LC2 as [LC2 | LC2]; [ subst | ].
   destruct l; simpl in INC1.
   inversion INC1; subst.
   apply H7.
   apply lin_or_app; right.
   apply lin_or_app; left.
   exact (in_lin _ _ LC1).
   destruct l0; simpl in INC1.
   inversion INC1; subst.
   clear -LC1 LC2 H5.
   revert p0 p pl H5.
   induction lxy; intros; simpl in *.
    exact LC2.
    destruct LC2 as [LC2 | LC2]; [ subst | ].
     inversion H5; subst.
     apply H7.
     apply lin_or_app; right.
     apply lin_or_app; left.
     exact (in_lin _ _ LC1).
     inversion H5; subst.
     exact (IHlxy LC2 _ _ _ H1).
   simpl in LC2.
   destruct LC2 as [LC2 | LC2]; [ subst | ].
   apply (CL (y, s)) in INC.
   apply (F s (path_start_pl ph)).
   apply LIn_In in INC.
   destruct INC as [INC | INC].
   exact (LP s _ INC (InPL_right _ _)).
   unfold reverse in INC; simpl in INC; exact (LP s _ INC (InPL_left _ _)).
   apply in_app_iff; right.
   apply in_app_iff; left; exact LC1.
   apply in_app_iff in LC2.
   destruct LC2 as [LC2 | LC2].
   apply (CL l) in INC.
   destruct l.
   apply (path_ll_pl ph p) in LC2; [ | exact (InPL_left _ _) ].
   apply LIn_In in INC.
   destruct INC as [INC | INC].
   exact (F p LC2 (LP p _ INC (InPL_left _ _))).
   unfold reverse in INC; simpl in INC; exact (F p LC2 (LP p _ INC (InPL_right _ _))).
   apply in_app_iff; right.
   apply in_app_iff; left; exact LC1.
   simpl in LC2.
   destruct LC2 as [LC2 | LC2]; [ subst | ].
   apply (CL (e, z)) in INC.
   apply LIn_In in INC; destruct INC as [INC | INC].
   exact (F e (path_end_pl ph) (LP e _ INC (InPL_left _ _))).
   exact (F e (path_end_pl ph) (LP e _ INC (InPL_right _ _))).
   apply in_app_iff; right.
   apply in_app_iff; left; exact LC1.
   destruct (CT _ INC) as [[tx [ll INC1]] INC2].
   clear -LC1 LC2 INC1 INC2.
   destruct lxy.
   destruct lyz; simpl in LC1; [ exact LC1 | ].
   destruct LC1 as [LC1 | LC1]; [ subst | ].
   simpl in INC1.
   destruct l; simpl in INC1.
   inversion INC1; subst.
   apply H7.
   apply lin_or_app; right.
   exact (in_lin _ _ LC2).
   simpl in INC1.
   destruct l0; simpl in INC1.
   clear INC2.
   inversion INC1; subst.
   clear - LC1 LC2 H5.
   revert p0 p pl lzx H5 LC2.
   induction lyz; intros; simpl in *.
    exact LC1.
    destruct LC1 as [LC1 | LC1]; [ subst | ].
     inversion H5; subst.
     apply H7.
     apply lin_or_app; right.
     exact (in_lin _ _ LC2).
     inversion H5; subst.
     exact (IHlyz LC1 _ _ _ _ H1 LC2).
   simpl in INC1.
   destruct l0; simpl in INC1.
   clear INC2.
   inversion INC1; subst.
   clear -LC1 LC2 H5.
   revert p0 p pl H5.
   induction lxy; intros.
   revert p0 p pl H5.
   induction lyz; intros; simpl in *.
    exact LC1.
    destruct LC1 as [LC1 | LC1]; [ subst | ].
     inversion H5; subst.
     apply H7.
     apply lin_or_app; right.
     exact (in_lin _ _ LC2).
     inversion H5; subst.
     exact (IHlyz LC1 _ _ _ H1).
    inversion H5; subst.
    exact (IHlxy _ _ _ H1).
   simpl in LC1.
   destruct LC1 as [LC1 | LC1]; [ subst | ].
   apply in_app_iff in LC2.
   destruct LC2 as [LC2 | LC2].
   apply (CL (z, e)) in INC; [ | apply in_app_iff; left; exact LC2 ].
   apply LIn_In in INC; destruct INC as [INC | INC].
   exact (F e (path_end_pl ph) (LP e _ INC (InPL_right _ _))).
   exact (F e (path_end_pl ph) (LP e _ INC (InPL_left _ _))).
   simpl in LC2.
   destruct LC2 as [LC2 | LC2].
   inversion LC2; subst.
   inversion ph; subst.
    destruct NS as [NS | NS]; apply NS; reflexivity.
    exact (H0 (path_end_pl H)).
   apply in_app_iff in LC2.
   destruct LC2 as [LC2 | LC2].
   exact (F z (path_ll_pl ph z (z, e) (InPL_left _ _) LC2) IZ).
   simpl in LC2.
   destruct LC2 as [LC2 | LC2].
   inversion LC2; subst.
   exact (F z (path_end_pl ph) IZ).
   apply (CL (z, e)) in INC; [ | apply in_app_iff; right; apply in_app_iff; right; exact LC2 ].
   apply LIn_In in INC; destruct INC as [INC | INC].
   exact (F e (path_end_pl ph) (LP e _ INC (InPL_right _ _))).
   exact (F e (path_end_pl ph) (LP e _ INC (InPL_left _ _))).
   unfold reverse_linelist in LC1.
   rewrite <- in_rev, map_reverse in LC1.
   destruct l; unfold reverse in LC1; simpl in LC1.
   apply in_app_or in LC2.
   destruct LC2 as [LC2 | LC2].
   apply (CL (p, p0)) in INC; [ | apply in_app_iff; left; exact LC2 ].
   apply (path_ll_pl ph p _ (InPL_right _ _)) in LC1.
   apply LIn_In in INC; destruct INC as [INC | INC].
   exact (F p LC1 (LP p _ INC (InPL_left _ _))).
   exact (F p LC1 (LP p _ INC (InPL_right _ _))).
   simpl in LC2.
   destruct LC2 as [LC2 | LC2].
   inversion LC2; subst.
   exact (F p (path_ll_pl ph p _ (InPL_right _ _) LC1) IY).
   apply in_app_iff in LC2.
   destruct LC2 as [LC2 | LC2].
   clear -ph LC1 LC2.
   induction ph.
    exact LC1.
    simpl in LC1.
    destruct LC1 as [LC1 | LC1].
    inversion LC1; subst.
    simpl in LC2.
    destruct LC2 as [LC2 | LC2].
    inversion LC2; subst.
    exact (H (path_start_pl ph)).
    exact (H (path_ll_pl ph p0 (p, p0) (InPL_right _ _) LC2)).
    simpl in LC2.
    destruct LC2 as [LC2 | LC2].
    inversion LC2; subst.
    exact (H (path_ll_pl ph p _ (InPL_right _ _) LC1)).
    exact (IHph LC1 LC2).
   simpl in LC2.
   destruct LC2 as [LC2 | LC2].
   inversion LC2; subst.
   exact (F p0 (path_ll_pl ph p0 (p0, p) (InPL_left _ _) LC1) IZ).
   apply (CL (p, p0)) in INC; [ | apply in_app_iff; right; apply in_app_iff; right; exact LC2 ].
   apply LIn_In in INC; destruct INC as [INC | INC].
   exact (F p (path_ll_pl ph p (p0, p) (InPL_right _ _) LC1) (LP p _ INC (InPL_left _ _))).
   exact (F p (path_ll_pl ph p (p0, p) (InPL_right _ _) LC1) (LP p _ INC (InPL_right _ _))).
   simpl in LC1.
   destruct LC1 as [LC1 | LC1]; [ subst | ].
   apply set_remove_in in IC2.
   destruct (LIn_In _ _ (CL (s, y) c2 IC2 LC2)) as [H | H].
   exact (F s (path_start_pl ph)
              (LP s  _ H (InPL_left _ _))).
   exact (F s (path_start_pl ph)
              (LP s  _ H (InPL_right _ _))).
   apply in_app_iff in LC1.
   destruct LC1 as [LC1 | LC1].
   destruct (eq_circuit_dec (lxy ++ lyz ++ lzx) c2) as [ROT | NROT].
    apply (DC _ INC).
   apply eq_rin_elem with c2.
    apply Rot_sym; exact ROT.
   apply in_rin; exact IC2.
    apply NROT, Rot_sym.
    assert (c2 = lxy ++ lyz ++ lzx).
    refine (CC l _ _ (set_remove_in _ _ _ _ IC2) INC LC2 _ ).
    apply in_or_app; right; apply in_or_app; left; exact LC1.
    rewrite H; exact (rotSame _).
   simpl in LC1.
   destruct LC1 as [LC1 | LC1]; [ subst | ].
   apply set_remove_in in IC2.
   destruct (LIn_In _ _ (CL (z, e) c2 IC2 LC2)) as [H | H].
   exact (F e (path_end_pl ph)
              (LP e  _ H (InPL_right _ _))).
   exact (F e (path_end_pl ph)
              (LP e  _ H (InPL_left _ _))).
   unfold reverse_linelist in LC1.
   rewrite <- in_rev, map_reverse in LC1.
   apply set_remove_in in IC2.
   unfold reverse in LC1; destruct l; simpl in LC1.
   destruct (LIn_In _ _ (CL (p, p0) c2 IC2 LC2)) as [H | H].
   exact (F p (path_ll_pl ph p (p0, p) (InPL_right _ _) LC1)
              (LP p  _ H (InPL_left _ _))).
   exact (F p (path_ll_pl ph p (p0, p) (InPL_right _ _) LC1)
              (LP p  _ H (InPL_right _ _))).
   destruct IC2 as [IC2 | [IC2 | IC2]];
   [ elimtype False; subst
   | subst; reflexivity
   | elimtype False ].
   apply in_app_iff in LC1.
   destruct LC1 as [LC1 | LC1].
   simpl in LC2.
   destruct LC2 as [LC2 | LC2]; [ subst | ].
   apply (CL (s, y)) in INC; [ | apply in_app_iff; left; exact LC1 ].
   apply LIn_In in INC; destruct INC as [INC | INC].
   exact (F s (path_start_pl ph)
              (LP _  _ INC (InPL_left _ _))).
   exact (F s (path_start_pl ph)
              (LP _  _ INC (InPL_right _ _))).
   apply in_app_iff in LC2.
   destruct LC2 as [LC2 | LC2].
   destruct (CT _ INC) as [[tx [c CT1]] CT2].
   clear -CT1 LC1 LC2.
   destruct lxy.
   exact LC1.
   simpl in LC1.
   destruct LC1 as [LC1 | LC1]; [ subst | ].
   destruct l.
   simpl in CT1.
   inversion CT1; subst.
   apply H7.
   apply lin_or_app; right.
   apply lin_or_app; left.
   exact (in_lin _ _ LC2).
   destruct l0.
   simpl in CT1.
   inversion CT1; subst.
   clear -LC1 LC2 H5.
   revert p0 p pl lyz lzx LC1 LC2 H5.
   induction lxy; intros.
    exact LC1.
    simpl in LC1, H5.
    destruct LC1 as [LC1 | LC1]; [ subst | ].
    inversion H5; subst.
    apply H7.
    apply lin_or_app; right.
    apply lin_or_app; left.
    exact (in_lin _ _ LC2).
    inversion H5; subst.
    exact (IHlxy _ _ _ _ _ LC1 LC2 H1).
   simpl in LC2.
   destruct LC2 as [LC2 | LC2]; [ subst | ].
   apply (CL (z, e)) in INC; [ | apply in_app_iff; left; exact LC1 ].
   apply LIn_In in INC; destruct INC as [INC | INC].
   exact (F e (path_end_pl ph)
              (LP _  _ INC (InPL_right _ _))).
   exact (F e (path_end_pl ph)
              (LP _  _ INC (InPL_left _ _))).
   unfold reverse_linelist in LC2.
   rewrite <- in_rev, map_reverse in LC2.
   unfold reverse in LC2.
   destruct l; simpl in *.
   apply (CL (p, p0)) in INC; [ | apply in_app_iff; left; exact LC1 ].
   apply LIn_In in INC; destruct INC as [INC | INC].
   exact (F p (path_ll_pl ph p _ (InPL_right _ _) LC2)
              (LP _  _ INC (InPL_left _ _))).
   exact (F p (path_ll_pl ph p _ (InPL_right _ _) LC2)
              (LP _ _ INC (InPL_right _ _))).
   simpl in LC1.
   destruct LC1 as [LC1 | LC1]; [ subst | ].
   simpl in LC2.
   destruct LC2 as [LC2 | LC2].
   inversion LC2; subst.
   exact (F y (path_start_pl ph) IY).
   apply in_app_iff in LC2.
   destruct LC2 as [LC2 | LC2].
   apply (CL (y, s)) in INC; [ | apply in_app_iff; right; apply in_app_iff; left; exact LC2 ].
   apply LIn_In in INC; destruct INC as [INC | INC].
   exact (F s (path_start_pl ph)
              (LP _ _ INC (InPL_right _ _))).
   exact (F s (path_start_pl ph)
              (LP _ _ INC (InPL_left _ _))).
   simpl in LC2.
   destruct LC2 as [LC2 | LC2].
   inversion LC2; subst.
   inversion ph; subst.
    destruct NS as [NS | NS]; apply NS; reflexivity.
    exact (not_simplepath ph).
   unfold reverse_linelist in LC2.
   rewrite <- in_rev, map_reverse in LC2.
   unfold reverse in LC2; simpl in LC2.
   exact (F y (path_ll_pl ph y _ (InPL_right _ _) LC2) IY).
   apply in_app_iff in LC1.
   destruct LC1 as [LC1 | LC1].
   simpl in LC2.
   destruct LC2 as [LC2 | LC2]; [ subst | ].
   exact (F y (path_ll_pl ph y _ (InPL_right _ _) LC1) IY).
   apply in_app_iff in LC2.
   destruct LC2 as [LC2 | LC2].
   destruct l.
   apply (CL (p, p0)) in INC; [ | apply in_app_iff; right; apply in_app_iff; left; exact LC2 ].
   apply LIn_In in INC; destruct INC as [INC | INC].
   exact (F p (path_ll_pl ph p (p, p0) (InPL_left _ _) LC1)
              (LP _ _ INC (InPL_left _ _))).
   exact (F p (path_ll_pl ph p (p, p0) (InPL_left _ _) LC1)
              (LP _ _ INC (InPL_right _ _))).
   simpl in LC2.
   destruct LC2 as [LC2 | LC2]; [ subst | ].
   exact (F z (path_ll_pl ph z _ (InPL_left _ _) LC1) IZ).
   unfold reverse_linelist in LC2.
   rewrite <- in_rev, map_reverse in LC2.
   unfold reverse in LC2.
   destruct l; simpl in LC2.
   clear -ph LC1 LC2.
   revert p p0 LC1 LC2.
   induction ph; intros.
    exact LC1.
    simpl in LC1, LC2.
    destruct LC1 as [LC1 | LC1].
    inversion LC1; subst.
    destruct LC2 as [LC2 | LC2].
    inversion LC2; subst.
    exact (H (path_start_pl ph)).
    exact (H (path_ll_pl ph p _ (InPL_right _ _) LC2)).
    destruct LC2 as [LC2 | LC2].
    inversion LC2; subst.
    exact (H (path_ll_pl ph p0 _ (InPL_right _ _) LC1)).
    exact (IHph _ _ LC1 LC2).
   simpl in LC1.
   destruct LC1 as [LC1 | LC1]; [ subst | ].
   simpl in LC2.
   destruct LC2 as [LC2 | LC2].
   inversion LC2; subst.
   inversion ph; subst.
    destruct NS as [NS | NS]; apply NS; reflexivity.
    exact (not_simplepath ph).
   apply in_app_iff in LC2.
   destruct LC2 as [LC2 | LC2].
   apply (CL (e, z)) in INC; [ | apply in_app_iff; right; apply in_app_iff; left; exact LC2 ].
   apply LIn_In in INC; destruct INC as [INC | INC].
   exact (F e (path_end_pl ph)
              (LP _ _ INC (InPL_left _ _))).
   exact (F e (path_end_pl ph)
              (LP _ _ INC (InPL_right _ _))).
   simpl in LC2.
   destruct LC2 as [LC2 | LC2].
   inversion LC2; subst.
   exact (F e (path_end_pl ph) IZ).
   unfold reverse_linelist in LC2.
   rewrite <- in_rev, map_reverse in LC2.
   unfold reverse in LC2; simpl in LC2.
   exact (F z (path_ll_pl ph z _ (InPL_left _ _) LC2) IZ).
   simpl in LC2.
   destruct LC2 as [LC2 | LC2]; [ subst | ].
   apply (CL (s, y)) in INC; [ | apply in_app_iff; right; apply in_app_iff; right; exact LC1 ]. 
   apply LIn_In in INC; destruct INC as [INC | INC].
   exact (F s (path_start_pl ph)
              (LP _ _ INC (InPL_left _ _))).
   exact (F s (path_start_pl ph)
              (LP _ _ INC (InPL_right _ _))).
   apply in_app_iff in LC2.
   destruct LC2 as [LC2 | LC2].
   destruct (CT _ INC) as [[tx [c CT1]] CT2].
   clear -CT1 LC1 LC2.
   destruct lxy.
   destruct lyz; [ exact LC2 | ].
   destruct l0.
   simpl in LC2, CT1.
   destruct LC2 as [LC2 | LC2]; [ subst | ].
   inversion CT1; subst.
   apply H7.
   apply lin_or_app; right.
   exact (in_lin _ _ LC1).
   inversion CT1; subst.
   clear -LC1 LC2 H5.
   revert p0 p pl LC1 LC2 H5.
   induction lyz; intros; simpl in H5.
    exact LC2.
    simpl in LC2.
    destruct LC2 as [LC2 | LC2]; [ subst | ].
    inversion H5; subst.
    apply H7.
    apply lin_or_app; right.
    exact (in_lin _ _ LC1).
    inversion H5; subst.
    exact (IHlyz _ _ _ LC1 LC2 H1).
   destruct l0.
   simpl in CT1.
   inversion CT1; subst.
   clear -LC1 LC2 H5.
   revert p0 p pl H5.
   induction lxy; intros; simpl in H5.
    revert p0 p pl LC1 LC2 H5.
    induction lyz; intros; simpl in H5.
     exact LC2.
     simpl in LC2.
     destruct LC2 as [LC2 | LC2]; [ subst | ].
     inversion H5; subst.
     apply H7.
     apply lin_or_app; right.
     exact (in_lin _ _ LC1).
     inversion H5; subst.
     exact (IHlyz _ _ _ LC1 LC2 H1).
   inversion H5; subst.
   exact (IHlxy _ _ _ H1).
   simpl in LC2.
   destruct LC2 as [LC2 | LC2]; [ subst | ].
   apply (CL (z, e)) in INC; [ | apply in_app_iff; right; apply in_app_iff; right; exact LC1 ].
   apply LIn_In in INC; destruct INC as [INC | INC].
   exact (F e (path_end_pl ph)
              (LP _  _ INC (InPL_right _ _))).
   exact (F e (path_end_pl ph)
              (LP _  _ INC (InPL_left _ _))).
   unfold reverse_linelist in LC2.
   rewrite <- in_rev, map_reverse in LC2.
   unfold reverse in LC2.
   destruct l; simpl in LC2.
   apply (CL (p, p0)) in INC; [ | apply in_app_iff; right; apply in_app_iff; right; exact LC1 ].
   apply LIn_In in INC; destruct INC as [INC | INC].
   exact (F p (path_ll_pl ph p _ (InPL_right _ _) LC2)
              (LP _  _ INC (InPL_left _ _))).
   exact (F p (path_ll_pl ph p _ (InPL_right _ _) LC2)
              (LP _  _ INC (InPL_right _ _))).
   apply in_app_iff in LC1.
   destruct LC1 as [LC1 | LC1].
   destruct (eq_listline_dec (lxy ++ lyz ++ lzx) c2) as [ROT | NROT].
   apply (DC (lxy ++ lyz ++ lzx)); subst.
   exact INC.
   apply in_rin.
   exact IC2.
   apply set_remove_in in IC2.
   refine (NROT (CC l (lxy ++ lyz ++ lzx) c2 INC IC2 _ LC2)).
   apply in_app_iff; left; exact LC1.
   simpl in LC1.
   destruct LC1 as [LC1 | LC1]; [ subst | ].
   apply set_remove_in in IC2.
   apply (CL (y, s)) in IC2; [ | exact LC2].
   apply LIn_In in IC2; destruct IC2 as [IC2 | IC2].
   exact (F s (path_start_pl ph)
              (LP _  _  IC2 (InPL_right _ _))).
   exact (F s (path_start_pl ph)
              (LP _  _  IC2 (InPL_left _ _))).
   apply in_app_iff in LC1.
   destruct LC1 as [LC1 | LC1].
   destruct l.
   apply set_remove_in in IC2.
   apply (CL (p, p0) _ IC2) in LC2.
   apply LIn_In in LC2; destruct LC2 as [LC2 | LC2].
   exact (F p (path_ll_pl ph p _ (InPL_left _ _) LC1)
              (LP _  _ LC2 (InPL_left _ _))).
   exact (F p (path_ll_pl ph p _ (InPL_left _ _) LC1)
              (LP _  _ LC2 (InPL_right _ _))).
   simpl in LC1.
   destruct LC1 as [LC1 | LC1]; [ subst | ].
   apply set_remove_in in IC2.
   apply (CL (e, z) _ IC2) in LC2.
   apply LIn_In in LC2; destruct LC2 as [LC2 | LC2].
   exact (F e (path_end_pl ph)
              (LP _  _  LC2 (InPL_left _ _))).
   exact (F e (path_end_pl ph)
              (LP _  _ LC2 (InPL_right _ _))).
   destruct (eq_listline_dec c2 (lxy ++ lyz ++ lzx)) as [ROT | NROT].
   apply (DC (lxy ++ lyz ++ lzx)); subst.
   exact INC.
   apply in_rin.
   exact IC2.
   apply set_remove_in in IC2.
   refine (NROT (CC l c2 (lxy ++ lyz ++ lzx) IC2 INC LC2 _)).
   apply in_app_iff; right; apply in_app_iff; right; exact LC1.
   destruct IC2 as [IC2 | [IC2 | IC2]]; [ subst; elimtype False | subst; elimtype False | ].
   simpl in LC2.
   destruct LC2 as [LC2 | LC2]; [ subst | ].
   apply set_remove_in in IC1.
   destruct (LIn_In _ _ (CL (s, y) _ IC1 LC1)) as [H | H].
   exact (F s (path_start_pl ph)
              (LP _  _ H (InPL_left _ _))).
   exact (F s (path_start_pl ph)
              (LP _  _ H (InPL_right _ _))).
   apply in_app_iff in LC2.
   destruct LC2 as [LC2 | LC2].
   destruct (eq_listline_dec c1 (lxy ++ lyz ++ lzx)) as [ROT | NROT].
   apply (DC (lxy ++ lyz ++ lzx)); subst.
   exact (set_remove_in _ _ _ _ IC1).
   apply in_rin.
   exact IC1.
   apply set_remove_in in IC1.
   refine (NROT (CC l c1 (lxy ++ lyz ++ lzx) IC1 INC LC1 _)).
   apply in_app_iff; right.
   apply in_app_iff; left; exact LC2.
   apply set_remove_in in IC1.
   simpl in LC2.
   destruct LC2 as [LC2 | LC2]; [ subst | ].
   destruct (LIn_In _ _ (CL (z, e) _ IC1 LC1)) as [H | H].
   exact (F e (path_end_pl ph)
              (LP _  _ H (InPL_right _ _))).
   exact (F e (path_end_pl ph)
              (LP _  _ H (InPL_left _ _))).
   unfold reverse_linelist in LC2.
   rewrite <- in_rev, map_reverse in LC2.
   destruct l; unfold reverse in LC2; simpl in LC2.
   destruct (LIn_In _ _ (CL (p, p0) _ IC1 LC1)) as [H | H].
   exact (F p (path_ll_pl ph p _ (InPL_right _ _) LC2)
              (LP _  _ H (InPL_left _ _))).
   exact (F p (path_ll_pl ph p _ (InPL_right _ _) LC2)
              (LP _  _ H (InPL_right _ _))).
   apply in_app_iff in LC2.
   destruct LC2 as [LC2 | LC2].
   destruct (eq_listline_dec c1 (lxy ++ lyz ++ lzx)) as [ROT | NROT].
   apply (DC (lxy ++ lyz ++ lzx)); subst.
   exact (set_remove_in _ _ _ _ IC1).
   apply in_rin.
   exact IC1.
   apply set_remove_in in IC1.
   refine (NROT (CC l c1 (lxy ++ lyz ++ lzx) IC1 INC LC1 _)).
   apply in_app_iff; left; exact LC2.
   simpl in LC2.
   destruct LC2 as [LC2 | LC2]; [ subst | ].
   apply set_remove_in in IC1.
   destruct (LIn_In _ _ (CL (y, s) _ IC1 LC1)) as [H | H].
   exact (F s (path_start_pl ph)
              (LP _  _ H (InPL_right _ _))).
   exact (F s (path_start_pl ph)
              (LP _  _ H (InPL_left _ _))).
   apply in_app_iff in LC2.
   destruct LC2 as [LC2 | LC2].
   apply set_remove_in in IC1.
   destruct l.
   destruct (LIn_In _ _ (CL (p, p0) _ IC1 LC1)) as [H | H].
   exact (F p (path_ll_pl ph p _ (InPL_left _ _) LC2)
              (LP _  _ H (InPL_left _ _))).
   exact (F p (path_ll_pl ph p _ (InPL_left _ _) LC2)
              (LP _  _ H (InPL_right _ _))).
   simpl in LC2.
   destruct LC2 as [LC2 | LC2]; [ subst | ].
   apply set_remove_in in IC1.
   destruct (LIn_In _ _ (CL _ _ IC1 LC1)) as [H | H].
   exact (F e (path_end_pl ph)
              (LP _  _ H (InPL_left _ _))).
   exact (F e (path_end_pl ph)
              (LP _  _ H (InPL_right _ _))).
   destruct (eq_listline_dec c1 (lxy ++ lyz ++ lzx)) as [ROT | NROT].
   apply (DC (lxy ++ lyz ++ lzx)); subst.
   exact (set_remove_in _ _ _ _ IC1).
   apply in_rin.
   exact IC1.
   apply set_remove_in in IC1.
   refine (NROT (CC l c1 (lxy ++ lyz ++ lzx) IC1 INC LC1 _)).
   apply in_app_iff; right; apply in_app_iff; right; exact LC2.
   unfold Circuitconsistency in CC.
   exact (CC l c1 c2 (set_remove_in _ _ _ _ IC1) (set_remove_in _ _ _ _ IC2) LC1 LC2).
  split.
   (* Areaconsistency *)
   unfold Areaconsistency.
   intros c a1 a2 IA1 IA2 CA1 CA2.
   simpl in IA1, IA2.
   destruct IA1 as [IA1 | IA1]; [ subst | ].
   simpl in CA1.
   destruct CA1 as [CA1 | CA1]; [subst | elim CA1].
   destruct IA2 as [IA2 | IA2];
   [ subst; exact (eq_refl _)
   | elimtype False ].
   apply (AC _ _ IA2) in CA2.
   apply (F s (path_start_pl ph)).
   exact (CPconsistency s (s, y) _ LP CL CA2 (in_eq _ _) (InPL_left _ _)).
   destruct IA2 as [IA2 | IA2]; subst.
   elimtype False; simpl in CA2; destruct CA2 as [CA2 | CA2]; [subst; simpl in CA1 | exact CA2].
   apply (F s (path_start_pl ph)).
   apply (AC _ _ IA1) in CA1.
   exact (CPconsistency _ _ _ LP CL CA1 (in_eq _ _) (InPL_left _ _)).
   exact (AA _ _ _ IA1 IA2 CA1 CA2).
  split.
   (* DistinctP *)
   unfold DistinctP.
   intros p Ip D.
   clear -ph D DP F.
   revert s e al ph.
   induction ap; intros; simpl in D.
    exact (DP p (set_remove_in _ _ _ _ D) D).
    destruct (eq_point_dec p a); [subst | ].
     apply in_app_iff in D.
     destruct D as [D | D].
     inversion ph; subst.
     exact D.
     exact (H5 D).
     exact (F a (in_eq _ _) D).
     simpl in D.
     destruct D as [D | D]; [ subst; exact (n (eq_refl _)) | ].
      inversion ph; subst.
      exact (DP p (set_remove_in _ _ _ _ D) D).
      apply IHap with(s:=p2)(e:=e)(al:=ll); [ | exact D | exact H3 ].
      intros p0 INP DIP.
      exact (F p0 (or_intror _ INP) DIP).
  split.
   (* Distinct L *)
   unfold DistinctL.
   intros l Il D.
   simpl in D.
   destruct (eq_dline_dec l (y, s)) as [EQ1 | NEQ1].
   subst; simpl in D.
   destruct D as [D | D].
   inversion D; subst.
    exact (F _ (path_end_pl ph) IY).
    cbv in H; inversion H; subst.
     inversion ph; subst.
      destruct NS as [NS | NS]; apply NS; reflexivity.
      exact (not_simplepath ph).
   apply lin_app_or in D.
    destruct D as [D | D].
     destruct (LIn_In _ _ D) as [LIN1 | LIN2].
      exact (F y (path_ll_pl ph y _ (InPL_left _ _) LIN1) IY).
      exact (F y (path_ll_pl ph y _ (InPL_right _ _) LIN2) IY).
     apply LIn_In in D.
      destruct D as [D | D].
       exact (F s (path_start_pl ph)
                  (LP _ _ D (InPL_right _ _))).
       exact (F s (path_start_pl ph)
                  (LP _ _ D (InPL_left _ _))).
   destruct Il as [Il | Il]; [symmetry in Il; exact (NEQ1 Il) | ].
   destruct D as [D | D].
   inversion D; subst.
   apply NEQ1; reflexivity.
   destruct l; inversion H; subst; clear H D.
   destruct Il as [Il | Il].
   inversion Il; subst; clear Il.
   inversion ph; subst.
    destruct NS as [NS | NS]; apply NS; reflexivity.
    exact (not_simplepath ph).
   apply in_app_or in Il; destruct Il as [Il | Il].
   exact (F _ (path_ll_pl ph y _ (InPL_right _ _) Il) IY).
   exact (F _ (path_start_pl ph) (LP _ _ Il (InPL_left _ _))).
   destruct (eq_dline_dec l (e, z)) as [EQ2 | NEQ2].
   subst.
   apply lin_app_or in D.
   destruct D as [D | D].
    destruct (LIn_In _ _ D) as [LIN1 | LIN2].
     exact (F z (path_ll_pl ph z _ (InPL_right _ _) LIN1) IZ).
     exact (F z (path_ll_pl ph z _ (InPL_left _ _) LIN2) IZ).
    apply LIn_In in D; destruct D as [D | D].
     exact (F e (path_end_pl ph)
                (LP _ _ D (InPL_left _ _))).
     exact (F e (path_end_pl ph)
                (LP _ _ D (InPL_right _ _))).
   destruct Il as [Il | Il]; [symmetry in Il; exact (NEQ2 Il) | ].
   simpl in D; destruct D as [D | D].
   inversion D; subst.
   apply NEQ2; reflexivity.
   destruct l; inversion H; clear H; subst.
   apply in_app_or in Il.
   destruct Il as [Il | Il].
    exact (F z (path_ll_pl ph z _ (InPL_left _ _) Il) IZ).
    exact (F e (path_end_pl ph)
               (LP _ _ Il (InPL_right _ _))).
   cut (~LIn l (set_remove eq_dline_dec l al)); [ intros AL | ].
   cut (forall(l : Line), In l al -> ~LIn l L); [ intros FL | ].
   clear -D Il AL FL DL.
   induction al; simpl in *.
    exact (DL l Il D).
    destruct (eq_dline_dec l a) as [EQ | NEQ].
    apply lin_app_or in D.
    destruct D as [LIN | LIN].
    exact (AL LIN).
    inversion EQ; subst.
    exact (FL _ (or_introl _ (eq_refl _)) LIN).
    simpl in D, AL.
    destruct Il as [Il | Il]; [ symmetry in Il; exact (NEQ Il) | ].
    destruct D as [D | D]; [ subst; exact (AL (or_introl D)) | ].
    apply (IHal Il D).
    exact (fun D => AL (or_intror _ D)).
    exact (fun l0 LAL D => FL l0 (or_intror _ LAL) D).
   intros l0 LAL INL.
   destruct l0.
   destruct (LIn_In _ _ INL) as [LIN | LIN].
   exact (F p (path_ll_pl ph p (p, p0) (InPL_left _ _) LAL) (LP p _ LIN (InPL_left _ _))).
   exact (F p (path_ll_pl ph p (p, p0) (InPL_left _ _) LAL) (LP p _ LIN (InPL_right _ _))).
   clear -F LP Il ph.
   intro D.
   induction ph; simpl in D; [ exact D | ].
   destruct (eq_dline_dec l (p3, p2)) as [EQ | NEQ].
   subst.
   destruct (LIn_In _ _ D) as [D1 | D2].
    exact (H (path_ll_pl ph p3 _ (InPL_left _ _) D1)).
    exact (H (path_ll_pl ph p3 _ (InPL_right _ _) D2)).
   simpl in D.
   destruct Il as [Il | Il]; [ symmetry in Il; exact (NEQ Il) | ].
   assert (forall p : Point, In p pl -> ~ In p P) as F'.
   intros p0 H0; apply F.
   right; exact H0.
   destruct D as [D | D]; [ clear IHph | exact (IHph F' Il D) ].
   inversion D; clear D; subst.
    apply NEQ; reflexivity.
    destruct l; inversion H0; clear H0; subst.
    apply in_app_or in Il; destruct Il as [Il | Il].
    exact (H (path_ll_pl ph p3 _ (InPL_right _ _) Il)).
    apply (F p3).
     now apply in_eq.
     exact (LP _ _ Il (InPL_right _ _)).
  split.
   (* DistinctC *)
   unfold DistinctC.
   remember (((s, y) :: lyz) ++ (z, e) :: reverse_linelist al) as c1.
   remember (lxy ++ ((y, s) :: al) ++ (e, z) :: lzx) as c2.
   intros c Ic D.
   simpl in D.
   destruct (eq_listline_dec c c1) as [EQ1 | NEQ1].
   destruct D as [D | D].
   subst.
   assert(In (s, y) (lxy ++ ((y, s) :: al) ++ (e, z) :: lzx)).
    exact (RotIn _ _ _ (in_eq _ _) D).
   apply in_app_iff in H.
   destruct H as [D1 | D1].
   apply (CL (s, y)) in INC.
   apply LIn_In in INC; destruct INC as [INC | INC].
   exact (F s (path_start_pl ph)
              (LP _ _ INC (InPL_left _ _))).
   exact (F s (path_start_pl ph)
              (LP _ _ INC (InPL_right _ _))).
   apply in_app_iff; left.
   exact D1.
   simpl in D1.
   destruct D1 as [D1 | D1].
   inversion D1; subst.
   exact (F s (path_start_pl ph) IY).
   apply in_app_iff in D1.
   destruct D1 as [D1 | D1].
   exact (F y (path_ll_pl ph y _ (InPL_right _ _) D1) IY).
   simpl in D1.
   destruct D1; subst.
   inversion H; subst.
    inversion ph; subst.
     destruct NS as [NS | NS]; apply NS; reflexivity.
     exact (not_simplepath ph).
   apply (CL (s, y)) in INC.
   apply LIn_In in INC; destruct INC as [INC | INC].
   exact (F s (path_start_pl ph)
              (LP _ _ INC (InPL_left _ _))).
   exact (F s (path_start_pl ph)
              (LP _ _ INC (InPL_right _ _))).
   apply in_app_iff; right.
   apply in_app_iff; right.
   exact H.
   apply set_remove_in_r in D.
   subst.
   apply (F s (path_start_pl ph)).
   apply EqIn_In in D.
   destruct D as [b [D1  D2]].
   refine (CPconsistency s (s, y) b LP CL D2 _ (InPL_left _ _)).
   exact (RotIn _ _ _ (in_eq _ _) D1).
   destruct Ic as [Ic | Ic]; [ symmetry in Ic; exact (NEQ1 Ic) | ].
   destruct D as [D | D].
   destruct Ic as [Ic | Ic].
   subst. subst.
   assert (In (s, y) (lxy ++ lyz ++ lzx) \/ s = y \/ (y = z /\ s = e) \/ In (s, y) al).
   apply Rot_sym in D.
   apply (RotIn _ _ _ (in_eq _ _)) in D.
   apply in_app_or in D; destruct D as [D | [D | D]].
   left; apply in_or_app; left; exact D.
   inversion D; subst; right; left; reflexivity.
   apply in_app_or in D; destruct D as [D | [D | D]].
   right; right; right; exact D.
   inversion D; right; right; left; subst; split; reflexivity.
   left; apply in_or_app; right; apply in_or_app; right; exact D.
   assert (Is := path_start_pl ph).
   destruct H as [H | [H | [[Hy Hs] | H]]].
   exact (F _ Is (CPconsistency _ _ _ LP CL INC H (InPL_left _ _))).
   subst; exact (F _ Is IY).
   subst; inversion ph; subst.
    destruct NS as [NS | NS]; apply NS; reflexivity.
    exact (not_simplepath ph).
   exact (F _ (path_ll_pl ph _ _ (InPL_right _ _) H) IY).
   apply set_remove_in in Ic.
   subst.
   assert (Is := path_start_pl ph).
   apply Rot_sym in D; apply (RotIn _ _ _ (in_eq _ _)) in D.
   exact (F _ Is (CPconsistency _ _ _ LP CL Ic D (InPL_left _ _))).
   destruct (eq_listline_dec c c2) as [EQ2 | NEQ2].
   apply set_remove_in_r in D.
   subst.
   apply EqIn_In in D.
   destruct D as [c' [D1 D2]].
   apply (F _ (path_start_pl ph)).
   apply (CPconsistency _ (y, s) _ LP CL D2).
   refine (RotIn _ _ _ _ D1).
   apply in_or_app; right; left; reflexivity.
   now apply InPL_right.
   simpl in Ic; destruct Ic as [Ic | Ic]; [ symmetry in Ic; exact (NEQ2 Ic) |].
   simpl in D; destruct D as [D | D].
   apply (F _ (path_start_pl ph)).
   refine (CPconsistency _ _ c LP CL _ _ (InPL_right y s)).
   exact (set_remove_in _ _ _ _ Ic).
   rewrite Heqc2 in D; refine (RotIn_r _ _ _ _ D).
   apply in_or_app; right; left; reflexivity.
   apply (DC c).
   exact (set_remove_in _ _ _ _ Ic).
   clear -D.
   induction C; simpl in *.
    elim D.
    destruct (eq_listline_dec c a) as [EQ1 | NEQ1]; subst.
    destruct (eq_listline_dec (lxy ++ lyz ++ lzx) a) as [ROT | NROT].
    apply rin_in in D.
    destruct D as [ec [CRT ICC]].
    apply set_remove_in in ICC.
    apply (eq_rin_elem _ _ _ (Rot_sym _ _ CRT)).
    exact (in_rin _ _ ICC).
    simpl in D.
    destruct (eq_listline_dec a a) as [EQ1 | EQ1]; subst.
     exact (set_remove_in_r _ _ _ D).
     elim (EQ1 (eq_refl _)).
    destruct (eq_listline_dec (lxy ++ lyz ++ lzx) a) as [ROT | NROT].
     right; exact D.
     simpl in D.
    destruct (eq_listline_dec c a) as [EQ2 | NEQ2]; subst.
     elim (NEQ1 (eq_refl _)).
     simpl in D.
    destruct D as [D | D].
     left; exact D.
     right; exact (IHC D).
  (* DistinctA *)
   unfold DistinctA.
   intros a0 Ia D.
   remember ((((s, y) :: lyz) ++ (z, e) :: reverse_linelist al) :: nil) as a2 in D.
   simpl in D.
   destruct (eq_listcircuit_dec a0 a2) as [EQ1 | NEQ1].
   subst.
   apply ain_in in D.
   destruct D as [a' [D1 D2]].
   apply (eq_area_rin _ _ _ (in_rin _ _ (in_eq _ _))) in D1.
   apply rin_in in D1.
   destruct D1 as [c' [D3 D4]].
   apply (AC c' _ D2) in D4.
   apply (F s (path_start_pl ph)).
   refine (CPconsistency s (s, y) _ LP CL D4 _ (InPL_left _ _)).
   exact (RotIn _ _ _ (in_eq _ _) D3).
   simpl in D; destruct D as [D | D].
   destruct Ia as [Ia | Ia]; [ subst; apply NEQ1; reflexivity | ].
   subst; apply eq_area_sym in D; apply (eq_area_rin _ _ _ (in_rin _ _ (in_eq _ _))) in D.
   apply rin_in in D.
   destruct D as [c' [D1 D2]].
   apply (RotIn _ _ _ (in_eq _ _)) in D1.
   apply (AC c' _ Ia) in D2.
   apply (F s (path_start_pl ph)).
   exact (CPconsistency s (s, y) _ LP CL D2 D1 (InPL_left _ _)).
   destruct Ia as [Ia | Ia]; subst; [ apply NEQ1; reflexivity | ].
   apply (DA _ Ia).
   exact D.
 (* PLCAconstraints *)
 unfold PLCAconstraints.
 split.
  (* Lconstraints *)
  unfold Lconstraint, Line_constraint.
   intros l LIN D.
   simpl in LIN.
   destruct LIN as [LIN | [LIN | LIN]].
   destruct l as [lx ly]; simpl in D; subst.
   inversion LIN; subst.
   exact (F ly (path_start_pl ph) IY).
   destruct l as [lx ly]; simpl in D; subst.
   inversion LIN; subst.
   exact (F ly (path_end_pl ph) IZ).
   apply in_app_or in LIN.
   destruct LIN as [LIN | LIN].
   exact (((simplepathline_Lconstraint ph) _ LIN) D).
   exact ((LT _ LIN) D).
 split.
  (* Cconstraint *)
  unfold Cconstraint, Circuit_constraints.
   intros c ICC.
   unfold Circuit_constraints.
   simpl in ICC.
   destruct ICC as [ICC | [ICC | ICC]]; [ subst; simpl | subst | ].
   split; [ exists s; exists (s::pyz ++ (rev ap)) | ].
   apply step_trail.
   apply (app_trail tl2 (simplepath_onlyif_trail (rev_path ph))).
   intros l IYZ D.
   destruct l as [lx ly].
   unfold reverse_linelist in D.
   destruct (LIn_In _ _ IYZ) as [CYZ | CYZ].
    apply (CPconsistency lx (lx, ly) _ LP CL) in INC.
    refine (F lx  _ INC).
    destruct (LIn_In _ _ D) as [D1 | D1].
     rewrite <- in_rev, map_reverse in D1.
     exact (path_ll_pl ph lx _ (InPL_right _ _) D1).
     rewrite <- in_rev, map_reverse, reverse_reverse in D1.
     exact (path_ll_pl ph lx _ (InPL_left _ _) D1).
    apply in_app_iff; right.
    apply in_app_iff; left; exact CYZ.
    exact (InPL_left _ _).
    apply (CPconsistency lx (ly, lx) _ LP CL) in INC.
    refine (F lx  _ INC).
    destruct (LIn_In _ _ D) as [D1 | D1].
     rewrite <- in_rev, map_reverse in D1.
     exact (path_ll_pl ph lx _ (InPL_right _ _) D1).
     rewrite <- in_rev, map_reverse, reverse_reverse in D1.
     exact (path_ll_pl ph lx _ (InPL_left _ _) D1).
    apply in_app_iff; right.
    apply in_app_iff; left; exact CYZ.
    exact (InPL_right _ _).
   intro D; subst.
   exact (F e (path_end_pl ph) IZ).
   intro D.
   apply (F e (path_end_pl ph)).
   destruct (LIn_In _ _ D) as [D1 | D1].
    apply (CL (z, e)) in INC.
    apply LIn_In in INC; destruct INC as [INC | INC].
    exact (LP e _ INC (InPL_right _ _)).
    exact (LP e _ INC (InPL_left _ _)).
    apply in_app_iff; right.
    apply in_app_iff; left; exact D1.
    apply (CL (e, z)) in INC.
    apply LIn_In in INC; destruct INC as [INC | INC].
    exact (LP e _ INC (InPL_left _ _)).
    exact (LP e _ INC (InPL_right _ _)).
    apply in_app_iff; right.
    apply in_app_iff; left; exact D1.
   intro D.
   unfold reverse_linelist in D.
   refine (F z _ IZ).
   destruct (LIn_In _ _ D) as [D1 | D1].
    rewrite <- in_rev, map_reverse in D1.
    exact (path_ll_pl ph z _ (InPL_right _ _) D1).
    rewrite <- in_rev, map_reverse, reverse_reverse in D1.
    exact (path_ll_pl ph z _ (InPL_left _ _) D1).
   intro D; subst.
   exact (F y (path_start_pl ph) IY).
   intro D.
   apply lin_app_or in D.
   destruct D as [D | D].
   apply (F s (path_start_pl ph)).
   destruct (LIn_In _ _ D) as [D1 | D1].
    apply (CL (s, y)) in INC.
    apply LIn_In in INC; destruct INC as [INC | INC].
    exact (LP s _ INC (InPL_left _ _)).
    exact (LP s _ INC (InPL_right _ _)).
    apply in_app_iff; right.
    apply in_app_iff; left; exact D1.
    apply (CL (y, s)) in INC.
    apply LIn_In in INC; destruct INC as [INC | INC].
    exact (LP s _ INC (InPL_right _ _)).
    exact (LP s _ INC (InPL_left _ _)).
    apply in_app_iff; right.
    apply in_app_iff; left; exact D1.
   simpl in D.
   destruct D as [D | D].
   inversion D; subst.
    exact (F z (path_start_pl ph) IZ).
    inversion H; subst.
    inversion ph; subst.
     destruct NS as [NS | NS]; apply NS; reflexivity.
     exact (not_simplepath ph).
   unfold reverse_linelist in D.
   refine (F y _ IY).
   destruct (LIn_In _ _ D) as [D1 | D1].
    rewrite <- in_rev, map_reverse in D1.
    exact (path_ll_pl ph y _ (InPL_left _ _) D1).
    rewrite <- in_rev, map_reverse, reverse_reverse in D1.
    exact (path_ll_pl ph y _ (InPL_right _ _) D1).
   unfold reverse_linelist.
   rewrite app_length; simpl.
   rewrite rev_length, map_length.
   rewrite plus_comm; simpl.
   apply le_n_S, le_n_S; rewrite app_length in lg1; exact lg1.
   split; [ exists x; exists (pxy ++ ap ++ pzx) | ].
   apply (app_trail tl1).
   apply (app_trail (simplepath_onlyif_trail ph) tl3).
   intros l LIN LZX.
   destruct l as [lx ly].
   destruct (LIn_In _ _ LZX) as [LZX1 | LZX2].
    apply (CPconsistency lx (lx, ly) _ LP CL) in INC.
    refine (F lx _ INC).
    destruct (LIn_In _ _ LIN) as [LIN1 | LIN2].
     exact (path_ll_pl ph lx _ (InPL_left _ _) LIN1).
     exact (path_ll_pl ph lx _ (InPL_right _ _) LIN2). 
    apply in_app_iff; right.
    apply in_app_iff; right; exact LZX1.
    exact (InPL_left _ _).
    apply (CPconsistency lx (ly, lx) _ LP CL) in INC.
    refine (F lx _ INC).
    destruct (LIn_In _ _ LIN) as [LIN1 | LIN2].
     exact (path_ll_pl ph lx _ (InPL_left _ _) LIN1).
     exact (path_ll_pl ph lx _ (InPL_right _ _) LIN2). 
    apply in_app_iff; right.
    apply in_app_iff; right; exact LZX2.
    exact (InPL_right _ _).
   intros D; subst.
   exact (F z (path_end_pl ph) IZ).
   intros D.
   refine (F z _ IZ).
   destruct (LIn_In _ _ D) as [D1 | D1].
    exact (path_ll_pl ph z _ (InPL_right _ _) D1).
    exact (path_ll_pl ph z _ (InPL_left _ _) D1).
   intros D.
   apply (F e (path_end_pl ph)).
   destruct (LIn_In _ _ D) as [D1 | D1].
    refine (CPconsistency e (e, z) _ LP CL INC _ (InPL_left _ _)).
    apply in_app_iff; right.
    apply in_app_iff; right; exact D1.
    refine (CPconsistency e (z, e) _ LP CL INC _ (InPL_right _ _)).
    apply in_app_iff; right.
    apply in_app_iff; right; exact D1.
   intros l LXY LIN.
   apply lin_app_or in LIN.
   destruct LIN as [LIN | LIN].
   destruct l as [lx ly].
   destruct (LIn_In _ _ LXY) as [LXY1 | LXY1].
    apply (CPconsistency lx (lx, ly) _ LP CL) in INC.
    refine (F lx _ INC).
    destruct (LIn_In _ _ LIN) as [LIN1 | LIN1].
     exact (path_ll_pl ph lx _ (InPL_left _ _) LIN1).
     exact (path_ll_pl ph lx _ (InPL_right _ _) LIN1).
    apply in_app_iff; left; exact LXY1.
    exact (InPL_left _ _).
    apply (CPconsistency lx (ly, lx) _ LP CL) in INC.
    refine (F lx _ INC).
    destruct (LIn_In _ _ LIN) as [LIN1 | LIN1].
     exact (path_ll_pl ph lx _ (InPL_left _ _) LIN1).
     exact (path_ll_pl ph lx _ (InPL_right _ _) LIN1).
    apply in_app_iff; left; exact LXY1.
    exact (InPL_right _ _).
   simpl in LIN.
   destruct LIN as [LIN | LIN].
   inversion LIN; subst.
   apply (F e (path_end_pl ph)).
   destruct (LIn_In _ _ LXY) as [LXY1 | LXY1].
    refine (CPconsistency e (e, z) _ LP CL INC _ (InPL_left _ _ )).
    apply in_app_iff; left; exact LXY1.
    refine (CPconsistency e (z, e) _ LP CL INC _ (InPL_right _ _ )).
    apply in_app_iff; left; exact LXY1.
   apply f_equal with(f:=reverse) in H.
   rewrite reverse_reverse in H.
   subst.
   apply (F e (path_end_pl ph)).
   destruct (LIn_In _ _ LXY) as [LXY1 | LXY1].
    refine (CPconsistency e (z, e) _ LP CL INC _ (InPL_right _ _ )).
    apply in_app_iff; left; exact LXY1.
    refine (CPconsistency e (e, z) _ LP CL INC _ (InPL_left _ _ )).
    apply in_app_iff; left; exact LXY1.
   apply CT in INC.
   destruct INC as [[ex [epl INC1]] INC2].
   clear -INC1 LXY LIN.
   cut (exists e : Point, trail e ex epl (lxy ++ lyz ++ lzx)); intros.
   destruct H as [e TL].
   clear INC1.
   revert e epl TL.
   induction lxy; intros; simpl in *.
    exact LXY.
    destruct LXY as [LXY | LXY].
     inversion TL; subst.
     apply H6.
     apply (eq_lin_elem _ _ _ LXY).
     apply lin_or_app; right.
     apply lin_or_app; right; exact LIN.
    inversion TL; subst.
    exact (IHlxy LXY _ _ H1).
   exists ex; exact INC1.
   intros D; subst.
   exact (F s (path_start_pl ph) IY).
   intros D.
   apply (F s (path_start_pl ph)).
   destruct (LIn_In _ _ D) as [D1 | D1].
    refine (CPconsistency s (y, s) _ LP CL INC _ (InPL_right _ _)).
    apply in_app_iff; left; exact D1.
    refine (CPconsistency s (s, y) _ LP CL INC _ (InPL_left _ _)).
    apply in_app_iff; left; exact D1.
   intros D.
   apply lin_app_or in D.
   destruct D as [D | D].
    refine (F y _ IY).
    destruct (LIn_In _ _ D) as [D1 | D1].
     exact (path_ll_pl ph y (y, s) (InPL_left _ _) D1).
     exact (path_ll_pl ph y (s, y) (InPL_right _ _) D1).
   simpl in D.
   destruct D as [D | D].
   inversion D; subst.
   exact (F z (path_start_pl ph) IZ).
   inversion H; subst.
   inversion ph; subst.
    destruct NS as [NS | NS]; apply NS; reflexivity.
    exact (not_simplepath ph).
   apply (F s (path_start_pl ph)).
   destruct (LIn_In _ _ D) as [D1 | D1].
    refine (CPconsistency s (y, s) _ LP CL INC _ (InPL_right _ _)).
    apply in_app_iff; right.
    apply in_app_iff; right; exact D1.
    refine (CPconsistency s (s, y) _ LP CL INC _ (InPL_left _ _)).
    apply in_app_iff; right.
    apply in_app_iff; right; exact D1.
   do 2 (rewrite app_length; simpl).
   rewrite plus_comm; simpl.
   apply le_n_S; rewrite <- plus_assoc.
   apply le_trans with (S (length lzx) + length lxy).
    apply le_n_S.
    rewrite plus_comm; rewrite <- app_length; exact lg2.
    now apply le_plus_r.
   exact (CT c (set_remove_in _ _ _ _ ICC)).
 split.
  (* Aconstraint *)
  unfold Aconstraint.
   remember (((s, y) :: lyz) ++ (z, e) :: reverse_linelist al) as c1.
   remember (lxy ++ ((y, s) :: al) ++ (e, z) :: lzx) as c2.
   intros a IAA.
   unfold Area_constraints.
   simpl in IAA.
   destruct IAA as [IAA | IAA].
    split; [ subst; intros c1 c2 IC1 IC2 NC | ].
    simpl in IC1, IC2.
    cut(~point_connect c1 c2); intros PC.
    split; [ exact PC | ].
    intros D; apply PC.
    exact (lc_only_if_pc _ _ D).
    destruct IC1 as [IC1 | IC1]; [ subst | exact IC1 ].
    destruct IC2 as [IC2 | IC2]; [ subst; exact (NC (eq_refl _)) | exact IC2 ].
    split; subst a; simpl; [ intros c Ic D | exact (le_n _) ].
    destruct Ic as [Ic | Ic]; [ | exact Ic].
    destruct (eq_listline_dec c c1) as [ROT | NROT]; [ exact D | symmetry in Ic; exact (NROT Ic)].
    apply AT.
    exact IAA.
  (* Outermost_constraints *)
  unfold Circuit_constraints.
   split; [ exists x; exists (pxy ++ ap ++ pzx) | ].
   apply (app_trail tl1).
   apply (app_trail (simplepath_onlyif_trail ph) tl3).
   intros l LIN LZX.
   destruct l as [lx ly].
   destruct (LIn_In _ _ LZX) as [LZX1 | LZX2].
    apply (CPconsistency lx (lx, ly) _ LP CL) in INC.
    refine (F lx _ INC).
    destruct (LIn_In _ _ LIN) as [LIN1 | LIN2].
     exact (path_ll_pl ph lx _ (InPL_left _ _) LIN1).
     exact (path_ll_pl ph lx _ (InPL_right _ _) LIN2). 
    apply in_app_iff; right.
    apply in_app_iff; right; exact LZX1.
    exact (InPL_left _ _).
    apply (CPconsistency lx (ly, lx) _ LP CL) in INC.
    refine (F lx _ INC).
    destruct (LIn_In _ _ LIN) as [LIN1 | LIN2].
     exact (path_ll_pl ph lx _ (InPL_left _ _) LIN1).
     exact (path_ll_pl ph lx _ (InPL_right _ _) LIN2). 
    apply in_app_iff; right.
    apply in_app_iff; right; exact LZX2.
    exact (InPL_right _ _).
   intros D; subst.
   exact (F z (path_end_pl ph) IZ).
   intros D.
   refine (F z _ IZ).
   destruct (LIn_In _ _ D) as [D1 | D1].
    exact (path_ll_pl ph z _ (InPL_right _ _) D1).
    exact (path_ll_pl ph z _ (InPL_left _ _) D1).
   intros D.
   apply (F e (path_end_pl ph)).
   destruct (LIn_In _ _ D) as [D1 | D1].
    refine (CPconsistency e (e, z) _ LP CL INC _ (InPL_left _ _)).
    apply in_app_iff; right.
    apply in_app_iff; right; exact D1.
    refine (CPconsistency e (z, e) _ LP CL INC _ (InPL_right _ _)).
    apply in_app_iff; right.
    apply in_app_iff; right; exact D1.
   intros l LXY LIN.
   apply lin_app_or in LIN.
   destruct LIN as [LIN | LIN].
   destruct l as [lx ly].
   destruct (LIn_In _ _ LXY) as [LXY1 | LXY1].
    apply (CPconsistency lx (lx, ly) _ LP CL) in INC.
    refine (F lx _ INC).
    destruct (LIn_In _ _ LIN) as [LIN1 | LIN1].
     exact (path_ll_pl ph lx _ (InPL_left _ _) LIN1).
     exact (path_ll_pl ph lx _ (InPL_right _ _) LIN1).
    apply in_app_iff; left; exact LXY1.
    exact (InPL_left _ _).
    apply (CPconsistency lx (ly, lx) _ LP CL) in INC.
    refine (F lx _ INC).
    destruct (LIn_In _ _ LIN) as [LIN1 | LIN1].
     exact (path_ll_pl ph lx _ (InPL_left _ _) LIN1).
     exact (path_ll_pl ph lx _ (InPL_right _ _) LIN1).
    apply in_app_iff; left; exact LXY1.
    exact (InPL_right _ _).
   simpl in LIN.
   destruct LIN as [LIN | LIN].
   inversion LIN; subst.
   apply (F e (path_end_pl ph)).
   destruct (LIn_In _ _ LXY) as [LXY1 | LXY1].
    refine (CPconsistency e (e, z) _ LP CL INC _ (InPL_left _ _ )).
    apply in_app_iff; left; exact LXY1.
    refine (CPconsistency e (z, e) _ LP CL INC _ (InPL_right _ _ )).
    apply in_app_iff; left; exact LXY1.
   apply f_equal with(f:=reverse) in H.
   rewrite reverse_reverse in H.
   subst.
   apply (F e (path_end_pl ph)).
   destruct (LIn_In _ _ LXY) as [LXY1 | LXY1].
    refine (CPconsistency e (z, e) _ LP CL INC _ (InPL_right _ _ )).
    apply in_app_iff; left; exact LXY1.
    refine (CPconsistency e (e, z) _ LP CL INC _ (InPL_left _ _ )).
    apply in_app_iff; left; exact LXY1.
   apply CT in INC.
   destruct INC as [[ex [epl INC1]] INC2].
   clear -INC1 LXY LIN.
   cut (exists e : Point, trail e ex epl (lxy ++ lyz ++ lzx)); intros.
   destruct H as [e TL].
   clear INC1.
   revert e epl TL.
   induction lxy; intros; simpl in *.
    exact LXY.
    destruct LXY as [LXY | LXY].
     inversion TL; subst.
     apply H6.
     apply (eq_lin_elem _ _ _ LXY).
     apply lin_or_app; right.
     apply lin_or_app; right; exact LIN.
    inversion TL; subst.
    exact (IHlxy LXY _ _ H1).
   exists ex; exact INC1.
   intros D; subst.
   exact (F s (path_start_pl ph) IY).
   intros D.
   apply (F s (path_start_pl ph)).
   destruct (LIn_In _ _ D) as [D1 | D1].
    refine (CPconsistency s (y, s) _ LP CL INC _ (InPL_right _ _)).
    apply in_app_iff; left; exact D1.
    refine (CPconsistency s (s, y) _ LP CL INC _ (InPL_left _ _)).
    apply in_app_iff; left; exact D1.
   intros D.
   apply lin_app_or in D.
   destruct D as [D | D].
    refine (F y _ IY).
    destruct (LIn_In _ _ D) as [D1 | D1].
     exact (path_ll_pl ph y (y, s) (InPL_left _ _) D1).
     exact (path_ll_pl ph y (s, y) (InPL_right _ _) D1).
   simpl in D.
   destruct D as [D | D].
   inversion D; subst.
   exact (F z (path_start_pl ph) IZ).
   inversion H; subst.
   inversion ph; subst.
    destruct NS as [NS | NS]; apply NS; reflexivity.
    exact (not_simplepath ph).
   apply (F s (path_start_pl ph)).
   destruct (LIn_In _ _ D) as [D1 | D1].
    refine (CPconsistency s (y, s) _ LP CL INC _ (InPL_right _ _)).
    apply in_app_iff; right.
    apply in_app_iff; right; exact D1.
    refine (CPconsistency s (s, y) _ LP CL INC _ (InPL_left _ _)).
    apply in_app_iff; right.
    apply in_app_iff; right; exact D1.
   do 2 (rewrite app_length; simpl).
   rewrite plus_comm; simpl.
   apply le_n_S; rewrite <- plus_assoc.
   apply le_trans with (S (length lzx) + length lxy).
    apply le_n_S.
    rewrite plus_comm; rewrite <- app_length; exact lg2.
    now apply le_plus_r.
 (* add_loop *)
+destruct IND as [CSS CST].
 destruct CSS as [PL [LP [LC [CL [CA [AC [O1 [O2 [CC [AA [DP [DL [DC DA]]]]]]]]]]]]].
 destruct CST as [LT [CT [AT OT]]].
 split.
  (* PLCAconsisntency *)
  unfold PLCAconsistency.
  split.
   (* PLconsistency *)
   unfold PLconsistency.
   intros p INP.
   apply in_app_iff in INP.
   destruct INP as [INP | INP].
   apply (path_pl_ll ph) in INP; [ | omega ].
   destruct INP as [l [INP1 INP2]].
   exists l; split; [ right; apply in_or_app; left; exact INP2 | exact INP1 ].
   apply (PL p) in INP.
   destruct INP as [l [INP2 INP3]].
   exists l; split; [ right; apply in_or_app; right; exact INP2 | exact INP3 ].
  split.
   (* LPconsistency *)
   unfold LPconsistency.
   intros p l LIN IPL.
   simpl in LIN.
   destruct LIN as [LIN | LIN]; [ subst | ].
   apply in_app_iff; left.
   unfold InPL in IPL; simpl in IPL.
   destruct IPL; subst.
    exact (path_end_pl ph).
    exact (path_start_pl ph).
   apply in_app_iff in LIN.
   destruct LIN as [LIN | LIN].
   apply in_app_iff; left.
   exact (path_ll_pl ph p l IPL LIN).
   apply in_app_iff; right.
   exact (LP p l LIN IPL).
  split.
   (* LCconsistency *)
   unfold LCconsistency.
   intros l LIN.
   simpl in LIN.
   destruct LIN as [LIN | LIN].
   inversion LIN; subst.
   exists ((y, x) :: al); split; [ exact (in_eq _ _) | exact (in_eq _ _) ].
   apply f_equal with(f:=reverse) in H.
   rewrite reverse_reverse in H.
   subst.
   exists (reverse_linelist ((y, x) :: al)); split.
   unfold reverse_linelist.
   rewrite <- in_rev, map_reverse, reverse_reverse.
   exact (in_eq _ _).
   exact (in_cons _ _ _ (in_eq _ _)).
   apply lin_app_or in LIN.
   destruct LIN as [LIN | LIN].
    destruct (LIn_In _ _ LIN) as [LIN1 | LIN2].
     exists ((y, x) :: al); split; [ exact (in_cons _ _ _ LIN1) | exact (in_eq _ _) ].
     exists (reverse_linelist ((y, x) :: al)); split; [ | exact (in_cons _ _ _ (in_eq _ _)) ].
     unfold reverse_linelist.
     rewrite <- in_rev, map_reverse.
     exact (in_cons _ _ _ LIN2).
     destruct (LC _ LIN) as [c [INC1 INC2]].
     exists c; split; [ exact INC1 | exact (in_cons _ _ _ (in_cons _ _ _ INC2)) ].
  split.
   (* CLconsistency *)
   unfold CLconsistency.
   intros l c ICC ILC.
   simpl in ICC.
   destruct ICC as [ICC | [ICC | ICC]]; [ subst | subst | ].
   simpl in ILC.
   destruct ILC as [ILC | ILC]; [ subst; left; exact (ul_refl _) | ].
   right; apply lin_or_app; left.
   exact (in_lin _ _ ILC).
   unfold reverse_linelist in ILC.
   rewrite <- in_rev, map_reverse in ILC.
   simpl in ILC.
   destruct ILC as [ILC | ILC]; [ left | right ].
    rewrite ILC.
    exact (reverse_ul _ _ (eq_refl _)).
    apply lin_or_app; left.
    exact (In_LIn_r _ _ ILC).
   right; apply lin_or_app; right.
   exact (CL _ _ ICC ILC).
  split.
   (* CAconsisntency *)
   unfold CAconsistency.
   intros c ICC NCO.
   simpl in ICC.
   destruct ICC as [ICC | [ICC2| ICC]]; [ subst | subst | ].
   exists (((y, x) :: al) :: nil); split; exact (in_eq _ _).
   exists (reverse_linelist ((y, x) :: al) :: a); split; [ exact (in_eq _ _) | exact (in_cons _ _ _ (in_eq _ _)) ].
   apply (CA c ICC) in NCO.
   destruct NCO as [a0 [NCO1 NCO2]].
   destruct (eq_listcircuit_dec a0 a) as [EQ | NEQ]; [ subst | ].
   exists (reverse_linelist ((y, x) :: al) :: a); split; [ | exact (in_cons _ _ _ (in_eq _ _)) ].
   exact (in_cons _ _ _ NCO1).
   destruct (eq_area_dec a0 a) as [EQA | NEQA].
   elimtype False.
   exact (eq_area_not_eq _ _ _ DA NCO2 INA NEQ EQA).
   exists a0; split; [ exact NCO1 | ].
   right; right.
   refine (in_set_remove _ _ _ _ _ NCO2).
   intros D; apply NEQA.
   rewrite D; now apply eq_area_refl.
  unfold PLCAconstraints.
  split.
   (* ACconsistency *)
   unfold ACconsistency.
   intros c a0 IAA IAC.
   simpl in IAA.
   destruct IAA as [IAA | [ IAA | IAA]]; [ subst | subst | ].
   simpl in IAC.
   destruct IAC as [IAC | IAC]; [ subst | elimtype False; exact IAC].
   exact (in_eq _ _).
   simpl in IAC.
   destruct IAC as [IAC | IAC]; [ subst; exact (in_cons _ _ _ (in_eq _ _)) | ].
   exact (in_cons _ _ _ (in_cons _ _ _ (AC c a INA IAC))).
   exact (in_cons _ _ _ (in_cons _ _ _ (AC c a0 (set_remove_in _ _ _ _ IAA) IAC))).
  split.
   (* Outermostconsistency1 *)
   unfold Outermostconsistency1.
   right; right; exact O1.
  split.
   (* Outermostconsistency2 *)
   unfold Outermostconsistency2.
   intros a0 IAA D.
   simpl in IAA.
   destruct IAA as [IAA | [IAA | IAA]]; [ subst | subst | ].
   simpl in D.
   destruct D as [D | D]; [ subst | exact D].
   apply Rot_sym, (RotIn _ _ _ (in_eq _ _)) in D.
   apply (F x (path_start_pl ph)).
   exact (CPconsistency x (y, x) _ LP CL O1 D (InPL_right _ _)).
   simpl in D.
   destruct D as [D | D].
   assert (In (x, y) (reverse_linelist ((y, x) :: al))) as oxy.
   unfold reverse_linelist; simpl.
   apply in_or_app; right; now apply in_eq.
   apply Rot_sym, (RotIn _ _ _ oxy) in D.
   apply (F x (path_start_pl ph)).
   exact (CPconsistency x (x, y) _ LP CL O1 D (InPL_left _ _)).
   exact (O2 a INA D).
   exact (O2 a0 (set_remove_in _ _ _ _ IAA) D).
  split.
   (* Circuitconsistency *)
   unfold Circuitconsistency.
   intros l c1 c2 IC1 IC2 LC1 LC2.
   simpl in IC1, IC2.
   destruct IC1 as [IC1 | [IC1 | IC1]]; [ subst | subst | ].
   destruct IC2 as [IC2 | [IC2 | IC2]]; [ exact IC2 | subst; elimtype False | elimtype False ].
   simpl in LC1, LC2.
   destruct LC1 as [LC1 | LC1]; [ subst | ].
   unfold reverse_linelist in LC2.
   rewrite <- in_rev, map_reverse in LC2.
   unfold reverse in LC2; simpl in LC2.
   destruct LC2 as [LC2 | LC2].
   inversion LC2; subst.
   inversion ph; subst.
    simpl in lg; omega.
    exact (not_simplepath ph).
   inversion ph; subst.
    simpl in lg; omega.
    simpl in LC2.
    destruct LC2 as [LC2 | LC2].
     inversion LC2; subst.
     inversion H; subst.
      simpl in lg; omega.
      exact (not_simplepath H).
     exact (H0 (path_ll_pl H x (x, y) (InPL_left _ _) LC2)).
     unfold reverse_linelist in LC2.
    rewrite <- in_rev, map_reverse in LC2.
    destruct l; unfold reverse in LC2; simpl in LC2.
    destruct LC2 as [LC2 | LC2].
    inversion LC2; subst.
    inversion ph; subst.
     exact LC1.
     simpl in LC1.
    destruct LC1 as [LC1 | LC1].
    inversion LC1; subst.
    inversion H; subst.
     simpl in lg; omega.
     exact (not_simplepath H).
    exact (H0 (path_ll_pl H p _ (InPL_left _ _) LC1)).
   clear -ph LC1 LC2.
   induction ph; simpl in *.
    exact LC1.
    destruct LC1 as [LC1 | LC1].
    inversion LC1; subst.
    destruct LC2 as [LC2 | LC2].
    inversion LC2; subst.
    exact (H (path_start_pl ph)).
    exact (H (path_ll_pl ph _ _ (InPL_right _ _) LC2)).
    destruct LC2 as [LC2 | LC2].
    inversion LC2; subst.
    exact (H (path_ll_pl ph _ _ (InPL_right _ _) LC1)).
    exact (IHph LC1 LC2).
   simpl in LC1.
   destruct LC1 as [LC1 | LC1]; [ subst | ].
   apply (F x (path_start_pl ph)).
   exact (CPconsistency x (y, x) _ LP CL IC2 LC2 (InPL_right _ _)).
   destruct l.
   apply (F p (path_ll_pl ph p (p, p0) (InPL_left _ _) LC1)).
   exact (CPconsistency p (p, p0) _ LP CL IC2 LC2 (InPL_left _ _)).
   destruct IC2 as [IC2 | [IC2 | IC2]];
   [ subst; elimtype False
   | exact IC2
   | elimtype False].
   unfold reverse_linelist in LC1.
   rewrite <- in_rev, map_reverse in LC1.
   simpl in LC1, LC2.
   destruct LC1 as [LC1 | LC1].
   apply f_equal with(f:=reverse) in LC1.
   rewrite reverse_reverse in LC1.
   destruct LC2 as [LC2 | LC2].
   inversion LC1; subst.
   inversion LC2; subst.
   exact (XY (eq_refl _)).
   subst.
   inversion ph; subst.
    simpl in lg; omega.
    simpl in LC2.
    destruct LC2 as [LC2 | LC2].
    inversion LC2; subst.
    inversion H; subst.
     simpl in lg; omega.
     exact (not_simplepath H).
    exact (H0 (path_ll_pl H x _ (InPL_left _ _) LC2)).
   destruct LC2 as [LC2 | LC2].
   subst.
   inversion ph; subst.
    simpl in lg; omega.
    simpl in LC1.
    destruct LC1 as [LC1 | LC1].
    inversion LC1; subst.
    inversion H; subst.
     simpl in lg; omega.
     exact (not_simplepath H).
    exact (H0 (path_ll_pl H x _ (InPL_left _ _) LC1)).
   clear -ph LC1 LC2.
   induction ph; simpl in *.
    exact LC1.
    destruct LC1 as [LC1 | LC1].
    apply f_equal with (f:=reverse) in LC1.
    rewrite reverse_reverse in LC1.
    destruct LC2 as [LC2 | LC2].
    inversion LC1; subst.
    inversion LC2; subst.
    exact (H (path_start_pl ph)).
    subst.
    exact (H (path_ll_pl ph _ _ (InPL_right _ _) LC2)).
    destruct LC2 as [LC2 | LC2].
    subst.
    exact (H (path_ll_pl ph _ _ (InPL_right _ _) LC1)).
    exact (IHph LC1 LC2).
   unfold reverse_linelist in LC1.
   rewrite <- in_rev, map_reverse in LC1.
   simpl in LC1.
   destruct LC1 as [LC1 | LC1].
   apply f_equal with(f:=reverse) in LC1.
   rewrite reverse_reverse in LC1.
   subst.
   apply (F x (path_start_pl ph)).
   exact (CPconsistency x (x, y) _ LP CL IC2 LC2 (InPL_left _ _)).
   destruct l as [lx ly].
   apply (F lx (path_ll_pl ph lx _ (InPL_right _ _) LC1)).
   exact (CPconsistency lx (lx, ly) _ LP CL IC2 LC2 (InPL_left _ _)).
   destruct IC2 as [IC2 | [IC2 | IC2]];
   [ subst; elimtype False
   | subst; elimtype False
   | ].
   simpl in LC2.
   destruct LC2 as [LC2 | LC2]; [ subst | ].
   apply (F x (path_start_pl ph)).
   exact (CPconsistency x (y, x) _ LP CL IC1 LC1 (InPL_right _ _)).
   destruct l as [lx ly].
   apply (F lx (path_ll_pl ph lx _ (InPL_left _ _) LC2)).
   exact (CPconsistency lx (lx, ly) _ LP CL IC1 LC1 (InPL_left _ _)).
   unfold reverse_linelist in LC2.
   rewrite <- in_rev, map_reverse in LC2.
   simpl in LC2.
   destruct LC2 as [LC2 | LC2].
   apply f_equal with (f:=reverse) in LC2.
   rewrite reverse_reverse in LC2.
   subst.
   apply (F x (path_start_pl ph)).
   exact (CPconsistency x (x, y) _ LP CL IC1 LC1 (InPL_left _ _)).
   destruct l as [lx ly].
   apply (F lx (path_ll_pl ph lx _ (InPL_right _ _) LC2)).
   exact (CPconsistency lx (lx, ly) _ LP CL IC1 LC1 (InPL_left _ _)).
   exact (CC l c1 c2 IC1 IC2 LC1 LC2).
  split.
   (* Areaconsistency *)
   unfold Areaconsistency.
   intros c a1 a2 IA1 IA2 CA1 CA2.
   simpl in IA1, IA2.
   destruct IA1 as [IA1 | [IA1 | IA1]]; [ subst | subst | ].
   destruct IA2 as [IA2 | [IA2 | IA2]];
   [ exact IA2
   | subst; elimtype False
   | elimtype False ].
   simpl in CA1, CA2.
   destruct CA1 as [CA1 | CA1]; [ subst | exact CA1].
   destruct CA2 as [CA2 | CA2]; [ subst | ].
   apply (Rot_reverse_linelist ((y, x) :: al)).
   unfold Circuit_constraints.
   split; [exists y; exists (y :: ap) | simpl; omega].
   apply step_trail.
   exact (simplepath_onlyif_trail ph).
   exact (not_eq_sym XY).
   intros D.
   inversion ph; subst.
    simpl in lg; omega.
    simpl in D.
    destruct D as [D | D].
     inversion D; subst.
     exact (XY (eq_refl _)).
     inversion H1; subst.
     inversion H; subst.
      simpl in lg; omega.
      exact (not_simplepath H).
     destruct (LIn_In _ _ D) as [D1 | D2].
      exact (H0 (path_ll_pl H x _ (InPL_right _ _) D1)).
      exact (H0 (path_ll_pl H x _ (InPL_left _ _) D2)).
   rewrite CA2; now apply rotSame.
   apply (AC _ _ INA) in CA2.
   apply (F x (path_start_pl ph)).
   exact (CPconsistency x (y, x) _ LP CL CA2 (in_eq _ _) (InPL_right _ _)).
   simpl in CA1.
   destruct CA1 as [CA1 | CA1]; [ subst | exact CA1].
   apply (AC _ _ (set_remove_in _ _ _ _ IA2)) in CA2.
   apply (F x (path_start_pl ph)).
   exact (CPconsistency x (y, x) _ LP CL CA2 (in_eq _ _) (InPL_right _ _)).
   destruct IA2 as [IA2 | [IA2 | IA2]];
   [ subst; elimtype False
   | subst; reflexivity
   | elimtype False ].
   simpl in CA1, CA2.
   destruct CA2 as [CA2 | CA2]; [ subst | exact CA2].
   destruct CA1 as [CA1 | CA1]; [ subst | ].
   assert (Rot (reverse_linelist ((y, x) :: al)) ((y, x) :: al)) as ROT.
   rewrite CA1; now apply rotSame.
   refine (Rot_reverse_linelist _ _ (Rot_sym _ _ ROT)).
   unfold Circuit_constraints.
   split; [exists y; exists (y :: ap) | simpl; omega].
   apply step_trail.
   exact (simplepath_onlyif_trail ph).
   exact (not_eq_sym XY).
   intros D.
   inversion ph; subst.
    simpl in lg; omega.
    simpl in D.
    destruct D as [D | D].
     inversion D; subst.
     exact (XY (eq_refl _)).
     inversion H1; subst.
     inversion H; subst.
      simpl in lg; omega.
      exact (not_simplepath H).
     destruct (LIn_In _ _ D) as [D1 | D2].
      exact (H0 (path_ll_pl H x _ (InPL_right _ _) D1)).
      exact (H0 (path_ll_pl H x _ (InPL_left _ _) D2)).
   apply (AC _ _ INA) in CA1.
   apply (F x (path_start_pl ph)).
   exact (CPconsistency x (y, x) _ LP CL CA1 (in_eq _ _) (InPL_right _ _)).
   simpl in CA1.
   destruct CA1 as [CA1 | CA1]; [ subst | ].
   apply (AC _ _ (set_remove_in _ _ _ _ IA2)) in CA2.
   apply (F x (path_start_pl ph)).
   refine (CPconsistency x (x, y) _ LP CL CA2 _ (InPL_left _ _)).
   unfold reverse_linelist.
   rewrite <- in_rev, map_reverse.
   exact (in_eq _ _).
   assert (EQ := AA _ _ _ INA (set_remove_in _ _ _ _ IA2) CA1 CA2); subst.
   apply (DA a2 INA).
   apply in_ain.
   exact IA2.
   destruct IA2 as [IA2 | [IA2 | IA2]];
   [ subst; elimtype False
   | subst; elimtype False
   | ].
   simpl in CA2.
   destruct CA2 as [CA2 | CA2]; [ subst | exact CA2].
   apply (AC _ _ (set_remove_in _ _ _ _ IA1)) in CA1.
   apply (F x (path_start_pl ph)).
   exact (CPconsistency x (y, x) _ LP CL CA1 (in_eq _ _) (InPL_right _ _)).
   simpl in CA2.
   destruct CA2 as [CA2 | CA2]; [ subst | ].
   apply (AC _ _ (set_remove_in _ _ _ _ IA1)) in CA1.
   apply (F x (path_start_pl ph)).
   refine (CPconsistency x (x, y) _ LP CL CA1 _ (InPL_left _ _)).
   unfold reverse_linelist.
   rewrite <- in_rev, map_reverse.
   exact (in_eq _ _).
   assert (EQ := AA _ _ _ INA (set_remove_in _ _ _ _ IA1) CA2 CA1); subst.
   apply (DA a1 INA).
   apply in_ain.
   exact IA1.
   exact (AA c a1 a2 (set_remove_in _ _ _ _ IA1) (set_remove_in _ _ _ _ IA2) CA1 CA2).
  split.
   (* DistinctP *)
   unfold DistinctP.
   intros p _ D.
   clear -ph D DP F.
   induction ph; simpl in D.
    destruct (eq_point_dec p p0); [subst | ].
    exact (F _ (in_eq _ _) D).
    destruct D as [D | D]; [symmetry in D; exact (n D) | ].
    exact (DP _ (set_remove_in _ _ _ _ D) D).
    destruct (eq_point_dec p p3); [subst | ].
     apply in_app_iff in D.
     destruct D as [D | D].
     exact (H D).
     exact (F _ (in_eq _ _) D).
     destruct D as [D | D]; [ subst; apply n; reflexivity | ].
      apply IHph; [ | exact D ].
      intros p0 INP D1.
      exact (F p0 (or_intror _ INP) D1).
  split.
   (* DistinctL *)
   unfold DistinctL.
   intros l Il D.
   simpl in D.
   destruct (eq_dline_dec l (y, x)) as [EQ | NEQ].
   apply lin_app_or in D.
   destruct D as [D | D].
    inversion EQ; subst.
    inversion ph; subst.
     simpl in lg; omega.
     simpl in D.
     destruct D as [D | D].
     inversion D; subst.
      exact (not_simplepath ph).
     inversion H2; subst.
     inversion H0; subst.
      simpl in lg; omega.
      exact (not_simplepath H0).
     destruct (LIn_In _ _ D) as [D1 | D1].
      exact (H1 (path_ll_pl H0 x (y, x) (InPL_right _ _) D1)).
      exact (H1 (path_ll_pl H0 x (x, y) (InPL_left _ _) D1)).
    subst.
    inversion ph; subst.
     simpl in lg; omega.
     apply (F _ (in_eq _ _)).
     apply LIn_In in D.
     destruct D as [D | D].
     exact (LP _ _ D (InPL_right _ _)).
     exact (LP _ _ D (InPL_left _ _)).
     destruct Il as [Il | Il]; [symmetry in Il; exact (NEQ Il) | ].
     simpl in D.
     destruct D as [D | D].
     inversion D; subst.
     apply NEQ; reflexivity.
     destruct l; inversion H; subst.
     apply in_app_or in Il; destruct Il as [Il | Il].
     inversion ph; subst.
      simpl in lg; omega.
      destruct Il as [Il | Il].
      inversion Il; subst.
      inversion H0; subst.
      simpl in lg; omega.
      exact (not_simplepath H0).
      apply H1.
      exact (path_ll_pl H0 _ _ (InPL_left _ _) Il).
     inversion ph; subst.
     simpl in lg; omega.
     apply (F _ (in_eq _ _)).
     exact (LP _ _ Il (InPL_left _ _)).
   clear -DL Il D F ph LP.
   induction ph.
    exact (DL _ Il D).
    simpl in D.
    destruct (eq_dline_dec l (p3, p2)) as [EQ | NEQ].
    subst; apply lin_app_or in D.
    destruct D as [D | D].
     destruct (LIn_In _ _ D) as [D1 | D1].
      exact (H (path_ll_pl ph p3 (p3, p2) (InPL_left _ _) D1)).
      exact (H (path_ll_pl ph p3 (p2, p3) (InPL_right _ _) D1)).
     apply (F _ (in_eq _ _)).
     destruct (LIn_In _ _ D) as [D1 | D1].
      exact (LP p3 (p3, p2) D1 (InPL_left _ _)).
      exact (LP p3 (p2, p3) D1 (InPL_right _ _)).
    simpl in Il, D.
    destruct Il as [Il | Il]; [ subst; apply NEQ; reflexivity | ].
    destruct D as [D | D].
    inversion D; subst.
    apply NEQ; reflexivity.
    destruct l; inversion H0; clear H0 D; subst.
    apply in_app_or in Il; destruct Il as [Il | Il].
     apply H.
     exact (path_ll_pl ph _ _ (InPL_right _ _) Il).
     apply (F _ (in_eq _ _)).
     exact (LP _ _ Il (InPL_right _ _)).
    apply IHph; [ | exact Il | exact D].
    intros p PL D1.
    exact (F p (or_intror _ PL) D1).
  split.
   (* DistinctC *)
   unfold DistinctC.
   intros c Ic D.
   simpl in D.
   destruct (eq_listline_dec c ((y, x) :: al)) as [EQ1 | NEQ1].
   subst; simpl in D.
   destruct D as [D | D].
   refine (Rot_reverse_linelist _ _ D).
   unfold Circuit_constraints.
   split; [exists y; exists (y :: ap) | simpl; omega].
   apply step_trail.
   exact (simplepath_onlyif_trail ph).
   exact (not_eq_sym XY).
   intros D1.
   inversion ph; subst.
    simpl in lg; omega.
    simpl in D1.
    destruct D1 as [D1 | D1].
     inversion D1; subst.
     exact (XY (eq_refl _)).
     inversion H1; subst.
     inversion H; subst.
      simpl in lg; omega.
      exact (not_simplepath H).
     destruct (LIn_In _ _ D1) as [D2 | D2].
      exact (H0 (path_ll_pl H x _ (InPL_right _ _) D2)).
      exact (H0 (path_ll_pl H x _ (InPL_left _ _) D2)).
   apply rin_in in D.
   destruct D as [c' [D1 D2]].
   apply (F x (path_start_pl ph)).
   refine (CPconsistency x (y, x) _ LP CL D2 _ (InPL_right _ _)).
   exact (RotIn_r _ _ _ (in_eq _ _) (Rot_sym _ _ D1)).
   destruct Ic as [Ic | Ic]; [symmetry in Ic; exact (NEQ1 Ic) | ].
   simpl in D.
   destruct D as [D | D].
    destruct Ic as [Ic | Ic].
   subst; refine (Rot_reverse_linelist _ _ (Rot_sym _ _ D)).
   unfold Circuit_constraints.
   split; [exists y; exists (y :: ap) | simpl; omega].
   apply step_trail.
   exact (simplepath_onlyif_trail ph).
   exact (not_eq_sym XY).
   intros D1.
   inversion ph; subst.
    simpl in lg; omega.
    simpl in D1.
    destruct D1 as [D1 | D1].
     inversion D1; subst.
     exact (XY (eq_refl _)).
     inversion H1; subst.
     inversion H; subst.
      simpl in lg; omega.
      exact (not_simplepath H).
     destruct (LIn_In _ _ D1) as [D2 | D2].
      exact (H0 (path_ll_pl H x _ (InPL_right _ _) D2)).
      exact (H0 (path_ll_pl H x _ (InPL_left _ _) D2)).
   apply (F x (path_start_pl ph)).
   refine (CPconsistency x (y, x) _ LP CL Ic _ (InPL_right _ _)).
   exact (RotIn_r _ _ _ (in_eq _ _) D).
   destruct (eq_listline_dec c (reverse_linelist ((y, x) :: al))) as [EQ2 | NEQ2].
   apply rin_in in D.
   destruct D as [c' [D1 D2]].
   apply (F x (path_start_pl ph)).
   refine (CPconsistency x (x, y) _ LP CL D2 _ (InPL_left _ _)).
   refine (RotIn _ _ _ _ D1).
   subst; unfold reverse_linelist; simpl.
   apply in_or_app; right; now apply in_eq.
   destruct Ic as [Ic | Ic]; [symmetry in Ic; exact (NEQ2 Ic)| ].
   destruct D as [D | D].
   apply (F x (path_start_pl ph)).
   refine (CPconsistency x (x, y) _ LP CL Ic _ (InPL_left _ _)).
   refine (RotIn _ _ _ _ (Rot_sym _ _ D)).
   unfold reverse_linelist; simpl.
   apply in_or_app; right; now apply in_eq.
   exact (DC _ Ic D).
  (* DistinctA *)
   unfold DistinctA.
   intros a0 Ia D.
   simpl in D.
   destruct (eq_listcircuit_dec a0 (((y, x) :: al) :: nil)) as [EQ1 | NEQ1].
   subst; simpl in D.
   destruct D as [D | D].
   clear -D ph lg XY.
   assert(RIn (reverse_linelist ((y, x) :: al)) (((y, x) :: al) :: nil)).
    refine (eq_area_rin _ _ _ _ (eq_area_sym _ _ D)).
    exact (rin_eq _ _ _ (rotSame _)).
   simpl in H.
   destruct H as [D1 | D1]; [ | exact D1].
   refine (Rot_reverse_linelist _ _ (Rot_sym _ _ D1)).
   unfold Circuit_constraints.
   split; [ exists y; exists (y::ap) | simpl; omega ].
   apply step_trail.
   exact (simplepath_onlyif_trail ph).
   exact (not_eq_sym XY).
   intros D2.
   inversion ph; subst.
    simpl in lg; omega.
    simpl in D2.
    destruct D2 as [D2 | D2].
    inversion D2; subst.
     exact (XY (eq_refl _)).
     inversion H1; subst.
     inversion H; subst.
      simpl in lg; omega.
      exact (not_simplepath H).
     destruct (LIn_In _ _ D2) as [D3 | D3].
      exact (H0 (path_ll_pl H x (y, x) (InPL_right _ _) D3)).
      exact (H0 (path_ll_pl H x (x, y) (InPL_left _ _) D3)).
   apply set_remove_in_a in D.
   apply ain_in in D.
   destruct D as [a' [D1 D2]].
   apply (eq_area_rin _ _ ((y, x) :: al)) in D1.
   apply rin_in in D1.
   destruct D1 as [c' [D3 D4]].
   apply (F x (path_start_pl ph)).
   apply (AC _ _ D2) in D4.
   refine (CPconsistency x (y, x) _ LP CL D4 _ (InPL_right _ _)).
   exact (RotIn _ _ _ (in_eq _ _) D3).
   left; now apply rotSame.
   destruct Ia as [Ia | Ia]; [symmetry in Ia; exact (NEQ1 Ia) | ].
   simpl in D.
   destruct D as [D | D].
   destruct Ia as [Ia | Ia].
   subst.
   apply eq_area_rin with _ _ (reverse_linelist ((y, x) :: al)) in D.
   destruct D as [D | D]; [ | exact D].
   assert (In (x, y) (reverse_linelist ((y, x) :: al))).
    unfold reverse_linelist; simpl.
    apply in_or_app; right; now apply in_eq.
   apply (RotIn _ _ _ H) in D.
   simpl in D; destruct D as [D | D].
    inversion D; subst; inversion ph; subst.
     simpl in lg; omega.
     exact (not_simplepath ph).
   inversion ph; subst.
    simpl in lg; omega.
    simpl in D.
    destruct D as [D | D].
    inversion D; subst.
     inversion H0; subst.
      simpl in lg; omega.
      exact (not_simplepath H0).
     exact (H1 (path_ll_pl H0 x (x, y) (InPL_left _ _) D)).
   left; now apply rotSame.
   apply set_remove_in in Ia.
   apply eq_area_sym, (eq_area_rin _ _ ((y, x) :: al)) in D.
   apply rin_in in D.
   destruct D as [c' [D1 D2]].
   apply (AC _ _ Ia) in D2.
   apply (F x (path_start_pl ph)).
   apply (RotIn (y, x) _ _ (in_eq _ _)) in D1.
   exact (CPconsistency x (y, x) _ LP CL D2 D1 (InPL_right _ _)).
   left; now apply rotSame.
   destruct (eq_listcircuit_dec a0 (reverse_linelist ((y, x) :: al) :: a)) as [EQ | NEQ2].
   apply ain_in in D.
   destruct D as [a' [D1 D2]].
   apply set_remove_in in D2.
   assert (RIn (reverse_linelist ((y, x) :: al)) a0) as RI.
   subst; left; now apply rotSame.
   apply (eq_area_rin _ _ _ RI) in D1.
   apply rin_in in D1.
   destruct D1 as [c' [D3 D4]].
   apply (AC _ _ D2) in D4.
   apply (F _ (path_start_pl ph)).
   refine (CPconsistency _ (x, y) _ LP CL D4 _ (InPL_left _ _)).
   refine (RotIn _ _ _ _ D3).
   unfold reverse_linelist; simpl.
   apply in_or_app; right; now apply in_eq.
   destruct Ia as [Ia | Ia]; [symmetry in Ia; exact (NEQ2 Ia) | ].
   apply set_remove_in in Ia.
   destruct D as [D | D].
   apply eq_area_sym, (eq_area_rin _ _ (reverse_linelist ((y, x) :: al))) in D.
   apply rin_in in D.
   destruct D as [c' [D1 D2]].
   apply (AC _ _ Ia) in D2.
   apply (F x (path_start_pl ph)).
   assert (In (x, y) (reverse_linelist ((y, x) :: al))) as D.
   unfold reverse_linelist; simpl.
   apply in_or_app; right; now apply in_eq.
   apply (RotIn (x, y) _ _ D) in D1.
   exact (CPconsistency x (x, y) _ LP CL D2 D1 (InPL_left _ _)).
   left; now apply rotSame.
   apply (DA a0 Ia).
   clear -D.
   induction A; simpl in *.
    elim D.
    destruct (eq_listcircuit_dec a0 a1) as [EQ1 | NEQ1]; subst.
    destruct (eq_listcircuit_dec a a1) as [EQA | NEQA].
     apply ain_in in D.
     destruct D as [ea [EQAA IAA]].
     apply set_remove_in in IAA.
     apply (eq_ain_elem _ _ _ (eq_area_sym _ _ EQAA)).
     exact (in_ain _ _ IAA).
    simpl in D.
    destruct (eq_listcircuit_dec a1 a1) as [EQ1 | NEQ1]; subst.
     exact (set_remove_in_a _ _ _ D).
     elim (NEQ1 (eq_refl _)).
    destruct (eq_listcircuit_dec a a1) as [EQA | NEQA].
     right; exact D.
    simpl in D.
    destruct (eq_listcircuit_dec a0 a1) as [EQ2 | NEQ2]; subst.
     left; exact (eq_area_refl _).
    simpl in D.
    destruct D as [D | D].
     left; exact D.
     right; exact (IHA D).
  (* PLCAconstraints *)
  unfold PLCAconstraints.
  split.
   (* Lconstraint *)
   unfold Lconstraint, Line_constraint.
   intros l LIN D.
   simpl in LIN.
   destruct LIN as [LIN | LIN].
   destruct l as [lx ly]; simpl in D; subst.
   inversion LIN; subst.
    exact (XY (eq_refl _)).
   apply in_app_or in LIN.
   destruct LIN as [LIN | LIN].
   exact (((simplepathline_Lconstraint ph) _ LIN) D).
   exact ((LT _ LIN) D).
  split.
   (* Cconstraint *)
   unfold Cconstraint.
   unfold Circuit_constraints.
   intros c ICC.
   simpl in ICC.
   destruct ICC as [ICC | [ICC | ICC]];
   [ subst; simpl
   | subst; simpl
   | ].
   split; [ exists y; exists (y::ap) | exact (le_n_S _ _ lg)].
   apply step_trail; [ exact (simplepath_onlyif_trail ph) | exact (not_eq_sym XY) | ].
   intros D.
   inversion ph; subst.
    simpl in lg; omega.
    simpl in D.
    destruct D as [D | D].
     inversion D; subst.
      exact (XY (eq_refl _)).
      inversion H1; subst.
      inversion H; subst.
       simpl in lg; omega.
       exact (not_simplepath H).
     destruct (LIn_In _ _ D) as [D1 | D1].
      exact (H0 (path_ll_pl H x _ (InPL_right _ _) D1)).
      exact (H0 (path_ll_pl H x _ (InPL_left _ _) D1)).
   split; [ exists y; exists (rev (y::ap)) | ].
   apply rev_trail.
   apply step_trail; [ exact (simplepath_onlyif_trail ph) | exact (not_eq_sym XY) | ].
   intros D.
   destruct (LIn_In _ _ D) as [D1 | D1].
    inversion ph; subst.
     simpl in lg; omega.
     simpl in D1.
     destruct D1 as [D1 | D1].
     inversion D1; subst.
      exact (XY (eq_refl _)).
      exact (H0 (path_ll_pl H x _ (InPL_right _ _) D1)).
   inversion ph; subst.
    simpl in lg; omega.
    simpl in D1.
    destruct D1 as [D1 | D1].
     inversion D1; subst.
     inversion H; subst.
      simpl in lg; omega.
      exact (not_simplepath H).
      exact (H0 (path_ll_pl H x _ (InPL_left _ _) D1)).
    unfold reverse_linelist.
    rewrite rev_length, map_length.
    exact (le_n_S _ _ lg).
   exact (CT c ICC).
  split.
   (* Aconstraint *)
   unfold Aconstraint.
   unfold Area_constraints.
   intros a0 IAA.
   simpl in IAA.
   destruct IAA as [IAA | [IAA | IAA]]; [ subst | subst | ].
    split; [intros c1 c2 IC1 IC2 NC | ].
    simpl in IC1, IC2.
    destruct IC1 as [IC1 | IC1]; [ subst | elimtype False; exact IC1].
    destruct IC2 as [IC2 | IC2]; elimtype False; [ subst; exact (NC (eq_refl _)) | exact IC2].
    split; [intros c Ic D | simpl; omega].
    simpl in D.
    destruct (eq_listline_dec c ((y, x) :: al)) as [EQ | NEQ]; [exact D | ].
    simpl in Ic.
    destruct Ic as [Ic | Ic]; [ symmetry in Ic; exact (NEQ Ic) | exact Ic].
    split; [intros c1 c2 IC1 IC2 NC | ].
    cut (~point_connect c1 c2); intros PC.
    split; [ exact PC | ].
    intros LCC; apply PC.
    exact (lc_only_if_pc c1 c2 LCC).
    simpl in IC1, IC2.
    destruct IC1 as [IC1 | IC1]; [ subst | ].
    destruct IC2 as [IC2 | IC2];  [ elimtype False; subst; exact (NC (eq_refl _)) | ].
    unfold point_connect in PC.
    destruct PC as [l1 [l2 [p [IL1 [IL2 [IPL1 IPL2]]]]]].
    simpl in IL1.
    unfold reverse_linelist in IL1.
    rewrite <- in_rev, map_reverse in IL1.
    destruct l1; unfold reverse in IL1; simpl in IL1.
    destruct IL1 as [IL1 | IL1].
    inversion IL1; subst.
    unfold InPL in IPL1; simpl in IPL1.
    apply (AC _ _ INA) in IC2.
    refine (F p _ (CPconsistency p l2 c2 LP CL IC2 IL2 IPL2)).
    destruct IPL1; subst.
     exact (path_start_pl ph).
     exact (path_end_pl ph).
    apply (AC _ _ INA) in IC2.
    refine (F p _ (CPconsistency p l2 c2 LP CL IC2 IL2 IPL2)).
    refine (path_ll_pl ph p _ _ IL1).
    unfold InPL in *; simpl in *.
    destruct IPL1 as [IPL1 | IPL1]; [right; exact IPL1 | left; exact IPL1].
    destruct IC2 as [IC2 | IC2]; [subst | ].
    unfold point_connect in PC.
    destruct PC as [l1 [l2 [p [IL1 [IL2 [IPL1 IPL2]]]]]].
    unfold reverse_linelist in IL2.
    rewrite <- in_rev, map_reverse in IL2.
    simpl in IL2.
    apply (AC _ _ INA) in IC1.
    refine(F p _ (CPconsistency p l1 c1 LP CL IC1 IL1 IPL1)).
    destruct IL2 as [IL2 | IL2].
    destruct l2.
    inversion IL2; subst.
    inversion IPL2; simpl in *; subst.
     exact (path_start_pl ph).
     exact (path_end_pl ph).
    refine (path_ll_pl ph p _ _ IL2).
    destruct IPL2 as [IPL2 | IPL2];
    [right; exact IPL2 | left; exact IPL2].
    destruct (AT a INA) as [AT1 AT2].
    apply (AT1 c1 c2 IC1 IC2 NC) in PC.
    exact PC.
    split; [intros c Ic D | simpl; omega].
    simpl in D.
    destruct (eq_listline_dec c (reverse_linelist ((y, x) :: al))) as [EQ | NEQ].
    apply rin_in in D.
    destruct D as [c' [D1 D2]].
    apply (AC _ _ INA) in D2.
    apply (F x (path_start_pl ph)).
    refine (CPconsistency x (x, y) _ LP CL D2 _ (InPL_left _ _)).
    refine (RotIn _ _ _ _ D1).
    subst; unfold reverse_linelist; simpl.
    apply in_or_app; right; now apply in_eq.
    destruct Ic as [Ic | Ic]; [symmetry in Ic; exact (NEQ Ic) | ].
    destruct D as [D | D].
    apply (AC _ _ INA) in Ic.
    apply (F x (path_start_pl ph)).
    refine (CPconsistency x (x, y) _ LP CL Ic _ (InPL_left _ _)).
    refine (RotIn _ _ _ _ (Rot_sym _ _ D)).
    unfold reverse_linelist; simpl.
    apply in_or_app; right; now apply in_eq.
    destruct (AT a INA) as [_ [AT2 _]].
    exact (AT2 _ Ic D).
    destruct (AT a0 (set_remove_in _ _ _ _ IAA)) as [AT1 [AT2 AT3]].
    split; [intros c1 c2 IC1 IC2 NC | ].
    exact (AT1 c1 c2 IC1 IC2 NC).
    split; [intros c D; exact (AT2 c D) | exact AT3 ].
   (* Outermostconstraints *)
   exact OT.
Qed.


(* decompose *)
Definition InductivePLCAconsistency :
forall{P : list Point}{L : list Line}{C : list Circuit}{A : list Area}{o : Circuit}, 
 I P L C A o -> PLCAconsistency P L C A o :=
fun P L C A o I => match (IPLCA_is_satisfied_with_consistency_and_constraints P L C A o I) with
 | conj P _ => P
 end.

Definition InductivePLCAconstraints :
forall{P : list Point}{L : list Line}{C : list Circuit}{A : list Area}{o : Circuit}, 
 I P L C A o -> PLCAconstraints L C A o :=
fun P L C A o I => match (IPLCA_is_satisfied_with_consistency_and_constraints P L C A o I) with
 | conj _ P => P
 end.

(* decompose InductivePLCAconsistency *)
Definition InductivePLCAPLconsistency :
forall{P : list Point}{L : list Line}{C : list Circuit}{A : list Area}{o : Circuit}, 
 I P L C A o -> PLconsistency P L :=
fun P L C A o I => match (InductivePLCAconsistency I) with
 | conj P _ => P
 end.

Definition InductivePLCALPconsistency :
forall{P : list Point}{L : list Line}{C : list Circuit}{A : list Area}{o : Circuit}, 
 I P L C A o -> LPconsistency P L :=
fun P L C A o I => match (InductivePLCAconsistency I) with
 | conj _ (conj P _) => P
 end.

Definition InductivePLCALCconsistency :
forall{P : list Point}{L : list Line}{C : list Circuit}{A : list Area}{o : Circuit}, 
 I P L C A o -> LCconsistency L C :=
fun P L C A o I => match (InductivePLCAconsistency I) with
 | conj _ (conj _ (conj P _)) => P
 end.

Definition InductivePLCACLconsistency :
forall{P : list Point}{L : list Line}{C : list Circuit}{A : list Area}{o : Circuit}, 
 I P L C A o -> CLconsistency L C :=
fun P L C A o I => match (InductivePLCAconsistency I) with
 | conj _ (conj _ (conj _ (conj P _))) => P
 end.

Definition InductivePLCACAconsistency :
forall{P : list Point}{L : list Line}{C : list Circuit}{A : list Area}{o : Circuit}, 
 I P L C A o -> CAconsistency C A o :=
fun P L C A o I => match (InductivePLCAconsistency I) with
 | conj _ (conj _ (conj _ (conj _ (conj P _)))) => P
 end.

Definition InductivePLCAACconsistency :
forall{P : list Point}{L : list Line}{C : list Circuit}{A : list Area}{o : Circuit}, 
 I P L C A o -> ACconsistency C A :=
fun P L C A o I => match (InductivePLCAconsistency I) with
 | conj _ (conj _ (conj _ (conj _ (conj _ (conj P _))))) => P
 end.

Definition InductivePLCAoutermostconsistency1 :
forall{P : list Point}{L : list Line}{C : list Circuit}{A : list Area}{o : Circuit}, 
 I P L C A o -> Outermostconsistency1 C o :=
fun P L C A o I => match (InductivePLCAconsistency I) with
 | conj _ (conj _ (conj _ (conj _ (conj _ (conj _ (conj P _)))))) => P
 end.

Definition InductivePLCAoutermostconsistency2 :
forall{P : list Point}{L : list Line}{C : list Circuit}{A : list Area}{o : Circuit}, 
 I P L C A o -> Outermostconsistency2 A o :=
fun P L C A o I => match (InductivePLCAconsistency I) with
 | conj _ (conj _ (conj _ (conj _ (conj _ (conj _ (conj _ (conj P _))))))) => P
 end.

Definition InductivePLCACircuitconsistency :
forall{P : list Point}{L : list Line}{C : list Circuit}{A : list Area}{o : Circuit}, 
 I P L C A o -> Circuitconsistency C :=
fun P L C A o I => match (InductivePLCAconsistency I) with
 | conj _ (conj _ (conj _ (conj _ (conj _ (conj _ (conj _ (conj _ (conj P _)))))))) => P
 end.

Definition InductivePLCAAreaconsistency :
forall{P : list Point}{L : list Line}{C : list Circuit}{A : list Area}{o : Circuit}, 
 I P L C A o -> Areaconsistency A :=
fun P L C A o I => match (InductivePLCAconsistency I) with
 | conj _ (conj _ (conj _ (conj _ (conj _ (conj _ (conj _ (conj _ (conj _ (conj P _))))))))) => P
 end.

Definition InductivePLCADistinctP :
forall{P : list Point}{L : list Line}{C : list Circuit}{A : list Area}{o : Circuit}, 
 I P L C A o -> DistinctP P :=
fun P L C A o I => match (InductivePLCAconsistency I) with
 | conj _ (conj _ (conj _ (conj _ (conj _ (conj _ (conj _ (conj _ (conj _ (conj _ (conj P _)))))))))) => P
 end.

Definition InductivePLCADistinctL :
forall{P : list Point}{L : list Line}{C : list Circuit}{A : list Area}{o : Circuit}, 
 I P L C A o -> DistinctL L :=
fun P L C A o I => match (InductivePLCAconsistency I) with
 | conj _ (conj _ (conj _ (conj _ (conj _ (conj _ (conj _ (conj _ (conj _ (conj _ (conj _ (conj P _))))))))))) => P
 end.

Definition InductivePLCADistinctC :
forall{P : list Point}{L : list Line}{C : list Circuit}{A : list Area}{o : Circuit}, 
 I P L C A o -> DistinctC C :=
fun P L C A o I => match (InductivePLCAconsistency I) with
 | conj _ (conj _ (conj _ (conj _ (conj _ (conj _ (conj _ (conj _ (conj _ (conj _ (conj _ (conj _ (conj P _)))))))))))) => P
 end.

Definition InductivePLCADistinctA :
forall{P : list Point}{L : list Line}{C : list Circuit}{A : list Area}{o : Circuit}, 
 I P L C A o -> DistinctA A :=
fun P L C A o I => match (InductivePLCAconsistency I) with
 | conj _ (conj _ (conj _ (conj _ (conj _ (conj _ (conj _ (conj _ (conj _ (conj _ (conj _ (conj _ (conj _ P)))))))))))) => P
 end.


(* decompose InductivePLCAconstraints *)
Definition InductivePLCALconstraint :
forall{P : list Point}{L : list Line}{C : list Circuit}{A : list Area}{o : Circuit}, 
 I P L C A o -> Lconstraint L :=
fun P L C A o I => match (InductivePLCAconstraints I) with
 | conj P _ => P
 end.


Definition InductivePLCACconstraint :
forall{P : list Point}{L : list Line}{C : list Circuit}{A : list Area}{o : Circuit}, 
 I P L C A o -> Cconstraint C :=
fun P L C A o I => match (InductivePLCAconstraints I) with
 | conj _ (conj P _) => P
 end.


Definition InductivePLCAAconstraint :
forall{P : list Point}{L : list Line}{C : list Circuit}{A : list Area}{o : Circuit}, 
 I P L C A o -> Aconstraint A :=
fun P L C A o I => match (InductivePLCAconstraints I) with
 | conj _ (conj _ (conj P _))  => P
 end.

Definition InductivePLCAoutermostconstraints :
forall{P : list Point}{L : list Line}{C : list Circuit}{A : list Area}{o : Circuit}, 
 I P L C A o -> Circuit_constraints o :=
fun P L C A o I => match (InductivePLCAconstraints I) with
 | conj _ (conj _ (conj _ P))  => P
 end.

