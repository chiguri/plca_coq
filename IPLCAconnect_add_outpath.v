Require Import List.
Require Import EqIn Rotate SetRemove PLCAobject PLCAtrail PLCApath InductivePLCA.
Require Import PLCAconsistency PLCAplanar IPLCAconsistency.


Lemma IPLCAconnect_add_outpath :
forall{P : list Point}{L : list Line}{C : list Circuit}{A : list Area}{o : Circuit}(i : I P L C A o)
(o1 o2 : object)(x y z s e : Point)(ap : list Point)(al lxy lyz lzx : list Line)
(oeq : o = lxy ++ lyz ++ lzx),
   DistinctC C
-> DistinctA A
-> Outermostconsistency2 A o
-> PLCAconnect P L C A o1 o2
->
 (PLCAconnect (ap++P) ((y,s)::(e,z)::al++L)
 ((((s,y)::lyz)++((z,e)::(reverse_linelist al)))::(lxy++((y,s)::al)++((e,z)::lzx))::(set_remove eq_listline_dec (lxy ++ lyz ++ lzx) C))
 (((((s,y)::lyz)++((z,e)::(reverse_linelist al)))::nil)::A)
 o1 o2 /\ o1 <> o_circuit o /\ o2 <> o_circuit o)
\/
 (PLCAconnect (ap++P) ((y,s)::(e,z)::al++L)
 ((((s,y)::lyz)++((z,e)::(reverse_linelist al)))::(lxy++((y,s)::al)++((e,z)::lzx))::(set_remove eq_listline_dec (lxy ++ lyz ++ lzx) C))
 (((((s,y)::lyz)++((z,e)::(reverse_linelist al)))::nil)::A)
 (o_circuit (lxy++((y,s)::al)++((e,z)::lzx))) o2 /\ o1 = o_circuit o /\ o2 <> o_circuit o)
\/
 (PLCAconnect (ap++P) ((y,s)::(e,z)::al++L)
 ((((s,y)::lyz)++((z,e)::(reverse_linelist al)))::(lxy++((y,s)::al)++((e,z)::lzx))::(set_remove eq_listline_dec (lxy ++ lyz ++ lzx) C))
 (((((s,y)::lyz)++((z,e)::(reverse_linelist al)))::nil)::A)
 o1 (o_circuit (lxy++((y,s)::al)++((e,z)::lzx))) /\ o1 <> o_circuit o /\ o2 = o_circuit o)
\/
 (PLCAconnect (ap++P) ((y,s)::(e,z)::al++L)
 ((((s,y)::lyz)++((z,e)::(reverse_linelist al)))::(lxy++((y,s)::al)++((e,z)::lzx))::(set_remove eq_listline_dec (lxy ++ lyz ++ lzx) C))
 (((((s,y)::lyz)++((z,e)::(reverse_linelist al)))::nil)::A)
 (o_circuit (lxy++((y,s)::al)++((e,z)::lzx))) (o_circuit (lxy++((y,s)::al)++((e,z)::lzx)))
  /\ o1 = o_circuit o /\ o2 = o_circuit o).
Proof.
intros P L C A o i o1 o2 x y z s e ap al lxy lyz lzx oeq DC DA O2 PC.
induction PC as [ p l INP LIN IPL | l c LIN INC ILC | c a1 INC INA1 ICA | | ].
+left; split; [ | split; discriminate ].
  apply PLcon; [ apply in_app_iff; right; exact INP | right; right; apply in_or_app; right; exact LIN | exact IPL ].
+destruct eq_listline_dec with c o.
  subst; subst; right; right; left.
   split; [ | split; [ discriminate | auto ]].
    apply lin_app_or in ILC; destruct ILC as [ILC | ILC].
     apply LCcon.
      right; right; apply in_or_app; right; exact LIN.
      right; left; now auto.
      apply lin_or_app; left; exact ILC.
     apply lin_app_or in ILC; destruct ILC as [ILC | ILC].
      apply TRANS with (o_circuit (((s, y) :: lyz) ++ (z, e) :: reverse_linelist al)).
       apply LCcon.
        right; right; apply in_or_app; right; exact LIN.
        left; now auto.
        apply lin_or_app; left; right; exact ILC.
       apply TRANS with (o_line (y, s)).
        apply SYMME; apply LCcon.
         left; now auto.
         left; now auto.
         left; constructor.
          unfold reverse; simpl; now auto.
        apply LCcon.
         left; now auto.
         right; left; now auto.
         apply lin_or_app; right.
          apply lin_or_app; left; left; now constructor.
      apply LCcon.
       right; right; apply in_or_app; right; exact LIN.
       right; left; now auto.
       apply lin_or_app; right.
        apply lin_or_app; right; right; exact ILC.
  left; split; [ | split; [discriminate | ]].
   apply LCcon.
    right; right; apply in_or_app; right; exact LIN.
    subst; right; right; refine (in_set_remove _ _ _ _ _ INC).
     intro; apply n; symmetry; now auto.
    exact ILC.
   intro H; apply n; inversion H; now auto.
+destruct eq_listline_dec with c o.
  subst; subst; elim O2 with a1.
   exact INA1.
   apply in_rin; exact ICA.
  subst; left; split; [ | split; [ | discriminate ]].
   apply CAcon.
    right; right; refine (in_set_remove _ _ _ _ _ INC).
     intro; apply n; symmetry; now auto.
    right; exact INA1.
    exact ICA.
   intro H; apply n; inversion H; now auto.
+clear O2; destruct IHPC as [[IND [O1 O2]] | [[IND [O1 O2]] | [[IND [O1 O2]] | [IND [O1 O2]]]]].
  left; split; [ exact (SYMME _ _ _ _ _ _ IND) | split; [ exact O2 | exact O1 ]].
  do 2 right; left; split; [ exact (SYMME _ _ _ _ _ _ IND) | split; [ exact O2 | exact O1 ] ].
  right; left; split; [ exact (SYMME _ _ _ _ _ _ IND) | split; [ exact O2 | exact O1 ] ].
  do 3 right; split; [ exact (SYMME _ _ _ _ _ _ IND) | split; [ exact O2 | exact O1 ] ].
+clear O2; destruct IHPC1 as [[IND1 [O11 O12]] | [[IND1 [O11 O12]] | [[IND1 [O11 O12]] | [IND1 [O11 O12]]]]].
  destruct IHPC2 as [[IND2 [O21 O22]] | [[IND2 [O21 O22]] | [[IND2 [O21 O22]] | [IND2 [O21 O22]]]]].
   left; split; [ exact (TRANS _ _ _ _ _ _ _ IND1 IND2) | split; [ exact O11 | exact O22 ]].
   elimtype False; exact (O12 O21).
   do 2 right; left; split; [ exact (TRANS _ _ _ _ _ _ _ IND1 IND2) | split; [exact O11 | exact O22]].
   elimtype False; exact (O12 O21).
  destruct IHPC2 as [[IND2 [O21 O22]] | [[IND2 [O21 O22]] | [[IND2 [O21 O22]] | [IND2 [O21 O22]]]]].
   right; left; split; [ exact (TRANS _ _ _ _ _ _ _ IND1 IND2) | split; [exact O11 | exact O22]].
   elimtype False; exact (O12 O21).
   do 3 right; split; [ exact (TRANS _ _ _ _ _ _ _ IND1 IND2) | split; [exact O11 | exact O22]].
   elimtype False; exact (O12 O21).
  destruct IHPC2 as [[IND2 [O21 O22]] | [[IND2 [O21 O22]] | [[IND2 [O21 O22]] | [IND2 [O21 O22]]]]].
   elimtype False; exact (O21 O12).
   left; split; [ exact (TRANS _ _ _ _ _ _ _ IND1 IND2) | split; [exact O11 | exact O22]].
   elimtype False; exact (O21 O12).
   do 2 right; left; split; [ exact (TRANS _ _ _ _ _ _ _ IND1 IND2) | split; [exact O11 | exact O22]].
  destruct IHPC2 as [[IND2 [O21 O22]] | [[IND2 [O21 O22]] | [[IND2 [O21 O22]] | [IND2 [O21 O22]]]]].
   elimtype False; exact (O21 O12).
   right; left; split; [ exact (TRANS _ _ _ _ _ _ _ IND1 IND2) | split; [exact O11 | exact O22]].
   elimtype False; exact (O21 O12).
   do 3 right; split; [ exact (TRANS _ _ _ _ _ _ _ IND1 IND2) | split; [exact O11 | exact O22]].
Qed.
