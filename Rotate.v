Require Import List.
Require Import EqIn.
Import ListNotations.

Require Import Arith.


Section Rotate.
Context {A : Type}.

(* refl  : rotSame *)
(* sym   : Rot_sym *)
(* trans : Rot_trans *)
Inductive Rot : list A -> list A -> Prop :=
| rotSame : forall (l : list A), Rot l l
| rotNext : forall (l l' : list A) (a : A), Rot l (a :: l') -> Rot l (l' ++ [a]).

Hint Constructors Rot.

Lemma Rot_length : forall (l l' : list A), 
 Rot l l' -> length l = length l'.
intros; induction H.
 auto.
 simpl in *.
 rewrite app_length; simpl; rewrite plus_comm; auto.
Qed.

Lemma Rot_length' : forall (l l' : list A),
 length l <> length l' -> ~ Rot l l'.
intros.
intro.
apply H; apply Rot_length; auto.
Qed.

Lemma Not_Rot_nil : forall (a : A) (l : list A),
  ~ Rot [] (a :: l).
intros.
intro H; apply (Rot_length' [] (a :: l)).
 intro H0; inversion H0.
 auto.
Qed.

Lemma Not_Rot_nil_app : forall (a : A) (l : list A),
  ~ Rot [] (l ++ [a]).
intros; intro H.
apply (Rot_length' [] (l ++ [a])).
 intro H0; rewrite app_length in H0; rewrite plus_comm in H0; inversion H0.
 auto.
Qed.

Lemma Rot_nil_nil_l : forall (l : list A),
 Rot nil l -> l = nil.
intros.
destruct l.
 auto.
 apply Rot_length in H.
  inversion H.
Qed.
Lemma Rot_nil_nil_r : forall (l : list A),
 Rot l nil -> l = nil.
intros.
destruct l.
 auto.
 apply Rot_length in H.
  inversion H.
Qed.


Lemma Rot_trans : forall (l1 l2 l3 : list A),
 Rot l1 l2 -> Rot l2 l3 -> Rot l1 l3.
intros l1 l2 l3 H H0; revert l1 H; induction H0; intros.
 auto.
 constructor; auto.
Qed.

Lemma Rot_inv_l : forall (a : A) (l l' : list A),
 Rot (l ++ [a]) l' -> Rot (a :: l) l'.
intros.
apply Rot_trans with (l2 := l ++ [a]).
 repeat constructor.
 auto.
Qed.

Lemma Rot_app_r : forall (l l1 l2 : list A),
 Rot l (l1 ++ l2) -> Rot l (l2 ++ l1).
intros l l1; induction l1; intros.
 simpl in H; rewrite app_nil_r; auto.
 simpl in H; apply rotNext in H.
  rewrite <- app_assoc in H.
   apply IHl1 in H.
    rewrite <- app_assoc in H; auto.
Qed.

Lemma Rot_app : forall (l l' : list A),
 Rot (l ++ l') (l' ++ l).
intros; apply Rot_app_r; constructor.
Qed.


(* for special case of app *)
Lemma app_one_tail : forall (a a' : A) l l',
  l ++ [a] = l' ++ [a'] -> l = l' /\ a = a'.
induction l; intros.
 simpl in H; destruct l'.
  simpl in H; inversion H; auto.
  simpl in H; inversion H.
   elim (app_cons_not_nil l' [] a'); auto.
 destruct l'; simpl in H.
  inversion H.
   elim (app_cons_not_nil l [] a); symmetry; auto.
  inversion H.
   apply IHl in H2.
    inversion H2.
     split; [ f_equal | idtac ]; auto.
Qed.


Lemma Rot_inv_r : forall (a : A) (l l' : list A),
 Rot l (l' ++  [a]) -> Rot l (a :: l').
intros.
inversion H.
apply Rot_trans with (l2 := [a] ++ l').
 apply Rot_app.
 auto.
destruct (app_one_tail a0 a l'0 l'); auto.
 rewrite <- H3; rewrite <- H4; auto.
Qed.

Lemma Rot_sym : forall (l l' : list A),
 Rot l l' -> Rot l' l.
intros; induction H.
auto.
apply Rot_trans with (l2 := [a] ++ l').
 apply Rot_app.
 auto.
Qed.



Lemma RotIn : forall (a : A) (l l' : list A), In a l -> Rot l l' -> In a l'.
intros.
induction H0.
auto.
apply IHRot in H.
apply in_or_app; simpl; destruct H as [ H | H ].
 rewrite H; auto.
 auto.
Qed.

Lemma RotIn_r : forall (a : A) (l l' : list A), In a l' -> Rot l l' -> In a l.
intros.
apply Rot_sym in H0.
eapply RotIn; eauto.
Qed.


Lemma Rot_split : forall l1 l2 : list A,
 Rot l1 l2 -> exists l3 l4 : list A, l1 = l3 ++ l4 /\ l2 = l4 ++ l3.
intros; induction H.
 exists [], l; split.
  simpl; now auto.
  rewrite app_nil_r; now auto.
 destruct IHRot as [l3 [l4 [H1 H2]]].
 destruct l4.
  simpl in H2; destruct l3.
   inversion H2.
   inversion H2; subst.
    exists [a0], l3; split.
     rewrite app_nil_r; simpl; now auto.
     now auto.
  inversion H2; subst.
   exists (l3 ++ [a0]), l4; split.
    rewrite <- app_assoc.
     simpl; now auto.
    rewrite app_assoc.
     now auto.
Qed.
 


Lemma Rot_divide : forall (l l1 l2 : list A),
  Rot (l1 ++ l2) l ->
    (exists l3 l4 : list A, l2 = l3 ++ l4 /\ l = l4 ++ l1 ++ l3)
    \/ (exists l3 l4 : list A, l1 = l3 ++ l4 /\ l = l4 ++ l2 ++ l3).
intros.
remember (l1 ++ l2).
induction H.
 left; exists l2, []; simpl; rewrite app_nil_r; auto.
 destruct (IHRot Heql0) as [[l3 [l4 [Hl Hr]]] | [l3 [l4 [Hl Hr]]]]; clear IHRot.
  destruct l4; simpl in Hr.
   destruct l1; simpl in Hr.
    destruct l3; simpl in Hr.
     inversion Hr.
     inversion Hr; subst.
      left; exists [a0], l3; simpl; split.
       rewrite app_nil_r; reflexivity.
       auto.
    inversion Hr; subst.
     right; exists [a0], l1; simpl; split.
      auto.
      rewrite app_nil_r; rewrite app_assoc; auto.
   inversion Hr; subst.
    left; exists (l3 ++ [a0]), l4; simpl; split.
     rewrite <- app_assoc; simpl; auto.
     repeat (rewrite <- app_assoc); auto.
  destruct l4; simpl in Hr.
   destruct l2; simpl in Hr.
    destruct l3; simpl in Hr.
     inversion Hr.
     inversion Hr; subst.
      right; exists [a0], l3; simpl; split.
       rewrite app_nil_r; reflexivity.
       auto.
    inversion Hr; subst.
     left; exists [a0], l2; simpl; split.
      auto.
      rewrite app_nil_r; rewrite app_assoc; auto.
   inversion Hr; subst.
    right; exists (l3 ++ [a0]), l4; simpl; split.
     rewrite <- app_assoc; simpl; auto.
     repeat (rewrite <- app_assoc); auto.
Qed.




Section rotate_function.

Definition rot1 (l : list A) :=
match l with
| nil => nil
| a :: l' => l' ++ [a]
end.

Fixpoint rot_list n l :=
match n with
| O => nil
| S n' => l :: rot_list n' (rot1 l)
end.

Definition rot_set l := rot_list (length l) l.

Lemma rot1_Rot : forall l, Rot l (rot1 l).
intros.
destruct l; simpl; auto.
Qed.

Hint Resolve rot1_Rot.

Theorem rot_list_Rot : forall n l l', In l' (rot_list n l) -> Rot l l'.
induction n; simpl; intros.
 inversion H.
 destruct H.
  rewrite H; auto.
  apply IHn in H.
   apply Rot_trans with (l2 := rot1 l); auto.
Qed.

Lemma rot_set_Rot : forall l l', In l' (rot_set l) -> Rot l l'.
unfold rot_set; intro l; apply (rot_list_Rot (length l)).
Qed.




Fixpoint rotate n (l : list A) :=
match n with
| O => l
| S n' => rotate n' (rot1 l)
end.

Lemma rotate_m_n : forall m n (l : list A),
 rotate m (rotate n l) = rotate (m + n) l.
intros m n; revert m; induction n; intros; simpl.
 rewrite <- plus_n_O; auto.
 rewrite <- plus_n_Sm; simpl; auto.
Qed.

Lemma rotate_inv : forall n a (l : list A),
 rotate n (l ++ [a]) = rotate (S n) (a :: l).
auto.
Qed.


Lemma rotate_1_inv : forall a l,
 l ++ [a] = rotate 1 (a :: l).
auto.
Qed.

Lemma Rot_rotate : forall n (l : list A),
  Rot l (rotate n l).
induction n; intros.
simpl; auto.
destruct l; simpl.
 auto.
 apply Rot_trans with (l2 := l ++ [a]).
  auto.
  auto.
Qed.


Lemma Rot_rotate' : forall m n (l : list A),
 Rot (rotate m l) (rotate (m + n) l).
induction m; intros; simpl.
 apply Rot_rotate.
 auto.
Qed.

Lemma rotate_Rot : forall (l l' : list A),
 Rot l l' -> exists n : nat, l' = rotate n l.
intros.
induction H.
 exists 0; auto.
 destruct IHRot.
  exists (S x).
   rewrite rotate_1_inv.
    rewrite H0.
     rewrite rotate_m_n.
      auto.
Qed.

Lemma rotate_0 : forall l, l = rotate 0 l.
auto.
Qed.

Lemma rotate_app_length : forall l l', rotate (length l) (l ++ l') = l' ++ l.
induction l; simpl; intros.
 rewrite app_nil_r; auto.
 rewrite <- app_assoc.
  rewrite IHl.
   rewrite <- app_assoc.
    auto.
Qed.


Lemma rotate_length_eq : forall l, rotate (length l) l = l.
intros; rewrite <- app_nil_l.
 rewrite <- rotate_app_length.
  rewrite app_nil_r; auto.
Qed.



Variable Aeq_dec : forall a a' : A, {a = a'} + {a <> a'}.
Lemma Alist_dec : forall l l' : list A, { l = l' } + { l <> l' }.
intros; decide equality.
Qed.


Inductive Rot' l : list A -> Prop :=
| rot' : forall n, n < length l -> Rot' l (rotate n l).


Lemma Rot_Rot' : forall l l', l <> nil -> Rot l l' -> Rot' l l'.
intros.
 induction H0.
  destruct l.
   destruct H; auto.
   apply rot' with (n := 0).
    apply neq_0_lt.
     discriminate.
  apply IHRot in H.
   remember (a :: l') as l0 in H.
   clear IHRot; destruct H.
    unfold lt in H.
     destruct (le_lt_or_eq (S n) (length l) H).
      assert (rotate (S n) l = l' ++ [a]).
       rewrite rotate_1_inv.
        rewrite <- Heql0; rewrite rotate_m_n; auto.
       rewrite <- H2; apply rot' with (n := S n); auto.
      assert (l' ++ [a] = l).
       rewrite rotate_1_inv.
        rewrite <- Heql0; rewrite rotate_m_n.
         simpl plus; rewrite H1; apply rotate_length_eq.
       rewrite H2; rewrite rotate_0; constructor.
        rewrite <- H2; apply neq_0_lt.
         rewrite app_length; rewrite plus_comm; discriminate.
Qed.

Lemma Rot'_Rot : forall l l', Rot' l l' -> Rot l l'.
intros.
 destruct H.
  apply Rot_rotate.
Qed.

Lemma Rots_are_same : forall l l', l <> nil -> (Rot l l' <-> Rot' l l').
intros.
split.
apply Rot_Rot'; auto.
apply Rot'_Rot.
Qed.


Lemma lt_S_n : forall m n, S m < S n -> m < n.
intros.
unfold lt in *.
apply le_S_n; auto.
Qed.


Lemma rot_list_has_rotated_list : forall m n l, m < n -> In (rotate m l) (rot_list n l).
induction m; intros; simpl.
 destruct n.
  inversion H.
   simpl; auto.
 destruct n.
  inversion H.
   simpl; right; apply IHm.
    apply lt_S_n; auto.
Qed.


Lemma rotate_length : forall n l, length (rotate n l) = length l.
induction n; simpl; intros.
 auto.
 destruct l; simpl.
  rewrite IHn; auto.
  rewrite IHn.
   rewrite app_length; rewrite plus_comm; auto.
Qed.


Theorem Rot'_In_rot_set : forall l l', Rot' l l' -> In l' (rot_set l).
intros.
 destruct H; unfold rot_set.
  apply rot_list_has_rotated_list.
   auto.
Qed.



Theorem Rot_In_rot_set : forall l l', l <> nil -> Rot l l' -> In l' (rot_set l).
intros.
 destruct l.
  contradict H; auto.
  apply (Rot_Rot' (a :: l) l' H) in H0.
   apply Rot'_In_rot_set; auto.
Qed.



Lemma rot_list_has_rotated_list_inv : forall n l l', In l' (rot_list n l) -> exists m, l' = rotate m l.
induction n; intros.
simpl in H.
 inversion H.
 simpl in H.
  destruct H.
   exists O; auto.
   apply IHn in H.
    destruct H.
     exists (S x).
      auto.
Qed.

Lemma In_rot_set_Rot : forall l l', In l' (rot_set l) -> Rot l l'.
intros.
 apply rot_list_has_rotated_list_inv in H.
  destruct H.
   rewrite H; apply Rot_rotate.
Qed.



Lemma rot_set_dec : forall l l', { In l' (rot_set l) } + { ~ In l' (rot_set l) }.
intros.
 apply (in_dec Alist_dec l' (rot_set l)).
Qed.


Theorem Rot_dec : forall (l l' : list A), { Rot l l' } + { ~ Rot l l' }.
intros.
 destruct (rot_set_dec l l').
  left; apply In_rot_set_Rot; auto.
  destruct l.
   destruct l'.
    auto.
    right; apply Not_Rot_nil.
   right; intro H.
    apply n.
     apply Rot_In_rot_set.
      intro H0; inversion H0.
      auto.
Qed.


End rotate_function.

End Rotate.
