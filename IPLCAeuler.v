Require Import List Arith Omega.
Require Import Rotate SetRemove PLCAobject PLCApath InductivePLCA PLCAconsistency PLCAplanar.
Require Import IPLCAconsistency.


Lemma C_S_remove :
forall(C : list Circuit)(c : Circuit),
   In c C
-> S (length (set_remove eq_listline_dec c C)) = length C.
Proof.
intros C c INC.
induction C; simpl in *.
 elimtype False; exact INC.
 destruct INC as [INC | INC]; [subst | ].
 destruct (eq_listline_dec c c) as [EQ | NEQ]; [ exact (eq_refl _) | elimtype False; exact (NEQ (eq_refl _)) ].
 destruct (eq_listline_dec c a) as [EQ | NEQ]; [ exact (eq_refl _) | simpl ].
 rewrite (IHC INC); exact (eq_refl _).
Qed.


Lemma A_S_remove :
forall(A : list Area)(a : Area),
   In a A
-> S (length (set_remove eq_listcircuit_dec a A)) = length A.
Proof.
intros A a INA.
induction A; simpl in *.
 elimtype False; exact INA.
 destruct INA as [INA | INA]; [subst | ].
 destruct (eq_listcircuit_dec a a) as [EQ | NEQ]; [ exact (eq_refl _) | elimtype False; exact (NEQ (eq_refl _)) ].
 destruct (eq_listcircuit_dec a a0) as [EQ |NEQ]; [ exact (eq_refl _) | simpl ].
 rewrite (IHA INA); exact (eq_refl _).
Qed.


Theorem IPLCA_is_satisfied_PLCAeuler :
forall(P : list Point)(L : list Line)(C : list Circuit)(A : list Area)(o : Circuit),
 I P L C A o -> PLCAeuler P L C A.
Proof.
unfold PLCAeuler.
intros P L C A o I.
induction I.
 (* single_loop *)
 assert(length pl - length ll = 1).
  clear -H.
  induction H; simpl in *; omega.
 simpl; omega.
 (* add_inline *)
 simpl length; rewrite A_S_remove; [ | exact H].
 rewrite C_S_remove; [ | exact (InductivePLCAACconsistency I _ _ H H0) ].
 omega.
 (* add_inpath *)
 assert(length ap - length al = 1).
  clear -H4.
  induction H4; simpl in *; omega.
 simpl length.
 rewrite A_S_remove; [ | exact H].
 rewrite C_S_remove; [ | exact (InductivePLCAACconsistency I _ _ H H0) ].
 simpl in *.
 repeat (rewrite app_length).
 omega.
(* add_outpath *)
 assert(length ap - length al = 1).
  clear -H4.
  induction H4; simpl in *; omega.
 simpl length; rewrite C_S_remove; [ | exact H].
 repeat (rewrite app_length).
 omega.
 (* add_loop *)
 assert(length ap - length al = 1).
  clear -H0.
  induction H0; simpl in *; omega.
 simpl length.
 rewrite A_S_remove; auto.
 repeat (rewrite app_length).
 omega.
Qed.
