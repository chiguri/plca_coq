Require Import List Arith Permutation.
Require Import EqIn Rotate SetRemove PLCAobject PLCAtrail PLCApath.
Require Import PLCAconstraints PLCAconsistency PLCAplanar.
Require Import ReplaceCircuit.
Require Import PLCAconnectpath.



(* Equivalence of PLCA  *)
Inductive PLCAequivalence : list Point -> list Line -> list Circuit -> list Area -> Circuit ->
                            list Point -> list Line -> list Circuit -> list Area -> Circuit -> Prop :=
| permutePLCA : forall(P P' : list Point)(L L' : list Line)(C C' : list Circuit)(A A' : list Area)(o : Circuit),
                Permutation P P' 
             -> Permutation L L'
             -> Permutation C C'
             -> Permutation A A'
             -> PLCAequivalence P L C A o P' L' C' A' o

| reverseLine : forall(P : list Point)(L : list Line)(C : list Circuit)(A : list Area)(o : Circuit)(l l' : Line),
                eq_ul l l'
             -> PLCAequivalence P (l::L) C A o P (l'::L) C A o


| permuteArea : forall(P : list Point)(L : list Line)(C : list Circuit)(A : list Area)(o : Circuit)(a a' : Area),
                Permutation a a'
             -> PLCAequivalence P L C (a::A) o P L C (a'::A) o

| rotateCircuit : forall(P : list Point)(L : list Line)(C : list Circuit)(A : list Area)(o c c' : Circuit),
                   In c C
                -> c <> o
                -> Rot c c'
                -> PLCAequivalence P L C A o P L (map (replace_circuit c c') C)
                                            (map (map (replace_circuit c c')) A) o

| rotateOutermost : forall(P : list Point)(L : list Line)(C : list Circuit)(A : list Area)(o o' : Circuit),
                     Rot o o'
                  -> PLCAequivalence P L C A o P L (map (replace_circuit o o') C) A o'

| PLCAtrans : forall(P P' P'' : list Point)(L L' L'' : list Line)(C C' C'' : list Circuit)(A A' A'' : list Area)(o o' o'' : Circuit),
              PLCAequivalence P L C A o P' L' C' A' o'
           -> PLCAequivalence P' L' C' A' o' P'' L'' C'' A'' o''
           -> PLCAequivalence P L C A o P'' L'' C'' A'' o''.




Lemma PLCAeq_refl :
forall{P : list Point}{L : list Line}{C : list Circuit}{A : list Area}{o : Circuit},
 PLCAequivalence P L C A o P L C A o.
Proof.
intros P L C A o.
apply permutePLCA.
 exact (Permutation_refl _).
 exact (Permutation_refl _).
 exact (Permutation_refl _).
 exact (Permutation_refl _).
Qed.



(* To convert objects between equivalence relations *)
Definition rev_l_object (l : Line) (o : object) :=
 match o with
 | o_line l' =>
    o_line (if eq_dline_dec (reverse l) l' then l else l')
 | _ => o
 end.


Definition replace_c_object (c c' : Circuit) (o : object) :=
 match o with
 | o_circuit c'' =>
    o_circuit (if eq_listline_dec c c'' then c' else c'')
 | _ => o
 end.


Definition replace_a_object (a a' : Area) (o : object) :=
 match o with
 | o_area a'' =>
    o_area (if eq_listcircuit_dec a a'' then a' else a'')
 | _ => o
 end.


Ltac split_n n :=
 match n with
 | O => idtac
 | S ?n' => split; [ | split_n n']
 end.



Lemma PLCAeq_lengthA :
  forall {P P' : list Point} {L L' : list Line} {C C' : list Circuit} {A A' : list Area} {o o' : Circuit},
   PLCAequivalence P L C A o P' L' C' A' o'
   -> length A = length A'.
intros.
induction H.
+apply Permutation_length; auto.
+auto.
+simpl; auto.
+rewrite map_length; auto.
+auto.
+rewrite IHPLCAequivalence1; auto.
Qed.



Theorem PLCAeq_planar_except_for_connect :
  forall {P P' : list Point} {L L' : list Line} {C C' : list Circuit} {A A' : list Area} {o o' : Circuit},
    PLCAconstraints L C A o /\ PLCAconsistency P L C A o /\ PLCAeuler P L C A
    -> PLCAequivalence P L C A o P' L' C' A' o'
    -> PLCAconstraints L' C' A' o' /\ PLCAconsistency P' L' C' A' o' /\ PLCAeuler P' L' C' A'.
Proof with eauto.
intros P P' L L' C C' A A' o o'.
intro x.
destruct x as [[?LT [?CT [?AT ?OT]]]
               [[?PL [?LP [?LC [?CL [?CA [?AC [?O1 [?O2 [?CC [?AA [?DP [?DL [?DC ?DA]]]]]]]]]]]]]
               ?PLCAe]].
intro PLCAEQ.
induction PLCAEQ as
[ P P' L L' C C' A A' o PermP PermL PermC PermA
| P L C A o l l' eql
| P L C A o a a' eqa
| P L C A o c c' Ic Noc rotc
| P L C A o o' roto
| P P' P'' L L' L'' C C' C'' A A' A'' o o' o'' PLCAEQ1 IHPLCAEQ1 PLCAEQ2 IHPLCAEQ2].
(* permutePLCA *)
+split_n 2.
 (* PLCAconstraints *)
 -split_n 3.
  (* Lconstraint *)
  *unfold Lconstraint in *.
   intros l H; apply LT.
   apply Permutation_in with L'.
    apply Permutation_sym; exact PermL.
    exact H.
  (* Cconstraint *)
  *unfold Cconstraint in *.
   intros c H; apply CT.
   apply Permutation_in with C'.
    apply Permutation_sym; exact PermC.
    exact H.
  (* Aconstraint *)
  *unfold Aconstraint in *.
   intros a H; apply AT.
   apply Permutation_in with A'.
    apply Permutation_sym; exact PermA.
    exact H.
  (* outermost *)
  *exact OT.
 (* PLCAconsistency *)
 -split_n 13.
  *unfold PLconsistency in *.
   intros p H.
   assert (In p P).
    apply Permutation_in with P'.
    apply Permutation_sym; exact PermP.
    exact H.
   assert (H' := PL p H0).
   destruct H' as [l [H1 H2]].
   exists l; split.
    apply Permutation_in with L.
    exact PermL.
    exact H1.
    exact H2.
  *unfold LPconsistency in *.
   intros p l H H0.
   apply Permutation_in with P.
    exact PermP.
    apply LP with l.
     apply Permutation_in with L'.
      apply Permutation_sym; exact PermL.
      exact H.
     exact H0.
  *unfold LCconsistency in *.
   intros l H.
   assert (LIn l L) as H0.
    apply permutation_lin with L'.
     apply Permutation_sym; exact PermL.
     exact H.
   destruct (LC l H0) as [c [H1 H2]].
   exists c; split.
    exact H1.
    apply Permutation_in with C.
     exact PermC.
     exact H2.
  *unfold CLconsistency in *.
   intros l c H H0.
   apply permutation_lin with L.
    exact PermL.
    apply CL with c.
     apply Permutation_in with C'.
      apply Permutation_sym; exact PermC.
      exact H.
     exact H0.
  *unfold CAconsistency in *.
   intros c H H0.
   assert (In c C) as H1.
    apply Permutation_in with C'.
     apply Permutation_sym; exact PermC.
     exact H.
   destruct (CA c H1 H0) as [a [H2 H3]].
    exists a; split.
     exact H2.
     apply Permutation_in with A.
      exact PermA.
      exact H3.
  *unfold ACconsistency in *.
   intros c a H H0.
   apply Permutation_in with C.
    exact PermC.
    apply AC with a.
    apply Permutation_in with A'.
     apply Permutation_sym; exact PermA.
     exact H.
    exact H0.
  *unfold Outermostconsistency1 in *.
   apply Permutation_in with C.
    exact PermC.
    exact O1.
  *unfold Outermostconsistency2 in *.
   intros a H.
   apply O2.
   apply Permutation_in with A'.
    apply Permutation_sym; exact PermA.
    exact H.
  *unfold Circuitconsistency.
   intros l c1 c2 H H0.
   apply Permutation_sym in PermC.
   apply CC.
    apply Permutation_in with C'.
     exact PermC.
     exact H.
    apply Permutation_in with C'.
     exact PermC.
     exact H0.
  *unfold Areaconsistency.
   intros c a1 a2 H H0.
   apply Permutation_sym in PermA.
   apply AA.
    apply Permutation_in with A'.
     exact PermA.
     exact H.
    apply Permutation_in with A'.
     exact PermA.
     exact H0.
  *unfold DistinctP.
   intros p H H0.
   apply DP with p.
    apply Permutation_in with P'.
     apply Permutation_sym; exact PermP.
     exact H.
    apply eq_eqin_in in H0.
    apply eq_eqin_in.
    apply eqin_set_remove_permutation with P'; intuition.
  *unfold DistinctL.
   intros l H H0.
   apply DL with l.
    apply Permutation_in with L'.
     apply Permutation_sym; exact PermL.
     exact H.
    apply lin_eq_set_remove_permutation with L'.
     apply Permutation_sym; auto.
     auto.
  *unfold DistinctC.
   intros c H H0.
   apply DC with c.
    apply Permutation_in with C'.
     apply Permutation_sym; exact PermC.
     exact H.
    apply rin_eq_set_remove_permutation with C'.
     apply Permutation_sym; auto.
     auto.
  *unfold DistinctA.
   intros a H H0.
   apply DA with a.
    apply Permutation_in with A'.
     apply Permutation_sym; exact PermA.
     exact H.
    apply ain_eq_set_remove_permutation with A'.
     apply Permutation_sym; auto.
     auto.
 -unfold PLCAeuler.
  rewrite <- (Permutation_length PermP).
  rewrite <- (Permutation_length PermL).
  rewrite <- (Permutation_length PermC).
  rewrite <- (Permutation_length PermA).
  exact PLCAe.
(* reverseLine *) (* from here, use automation for convenience rather than readability *)
+inversion eql; subst.
 unfold PLCAconstraints, PLCAconsistency, PLCAeuler in *.
 intuition.
 split_n 2.
 -split_n 3...
  unfold Lconstraint.
  intros.
  destruct H as [H | H].
   subst.
   unfold Line_constraint.
   destruct l; unfold reverse; simpl.
   intro.
   apply LT with (p, p0); simpl...
  apply LT.
  right...
 -split_n 13...
  *unfold PLconsistency.
   intros.
   apply PL in H.
   destruct H as [l' [H H0]].
   destruct H as [E | H].
    exists (reverse l).
    subst; split; simpl...
    destruct l'; unfold reverse; simpl in *; inversion H0...
    exists l'; split...
    right...
  *unfold LPconsistency.
   intros.
   destruct H as [H | H].
    subst; apply LP with l.
     left...
     destruct l; unfold reverse in H0; simpl in H0; inversion H0; subst...
    apply LP with l0.
     right...
     auto.
  *unfold LCconsistency.
   intros.
   apply LC.
   destruct H as [H | H].
    left; inversion H.
     apply reverse_ul.
     apply reverse_reverse.
     destruct l, l0; unfold reverse in H0; simpl in H0; inversion H0; constructor.
    right...
  *unfold CLconsistency.
   intros.
   destruct CL with l0 c...
    left; inversion H1.
     constructor...
     subst; rewrite reverse_reverse; now apply ul_refl.
   right...
  *unfold DistinctL.
   intros l' H H0.
   destruct H.
    subst; apply DL with l.
     simpl...
     simpl in *.
     destruct eq_dline_dec with l l.
      destruct eq_dline_dec with (reverse l) (reverse l).
       apply LIn_reverse_inv...
       elim n...
      elim n...
    apply DL with l'.
     right...
     simpl.
      destruct eq_dline_dec with l' l.
       apply in_lin...
       simpl in H0.
       destruct eq_dline_dec with l' (reverse l).
        left.
        constructor.
         destruct l, l'; simpl in *; inversion e; subst...
        destruct H0.
         inversion H0; subst.
          elim n0...
          destruct l, l'; simpl in *; inversion H1; subst; elim n...
         right...
 -unfold PLCAeuler in *.
   simpl length in *.
   rewrite <- PLCAe...
(* permuteArea *)
+unfold PLCAconstraints, PLCAconsistency, PLCAeuler in *.
 apply Permutation_sym in eqa.
 intuition.
 -unfold Aconstraint in *.
  intros a0 H.
  destruct H; [ subst | apply AT; simpl; auto ].
  assert (Area_constraints a).
   apply AT; simpl...
  unfold Area_constraints in *.
  destruct H as [ H [ H0 H1 ] ].
  split.
   intros c1 c2 H2 H3 H4.
   apply (Permutation_in _ eqa) in H2.
   apply (Permutation_in _ eqa) in H3.
   now apply (H c1 c2 H2 H3 H4).
   intuition.
    apply H0 with c.
     apply Permutation_in with a0...
     apply rin_eq_set_remove_permutation with a0; auto.
    rewrite Permutation_length with _ a0 a...
 -unfold CAconsistency in *.
  intros.
  destruct (CA c H H0) as [x [H1 H2]].
  destruct H2 as [ H2 | H2 ]; [ exists a'; subst | exists x].
   intuition.
   apply Permutation_sym in eqa; apply Permutation_in with x...
   intuition.
 -unfold ACconsistency in *.
  intros c a0 H H0; destruct H; [ apply AC with a; simpl; subst | apply AC with a0; simpl ]...
   apply Permutation_in with a0...
 -unfold Outermostconsistency2 in *.
  intros a0 H H0; destruct H as [H | H]; [ apply O2 with a | apply O2 with a0 ]; simpl...
  apply permutation_rin with a0; subst...
 -unfold Areaconsistency in *.
  intros c a1 a2 HIn1 HIn2 HIna1 HIna2.
  destruct HIn1 as [HIn1 | HIn1]; destruct HIn2 as [HIn2 | HIn2]; subst.
  *auto.
  *apply (Permutation_in _ eqa) in HIna1.
   elim DA with a.
    left...
    simpl.
    destruct eq_listcircuit_dec with a a.
     rewrite AA with c a a2; simpl...
      apply in_ain...
     elim n...
  *apply (Permutation_in _ eqa) in HIna2.
   elim DA with a.
    left...
    simpl.
    destruct eq_listcircuit_dec with a a.
     rewrite AA with c a a1; simpl...
      apply in_ain...
     elim n...
  *apply AA with c; try right...
 -unfold DistinctA in *.
  intros a0 H HA.
  simpl in HA.
  destruct H.
   subst.
   destruct eq_listcircuit_dec with a0 a0.
    apply DA with a.
     left...
     simpl; destruct eq_listcircuit_dec with a a.
      apply eq_ain_elem with a0...
      apply permute_area...
      elim n...
    elim n...
   destruct eq_listcircuit_dec with a0 a'.
    subst.
    apply DA with a'.
     right...
     simpl; destruct eq_listcircuit_dec with a' a.
      auto.
      left; apply permute_area...
    apply DA with a0.
     right...
     simpl; destruct eq_listcircuit_dec with a0 a.
      apply in_ain...
      destruct HA.
       left.
       apply trans_area with a'...
        apply permute_area...
       right...
(* rotateCircuit *)
+unfold PLCAconstraints, PLCAconsistency, PLCAeuler in *.
 destruct (in_split _ _ Ic) as [C1 [C2 H]]; subst.
 assert (H := ReplaceCircuit_C _ _ _ c' DC).
 rewrite H in *; clear H.
 destruct (CA c) as [a [H H0]].
  apply in_or_app; right; left...
  auto.
 destruct (in_split _ _ H0) as [A1 [A2 H1]].
 destruct (in_split _ _ H) as [a1 [a2 H2]].
 clear H0 H; subst.
 assert (H := ReplaceCircuit_A _ _ _ _ _ c' DA AA AT).
(* We cannot "rewrite" goal (too big to infer?). So we explicitly declare where/how to rewrite. *)
 refine (eq_ind_r (fun x => (Lconstraint L /\
 Cconstraint (C1 ++ c' :: C2) /\
 Aconstraint x /\
 Circuit_constraints o) /\
 (PLconsistency P L /\
  LPconsistency P L /\
  LCconsistency L (C1 ++ c' :: C2) /\
  CLconsistency L (C1 ++ c' :: C2) /\
  CAconsistency (C1 ++ c' :: C2) x o /\
  ACconsistency (C1 ++ c' :: C2) x /\
  Outermostconsistency1 (C1 ++ c' :: C2) o /\
  Outermostconsistency2 x o /\
  Circuitconsistency (C1 ++ c' :: C2) /\
  Areaconsistency x /\
  DistinctP P /\
  DistinctL L /\
  DistinctC (C1 ++ c' :: C2) /\
  DistinctA x) /\
 length P + 2 * length x = length L + length (C1 ++ c' :: C2)) _ H).
 clear H; intuition.
 -unfold Cconstraint in *. 
  intros c0 H.
  apply in_app_or in H; destruct H.
   apply CT.
   apply in_or_app; left...
  destruct H; [ subst | apply CT, in_or_app; right; right; assumption ].
  unfold Circuit_constraints.
  assert (Circuit_constraints c).
   apply CT; apply in_or_app; right; left...
  destruct H as [[x [pl Hx1]] Hx2].
  split; [ | rewrite <- (Rot_length _ _ rotc); auto ].
  clear -rotc Hx1.
  induction rotc.
  exists x; exists pl...
  apply IHrotc in Hx1; clear -Hx1.
  destruct Hx1 as [x [pl Hx]].
  inversion Hx; subst.
  inversion H1; subst.
   exists x; exists (x :: x :: nil); simpl...
  exists p2; exists ((p2 :: pl) ++ p2 :: nil).
  apply reverse_step_trail...
 -unfold Aconstraint in *.
  intros a H.
  apply in_app_or in H; destruct H.
  apply AT.
   apply in_or_app; left...
  destruct H; [ | apply AT; apply in_or_app; right; right; auto].
  assert (Area_constraints (a1 ++ c :: a2)).
   apply AT.
   apply in_or_app; right; left...
  unfold Area_constraints in *.
  destruct H0 as [H0 [H1 H2]].
  subst; split_n 2; intros.
  *apply in_app_or in H; apply in_app_or in H3.
   destruct H as [H | [H | H]].
    destruct H3 as [H3 | [H3 | H3]].
     destruct (H0 c1 c2); auto.
      apply in_or_app; left...
      apply in_or_app; left...
     destruct (H0 c1 c).
      apply in_or_app; left...
      apply in_or_app; right; left...
      intro H5.
      destruct (AT (a1 ++ c :: a2)) as [_ [H6 _]].
       apply in_or_app; right; left...
       apply H6 with c1; subst.
        apply in_or_app; right; left...
        rewrite rin_distinct_setremove.
         apply rin_or_app; left; apply in_rin; auto.
         destruct (AT (a1 ++ c :: a2)).
          apply in_or_app; right; left; auto.
          destruct H5.
          apply H5.
          apply in_or_app; right; left; auto.
      split.
       intro H7; apply H5; unfold point_connect in *.
       destruct H7 as [l1 [l2 [p [H7_1 [H7_2 [H7_3 H7_4]]]]]].
       exists l1, l2, p; intuition.
        apply RotIn_r with c'.
        rewrite H3; auto.
        auto.
       intro H7; apply H6; unfold line_connect in *.
       destruct H7 as [l [H7_1 H7_2]].
       exists l; intuition.
        rewrite H3 in rotc; apply RotIn_r with c2...
     destruct (H0 c1 c2); auto.
      apply in_or_app; left...
      apply in_or_app; right; right...
    destruct H3 as [H3 | [H3 | H3]].
     destruct (H0 c c2).
      apply in_or_app; right; left...
      apply in_or_app; left...
      intro H5.
      destruct (AT (a1 ++ c :: a2)) as [_ [H6 _]].
       apply in_or_app; right; left...
       apply H6 with c2; subst.
        apply in_or_app; right; left...
        rewrite rin_distinct_setremove.
         apply rin_or_app; left; apply in_rin; auto.
         destruct (AT (a1 ++ c2 :: a2)).
          apply in_or_app; right; left; auto.
          destruct H5.
          apply H5.
          apply in_or_app; right; left; auto.
      split.
       intro H7; apply H5; unfold point_connect in *.
       destruct H7 as [l1 [l2 [p [H7_1 [H7_2 [H7_3 H7_4]]]]]].
       exists l1, l2, p; intuition.
        apply RotIn_r with c'.
        rewrite H; auto.
        auto.
       intro H7; apply H6; unfold line_connect in *.
       destruct H7 as [l [H7_1 H7_2]].
       exists l; intuition.
        rewrite H in rotc; apply RotIn_r with c1...
     elim H4; rewrite <- H; rewrite H3; auto.
     destruct (H0 c c2).
      apply in_or_app; right; left...
      apply in_or_app; right; right...
      intro H5.
      destruct (AT (a1 ++ c :: a2)) as [_ [H6 _]].
       apply in_or_app; right; left...
       apply H6 with c2; subst.
        apply in_or_app; right; left...
        rewrite rin_distinct_setremove.
         apply rin_or_app; right; apply in_rin; auto.
         destruct (AT (a1 ++ c2 :: a2)).
          apply in_or_app; right; left; auto.
          destruct H5.
          apply H5.
          apply in_or_app; right; left; auto.
      split.
       intro H7; apply H5; unfold point_connect in *.
       destruct H7 as [l1 [l2 [p [H7_1 [H7_2 [H7_3 H7_4]]]]]].
       exists l1, l2, p; intuition.
        apply RotIn_r with c'.
        rewrite H; auto.
        auto.
       intro H7; apply H6; unfold line_connect in *.
       destruct H7 as [l [H7_1 H7_2]].
       exists l; intuition.
        rewrite H in rotc; apply RotIn_r with c1...
    destruct H3 as [H3 | [H3 | H3]].
     destruct (H0 c1 c2); auto.
      apply in_or_app; right; right...
      apply in_or_app; left...
     destruct (H0 c1 c).
      apply in_or_app; right; right...
      apply in_or_app; right; left...
      intro H5.
      destruct (AT (a1 ++ c :: a2)) as [_ [H6 _]].
       apply in_or_app; right; left...
       apply H6 with c1; subst.
        apply in_or_app; right; left...
        rewrite rin_distinct_setremove.
         apply rin_or_app; right; apply in_rin; auto.
         destruct (AT (a1 ++ c :: a2)).
          apply in_or_app; right; left; auto.
          destruct H5.
          apply H5.
          apply in_or_app; right; left; auto.
      split.
       intro H7; apply H5; unfold point_connect in *.
       destruct H7 as [l1 [l2 [p [H7_1 [H7_2 [H7_3 H7_4]]]]]].
       exists l1, l2, p; intuition.
        apply RotIn_r with c'.
        rewrite H3; auto.
        auto.
       intro H7; apply H6; unfold line_connect in *.
       destruct H7 as [l [H7_1 H7_2]].
       exists l; intuition.
        rewrite H3 in rotc; apply RotIn_r with c2...
     destruct (H0 c1 c2); auto.
      apply in_or_app; right; right...
      apply in_or_app; right; right...
  *intro H3.
   apply in_app_or in H; destruct H as [H | [H | H]].
    apply H1 with c0.
     apply in_or_app; left...
     apply in_split in H.
     destruct H as [a3 [a4 H]]; subst.
     rewrite <- app_assoc in H3; simpl in H3.
     rewrite <- app_assoc; simpl.
     apply rin_setremove_remove in H3.
     apply rin_setremove_remove.
     apply rin_app_or in H3.
      apply rin_or_app; destruct H3 as [H3 | H3]; [auto | right].
     apply rin_app_or in H3.
      apply rin_or_app; destruct H3 as [H3 | H3]; [auto | right].
     simpl; destruct H3; auto.
     left; apply Rot_sym in rotc; eapply Rot_trans...
    apply H1 with c.
     apply in_or_app; right; left...
     subst; apply rin_setremove_remove in H3; apply rin_setremove_remove.
     apply eq_rin_elem with c0; auto.
     apply Rot_sym; auto.
    apply H1 with c0.
     apply in_or_app; right; right...
     apply in_split in H.
     destruct H as [a3 [a4 H]]; subst.
     rewrite app_comm_cons in H3.
     rewrite app_assoc in H3.
     rewrite app_comm_cons.
     rewrite app_assoc.
     apply rin_setremove_remove in H3.
     apply rin_setremove_remove.
     apply rin_app_or in H3.
      apply rin_or_app; destruct H3 as [H3 | H3]; [left | auto].
     apply rin_app_or in H3.
      apply rin_or_app; destruct H3 as [H3 | H3]; [auto | right].
     simpl; destruct H3; auto.
     left; apply Rot_sym in rotc; eapply Rot_trans...
  *rewrite app_length.
   rewrite app_length in H2.
   simpl in *; exact H2.
 -unfold LCconsistency in *.
  intros l HL.
  destruct (LC l HL) as [c0 [H H0]].
  apply in_app_or in H0; destruct H0 as [H0 | [H0 | H0]].
   exists c0; split; [auto | apply in_or_app]; left...
   exists c'; subst; split.
    apply RotIn with c0; auto.
    apply in_or_app; right; left...
   exists c0; split; [auto | apply in_or_app]; right; right...
 -unfold CLconsistency in *.
  intros l c0 H Hin.
  apply in_app_or in H; destruct H as [H | [H | H]].
   apply CL with c0.
    apply in_or_app; left...
    auto.
   apply CL with c.
    apply in_or_app; right; left...
    apply RotIn_r with c0; subst...
   apply CL with c0.
    apply in_or_app; right; right...
    auto.
 -unfold CAconsistency in *.
  intros c0 H H0.
  apply in_app_or in H; destruct H as [H | [H | H]].
  *destruct CA with c0 as [a [Ha Hi]].
    apply in_or_app; left...
    auto.
   apply in_app_or in Hi; destruct Hi as [Hi | [Hi | Hi]].
    exists a; intuition.
    subst; apply in_app_or in Ha; destruct Ha as [Ha | [Ha | Ha]].
     exists (a1 ++ c' :: a2); intuition.
     rewrite <- Ha in *; elim DC with c.
     apply in_or_app; right; left...
     apply rin_setremove_remove.
     apply rin_or_app; left; apply in_rin; auto.
    exists (a1 ++ c' :: a2); intuition.
   exists a; intuition.
  *exists (a1 ++ c' :: a2); subst; intuition.
  *destruct CA with c0 as [a [Ha Hi]].
    apply in_or_app; right; right...
    auto.
   apply in_app_or in Hi; destruct Hi as [Hi | [Hi | Hi]].
    exists a; intuition.
    subst; apply in_app_or in Ha; destruct Ha as [Ha | [Ha | Ha]].
     exists (a1 ++ c' :: a2); intuition.
     rewrite <- Ha in *; elim DC with c.
     apply in_or_app; right; left...
     apply rin_setremove_remove.
     apply rin_or_app; right; apply in_rin; auto.
    exists (a1 ++ c' :: a2); intuition.
   exists a; intuition.
 -unfold ACconsistency in *.
  intros c0 a Ha Hc.
  apply in_app_or in Ha; destruct Ha as [Ha | [Ha | Ha]].
  *assert (In c0 (C1 ++ c :: C2)).
    apply AC with a; intuition.
   apply in_app_or in H; destruct H as [H | [H | H]]; intuition.
   subst.
   assert (a = a1 ++ c0 :: a2).
   apply AA with c0; intuition.
   elim DA with a; subst.
   intuition.
   apply ain_setremove_remove.
   apply ain_or_app; left; apply in_ain; auto.
  *subst; apply in_app_or in Hc; destruct Hc as [Hc | [Hc | Hc]].
    assert (In c0 (C1 ++ c :: C2)).
     apply AC with (a1 ++ c :: a2); intuition.
    apply in_app_or in H; destruct H as [H | [H | H]]; intuition.
    subst.
    destruct (AT (a1 ++ c0 :: a2)) as [_ [H _]].
     intuition.
    elim H with c0.
    intuition.
    apply rin_setremove_remove.
    apply rin_or_app; left; apply in_rin; auto.
    subst; intuition.
    assert (In c0 (C1 ++ c :: C2)).
     apply AC with (a1 ++ c :: a2); intuition.
    apply in_app_or in H; destruct H as [H | [H | H]]; intuition.
    subst.
    destruct (AT (a1 ++ c0 :: a2)) as [_ [H _]].
     intuition.
    elim H with c0.
    intuition.
    apply rin_setremove_remove.
    apply rin_or_app; right; apply in_rin; auto.
  *assert (In c0 (C1 ++ c :: C2)).
    apply AC with a; intuition.
   apply in_app_or in H; destruct H as [H | [H | H]]; intuition.
   subst.
   assert (a = a1 ++ c0 :: a2).
   apply AA with c0; intuition.
   elim DA with a; subst.
   intuition.
   apply ain_setremove_remove.
   apply ain_or_app; right; apply in_ain; auto.
 -unfold Outermostconsistency1 in *.
  apply in_app_or in O1; destruct O1 as [O1 | [O1 | O1]]; intuition.
 -unfold Outermostconsistency2 in *.
  intros a Ha Hr.
  apply in_app_or in Ha; destruct Ha as [Ha | [Ha | Ha]].
   apply O2 with a; intuition.
   subst; apply rin_app_or in Hr; destruct Hr as [Hr | [Hr | Hr]].
    apply O2 with (a1 ++ c :: a2).
    intuition.
    apply rin_or_app...
    apply O2 with (a1 ++ c :: a2).
     intuition.
     apply rin_or_app; right; left.
     apply Rot_trans with c'.
      auto.
      apply Rot_sym...
    apply O2 with (a1 ++ c :: a2).
    intuition.
    apply rin_or_app; right; right...
   apply O2 with a; intuition.
 -unfold Circuitconsistency in *.
  intros l c1 c2 Hc1 Hc2 Hl1 Hl2.
  apply in_app_or in Hc1; apply in_app_or in Hc2.
  destruct Hc1 as [Hc1 | [Hc1 | Hc1]]; destruct Hc2 as [Hc2 | [Hc2 | Hc2]];
   try (apply CC with l; intuition; fail).
  *subst; assert (H := RotIn_r _ _ _ Hl2 rotc).
   assert (c1 = c) by (apply CC with l; intuition).
   rewrite H0 in *; clear c1 H0.
   elim DC with c.
    apply in_or_app; simpl; auto.
    apply rin_setremove_remove.
    apply rin_or_app; left; apply in_rin; auto.
  *subst; assert (H := RotIn_r _ _ _ Hl1 rotc).
   assert (c2 = c) by (apply CC with l; intuition).
   rewrite H0 in *; clear c2 H0.
   elim DC with c.
    apply in_or_app; simpl; auto.
    apply rin_setremove_remove.
    apply rin_or_app; left; apply in_rin; auto.
  *rewrite <- Hc1; rewrite Hc2; auto.
  *subst; assert (H := RotIn_r _ _ _ Hl1 rotc).
   assert (c2 = c) by (apply CC with l; intuition).
   rewrite H0 in *; clear c2 H0.
   elim DC with c.
    apply in_or_app; simpl; auto.
    apply rin_setremove_remove.
    apply rin_or_app; right; apply in_rin; auto.
  *subst; assert (H := RotIn_r _ _ _ Hl2 rotc).
   assert (c1 = c) by (apply CC with l; intuition).
   rewrite H0 in *; clear c1 H0.
   elim DC with c.
    apply in_or_app; simpl; auto.
    apply rin_setremove_remove.
    apply rin_or_app; right; apply in_rin; auto.
 -unfold Areaconsistency in *.
  intros c0 a3 a4 Ha3 Ha4 Hc3 Hc4.
  apply in_app_or in Ha3; apply in_app_or in Ha4.
  destruct Ha3 as [Ha3 | [Ha3 | Ha3]]; destruct Ha4 as [Ha4 | [Ha4 | Ha4]];
   try (apply AA with c0; intuition; fail).
  *subst.
   apply in_app_or in Hc4.
   destruct Hc4 as [Hc4 | [Hc4 | Hc4]].
    elim DA with (a1 ++ c :: a2).
     apply in_or_app; right; left; auto.
     apply ain_setremove_remove.
     apply ain_or_app; left; apply in_ain.
     rewrite (AA c0 a3 (a1 ++ c :: a2)) in Ha3.
      auto.
      apply in_or_app; auto.
      apply in_or_app; right; left; auto.
      auto.
      apply in_or_app; auto.
    destruct eq_listline_dec with c' c; subst.
     apply AA with c0; subst; intuition.
     assert (In c0 (C1 ++ c :: C2)).
      apply AC with a3; intuition.
     elim DC with c.
      apply in_or_app; right; left; auto.
      apply rin_setremove_remove.
      apply eq_rin_elem with c0.
       apply Rot_sym; auto.
       apply in_app_or in H; destruct H as [H | [H | H]].
        apply rin_or_app; left; apply in_rin; auto.
        elim n; rewrite H; auto.
        apply rin_or_app; right; apply in_rin; auto.
    elim DA with (a1 ++ c :: a2).
     apply in_or_app; right; left; auto.
     apply ain_setremove_remove.
     apply ain_or_app; left; apply in_ain.
     rewrite (AA c0 a3 (a1 ++ c :: a2)) in Ha3.
      auto.
      apply in_or_app; auto.
      apply in_or_app; right; left; auto.
      auto.
      apply in_or_app; right; right; auto.
  *subst.
   apply in_app_or in Hc3.
   destruct Hc3 as [Hc3 | [Hc3 | Hc3]].
    elim DA with (a1 ++ c :: a2).
     apply in_or_app; right; left; auto.
     apply ain_setremove_remove.
     apply ain_or_app; left; apply in_ain.
     rewrite (AA c0 a4 (a1 ++ c :: a2)) in Ha4.
      auto.
      apply in_or_app; auto.
      apply in_or_app; right; left; auto.
      auto.
      apply in_or_app; auto.
    destruct eq_listline_dec with c' c; subst.
     apply AA with c0; subst; intuition.
     assert (In c0 (C1 ++ c :: C2)).
      apply AC with a4; intuition.
     elim DC with c.
      apply in_or_app; right; left; auto.
      apply rin_setremove_remove.
      apply eq_rin_elem with c0.
       apply Rot_sym; auto.
       apply in_app_or in H; destruct H as [H | [H | H]].
        apply rin_or_app; left; apply in_rin; auto.
        elim n; rewrite H; auto.
        apply rin_or_app; right; apply in_rin; auto.
    elim DA with (a1 ++ c :: a2).
     apply in_or_app; right; left; auto.
     apply ain_setremove_remove.
     apply ain_or_app; left; apply in_ain.
     rewrite (AA c0 a4 (a1 ++ c :: a2)) in Ha4.
      auto.
      apply in_or_app; auto.
      apply in_or_app; right; left; auto.
      auto.
      apply in_or_app; right; right; auto.
  *rewrite <- Ha3; now exact Ha4.
  *subst.
   apply in_app_or in Hc3.
   destruct Hc3 as [Hc3 | [Hc3 | Hc3]].
    elim DA with (a1 ++ c :: a2).
     apply in_or_app; right; left; auto.
     apply ain_setremove_remove.
     apply ain_or_app; right; apply in_ain.
     rewrite (AA c0 a4 (a1 ++ c :: a2)) in Ha4.
      auto.
      apply in_or_app; right; right; auto.
      apply in_or_app; right; left; auto.
      auto.
      apply in_or_app; auto.
    destruct eq_listline_dec with c' c; subst.
     apply AA with c0; subst; intuition.
     assert (In c0 (C1 ++ c :: C2)).
      apply AC with a4; intuition.
     elim DC with c.
      apply in_or_app; right; left; auto.
      apply rin_setremove_remove.
      apply eq_rin_elem with c0.
       apply Rot_sym; auto.
       apply in_app_or in H; destruct H as [H | [H | H]].
        apply rin_or_app; left; apply in_rin; auto.
        elim n; rewrite H; auto.
        apply rin_or_app; right; apply in_rin; auto.
    elim DA with (a1 ++ c :: a2).
     apply in_or_app; right; left; auto.
     apply ain_setremove_remove.
     apply ain_or_app; right; apply in_ain.
     rewrite (AA c0 a4 (a1 ++ c :: a2)) in Ha4.
      auto.
      apply in_or_app; right; right; auto.
      apply in_or_app; right; left; auto.
      auto.
      apply in_or_app; right; right; auto.
  *subst.
   apply in_app_or in Hc4.
   destruct Hc4 as [Hc4 | [Hc4 | Hc4]].
    elim DA with (a1 ++ c :: a2).
     apply in_or_app; right; left; auto.
     apply ain_setremove_remove.
     apply ain_or_app; right; apply in_ain.
     rewrite (AA c0 a3 (a1 ++ c :: a2)) in Ha3.
      auto.
      apply in_or_app; right; right; auto.
      apply in_or_app; right; left; auto.
      auto.
      apply in_or_app; auto.
    destruct eq_listline_dec with c' c; subst.
     apply AA with c0; subst; intuition.
     assert (In c0 (C1 ++ c :: C2)).
      apply AC with a3; intuition.
     elim DC with c.
      apply in_or_app; right; left; auto.
      apply rin_setremove_remove.
      apply eq_rin_elem with c0.
       apply Rot_sym; auto.
       apply in_app_or in H; destruct H as [H | [H | H]].
        apply rin_or_app; left; apply in_rin; auto.
        elim n; rewrite H; auto.
        apply rin_or_app; right; apply in_rin; auto.
    elim DA with (a1 ++ c :: a2).
     apply in_or_app; right; left; auto.
     apply ain_setremove_remove.
     apply ain_or_app; right; apply in_ain.
     rewrite (AA c0 a3 (a1 ++ c :: a2)) in Ha3.
      auto.
      apply in_or_app; right; right; auto.
      apply in_or_app; right; left; auto.
      auto.
      apply in_or_app; right; right; auto.
 -unfold DistinctC in *.
  intros c0 Hc0 Hr.
  apply in_app_or in Hc0; destruct Hc0 as [Hc0 | [Hc0 | Hc0]].
  *apply DC with c0; intuition.
   apply in_split in Hc0.
   destruct Hc0 as [C3 [C4 Hc0]]; subst.
   rewrite <- app_assoc in *; simpl in *.
   apply rin_setremove_remove in Hr; apply rin_setremove_remove.
   apply rin_app_or in Hr; apply rin_or_app; destruct Hr as [Hr | Hr]; [auto | right].
   apply rin_app_or in Hr; apply rin_or_app; destruct Hr as [Hr | Hr]; [auto | right].
   destruct Hr as [Hr | Hr]; simpl; [left | auto].
   apply Rot_sym in rotc; eapply Rot_trans...
  *apply DC with c; intuition.
   subst; apply rin_setremove_remove in Hr.
   apply rin_setremove_remove.
   apply eq_rin_elem with c0.
    apply Rot_sym; auto.
    auto.
  *apply DC with c0; intuition.
   apply in_split in Hc0.
   destruct Hc0 as [C3 [C4 Hc0]]; subst.
   rewrite app_comm_cons in Hr; rewrite app_assoc in Hr.
   rewrite app_comm_cons; rewrite app_assoc.
   apply rin_setremove_remove in Hr; apply rin_setremove_remove.
   apply rin_app_or in Hr; apply rin_or_app; destruct Hr as [Hr | Hr]; [left | auto].
   apply rin_app_or in Hr; apply rin_or_app; destruct Hr as [Hr | Hr]; [auto | right].
   destruct Hr as [Hr | Hr]; simpl; [left | auto].
   apply Rot_sym in rotc; eapply Rot_trans...
 -unfold DistinctA in *.
  intros a Ha Hi.
  assert (eq_area (a1 ++ c' :: a2) (a1 ++ c :: a2)).
   apply trans_area with (c' :: a1 ++ a2).
    apply permute_area.
    apply Permutation_sym.
    apply Permutation_middle.
   apply trans_area with (c :: a1 ++ a2).
    apply rotate_c_area.
    apply Rot_sym; auto.
   apply permute_area.
   apply Permutation_middle.
  apply in_app_or in Ha; destruct Ha as [Ha | [Ha | Ha]].
  *apply DA with a; intuition.
   apply in_split in Ha.
   destruct Ha as [A3 [A4 Ha]]; subst.
   rewrite <- app_assoc in *; simpl in *.
   apply ain_setremove_remove in Hi; apply ain_setremove_remove.
   apply ain_app_or in Hi; apply ain_or_app; destruct Hi as [Hi | Hi]; [auto | right].
   apply ain_app_or in Hi; apply ain_or_app; destruct Hi as [Hi | Hi]; [auto | right].
   destruct Hi as [Hi | Hi]; simpl; [left | auto].
   eapply trans_area...
  *apply DA with (a1 ++ c :: a2); intuition.
   subst; apply ain_setremove_remove in Hi.
   apply ain_setremove_remove.
   apply eq_ain_elem with (a1 ++ c' :: a2); auto.
  *apply DA with a; intuition.
   apply in_split in Ha.
   destruct Ha as [A3 [A4 Ha]]; subst.
   rewrite app_comm_cons in Hi; rewrite app_assoc in Hi.
   rewrite app_comm_cons; rewrite app_assoc.
   apply ain_setremove_remove in Hi; apply ain_setremove_remove.
   apply ain_app_or in Hi; apply ain_or_app; destruct Hi as [Hi | Hi]; [left | auto].
   apply ain_app_or in Hi; apply ain_or_app; destruct Hi as [Hi | Hi]; [auto | right].
   destruct Hi as [Hi | Hi]; simpl; [left | auto].
   eapply trans_area...
 -rewrite app_length in *; rewrite app_length in *; simpl in *; auto.
(* rotateOutermost *)
+unfold PLCAconstraints, PLCAconsistency, PLCAeuler in *.
 destruct (in_split _ _ O1) as [C1 [C2 H]]; subst.
 assert (H := ReplaceCircuit_C _ _ _ o' DC).
 rewrite H in *; clear H.
 intuition.
 -unfold Cconstraint in *.
  intros c Hc.
  apply in_app_or in Hc; destruct Hc as [Hc | [Hc | Hc]].
  *apply CT; apply in_or_app; auto.
  *subst; unfold Circuit_constraints.
    assert (In o (C1 ++ o :: C2)) as Ho.
     apply in_or_app; simpl; auto.
    destruct (CT o Ho) as [[xo [plo Tt]] Hl].
    rewrite (Rot_length o c roto) in Hl.
    split; [ | auto].
    clear -roto Tt.
    induction roto.
     exists xo, plo; exact Tt.
     apply IHroto in Tt; clear -Tt.
     destruct Tt as [x [pl Tt]].
     inversion Tt; subst.
     inversion H1; subst.
      exists x; exists (x :: x :: nil); simpl...
     exists p2; exists ((p2 :: pl) ++ p2 :: nil).
     apply reverse_step_trail...
  *apply CT; apply in_or_app; right; right; auto.
 -unfold Circuit_constraints.
  assert (In o (C1 ++ o :: C2)) as Ho.
   apply in_or_app; simpl; auto.
  destruct (CT o Ho) as [[xo [plo Tt]] Hl].
  rewrite (Rot_length o o' roto) in Hl.
  split; [ | auto].
  clear -roto Tt.
  induction roto.
   exists xo, plo; exact Tt.
   apply IHroto in Tt; clear -Tt.
   destruct Tt as [x [pl Tt]].
   inversion Tt; subst.
   inversion H1; subst.
    exists x; exists (x :: x :: nil); simpl...
   exists p2; exists ((p2 :: pl) ++ p2 :: nil).
   apply reverse_step_trail...
 -unfold LCconsistency in *.
  intros l Hl.
  destruct (LC l Hl) as [c [Hc1 Hc2]].
  apply in_app_or in Hc2; destruct Hc2 as [Hc2 | [Hc2 | Hc2]].
  *exists c; intuition.
  *exists o'; subst; split.
    apply RotIn with c...
    apply in_or_app; right; left; auto.
  *exists c; intuition.
 -unfold CLconsistency in *.
  intros l c Hc Hlc.
  apply in_app_or in Hc; destruct Hc as [Hc | [Hc | Hc]].
  *apply CL with c; intuition.
  *apply CL with o; subst.
    intuition.
    eapply RotIn_r...
  *apply CL with c; intuition.
 -unfold CAconsistency in *.
  intros c Hc neq.
  assert (c <> o).
   intro H; subst.
   apply DC with o.
    intuition.
    apply rin_setremove_remove.
    apply in_app_or in Hc; destruct Hc as [Hc | [Hc | Hc]].
     apply rin_or_app; left; apply in_rin; auto.
     elim neq; subst; auto.
     apply rin_or_app; right; apply in_rin; auto.
  apply CA.
   apply in_app_or in Hc; destruct Hc as [Hc | [Hc | Hc]].
    apply in_or_app; auto.
    elim neq; subst; auto.
    apply in_or_app; right; right; auto.
   auto.
 -unfold ACconsistency.
  intros c a Ha Hc.
  assert (In c (C1 ++ o :: C2)).
   apply AC with a; auto.
  apply in_or_app; simpl.
  apply in_app_or in H; destruct H as [H | [H | H]]; auto.
  rewrite <- H in Hc; elim O2 with a; auto.
  apply in_rin; auto.
 -unfold Outermostconsistency1.
  intuition.
 -unfold Outermostconsistency2 in *.
  intros a H Hr.
  apply O2 with a.
   auto.
   apply eq_rin_elem with o'.
    apply Rot_sym; auto.
    auto.
 -unfold Circuitconsistency in *.
  intros l c1 c2 Hc1 Hc2 Hl1 Hl2.
  apply in_app_or in Hc1; apply in_app_or in Hc2.
  destruct Hc1 as [Hc1 | [Hc1 | Hc1]];
    destruct Hc2 as [Hc2 | [Hc2 | Hc2]];
    try (apply CC with l; intuition; fail); subst.
  *subst; assert (H := RotIn_r _ _ _ Hl2 roto).
   assert (c1 = o) by (apply CC with l; intuition).
   rewrite H0 in *; clear c1 H0.
   elim DC with o.
    apply in_or_app; simpl; auto.
    apply rin_setremove_remove.
    apply rin_or_app; left; apply in_rin; auto.
  *subst; assert (H := RotIn_r _ _ _ Hl1 roto).
   assert (c2 = o) by (apply CC with l; intuition).
   rewrite H0 in *; clear c2 H0.
   elim DC with o.
    apply in_or_app; simpl; auto.
    apply rin_setremove_remove.
    apply rin_or_app; left; apply in_rin; auto.
  *auto.
  *subst; assert (H := RotIn_r _ _ _ Hl1 roto).
   assert (c2 = o) by (apply CC with l; intuition).
   rewrite H0 in *; clear c2 H0.
   elim DC with o.
    apply in_or_app; simpl; auto.
    apply rin_setremove_remove.
    apply rin_or_app; right; apply in_rin; auto.
  *subst; assert (H := RotIn_r _ _ _ Hl2 roto).
   assert (c1 = o) by (apply CC with l; intuition).
   rewrite H0 in *; clear c1 H0.
   elim DC with o.
    apply in_or_app; simpl; auto.
    apply rin_setremove_remove.
    apply rin_or_app; right; apply in_rin; auto.
 -unfold DistinctC in *.
  intros c Hc Hr.
  apply in_app_or in Hc; destruct Hc as [Hc | [Hc | Hc]].
  *apply DC with c; intuition.
   apply in_split in Hc.
   destruct Hc as [C3 [C4 Hc]]; subst.
   rewrite <- app_assoc in *; simpl in *.
   apply rin_setremove_remove in Hr; apply rin_setremove_remove.
   apply rin_app_or in Hr; apply rin_or_app; destruct Hr as [Hr | Hr]; [auto | right].
   apply rin_app_or in Hr; apply rin_or_app; destruct Hr as [Hr | Hr]; [auto | right].
   destruct Hr as [Hr | Hr]; simpl; [left | auto].
   apply Rot_sym in roto; eapply Rot_trans...
  *apply DC with o; intuition.
   subst; apply rin_setremove_remove in Hr.
   apply rin_setremove_remove.
   apply eq_rin_elem with c.
    apply Rot_sym; auto.
    auto.
  *apply DC with c; intuition.
   apply in_split in Hc.
   destruct Hc as [C3 [C4 Hc]]; subst.
   rewrite app_comm_cons in Hr; rewrite app_assoc in Hr.
   rewrite app_comm_cons; rewrite app_assoc.
   apply rin_setremove_remove in Hr; apply rin_setremove_remove.
   apply rin_app_or in Hr; apply rin_or_app; destruct Hr as [Hr | Hr]; [left | auto].
   apply rin_app_or in Hr; apply rin_or_app; destruct Hr as [Hr | Hr]; [auto | right].
   destruct Hr as [Hr | Hr]; simpl; [left | auto].
   apply Rot_sym in roto; eapply Rot_trans...
 -rewrite app_length in *; simpl in *; auto.
(* PLCAtrans *)
+unfold PLCAconstraints, PLCAconsistency, PLCAeuler in *.
  intuition.
Qed.








Theorem PLCAeq_planar :
  forall {P P' : list Point} {L L' : list Line} {C C' : list Circuit} {A A' : list Area} {o o' : Circuit},
    PLCAplanarity_condition P L C A o
    -> PLCAequivalence P L C A o P' L' C' A' o'
    -> PLCAplanarity_condition P' L' C' A' o'.
Proof with eauto.
intros P P' L L' C C' A A' o o'.
intro x.
extractPLCAspec x.
intro PLCAEQ.
unfold PLCAplanarity_condition.
unfold PLCAconstraints, PLCAconsistency, PLCAconnected, PLCAeuler in *.
induction PLCAEQ as
[ P P' L L' C C' A A' o PermP PermL PermC PermA
| P L C A o l l' eql
| P L C A o a a' eqa
| P L C A o c c' Ic Noc rotc
| P L C A o o' roto
| P P' P'' L L' L'' C C' C'' A A' A'' o o' o'' PLCAEQ1 IHPLCAEQ1 PLCAEQ2 IHPLCAEQ2].
(* permutePLCA *)
+assert (PLCAconstraints L' C' A' o /\ PLCAconsistency P' L' C' A' o /\ PLCAeuler P' L' C' A').
  assert (PLCAequivalence P L C A o P' L' C' A' o).
   eapply permutePLCA; eauto...
  eapply PLCAeq_planar_except_for_connect; eauto; unfold PLCAconstraints, PLCAconsistency, PLCAeuler; intuition.
 unfold PLCAconstraints, PLCAconsistency, PLCAeuler in H;
  intuition.
 assert (OIn P L C A o1) as OI1.
  unfold OIn in *.
  destruct o1.
   apply Permutation_in with P'.
    apply Permutation_sym; exact PermP.
    exact H17.
   apply Permutation_in with L'.
    apply Permutation_sym; exact PermL.
    exact H17.
   apply Permutation_in with C'.
    apply Permutation_sym; exact PermC.
    exact H17.
   apply Permutation_in with A'.
    apply Permutation_sym; exact PermA.
    exact H17.
 assert (OIn P L C A o2) as OI2.
  unfold OIn in *.
  destruct o2.
   apply Permutation_in with P'.
    apply Permutation_sym; exact PermP.
    exact H19.
   apply Permutation_in with L'.
    apply Permutation_sym; exact PermL.
    exact H19.
   apply Permutation_in with C'.
    apply Permutation_sym; exact PermC.
    exact H19.
   apply Permutation_in with A'.
    apply Permutation_sym; exact PermA.
    exact H19.
 assert (Con := PLCAcon o1 o2 OI1 OI2).
 clear OI1 OI2 H17 H19.
  induction Con.
   apply PLcon.
    apply Permutation_in with P.
     exact PermP.
     exact H17.
    apply Permutation_in with L.
     exact PermL.
     exact H19.
    exact H20.
   apply LCcon.
    apply Permutation_in with L.
     exact PermL.
     exact H17.
    apply Permutation_in with C.
     exact PermC.
     exact H19.
    exact H20.
   apply CAcon.
    apply Permutation_in with C.
     exact PermC.
     exact H17.
    apply Permutation_in with A.
     exact PermA.
     exact H19.
    exact H20.
   apply SYMME.
    apply IHCon.
   apply TRANS with o2.
    exact IHCon1.
    exact IHCon2.
(* reverseLine *)
+assert (PLCAconstraints (l' :: L) C A o /\ PLCAconsistency P (l' :: L) C A o /\ PLCAeuler P (l' :: L) C A).
  assert (PLCAequivalence P (l :: L) C A o P (l' :: L) C A o).
   eapply reverseLine...
  eapply PLCAeq_planar_except_for_connect; eauto; unfold PLCAconstraints, PLCAconsistency, PLCAeuler; intuition.
 unfold PLCAconstraints, PLCAconsistency, PLCAeuler in H;
  intuition.
 inversion eql; subst.
  eauto.
 clear eql.
 assert (OIn P (l :: L) C A (rev_l_object l o1)).
  clear -H17; destruct o1; simpl in *; auto.
  destruct eq_dline_dec with (reverse l) l0.
   auto.
   destruct H17.
    elim n; auto.
    auto.
 assert (OIn P (l :: L) C A (rev_l_object l o2)).
  clear -H19; destruct o2; simpl in *; auto.
  destruct eq_dline_dec with (reverse l) l0.
   auto.
   destruct H19.
    elim n; auto.
    auto.
 assert (PLCAcon' := PLCAcon _ _ H20 H21).
 cut (exists path : list object, connectpath P (reverse l :: L) C A o1 o2 path).
  intro H22; destruct H22.
  eapply PLCAconnect_path; unfold PLCAconsistency; unfold PLCAconstraints; eauto.
  intuition.
 apply connectpath_PLCAconnect in PLCAcon'; auto.
 clear -PLCAcon' DL H17 H19.
 destruct PLCAcon'.
 revert o1 H17 H.
 induction x; intros.
  inversion H.
  inversion H; subst.
 -assert (o1 = o2).
   destruct o1; destruct o2; try (inversion H0; subst; auto; fail).
   simpl in H0.
   destruct eq_dline_dec with (reverse l) l0;
     destruct eq_dline_dec with (reverse l) l1;
     subst; inversion H0; auto.
   subst; simpl in H19.
   destruct H19.
    rewrite H1; auto.
    elim DL with l1.
     simpl; auto.
     simpl.
     destruct eq_dline_dec with l1 l1.
      apply in_lin; auto.
      left; constructor.
   subst; simpl in H17.
   destruct H17.
    rewrite H1; auto.
    elim DL with l.
     simpl; auto.
     simpl.
     destruct eq_dline_dec with l l.
      apply in_lin; auto.
      left; constructor.
  subst; clear H0.
  exists (o2 :: nil).
  constructor; auto.
 -destruct o1; simpl in H0; inversion H0; clear H0; subst.
  destruct H4.
  *subst.
   assert (o_line l0 = rev_l_object l0 (o_line (reverse l0))).
    simpl.
    destruct eq_dline_dec with (reverse l0) (reverse l0).
     auto.
     elim n; auto.
   rewrite H0 in *; clear H0.
   assert (OIn P (reverse l0 :: L) C A (o_line (reverse l0))).
    simpl; auto.
   destruct (IHx (o_line (reverse l0)) H0 H7).
   exists (o_point p0 :: x0).
   apply pathPL with (reverse l0); simpl; auto.
   destruct l0; simpl in *; destruct H6; auto.
  *assert (o_line l0 = rev_l_object l (o_line l0)).
    simpl.
    destruct eq_dline_dec with (reverse l) l0.
     elim DL with l0; subst.
      right; auto.
      simpl.
      destruct eq_dline_dec with (reverse l) l.
       apply in_lin; auto.
       left; constructor.
        destruct l; simpl; auto.
     auto.
   rewrite H1 in *; clear H1.
   assert (OIn P (reverse l :: L) C A (o_line l0)).
    simpl; auto.
   destruct (IHx (o_line l0) H1 H7).
   exists (o_point p0 :: x0).
   apply pathPL with l0; simpl; auto.
 -destruct o1; simpl in H0; inversion H0; clear H0; subst.
  destruct eq_dline_dec with (reverse l) l1.
  *subst.
   simpl in H.
   destruct (IHx (o_circuit c)).
    simpl; auto.
    simpl; auto.
   exists (o_line (reverse l) :: x0).
   apply pathLC with c.
    simpl; auto.
    auto.
    apply LIn_reverse; auto.
    auto.
  *destruct H3.
    simpl in H17; destruct H17.
    elim n; auto.
    elim DL with l1; subst; simpl; auto.
     destruct eq_dline_dec with l1 l1.
      apply in_lin; auto.
      left; constructor.
   destruct (IHx (o_circuit c)); simpl; auto.
   exists (o_line l1 :: x0).
   apply pathLC with c; simpl; auto.
 -destruct o1; simpl H0; inversion H0; clear H0; subst.
  destruct (IHx (o_area a0)); simpl; auto.
  exists (o_circuit c0 :: x0).
  apply pathCA with a0; simpl; auto.
 -destruct o1; simpl in H0; inversion H0; clear H0; subst.
  destruct eq_dline_dec with (reverse l) l1.
  *subst.
   simpl in H.
   destruct (IHx (o_point p)).
    simpl; auto.
    simpl; auto.
   exists (o_line (reverse l) :: x0).
   apply pathLP with p.
    simpl; auto.
    auto.
    destruct l; unfold reverse; simpl; simpl in H6; destruct H6; auto.
    auto.
  *destruct H3.
    simpl in H17; destruct H17.
    elim n; auto.
    elim DL with l1; subst; simpl; auto.
     destruct eq_dline_dec with l1 l1.
      apply in_lin; auto.
      left; constructor.
   destruct (IHx (o_point p)); simpl; auto.
   exists (o_line l1 :: x0).
   apply pathLP with p; simpl; auto.
 -destruct o1; simpl in H0; inversion H0; clear H0; subst.
  destruct H4.
  *subst.
   assert (o_line l0 = rev_l_object l0 (o_line (reverse l0))).
    simpl.
    destruct eq_dline_dec with (reverse l0) (reverse l0).
     auto.
     elim n; auto.
   rewrite H0 in *; clear H0.
   assert (OIn P (reverse l0 :: L) C A (o_line (reverse l0))).
    simpl; auto.
   destruct (IHx (o_line (reverse l0)) H0 H7).
   exists (o_circuit c0 :: x0).
   apply pathCL with (reverse l0); simpl; auto.
   apply LIn_reverse; auto.
  *assert (o_line l0 = rev_l_object l (o_line l0)).
    simpl.
    destruct eq_dline_dec with (reverse l) l0.
     elim DL with l0; subst.
      right; auto.
      simpl.
      destruct eq_dline_dec with (reverse l) l.
       apply in_lin; auto.
       left; constructor.
        destruct l; simpl; auto.
     auto.
   rewrite H1 in *; clear H1.
   assert (OIn P (reverse l :: L) C A (o_line l0)).
    simpl; auto.
   destruct (IHx (o_line l0) H1 H7).
   exists (o_circuit c0 :: x0).
   apply pathCL with l0; simpl; auto.
 -destruct o1; simpl H0; inversion H0; clear H0; subst.
  destruct (IHx (o_circuit c)); simpl; auto.
  exists (o_area a :: x0).
  apply pathAC with c; simpl; auto.
(* permuteArea *)
+assert (PLCAconstraints L C (a' :: A) o /\ PLCAconsistency P L C (a' :: A) o /\ PLCAeuler P L C (a' :: A)).
  assert (PLCAequivalence P L C (a :: A) o P L C (a' :: A) o).
   eapply permuteArea...
  eapply PLCAeq_planar_except_for_connect; eauto; unfold PLCAconstraints, PLCAconsistency, PLCAeuler; intuition.
 unfold PLCAconstraints, PLCAconsistency, PLCAeuler in H;
  intuition.
 assert (OIn P L C (a :: A) (replace_a_object a' a o1)).
  clear -H17; destruct o1; simpl in *; auto.
  destruct eq_listcircuit_dec with a' a0.
   auto.
   destruct H17.
    elim n; auto.
    auto.
 assert (OIn P L C (a :: A) (replace_a_object a' a o2)).
  clear -H19; destruct o2; simpl in *; auto.
  destruct eq_listcircuit_dec with a' a0.
   auto.
   destruct H19.
    elim n; auto.
    auto.
 assert (PLCAcon' := PLCAcon _ _ H20 H21).
 cut (exists path : list object, connectpath P L C (a' :: A) o1 o2 path).
  intro H22; destruct H22.
  eapply PLCAconnect_path; unfold PLCAconsistency; unfold PLCAconstraints; eauto.
  intuition.
 apply connectpath_PLCAconnect in PLCAcon'; auto.
 clear -PLCAcon' DA eqa H17 H19.
 destruct PLCAcon'.
 revert o1 H17 H.
 induction x; intros.
  inversion H.
  inversion H; subst.
 -assert (o1 = o2).
   destruct o1; destruct o2; try (inversion H0; subst; auto; fail).
   simpl in H0.
   destruct eq_listcircuit_dec with a' a0;
     destruct eq_listcircuit_dec with a' a1;
     subst; inversion H0; subst; auto.
   simpl in H19.
   destruct H19.
    rewrite H1; auto.
    elim DA with a1.
     simpl; auto.
     simpl.
     destruct eq_listcircuit_dec with a1 a1.
      apply in_ain; auto.
      left; constructor; apply Permutation_refl.
   simpl in H17.
   destruct H17.
    rewrite H1; auto.
    elim DA with a.
     simpl; auto.
     simpl.
     destruct eq_listcircuit_dec with a a.
      apply in_ain; auto.
      left; constructor; apply Permutation_refl.
  clear H0; subst.
  exists (o2 :: nil).
  constructor; auto.
 -destruct o1; simpl H0; inversion H0; clear H0; subst.
  destruct (IHx (o_line l)); simpl; auto.
  exists (o_point p0 :: x0).
  apply pathPL with l; simpl; auto.
 -destruct o1; simpl H0; inversion H0; clear H0; subst.
  destruct (IHx (o_circuit c)); simpl; auto.
  exists (o_line l0 :: x0).
  apply pathLC with c; simpl; auto.
 -destruct o1; simpl in H0; inversion H0; clear H0; subst.
  destruct H4.
  *subst.
   destruct (IHx (o_area a')); simpl; auto.
    destruct eq_listcircuit_dec with a' a'.
     auto.
     elim n; auto.
   exists (o_circuit c0 :: x0).
   apply pathCA with a'; simpl; auto.
   apply Permutation_in with a1; auto.
  *destruct (IHx (o_area a1)); simpl; auto.
    destruct eq_listcircuit_dec with a' a1.
     elim DA with a'; subst.
      simpl; auto.
      simpl; destruct eq_listcircuit_dec with a1 a.
       apply in_ain; auto.
       left.
       apply permute_area.
       apply Permutation_sym; auto.
     auto.
   exists (o_circuit c0 :: x0).
   apply pathCA with a1; simpl; auto.
 -destruct o1; simpl H0; inversion H0; clear H0; subst.
  destruct (IHx (o_point p)); simpl; auto.
  exists (o_line l0 :: x0).
  apply pathLP with p; simpl; auto.
 -destruct o1; simpl H0; inversion H0; clear H0; subst.
  destruct (IHx (o_line l)); simpl; auto.
  exists (o_circuit c0 :: x0).
  apply pathCL with l; simpl; auto.
 -destruct o1; simpl in *; inversion H0; clear H0; subst.
  destruct eq_listcircuit_dec with a' a0.
  *subst.
   destruct (IHx (o_circuit c)).
    simpl; auto.
    simpl; auto.
   exists (o_area a0 :: x0).
   apply pathAC with c.
    simpl; auto.
    auto.
    apply Permutation_in with a; auto.
    auto.
  *destruct H3.
    simpl in H17; destruct H17.
    elim n; auto.
    elim DA with a0; subst; simpl; auto.
     destruct eq_listcircuit_dec with a0 a0.
      apply in_ain; auto.
      left; constructor; apply Permutation_refl.
   destruct (IHx (o_circuit c)); simpl; auto.
   exists (o_area a0 :: x0).
   apply pathAC with c; simpl; auto.
(* rotateCircuit *)
+assert (PLCAconstraints L (map (replace_circuit c c') C) (map (map (replace_circuit c c')) A) o /\ PLCAconsistency P L (map (replace_circuit c c') C) (map (map (replace_circuit c c')) A) o /\ PLCAeuler P L (map (replace_circuit c c') C) (map (map (replace_circuit c c')) A)).
  assert (PLCAequivalence P L C A o P L (map (replace_circuit c c') C) (map (map (replace_circuit c c')) A) o).
   eapply rotateCircuit...
  eapply PLCAeq_planar_except_for_connect; eauto; unfold PLCAconstraints, PLCAconsistency, PLCAeuler; intuition.
 unfold PLCAconstraints, PLCAconsistency, PLCAeuler in H;
  intuition.
 destruct (in_split _ _ Ic) as [C1 [C2 H']]; subst.
 assert (H' := ReplaceCircuit_C _ _ _ c' DC).
 rewrite H' in *; clear H'.
 destruct (CA c) as [a [H' H'0]].
  apply in_or_app; right; left...
  auto.
 destruct (in_split _ _ H'0) as [A1 [A2 H'1]].
 destruct (in_split _ _ H') as [a1 [a2 H'2]].
 clear H'0 H'; subst.
 assert (H' := ReplaceCircuit_A _ _ _ _ _ c' DA AA AT).
(* map (map ...) cannot be rewritten... why? *)
 refine (eq_ind_r (fun x => PLCAconnect P L (C1 ++ c' :: C2) x o1 o2) _ H').
 apply (fun h => eq_ind _ (fun x => OIn P L (C1 ++ c' :: C2) x o1) h _ H') in H17.
 apply (fun h => eq_ind _ (fun x => OIn P L (C1 ++ c' :: C2) x o2) h _ H') in H19.
 apply (fun h => eq_ind _ (fun x => Aconstraint x) h _ H') in H0.
 apply (fun h => eq_ind _ (fun x => Outermostconsistency2 x o) h _ H') in H11.
 apply (fun h => eq_ind _ (fun x => Areaconsistency x) h _ H') in H13.
 apply (fun h => eq_ind _ (fun x => DistinctA x) h _ H') in H18.
 apply (fun h => eq_ind _ (fun x => CAconsistency (C1 ++ c' :: C2) x o) h _ H') in H8.
 apply (fun h => eq_ind _ (fun x => ACconsistency (C1 ++ c' :: C2) x) h _ H') in H9.
 clear H'.
 assert (OIn P L (C1 ++ c :: C2) (A1 ++ (a1 ++ c :: a2) :: A2) (replace_c_object c' c (replace_a_object (a1 ++ c' :: a2) (a1 ++ c :: a2) o1))).
  clear -H17.
  destruct o1; auto.
   simpl in *; destruct eq_listline_dec with c' c0.
    apply in_or_app; right; left; auto.
    apply in_or_app; simpl; apply in_app_or in H17; destruct H17 as [? | [? | ?]]; auto.
    elim n; auto.
   simpl in *; destruct eq_listcircuit_dec with (a1 ++ c' :: a2) a.
    apply in_or_app; right; left; auto.
    apply in_or_app; simpl; apply in_app_or in H17; destruct H17 as [? | [? | ?]]; auto.
    elim n; auto.
 assert (OIn P L (C1 ++ c :: C2) (A1 ++ (a1 ++ c :: a2) :: A2) (replace_c_object c' c (replace_a_object (a1 ++ c' :: a2) (a1 ++ c :: a2) o2))).
  clear -H19.
  destruct o2; auto.
   simpl in *; destruct eq_listline_dec with c' c0.
    apply in_or_app; right; left; auto.
    apply in_or_app; simpl; apply in_app_or in H19; destruct H19 as [? | [? | ?]]; auto.
    elim n; auto.
   simpl in *; destruct eq_listcircuit_dec with (a1 ++ c' :: a2) a.
    apply in_or_app; right; left; auto.
    apply in_or_app; simpl; apply in_app_or in H19; destruct H19 as [? | [? | ?]]; auto.
    elim n; auto.
 assert (PLCAcon' := PLCAcon _ _ H20 H21).
 cut (exists path : list object, connectpath P L (C1 ++ c' :: C2) (A1 ++ (a1 ++ c' :: a2) :: A2) o1 o2 path).
  intro H22; destruct H22.
  eapply PLCAconnect_path; unfold PLCAconsistency; unfold PLCAconstraints; eauto.
  intuition.
 apply connectpath_PLCAconnect in PLCAcon'; auto.
 clear -PLCAcon' rotc DC DA AA H17 H19.
 destruct PLCAcon'.
 revert o1 H17 H.
 induction x; intros.
  inversion H.
  inversion H; subst.
 -assert (o1 = o2).
   destruct o1; destruct o2; try (inversion H0; subst; auto; fail).
   simpl in H0.
   destruct eq_listline_dec with c' c0;
     destruct eq_listline_dec with c' c1;
     subst; inversion H0; subst; auto.
   simpl in H19.
   elim DC with c1.
    apply in_or_app; simpl; auto.
    apply rin_setremove_remove.
    apply rin_or_app.
    apply in_app_or in H19; destruct H19 as [? | [? | ?]].
     left; apply in_rin; auto.
     elim n; auto.
     right; apply in_rin; auto.
   simpl in H17.
   elim DC with c.
    apply in_or_app; simpl; auto.
    apply rin_setremove_remove.
    apply rin_or_app.
    apply in_app_or in H17; destruct H17 as [? | [? | ?]].
     left; apply in_rin; auto.
     elim n; auto.
     right; apply in_rin; auto.
   simpl in *.
   destruct eq_listcircuit_dec with (a1 ++ c' :: a2) a;
     destruct eq_listcircuit_dec with (a1 ++ c' :: a2) a0;
     subst; inversion H0; subst; auto.
   simpl in H19.
   elim DA with (a1 ++ c :: a2).
    apply in_or_app; simpl; auto.
    apply ain_setremove_remove.
    apply ain_or_app.
    apply in_app_or in H19; destruct H19 as [? | [? | ?]].
     left; apply in_ain; auto.
     elim n; auto.
     right; apply in_ain; auto.
   simpl in H17.
   elim DA with (a1 ++ c :: a2).
    apply in_or_app; simpl; auto.
    apply ain_setremove_remove.
    apply ain_or_app.
    apply in_app_or in H17; destruct H17 as [? | [? | ?]].
     left; apply in_ain; auto.
     elim n; auto.
     right; apply in_ain; auto.
  subst; clear H0.
  exists (o2 :: nil).
  constructor; auto.
 -destruct o1; simpl H0; inversion H0; clear H0; subst.
  destruct (IHx (o_line l)); simpl; auto.
  exists (o_point p0 :: x0).
  apply pathPL with l; simpl; auto.
 -destruct o1; simpl in H0; inversion H0; clear H0; subst.
  apply in_app_or in H4; destruct H4 as [? | [? | ?]].
  *destruct (IHx (o_circuit c0)); simpl; auto.
    apply in_or_app; auto.
    destruct eq_listline_dec with c' c0.
     elim DC with c; subst.
      apply in_or_app; simpl; auto.
      apply rin_setremove_remove.
      apply rin_or_app; left; apply eq_rin_elem with c0.
       apply Rot_sym; auto.
       apply in_rin; auto.
    auto.
   exists (o_line l0 :: x0); apply pathLC with c0; auto.
   apply in_or_app; auto.
  *subst.
   destruct (IHx (o_circuit c')); simpl; auto.
    apply in_or_app; simpl; auto.
    destruct eq_listline_dec with c' c'.
     auto.
     elim n; auto.
   exists (o_line l0 :: x0); apply pathLC with c'; auto.
   apply in_or_app; simpl; auto.
   apply LIn_In in H6; destruct H6.
    apply in_lin; apply RotIn with c0; auto.
    apply In_LIn_r; apply RotIn with c0; auto.
  *destruct (IHx (o_circuit c0)); simpl; auto.
    apply in_or_app; simpl; auto.
    destruct eq_listline_dec with c' c0.
     elim DC with c; subst.
      apply in_or_app; simpl; auto.
      apply rin_setremove_remove.
      apply rin_or_app; right; apply eq_rin_elem with c0.
       apply Rot_sym; auto.
       apply in_rin; auto.
    auto.
   exists (o_line l0 :: x0); apply pathLC with c0; auto.
   apply in_or_app; simpl; auto.
 -destruct o1; simpl in *; inversion H0; clear H0; subst.
  apply in_app_or in H4; destruct H4 as [? | [? | ?]].
  *destruct (IHx (o_area a0)).
    simpl; apply in_or_app; auto.
    simpl; destruct eq_listcircuit_dec with (a1 ++ c' :: a2) a0.
     elim DA with (a1 ++ c :: a2).
      apply in_or_app; simpl; auto.
      apply ain_setremove_remove; apply ain_or_app.
       left; apply eq_ain_elem with a0; auto.
        subst; apply eq_area_rot.
        apply Rot_sym; auto.
        apply in_ain; auto.
    auto.
   exists (o_circuit c1 :: x0).
   destruct eq_listline_dec with c' c1.
    assert (a0 = (a1 ++ c :: a2)).
    apply AA with c.
     apply in_or_app; simpl; auto.
     apply in_or_app; simpl; auto.
     auto.
     apply in_or_app; simpl; auto.
    elim DA with (a1 ++ c :: a2).
     apply in_or_app; simpl; auto.
     apply ain_setremove_remove.
     apply ain_or_app; left; rewrite <- H2; apply in_ain; auto.
   apply pathCA with a0; auto.
    apply in_or_app; auto.
  *destruct (IHx (o_area (a1 ++ c' :: a2))); subst.
    simpl; apply in_or_app; simpl; auto.
    simpl.
     destruct eq_listcircuit_dec with (a1 ++ c' :: a2) (a1 ++ c' :: a2).
      auto.
      elim n; auto.
   exists (o_circuit c1 :: x0).
   apply pathCA with (a1 ++ c' :: a2).
    auto.
    apply in_or_app; simpl; auto.
    destruct eq_listline_dec with c' c1.
     subst; apply in_or_app; simpl; auto.
     apply in_or_app; simpl; apply in_app_or in H6; destruct H6 as [? | [? | ?]]; auto.
     subst; apply in_app_or in H17; destruct H17 as [? | [? | ?]].
      elim DC with c1.
       apply in_or_app; simpl; auto.
       apply rin_setremove_remove.
       apply rin_or_app; left; apply in_rin; auto.
      auto.
      elim DC with c1.
       apply in_or_app; simpl; auto.
       apply rin_setremove_remove.
       apply rin_or_app; right; apply in_rin; auto.
    auto.
  *destruct (IHx (o_area a0)).
    simpl; apply in_or_app; simpl; auto.
    simpl; destruct eq_listcircuit_dec with (a1 ++ c' :: a2) a0.
     elim DA with (a1 ++ c :: a2).
      apply in_or_app; simpl; auto.
      apply ain_setremove_remove; apply ain_or_app.
       right; apply eq_ain_elem with a0; auto.
        subst; apply eq_area_rot.
        apply Rot_sym; auto.
        apply in_ain; auto.
    auto.
   exists (o_circuit c1 :: x0).
   destruct eq_listline_dec with c' c1.
    assert (a0 = (a1 ++ c :: a2)).
    apply AA with c.
     apply in_or_app; simpl; auto.
     apply in_or_app; simpl; auto.
     auto.
     apply in_or_app; simpl; auto.
    elim DA with (a1 ++ c :: a2).
     apply in_or_app; simpl; auto.
     apply ain_setremove_remove.
     apply ain_or_app; right; rewrite <- H2; apply in_ain; auto.
   apply pathCA with a0; auto.
    apply in_or_app; simpl; auto.
 -destruct o1; simpl in *; inversion H0; clear H0; subst.
  destruct (IHx (o_point p)); simpl; auto.
  exists (o_line l0 :: x0).
  apply pathLP with p; simpl; auto.
 -destruct o1; simpl in *; inversion H0; clear H0; subst.
  destruct eq_listline_dec with c' c1.
  *subst.
   destruct (IHx (o_line l)).
    simpl; auto.
    simpl; auto.
   exists (o_circuit c1 :: x0).
   apply pathCL with l.
    apply in_or_app; simpl; auto.
    auto.
    apply LIn_In in H6; destruct H6.
     apply in_lin; apply RotIn with c; auto.
     apply In_LIn_r; apply RotIn with c; auto.
    auto.
  *destruct (IHx (o_line l)).
    simpl; auto.
    simpl; auto.
   exists (o_circuit c1 :: x0).
   apply pathCL with l; auto.
 -destruct o1; simpl in *; inversion H0; clear H0; subst.
  apply in_app_or in H4; destruct H4 as [? | [? | ?]].
  *destruct (IHx (o_circuit c0)).
    simpl; apply in_or_app; auto.
    simpl; destruct eq_listline_dec with c' c0.
     elim DC with c.
      apply in_or_app; simpl; auto.
      apply rin_setremove_remove; apply rin_or_app.
       left; apply eq_rin_elem with c0; auto.
        subst; apply Rot_sym; auto.
        apply in_rin; auto.
    auto.
   exists (o_area a :: x0).
   destruct eq_listcircuit_dec with (a1 ++ c' :: a2) a.
   apply pathAC with c0; subst.
    apply in_or_app; simpl; auto.
    apply in_or_app; simpl; auto.
    apply in_or_app; simpl.
    apply in_app_or in H6; destruct H6 as [? | [? | ?]]; auto.
     elim DC with c; subst.
      apply in_or_app; simpl; auto.
      apply rin_setremove_remove.
      apply rin_or_app; left; apply in_rin; auto.
    auto.
   apply pathAC with c0.
    auto.
    apply in_or_app; simpl; auto.
    auto.
    auto.
  *destruct (IHx (o_circuit c')); subst.
    simpl; apply in_or_app; simpl; auto.
    simpl; destruct eq_listline_dec with c' c'.
     auto.
     elim n; auto.
   exists (o_area a :: x0).
   subst; apply pathAC with c'; auto.
    apply in_or_app; simpl; auto.
    destruct eq_listcircuit_dec with (a1 ++ c' :: a2) a.
     subst; apply in_or_app; simpl; auto.
     assert (a = (a1 ++ c0 :: a2)).
      apply AA with c0; auto.
       apply in_or_app; simpl; auto.
       apply in_or_app; simpl; auto.
     apply in_app_or in H17.
     destruct H17 as [? | [? | ?]].
      elim DA with a; subst.
       apply in_or_app; simpl; auto.
       apply ain_setremove_remove.
       apply ain_or_app; left; apply in_ain; auto.
      elim n; auto.
      elim DA with a; subst.
       apply in_or_app; simpl; auto.
       apply ain_setremove_remove.
       apply ain_or_app; right; apply in_ain; auto.
  *destruct (IHx (o_circuit c0)).
    simpl; apply in_or_app; simpl; auto.
    simpl; destruct eq_listline_dec with c' c0.
     elim DC with c.
      apply in_or_app; simpl; auto.
      apply rin_setremove_remove; apply rin_or_app.
       right; apply eq_rin_elem with c0; auto.
        subst; apply Rot_sym; auto.
        apply in_rin; auto.
    auto.
   exists (o_area a :: x0).
   destruct eq_listcircuit_dec with (a1 ++ c' :: a2) a.
   apply pathAC with c0; subst.
    apply in_or_app; simpl; auto.
    apply in_or_app; simpl; auto.
    apply in_or_app; simpl.
    apply in_app_or in H6; destruct H6 as [? | [? | ?]]; auto.
     elim DC with c; subst.
      apply in_or_app; simpl; auto.
      apply rin_setremove_remove.
      apply rin_or_app; right; apply in_rin; auto.
    auto.
   apply pathAC with c0.
    auto.
    apply in_or_app; simpl; auto.
    auto.
    auto.
(* rotateOutermost *)
+assert (PLCAconstraints L (map (replace_circuit o o') C) A o' /\ PLCAconsistency P L (map (replace_circuit o o') C) A o' /\ PLCAeuler P L (map (replace_circuit o o') C) A).
  assert (PLCAequivalence P L C A o P L (map (replace_circuit o o') C) A o').
   eapply rotateOutermost...
  eapply PLCAeq_planar_except_for_connect; eauto; unfold PLCAconstraints, PLCAconsistency, PLCAeuler; intuition.
 unfold PLCAconstraints, PLCAconsistency, PLCAeuler in H;
  intuition.
 destruct (in_split _ _ O1) as [C1 [C2 H']]; subst.
 assert (H' := ReplaceCircuit_C _ _ _ o' DC).
 rewrite H' in *; clear H'.
 assert (OIn P L (C1 ++ o :: C2) A (replace_c_object o' o o1)).
  destruct o1; simpl in *; auto.
  apply in_or_app; simpl.
  destruct eq_listline_dec with o' c.
   auto.
   apply in_app_or in H17; destruct H17 as [? | [? | ?]]; auto.
   elim n; auto.
 assert (OIn P L (C1 ++ o :: C2) A (replace_c_object o' o o2)).
  destruct o2; simpl in *; auto.
  apply in_or_app; simpl.
  destruct eq_listline_dec with o' c.
   auto.
   apply in_app_or in H19; destruct H19 as [? | [? | ?]]; auto.
   elim n; auto.
 assert (PLCAcon' := PLCAcon _ _ H20 H21).
 cut (exists path : list object, connectpath P L (C1 ++ o' :: C2) A o1 o2 path).
  intro H22; destruct H22.
  eapply PLCAconnect_path; unfold PLCAconsistency; unfold PLCAconstraints; eauto.
  intuition.
 apply connectpath_PLCAconnect in PLCAcon'; auto.
 clear -PLCAcon' roto DC O1 O2 H17 H19.
 destruct PLCAcon'.
 revert o1 H17 H.
 induction x; intros.
  inversion H.
  inversion H; subst.
 -exists (o1 :: nil).
  destruct o1; destruct o2; simpl in *; inversion H0; clear H0; subst.
  *constructor.
   simpl; auto.
  *constructor.
   simpl; auto.
  *destruct eq_listline_dec with o' c;
     destruct eq_listline_dec with o' c0;
     simpl in H2; inversion H2; clear H2; subst.
     subst; constructor.
     simpl; auto.
     elim DC with c0.
      apply in_or_app; simpl; auto.
      apply rin_setremove_remove.
      apply rin_or_app; apply in_app_or in H19; destruct H19 as [? | [? | ?]].
       left; apply in_rin; auto.
       elim n; auto.
       right; apply in_rin; auto.
     elim DC with o.
      apply in_or_app; simpl; auto.
      apply rin_setremove_remove.
      apply rin_or_app; apply in_app_or in H17; destruct H17 as [? | [? | ?]].
       left; apply in_rin; auto.
       elim n; auto.
       right; apply in_rin; auto.
     constructor; simpl; auto.
  *constructor.
   simpl; auto.
 -destruct o1; simpl in *; inversion H0; clear H0; subst.
  destruct (IHx (o_line l)); simpl; auto.
  exists (o_point p0 :: x0).
  apply pathPL with l; auto.
 -destruct o1; simpl in *; inversion H0; clear H0; subst.
  apply in_app_or in H4; destruct H4 as [? | [? | ?]].
  *destruct (IHx (o_circuit c)).
   simpl; apply in_or_app; simpl; auto.
   simpl.
    destruct eq_listline_dec with o' c.
     elim DC with o; subst.
      apply in_or_app; simpl; auto.
      apply rin_setremove_remove.
       apply rin_or_app; left; apply eq_rin_elem with c.
        apply Rot_sym; auto.
        apply in_rin; auto.
     auto.
   exists (o_line l0 :: x0).
   apply pathLC with c; auto.
   apply in_or_app; simpl; auto.
  *destruct (IHx (o_circuit o')); subst; simpl.
    apply in_or_app; simpl; auto.
    destruct eq_listline_dec with o' o'.
     auto.
     elim n; auto.
   exists (o_line l0 :: x0).
   apply pathLC with o'; auto.
    apply in_or_app; simpl; auto.
    apply LIn_In in H6; destruct H6.
     apply in_lin; apply RotIn with c; auto.
     apply In_LIn_r; apply RotIn with c; auto.
  *destruct (IHx (o_circuit c)).
   simpl; apply in_or_app; simpl; auto.
   simpl.
    destruct eq_listline_dec with o' c.
     elim DC with o; subst.
      apply in_or_app; simpl; auto.
      apply rin_setremove_remove.
       apply rin_or_app; right; apply eq_rin_elem with c.
        apply Rot_sym; auto.
        apply in_rin; auto.
     auto.
   exists (o_line l0 :: x0).
   apply pathLC with c; auto.
   apply in_or_app; simpl; auto.
 -destruct o1; simpl in *; inversion H0; clear H0; subst.
  destruct (IHx (o_area a0)); simpl; auto.
  exists (o_circuit c0 :: x0).
  apply pathCA with a0; auto.
   destruct eq_listline_dec with o' c0.
    elim O2 with a0; auto.
     apply in_rin; auto.
    auto.
 -destruct o1; simpl in *; inversion H0; clear H0; subst.
  destruct (IHx (o_point p)); simpl; auto.
  exists (o_line l0 :: x0).
  apply pathLP with p; auto.
 -destruct o1; simpl in *; inversion H0; clear H0; subst.
  destruct (IHx (o_line l)); simpl; auto.
  exists (o_circuit c0 :: x0).
  apply pathCL with l; auto.
   destruct eq_listline_dec with o' c0.
    apply LIn_In in H6; subst; destruct H6.
     apply in_lin; apply RotIn with o; auto.
     apply In_LIn_r; apply RotIn with o; auto.
    auto.
 -destruct o1; simpl in *; inversion H0; clear H0; subst.
  apply in_app_or in H4; destruct H4 as [? | [? | ?]].
  *destruct (IHx (o_circuit c)).
   simpl; apply in_or_app; simpl; auto.
   simpl.
    destruct eq_listline_dec with o' c.
     elim DC with o; subst.
      apply in_or_app; simpl; auto.
      apply rin_setremove_remove.
       apply rin_or_app; left; apply eq_rin_elem with c.
        apply Rot_sym; auto.
        apply in_rin; auto.
     auto.
   exists (o_area a :: x0).
   apply pathAC with c; auto.
   apply in_or_app; simpl; auto.
  *elim O2 with a; auto.
   rewrite H0; apply in_rin; auto.
  *destruct (IHx (o_circuit c)).
   simpl; apply in_or_app; simpl; auto.
   simpl.
    destruct eq_listline_dec with o' c.
     elim DC with o; subst.
      apply in_or_app; simpl; auto.
      apply rin_setremove_remove.
       apply rin_or_app; right; apply eq_rin_elem with c.
        apply Rot_sym; auto.
        apply in_rin; auto.
     auto.
   exists (o_area a :: x0).
   apply pathAC with c; auto.
   apply in_or_app; simpl; auto.
(* PLCAtrans *)
+intuition.
Qed.





