Require Import List Arith Omega.
Require Import EqIn PLCAobject PLCAtrail.


(* simple_path (terminal point) (terminal point) (points of path) (lines of path) *)
Inductive simplepath : Point -> Point -> list Point -> list Line -> Prop :=
| nil_path  : forall(p : Point), simplepath p p (p::nil) nil
| step_path : forall(p1 p2 p3 : Point)(pl : list Point)(ll : list Line),
   simplepath p2 p1 pl ll
-> ~In p3 pl
-> simplepath p3 p1 (p3::pl) ((p3, p2)::ll).


Lemma simplepath_onlyif_trail :
forall{x y : Point}{pl : list Point}{ll : list Line},
 simplepath x y pl ll -> trail x y pl ll.
Proof.
intros x y pl ll ph.
induction ph.
 exact (nil_trail _).
 apply (step_trail _ _ _ _ _ IHph).
 intros D.
 subst p2.
 exact (H (trail_start_pl IHph)).
 intros D.
 destruct (LIn_In _ _ D) as [D1 | D2].
  exact (H (trail_ll_pl IHph p3 _ (InPL_left _ _) D1)).
  exact (H (trail_ll_pl IHph p3 _ (InPL_right _ _) D2)).
Qed.


(* Trail property *)
Lemma path_head :
forall{x y : Point}{pl : list Point}{ll : list Line},
 simplepath x y pl ll -> x = fst (hd (y, y) ll).
Proof.
intros x y pl ll ph.
exact (trail_head (simplepath_onlyif_trail ph)).
Qed.


Lemma path_tail :
forall{x y : Point}{pl : list Point}{ll : list Line},
 simplepath x y pl ll -> y = snd (last ll (x, x)).
Proof.
intros x y pl ll ph.
exact (trail_tail (simplepath_onlyif_trail ph)).
Qed.


Lemma path_start_pl :
forall{s e : Point}{pl : list Point}{ll : list Line},
 simplepath s e pl ll -> In s pl.
Proof.
intros s e pl ll ph.
exact (trail_start_pl (simplepath_onlyif_trail ph)).
Qed.


Lemma path_end_pl :
forall{s e : Point}{pl : list Point}{ll : list Line},
 simplepath s e pl ll -> In e pl.
Proof.
intros s e pl ll ph.
exact (trail_end_pl (simplepath_onlyif_trail ph)).
Qed.


Lemma path_ll_pl :
forall{p1 p2 : Point}{pl : list Point}{ll : list Line}(ph : simplepath p1 p2 pl ll)(p : Point)(l : Line),
 InPL p l -> In l ll -> In p pl.
Proof.
intros p1 p2 pl ll ph p l PL INL.
exact (trail_ll_pl (simplepath_onlyif_trail ph) p l PL INL).
Qed.


Lemma path_pl_ll :
forall{x y p : Point}{pl : list Point}{ll : list Line},
   simplepath x y pl ll
-> 1 <= length ll
-> In p pl
-> exists l : Line, InPL p l /\ In l ll.
Proof.
intros x y p pl ll ph lg PL.
exact (trail_pl_ll (simplepath_onlyif_trail ph) lg PL).
Qed.


Lemma reverse_step_path :
forall{p1 p2 p3 : Point}{pl : list Point}{ll : list Line},
 simplepath p2 p1 pl ll -> ~In p3 pl -> simplepath p2 p3 (pl++(p3::nil)) (ll++((p1, p3)::nil)).
Proof.
intros.
induction H; intros.
 simpl in *.
 apply step_path; auto.
 exact (nil_path _).
 intros D; destruct D; try contradiction.
 apply H0; auto.
 simpl; apply step_path.
 apply IHsimplepath; auto.
 intros D; apply H0; simpl in *; auto.
 intros D.
 apply in_app_or in D.
 destruct D; auto.
 simpl in H2; destruct H2; try contradiction.
 subst.
 apply H0; simpl; auto.
Qed.


Lemma rev_path :
forall{x y : Point}{pl : list Point}{ll : list Line},
 simplepath x y pl ll -> simplepath y x (rev pl) (reverse_linelist ll).
Proof.
intros x y pl ll ph.
unfold reverse_linelist.
induction ph; simpl.
 exact (nil_path p).
 apply (reverse_step_path IHph).
 intros D.
 simpl in D.
 rewrite <- in_rev in D.
 exact (H D).
Qed.


Lemma not_simplepath :
forall{p : Point}{pl : list Point}{ll : list Line}{l : Line},
 ~simplepath p p pl (l::ll).
Proof.
intros p pl ll l D.
inversion D; subst.
exact (H5 (path_end_pl H4)).
Qed.


Lemma app_path :
forall{p1 p2 p3 p4 : Point}{pl1 pl2 : list Point}{ll1 ll2 : list Line},
   simplepath p1 p2 pl1 ll1
-> simplepath p3 p4 pl2 ll2
-> (forall(p : Point), In p pl1 -> ~In p pl2)
-> simplepath p1 p4 (pl1++pl2) (ll1 ++ (p2, p3) :: ll2).
Proof.
intros p1 p2 p3 p4 pl1 pl2 ll1 ll2 ph1 ph2 F.
induction ph1; simpl.
 apply (step_path _ _ _ _ _ ph2).
 intros D.
 exact (F p (or_introl _ (eq_refl _)) D).
 apply step_path.
 apply IHph1.
 intros.
 exact (F p (or_intror _ H0)).
 intros D.
 apply in_app_iff in D.
 destruct D.
 exact (H H0).
 exact (F p0 (or_introl _ (eq_refl _)) H0).
Qed.


Lemma U_turn_simplepath :
forall{x y : Point}{pl : list Point}{ll : list Line},
   simplepath x y pl ll
-> x <> y
-> 2 <= length ll
-> (y, x) :: ll = reverse_linelist ((y, x) :: ll)
-> False.
Proof.
intros.
assert(In (x, y) (reverse_linelist ((y, x) :: ll))).
  unfold reverse_linelist.
  simpl.
  apply in_app_iff.
 right; exact (in_eq _ _ ).
rewrite <- H2 in H3.
simpl in H3.
 destruct H3.
 inversion H3; subst.
 exact (H0 (eq_refl _)).
inversion H; subst.
  exact (H0 (eq_refl _)).
 simpl in H3.
  destruct H3.
  inversion H3; subst.
  inversion H4; subst.
  simpl in H1; omega.
  exact (not_simplepath H4).
  exact (H5 (path_ll_pl H4 x _ (InPL_left _ _) H3)).
Qed.


Lemma simplepath_if_nodup_trail :
forall{x y : Point}{pl : list Point}{ll : list Line},
 trail x y pl ll -> NoDup pl -> simplepath x y pl ll.
Proof.
intros; induction H.
 constructor.
 constructor.
  apply IHtrail.
   inversion H0; now auto.
  inversion H0; now auto.
Qed.
