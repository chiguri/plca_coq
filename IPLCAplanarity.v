Require Import List.
Require Import PLCAobject InductivePLCA PLCAconsistency PLCAconstraints PLCAplanar.
Require Import IPLCAconsistency IPLCAconnected IPLCAeuler.



Lemma IPLCA_satisfies_planarity_condition :
forall(P : list Point)(L : list Line)(C : list Circuit)(A : list Area)(o : Circuit),
 I P L C A o -> PLCAplanarity_condition P L C A o.
intros P L C A o I.
unfold PLCAplanarity_condition.
split.
exact (@InductivePLCAconstraints P L C A o I).
split.
exact (@InductivePLCAconsistency P L C A o I).
split.
exact (IPLCA_is_satisfied_PLCAconnected P L C A o I).
exact (IPLCA_is_satisfied_PLCAeuler P L C A o I).
Qed.

Theorem IPLCA_is_planar :
forall(P : list Point)(L : list Line)(C : list Circuit)(A : list Area)(o : Circuit),
 I P L C A o -> PLCAplanar P L C A o.
Proof.
intros P L C A o I.
apply PLCAplanarity_condition_implies_be_planar.
apply IPLCA_satisfies_planarity_condition.
exact I.
Qed.