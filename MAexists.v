Require Import List Arith Permutation.
Require Import EqIn Rotate SetRemove PLCAobject PLCAtrail PLCApath.
Require Import PLCAconstraints PLCAconsistency PLCAplanar.
Require Import PLCAequivalence PLCAequivalenceIso.
Require Import MergeArea.
Require Import PLCAplanarInOut.
Import ListNotations.

Lemma exists_innermost :
 forall {P : list Point} {L : list Line} {C : list Circuit} {A : list Area} {o : Circuit},
 PLCAplanarity_condition P L C A o ->
  exists (c : Circuit),
   forall (c' : Circuit),
    ConnectCircuit P L C A c c' ->
    c <> c' -> In [c'] A.
Admitted.


Theorem exists_PLCAeq :
 forall {P : list Point} {L : list Line} {C : list Circuit} {A : list Area} {o : Circuit},
 PLCAplanarity_condition P L C A o ->
  2 <= length A ->
  exists (Pl Pr : list Point) (Ll Lr : list Line) (Cl Cr : list Circuit) (Al Ar : list Area) (ol or : Circuit),
   PLCAequivalence P L C A o Pl Ll Cl Al ol /\
   MA_Relation Pl Ll Cl Al ol Pr Lr Cr Ar or.
Admitted.
