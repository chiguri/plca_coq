Require Import List Permutation.
Require Import EqIn Rotate SetRemove PLCAobject PLCAtrail PLCApath.
Require Import PLCAconstraints PLCAconsistency PLCAplanar.

Require Import MergeArea.
Require Import MAconsistency.

Theorem MAconnected :
 forall {Pl Pr : list Point} {Ll Lr : list Line} {Cl Cr : list Circuit} {Al Ar : list Area} {ol or : Circuit},
  MA_Relation Pl Ll Cl Al ol Pr Lr Cr Ar or ->
   PLCAconsistency Pl Ll Cl Al ol ->
   PLCAconstraints Ll Cl Al ol ->
   PLCAconnected Pl Ll Cl Al ->
    PLCAconnected Pr Lr Cr Ar.
intros Pl Pr Ll Lr Cl Cr Al Ar ol or ma consist constraint connected.
destruct ma as [P L C A o x y ap al a sp |
                P L C A o x y s ap pxy pyx al lxy lyx sp Hyx Hxy NLin len |
                P L C A o a s x ap pxx al lxx sp Hxx Nnil |
                P L C A o s x ap pxx al lxx sp Hxx Nnil].
+admit.
+admit.
+admit.
+admit.
Admitted.
