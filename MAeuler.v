Require Import List Arith Permutation.
Require Import EqIn Rotate SetRemove PLCAobject PLCAtrail PLCApath.
Require Import PLCAplanar.

Require Import MergeArea.

Lemma trail_pl_ll_len' :
 forall {x y : Point} {pl : list Point} {ll : list Line},
  trail x y pl ll -> length pl = S (length ll).
intros.
inversion H; subst.
 simpl; auto.
 simpl; rewrite (trail_pl_ll_len H); simpl; now auto.
Qed.

Ltac simple_narith H :=
 (rewrite plus_Sn_m in H; repeat rewrite plus_Sn_m in H; simple_narith H)
 || (rewrite <- plus_n_Sm in H; repeat rewrite <- plus_n_Sm in H; simple_narith H)
 || (rewrite <- plus_n_O in H; repeat rewrite <- plus_n_O in H; simple_narith H)
 || (inversion H; subst).

Ltac simple_narith_goal :=
 (rewrite plus_Sn_m; repeat rewrite plus_Sn_m; simple_narith_goal)
 || (rewrite <- plus_n_Sm; repeat rewrite <- plus_n_Sm; simple_narith_goal)
 || (rewrite <- plus_n_O; repeat rewrite <- plus_n_O; simple_narith_goal)
 || (repeat apply eq_S).


Theorem MAeuler :
 forall {Pl Pr : list Point} {Ll Lr : list Line} {Cl Cr : list Circuit} {Al Ar : list Area} {ol or : Circuit},
  MA_Relation Pl Ll Cl Al ol Pr Lr Cr Ar or ->
   PLCAeuler Pl Ll Cl Al ->
    PLCAeuler Pr Lr Cr Ar.
Proof.
intros Pl Pr Ll Lr Cl Cr Al Ar ol or Hma Heuler.
unfold PLCAeuler in *; inversion Hma; subst; simpl in *.
+repeat rewrite app_length in Heuler.
 apply simplepath_onlyif_trail in H.
 rewrite (trail_pl_ll_len' H) in Heuler.
 simple_narith Heuler.
  repeat rewrite plus_n_Sm in H1.
  repeat rewrite <- plus_assoc in H1.
  apply plus_reg_l in H1.
  rewrite <- plus_n_O; repeat rewrite plus_n_Sm.
  exact H1.
+repeat rewrite app_length in Heuler.
 apply simplepath_onlyif_trail in H.
 rewrite (trail_pl_ll_len H) in Heuler.
 simple_narith Heuler.
  repeat rewrite plus_n_Sm in H5.
  repeat rewrite <- plus_assoc in H5.
  apply plus_reg_l in H5.
  simple_narith_goal.
  rewrite <- H5.
  omega.
+repeat rewrite app_length in Heuler.
 apply simplepath_onlyif_trail in H.
 rewrite (trail_pl_ll_len H) in Heuler.
 simple_narith Heuler.
  repeat rewrite plus_n_Sm in H3.
  repeat rewrite <- plus_assoc in H3.
  apply plus_reg_l in H3.
  simple_narith_goal.
  rewrite <- H3.
  omega.
+repeat rewrite app_length in Heuler.
 apply simplepath_onlyif_trail in H.
 rewrite (trail_pl_ll_len H) in Heuler.
 simple_narith Heuler.
  repeat rewrite plus_n_Sm in H3.
  repeat rewrite <- plus_assoc in H3.
  apply plus_reg_l in H3.
  simple_narith_goal.
  simple_narith H3.
  omega.
Qed.
