Require Import List Arith.
Require Import EqIn SetRemove PLCAobject.
Require Import PLCAconstraints PLCAconsistency.


Section PLCAplanar.

Variable P : list Point.
Variable L : list Line.
Variable C : list Circuit.
Variable A : list Area.
Variable outermost : Circuit.


Inductive object :=
| o_point   : Point   -> object
| o_line    : Line    -> object
| o_circuit : Circuit -> object
| o_area    : Area    -> object.

(* for object *)
Definition OIn (o : object) : Prop :=
 match o with
| o_point p   => In p P
| o_line l    => In l L
| o_circuit c => In c C
| o_area a    => In a A
 end.

Lemma object_eq_dec : forall o1 o2 : object, {o1 = o2} + {o1 <> o2}.
intros; repeat (decide equality).
Qed.


Inductive PLCAconnect : object -> object -> Prop :=
| PLcon : forall(p : Point)(l : Line),   In p P  -> In l L -> InPL p l -> PLCAconnect (o_point p) (o_line l)
| LCcon : forall(l : Line)(c : Circuit), In l L  -> In c C  -> LIn l c  -> PLCAconnect (o_line l) (o_circuit c)
| CAcon : forall(c : Circuit)(a : Area), In c C  -> In a A  -> In c a   -> PLCAconnect (o_circuit c) (o_area a)
| SYMME : forall(o1 o2 : object),    PLCAconnect o1 o2 -> PLCAconnect o2 o1
| TRANS : forall(o1 o2 o3 : object), PLCAconnect o1 o2 -> PLCAconnect o2 o3 -> PLCAconnect o1 o3.


Definition PLCAconnected : Prop :=
 forall(o1 o2 : object), OIn o1 -> OIn o2 -> PLCAconnect o1 o2.




(* |P| - |L| - |C| + 2 * |A| = 0 *)
Definition PLCAeuler : Prop := length P + 2 * length A = length L + length C.



(* predicate of planarity *)
Parameter PLCAplanar : list Point -> list Line -> list Circuit -> list Area -> Circuit -> Prop.


Axiom PLCA_is_planar :
   PLCAconstraints L C A outermost
    -> PLCAconsistency P L C A outermost
    -> PLCAconnected
    -> (PLCAeuler <-> PLCAplanar P L C A outermost).


Definition PLCAplanarity_condition :=
 PLCAconstraints L C A outermost
 /\ PLCAconsistency P L C A outermost
 /\ PLCAconnected
 /\ PLCAeuler.


Lemma PLCAplanarity_condition_implies_be_planar :
  PLCAplanarity_condition -> PLCAplanar P L C A outermost.
Proof.
unfold PLCAplanarity_condition; intro H.
intuition; apply PLCA_is_planar; now auto.
Qed.



Lemma PLCAconnectReflectivity :
forall(o : object),
   PLCAconstraints L C A outermost
-> PLCAconsistency P L C A outermost
-> OIn o -> PLCAconnect o o.
Proof.
unfold PLCAconsistency, PLCAconstraints.
intros.
destruct H0 as [PL [LP [LC [CL [CA [AC [O1 [O2 [CC [AA [DP [DL [DC DA]]]]]]]]]]]]].
destruct H as [LT [CT [AT OC]]].
destruct o; simpl in *.
 destruct (PL _ H1) as [x [INL PIL]].
  apply (TRANS _ (o_line x)); [apply PLcon; auto | apply SYMME, PLcon; auto].
 destruct l as [pl pr].
  assert (In pl P).
   apply (LP _ _ H1).
    compute; auto.
   apply (TRANS _ (o_point pl)); [ apply SYMME, PLcon | apply PLcon]; auto.
 destruct c.
  destruct (CT _ H1) as [[ec tl] lg].
   elimtype False; simpl in lg; omega.
  assert (In l L \/ In (reverse l) L).
   apply LIn_In.
    apply (CL _ _ H1).
     simpl; auto.
   destruct H.
    assert (LIn l (l :: c)) by (left; constructor).
     apply (TRANS _ (o_line l)); [apply SYMME, LCcon | apply LCcon ]; auto.
    assert (LIn (reverse l) (l :: c)).
     left; constructor.
      apply reverse_reverse.
     apply (TRANS _ (o_line (reverse l))); [apply SYMME, LCcon | apply LCcon ]; auto.
 destruct a.
  destruct (AT _ H1) as [AT1 [AT2 AT3]].
   elimtype False; simpl in AT3; omega.
  assert (In c C).
   apply (AC _ _ H1).
    simpl; auto.
  apply (TRANS _ (o_circuit c)); [apply SYMME, CAcon | apply CAcon ]; simpl; auto.
Qed.


Lemma PLCAconnect_OIn :
 forall (o1 o2 : object), PLCAconnect o1 o2 -> OIn o1 /\ OIn o2.
intros; induction H; simpl in *; intuition.
Qed.



End PLCAplanar.


Hint Constructors PLCAconnect.


Ltac extractPLCAspec x :=
  ((try unfold PLCAplanarity_condition in x);
   destruct x as [[?LT [?CT [?AT ?OT]]]
                  [[?PL [?LP [?LC [?CL [?CA [?AC [?O1 [?O2 [?CC [?AA [?DP [?DL [?DC ?DA]]]]]]]]]]]]]
                  [?PLCAcon ?PLCAe]]]).
