Require Import List Arith.
Require Import EqIn SetRemove PLCAobject.
Require Import PLCAconstraints PLCAconsistency.
Require Import PLCAplanar.
Import ListNotations.

Section PLCAconnectpath.

Variable P : list Point.
Variable L : list Line.
Variable C : list Circuit.
Variable A : list Area.
Variable outermost : Circuit.



Inductive connectpath : object -> object -> list object -> Prop :=
| pathNil : forall (o : object), OIn P L C A o -> connectpath o o (o :: nil)
| pathPL : forall(p : Point)  (l : Line)   (o : object) (path : list object), In p P -> In l L -> InPL p l -> connectpath (o_line l)    o path -> connectpath (o_point p)   o (o_point p :: path)
| pathLC : forall(l : Line)   (c : Circuit)(o : object) (path : list object), In l L -> In c C -> LIn l c  -> connectpath (o_circuit c) o path -> connectpath (o_line l)    o (o_line l :: path)
| pathCA : forall(c : Circuit)(a : Area)   (o : object) (path : list object), In c C -> In a A -> In c a   -> connectpath (o_area a)    o path -> connectpath (o_circuit c) o (o_circuit c :: path)
| pathLP : forall(l : Line)   (p : Point)  (o : object) (path : list object), In l L -> In p P -> InPL p l -> connectpath (o_point p)   o path -> connectpath (o_line l)    o (o_line l :: path)
| pathCL : forall(c : Circuit)(l : Line)   (o : object) (path : list object), In c C -> In l L -> LIn l c  -> connectpath (o_line l)    o path -> connectpath (o_circuit c) o (o_circuit c :: path)
| pathAC : forall(a : Area)   (c : Circuit)(o : object) (path : list object), In a A -> In c C -> In c a   -> connectpath (o_circuit c) o path -> connectpath (o_area a)    o (o_area a :: path).



Definition connected_by_path : Prop :=
 forall(o1 o2 : object) (path : list object), OIn P L C A o1 -> OIn P L C A o2 -> connectpath o1 o2 path.


Lemma connectpath_cut_l :
 forall (o1 o2 o3 : object) (path1 path2 : list object),
  connectpath o1 o3 (path1 ++ o2 :: path2) -> connectpath o1 o2 (path1 ++ [o2]).
intros; revert o1 H; induction path1; intros.
+simpl in *.
 inversion H; subst; now constructor.
+simpl in H; inversion H; subst; simpl.
 -destruct path1; simpl in H4; inversion H4.
 -apply pathPL with l; now auto.
 -apply pathLC with c; now auto.
 -apply pathCA with a0; now auto.
 -apply pathLP with p; now auto.
 -apply pathCL with l; now auto.
 -apply pathAC with c; now auto.
Qed.


Lemma connectpath_cut_r :
 forall (o1 o2 o3 : object) (path1 path2 : list object),
  connectpath o1 o3 (path1 ++ o2 :: path2) -> connectpath o2 o3 (o2 :: path2).
intros; revert o1 H; induction path1; intros.
+simpl in *.
 inversion H; subst; now auto.
+simpl in H; inversion H; subst; simpl; eauto.
 destruct path1; simpl in H4; inversion H4.
Qed.



Lemma connectpath_in_l :
 forall (o1 o2 : object) (path : list object),
  connectpath o1 o2 path -> OIn P L C A o1.
intros; inversion H; simpl; now auto.
Qed.


Lemma connectpath_in_r :
 forall (o1 o2 : object) (path : list object),
  connectpath o1 o2 path -> OIn P L C A o2.
intros o1 o2 path; revert o1; induction path; intros.
 inversion H.
 inversion H; subst; try (eapply IHpath; eauto; fail).
  now auto.
Qed.






Lemma connectpath_trans :
 forall (o1 o2 o3 : object) (path1 path2 : list object),
  connectpath o1 o2 path1 -> connectpath o2 o3 (o2 :: path2) -> connectpath o1 o3 (path1 ++ path2).
intros o1 o2 o3 path1; revert o1 o2 o3; induction path1; intros.
 inversion H.
inversion H; subst.
 simpl; auto.
 eapply pathPL; eauto.
 eapply pathLC; eauto.
 eapply pathCA; eauto.
 eapply pathLP; eauto.
 eapply pathCL; eauto.
 eapply pathAC; eauto.
Qed.


Lemma connectpath_rev :
 forall (o1 o2 : object) (path : list object),
  connectpath o1 o2 path -> connectpath o2 o1 (rev path).
intros o1 o2 path; revert o1 o2; induction path; intros.
 inversion H.
inversion H; subst; simpl.
 constructor; auto.
 apply connectpath_trans with (o_line l); eauto.
  eapply pathLP; eauto; constructor; auto.
 apply connectpath_trans with (o_circuit c); eauto.
  eapply pathCL; eauto; constructor; auto.
 apply connectpath_trans with (o_area a0); eauto.
  eapply pathAC; eauto; constructor; auto.
 apply connectpath_trans with (o_point p); eauto.
  eapply pathPL; eauto; constructor; auto.
 apply connectpath_trans with (o_line l); eauto.
  eapply pathLC; eauto; constructor; auto.
 apply connectpath_trans with (o_circuit c); eauto.
  eapply pathCA; eauto; constructor; auto.
Qed.


Lemma connectpath_head :
 forall (o1 o2 : object) (path : list object),
  connectpath o1 o2 path ->
   exists (path' : list object),
    path = o1 :: path'.
intros.
inversion H; subst; eexists; eauto.
Qed.


(*
Lemma connectpath_tail :
 forall (o1 o2 : object) (path : list object),
  connectpath o1 o2 path ->
   exists (o : object) (path' : list object),
    path = path' ++ o' :: nil.
intros.
induction H.
?
*)

Theorem PLCAconnect_path :
 forall (o1 o2 : object) (path : list object),
   PLCAconstraints L C A outermost
   -> PLCAconsistency P L C A outermost
   -> connectpath o1 o2 path
   -> PLCAconnect P L C A o1 o2.
intros.
induction H1.
apply PLCAconnectReflectivity with outermost; auto.
apply TRANS with (o_line l).
constructor; auto.
auto.
apply TRANS with (o_circuit c).
constructor; auto.
auto.
apply TRANS with (o_area a).
constructor; auto.
auto.
apply TRANS with (o_point p).
constructor; auto.
auto.
apply TRANS with (o_line l).
constructor; auto.
auto.
apply TRANS with (o_circuit c).
constructor; auto.
auto.
Qed.


Theorem connectpath_PLCAconnect :
 forall (o1 o2 : object),
   OIn P L C A o1
   -> OIn P L C A o2
   -> PLCAconnect P L C A o1 o2
   -> exists path : list object, connectpath o1 o2 path.
intros.
induction H1.
exists (o_point p :: o_line l :: nil); eapply pathPL; eauto.
apply pathNil; auto.
exists (o_line l :: o_circuit c :: nil); eapply pathLC; eauto.
apply pathNil; auto.
exists (o_circuit c :: o_area a :: nil); eapply pathCA; eauto.
apply pathNil; auto.
destruct (IHPLCAconnect H0 H).
 exists (rev x); apply connectpath_rev; auto.
destruct PLCAconnect_OIn with P L C A o2 o3 as [H1 _]; auto.
destruct (IHPLCAconnect1 H H1).
destruct (IHPLCAconnect2 H1 H0).
destruct (connectpath_head _ _ _ H3) as [? ?]; subst.
exists (x ++ x1); eapply connectpath_trans; eauto.
Qed.



End PLCAconnectpath.


Hint Constructors connectpath.

