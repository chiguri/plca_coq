Require Import List Arith Permutation.
Require Import EqIn Rotate SetRemove PLCAobject PLCAtrail PLCApath InductivePLCA.
Require Import PLCAconstraints PLCAconsistency PLCAplanar PLCAequivalence PLCAequivalenceIso.
Require Import IPLCAconsistency IPLCAconnected IPLCAeuler IPLCAplanarity.
Require Import ReplaceCircuit MergeArea MAplanarity MAexists.

Import ListNotations.


Lemma NoDup_In_length :
forall {A : Type} (l1 l2 : list A),
 NoDup l1 -> (forall a : A, In a l1 -> In a l2) -> length l1 <= length l2.
induction l1; intros.
 simpl; omega.
 assert (In a l2) by (apply H0; simpl; auto).
 apply in_split in H1; destruct H1 as [l3 [l4 ?]]; subst.
 rewrite app_length; simpl.
 rewrite <- plus_n_Sm.
 apply le_n_S.
 rewrite <- app_length.
 inversion H; subst.
 apply IHl1.
  now auto.
  intros.
  assert (In a0 (l3 ++ a :: l4)) by (apply H0; simpl; auto).
  apply in_or_app; apply in_app_or in H2; destruct H2 as [H2 | [H2 | H2]]; auto.
  subst; now auto.
Qed.


Lemma NoDup_remove_In :
forall {A : Type} (a : A) (l11 l12 l21 l22 : list A),
 NoDup (l11 ++ a :: l12)
 -> (forall x : A, In x (l11 ++ a :: l12) -> In x (l21 ++ a :: l22))
 -> forall x : A, In x (l11 ++ l12) -> In x (l21 ++ l22).
intros.
assert (In x (l21 ++ a :: l22)).
 apply H0; apply in_app_or in H1; apply in_or_app; simpl; destruct H1; now auto.
apply in_app_or in H2; apply in_or_app; destruct H2 as [? | [? | ?]]; auto.
subst.
elim (NoDup_remove_2 _ _ _ H).
exact H1.
Qed.


Lemma Permutation_NoDup :
forall {A : Type} (l1 l2 : list A),
 NoDup l1 -> Permutation l1 l2 -> NoDup l2.
intros A l1 l2; revert l1; induction l2; intros.
 constructor.
 assert (In a l1).
  apply Permutation_in with (a :: l2).
   now apply Permutation_sym, H0.
   left; now auto.
 apply in_split in H1; destruct H1 as [l3 [l4 ?]]; subst.
  apply Permutation_sym, Permutation_cons_app_inv, Permutation_sym in H0.
   constructor.
    intro H1; apply (NoDup_remove_2 _ _ _ H).
     apply Permutation_in with l2.
      now apply Permutation_sym, H0.
      exact H1.
    apply IHl2 with (l3 ++ l4).
     apply NoDup_remove_1 with a.
      exact H.
      exact H0.
Qed.


Lemma trail_Distinct :
forall {x y : Point} {pl : list Point} {ll : list Line},
 trail x y pl ll -> DistinctL ll.
unfold DistinctL.
intros.
revert x pl H; induction ll; intros; simpl in *; inversion H; subst.
 now auto.
 destruct eq_dline_dec with l (x, p2).
  subst; now apply H8.
  destruct H0.
   subst; elim n; now auto.
   intro H1; destruct H1.
    apply H8; apply eq_lin_elem with l.
     now auto.
     apply in_lin; now auto.
    now apply (IHll H0 _ _ H3 H1).
Qed.


Lemma NoDup_In_NoDup :
 forall {A : Type} (l L : list A),
  (forall x y : A, {x = y} + {x <> y})
  -> (forall x, In x L -> In x l)
  -> length l = length L
  -> NoDup L
  -> NoDup l.
induction l; intros ? eq_dec; intros.
 constructor.
 destruct (in_dec eq_dec a L).
  apply in_split in i; destruct i as [L1 [L2 ?]]; subst.
   assert (~In a (L1 ++ L2)) by (apply NoDup_remove_2; auto).
   apply NoDup_remove_1 in H1.
   constructor.
    intro H3; apply in_split in H3; destruct H3 as [l1 [l2 ?]]; subst.
    assert (forall x, In x (L1 ++ L2) -> In x (l1 ++ l2)).
    {
     intros.
     destruct H with x.
     apply in_or_app; apply in_app_or in H3; destruct H3; simpl; now auto.
     subst; elim H2; now auto.
     apply in_or_app; apply in_app_or in H4; destruct H4 as [? | [? | ?]]; auto.
     subst; elim H2; now auto.
    }
    assert (length (L1 ++ L2) <= length (l1 ++ l2)) by (apply NoDup_In_length; auto).
    assert (length (L1 ++ L2) = S (length (l1 ++ l2))).
     rewrite app_length.
     rewrite app_length.
     rewrite app_length in H0.
     simpl in H0; rewrite app_length in H0; simpl in H0.
     omega.
    rewrite H5 in H4.
    omega.
    apply IHl with (L1 ++ L2).
    now auto.
    intros.
     destruct H with x.
      apply in_or_app; apply in_app_or in H3; destruct H3; simpl; now auto.
      subst; elim H2; now auto.
     now auto.
    rewrite app_length; rewrite app_length in H0; simpl in H0; omega.
   now auto.
  assert (forall x, In x L -> In x l).
   intros.
   destruct H with x.
    now auto.
    subst; elim n; now auto.
    now auto.
  assert (length L <= length l) by (apply NoDup_In_length; auto).
  rewrite <- H0 in H3; simpl in H3; omega.
Qed.



(* End Theorem *)
Theorem IPLCA_validity :
forall(P : list Point)(L : list Line)(C : list Circuit)(A : list Area)(o : Circuit),
 (PLCAplanarity_condition P L C A o)
<->
 (exists(P' : list Point)(L' : list Line)(C' : list Circuit)(A' : list Area)(o' : Circuit),
  I P' L' C' A' o' /\ PLCAequivalence P' L' C' A' o' P L C A o).
Proof.
intros P L C A o.
split.
(* -> *)
+remember (length A) as n.
 revert P L C A o Heqn; induction n; intros P L C A o Heqn CSP.
 elimtype False.
 extractPLCAspec CSP.
 destruct A.
 assert (exists l : Line, LIn l L /\ LIn l o).
   destruct (CT _ O1) as [CT1 CT2].
   destruct o.
   simpl in CT2; omega.
   apply (CL l) in O1; [ | exact (in_eq _ _) ].
   exists l; split; [ exact O1 | ].
   apply in_lin.
   left; now auto.
 destruct H as [l [LIN LLO]].
  destruct (LIn_In _ _ LLO) as [LL1 | LL2].
  apply LIn_reverse in LIN.
  apply LC in LIN.
  destruct LIN as [co [ILC INC]].
  apply CA in INC.
  destruct INC as [a' [IAO INA]].
   intros D.
   clear -ILC LL1 D OT.
   assert (Rot o co) by (rewrite D; exact (rotSame _)).
   exact (Rot_reverse_linelist_line l o co OT LL1 ILC H).
   exact INA.
   apply LC in LIN.
   destruct LIN as [co [ILC INC]].
   apply CA in INC.
   destruct INC as [a' [IAO INA]].
   intros D.
   clear -ILC LL2 D OT.
   assert (Rot co o) by (rewrite D; now apply rotSame).
   refine (Rot_reverse_linelist_line l co o _ ILC LL2 H).
    rewrite D; now exact OT.
   exact INA.
  simpl in Heqn; inversion Heqn.
 destruct n.
 destruct A.
  discriminate.
  simpl in Heqn; inversion Heqn.
  destruct A; try discriminate.
  clear H0 Heqn IHn.
  (* singleloop *)
  extractPLCAspec CSP.
  assert (Permutation C (o :: a)) as PermCoa.
  {
   unfold ACconsistency in AC.
   assert (forall (c : Circuit), In c (o :: a) -> In c C) as AC'.
    intros; destruct H.
     subst; apply O1.
     apply AC with a; simpl; now auto.
   unfold CAconsistency in CA.
   assert (forall (c : Circuit), In c C -> In c (o :: a)) as CA'.
    intros; simpl; destruct eq_listline_dec with c o.
     subst; left; now auto.
     right; destruct CA with c; auto.
      destruct H0 as [H1 [H2 | []]].
       subst; now auto.
   assert (NoDup C).
    apply DistinctC_NoDup; now auto.
   assert (NoDup (o :: a)).
    constructor.
     intro; apply O2 with a.
      simpl; now auto.
      apply in_rin; now auto.
    apply DistinctC_NoDup.
     destruct AT with a.
      simpl; now auto.
      unfold DistinctC; destruct H1; now auto.
   apply NoDup_Permutation; auto.
    intro; split; now auto.
  }
  assert (forall l : Line, In l L -> LIn l o) as LPerm.
  {
  intros l INl.
  assert (LIn (reverse l) L) as INl'.
   apply LIn_reverse.
   apply in_lin; exact INl.
  apply in_lin in INl.
  apply LC in INl.
  apply LC in INl'.
  destruct INl as [c [INl INc]].
  destruct INl' as [c' [INl' INc']].
  apply (Permutation_in _ PermCoa) in INc.
  destruct INc as [? | INc].
   subst.
   now apply in_lin, INl.
  apply (Permutation_in _ PermCoa) in INc'.
  destruct INc' as [? | INc'].
   subst.
   now apply LIn_reverse_inv, in_lin, INl'.
  destruct eq_listline_dec with c c'.
   destruct CT with c as [Tr _]; subst.
    apply Permutation_in with (o :: a).
    apply Permutation_sym, PermCoa.
    right; exact INc.
  destruct Tr as [x [pl Tr]].
  elim (trail_l_rev_l l Tr).
   exact INl.
   exact INl'.
  assert (~line_connect c c').
   destruct AT with a as [Con _].
    simpl; now auto.
    destruct (Con _ _ INc INc' n).
    exact H0.
  destruct H.
  unfold line_connect.
   exists l; split; now auto.
  }
  assert (forall l : Line, In l o -> LIn l L) as LPerm'.
  {
  intros; apply CL with o.
  now apply O1.
  now auto.
  }
  (* a has at least one element *)
  destruct a.
  {
   destruct C.
    unfold Outermostconsistency1 in O1; elim O1.
    destruct C.
     unfold Outermostconsistency1 in O1.
      simpl in O1; destruct O1 as [O1 | O1]; [ subst | elim O1 ].
       destruct o.
        destruct (CT []).
         simpl; now auto.
         simpl in H0; omega.
        assert (LIn l L).
        {
          apply CL with (l :: o);
           simpl; now auto.
        }
        destruct CT with (l :: o).
         simpl; now auto.
         destruct H0 as [x [pl H0]].
          inversion H0; subst.
           assert (In (p2, x)((x, p2) :: o)).
           {
            destruct LC with (p2, x).
             apply LIn_reverse_inv; unfold reverse; simpl; now auto.
             destruct H2.
              simpl in H3; destruct H3 as [? | []]; subst; now auto.
           }
           destruct H2.
            inversion H2; subst; elim H8; now auto.
            elim H9.
             apply LIn_reverse_inv; unfold reverse; simpl; apply in_lin; now auto.
     destruct O1 as [O1 | [O1 | O1]].
      destruct eq_listline_dec with c c0.
       elim DC with c; subst; subst; simpl.
        now auto.
        destruct eq_listline_dec with c0 c0;
           left; now apply rotSame.
       elim CA with c0; subst.
        intros.
         destruct H as [? [? | []]].
          subst; elim H.
        right; left; now auto.
        intro H; subst; now auto.
      destruct eq_listline_dec with c0 c.
       elim DC with c0; subst; subst; simpl.
        now auto.
        destruct eq_listline_dec with c c;
           left; now apply rotSame.
       elim CA with c; subst.
        intros.
         destruct H as [? [? | []]].
          subst; elim H.
        left; now auto.
        intro H; subst; now auto.
      destruct eq_listline_dec with o c.
       elim DC with o.
        left; now auto.
        simpl; destruct eq_listline_dec with o c; simpl.
         right; apply in_rin; now apply O1.
         now auto.
       destruct CA with c.
        left; now auto.
        intro; apply n; symmetry; now auto.
        destruct H.
         destruct H0 as [H0 | []].
          subst; elim H.
  }
  (* a does not have 2 or more circuits. *)
  destruct a.
   Focus 2.
   {
    elimtype False; clear CA AC DA.
    assert (~RIn o (c :: c0 :: a)) as O2'.
     intro; apply O2 with (c :: c0 :: a); simpl; now auto.
    clear O1 O2.
    clear AA.
    unfold Aconstraint in AT.
    assert (Area_constraints (c :: c0 :: a)) as AT' by (apply AT; simpl; auto).
    clear AT; unfold Area_constraints in AT'.
    destruct AT' as [AT1 [AT2 _]].
    unfold PLCAeuler in PLCAe.
    assert (length C = length (o :: c :: c0 :: a)) as Clen by (apply Permutation_length; auto).
    rewrite Clen in PLCAe.
    rewrite plus_comm in PLCAe; simpl in PLCAe; rewrite plus_comm in PLCAe; simpl in PLCAe.
    destruct OT as [[x [pl OT1]] OT2].
    inversion OT1; subst.
     simpl in OT2; omega.
    assert (length L = length pl0).
     rewrite (trail_pl_ll_len OT1).
      apply LIn_length.
       now auto.
       exact (trail_Distinct OT1).
       now auto.
       now auto.
    rewrite H2 in PLCAe.
    assert (forall p : Point, In p P -> In p pl0).
     intros.
     apply PL in H3.
     destruct H3 as [l [H3 H4]].
     apply LPerm in H3.
     destruct H3.
      inversion H3; subst.
       destruct H4; simpl in H4; inversion H4; subst.
        now apply (trail_end_pl H).
        now apply (trail_start_pl H).
       destruct l; unfold reverse in H5; simpl in H5; inversion H5; subst.
        destruct H4; simpl in H4; inversion H4; subst.
         now apply (trail_start_pl H).
         now apply (trail_end_pl H).
      apply LIn_In in H3; destruct H3.
       apply (trail_ll_pl H) with (l := l); now auto.
       apply (trail_ll_pl H) with (l := reverse l); auto.
        destruct l; unfold reverse; unfold InPL in *; simpl in *; destruct H4; now auto.
    assert (lenP := NoDup_In_length P pl0 (DistinctP_NoDup P DP) H3).
    injection PLCAe as PLCAe'; rewrite PLCAe' in lenP.
    revert lenP; generalize (length pl0); intros; omega.
   }
  Unfocus.
  assert (forall l : Line, In l o -> In (reverse l) c) as INoc.
  {
  intros l INo.
  assert (LIn l L) as INo'.
   now apply LPerm', INo.
  apply LIn_reverse, LC in INo'.
  destruct INo' as [c' [INc' INC]].
  apply (Permutation_in _ PermCoa) in INC.
  destruct INC as [? | [? | []]]; subst.
  destruct OT as [[x [pl Tr]] _].
  elim (trail_l_rev_l l Tr).
   exact INo.
   exact INc'.
  exact INc'.
  }
  assert (forall l : Line, In l c -> In (reverse l) o) as INco.
  {
  intros l INc.
  assert (LIn l L) as INc'.
   apply CL with c.
   apply Permutation_in with [o; c].
    apply Permutation_sym; exact PermCoa.
    right; left; now auto.
    exact INc.
  apply LIn_reverse, LC in INc'.
  destruct INc' as [c' [INc' INC]].
  apply (Permutation_in _ PermCoa) in INC.
  destruct INC as [? | [? | []]]; subst.
  exact INc'.
  destruct CT with c' as [[x [pl Tr]] _].
   apply Permutation_in with [o; c'].
    apply Permutation_sym; exact PermCoa.
    right; left; now auto.
  elim (trail_l_rev_l l Tr).
   exact INc.
   exact INc'.
  }
  destruct CT with c as [[y [pl' CT1]] CT2].
   apply AC with [c].
    simpl; now auto.
    simpl; now auto.
  inversion CT1; subst.
   simpl in CT2; omega.
  unfold PLCAeuler in PLCAe.
  rewrite (Permutation_length PermCoa) in PLCAe.
  simpl in PLCAe.
  rewrite plus_comm in PLCAe; simpl in PLCAe; rewrite plus_comm in PLCAe; simpl in PLCAe.
  injection PLCAe as PLCAe'; clear PLCAe.
  assert (forall p, In p pl -> In p P) as INplP.
  {
  intros p INp.
  assert (exists l : Line, InPL p l /\ In l ((y, p2) :: ll)).
   {
   apply (trail_pl_ll CT1).
   simpl; omega.
   right; exact INp.
   }
   destruct H2 as [l [INpl INl]].
   assert (LIn l L) as LINl.
   {
    apply CL with ((y, p2) :: ll).
    apply Permutation_in with [o; (y, p2) :: ll].
     now apply Permutation_sym, PermCoa.
     simpl; now auto.
    exact INl.
   }
   apply LIn_In in LINl; destruct LINl.
    apply LP with l.
     exact H2.
     exact INpl.
    apply LP with (reverse l).
     exact H2.
     now apply InPL_reverse, INpl.
  }
  assert (forall p, In p P -> In p pl) as INPpl.
  {
  intros p INP.
  apply PL in INP.
  destruct INP as [l [INL INpl]].
  apply LPerm in INL.
  apply lin_in in INL.
  destruct INL as [l' [eqll INo]].
  apply (InPL_eq_ul _ _ l' eqll) in INpl.
  inversion CT1; subst.
  apply INoc in INo.
  destruct INo as [e | INll].
   destruct l'; unfold reverse in e; simpl in e; inversion e; subst.
   simpl INpl; destruct INpl as [INpl | INpl]; inversion INpl; subst.
    apply (trail_start_pl H7).
    apply (trail_end_pl H7).
  apply InPL_reverse in INpl.
  exact (trail_ll_pl H7 p (reverse l') INpl INll).
  }
  assert (simplepath p2 y pl ll) as sp.
  {
   assert (NoDup pl) as nd.
   assert (length pl = length P) as lenP.
   {
    rewrite (trail_pl_ll_len CT1).
    rewrite PLCAe'.
    apply LIn_length.
     now apply (trail_Distinct CT1).
     exact DL.
     intros l INl.
      apply LIn_reverse_inv, LPerm'.
      now apply INco, INl.
     intros l INl.
      apply LPerm in INl.
      apply LIn_In in INl.
      destruct INl as [INl | INl].
       now apply LIn_reverse_inv, in_lin, INoc, INl.
       rewrite <- reverse_reverse with l.
       now apply in_lin, INoc, INl.
   }
   assert (NoDup P) as ndP by (exact (DistinctP_NoDup _ DP)).
   apply NoDup_In_NoDup with P.
    apply eq_point_dec.
    now auto.
    now auto.
    now auto.
   apply simplepath_if_nodup_trail; now auto.
  }
  simpl in CT2; apply le_S_n in CT2.
  assert (p2 <> y) as neq by (intro; elim H2; subst; auto).
  assert (IPLCA := single_loop _ _ _ _ sp neq CT2).
  repeat eexists.
  exact IPLCA.
  assert (Rot (reverse_linelist ((y, p2) :: ll)) o) as orot.
  {
   apply rev_path in sp.
   destruct OT as [[x [pl' tr]] OT].
   assert (In (p2, y) o) as INo.
    rewrite <- reverse_reverse with (p2, y).
    apply INco.
    unfold reverse; simpl; left; now auto.
   apply in_split in INo.
   destruct INo as [l1 [l2 ?]]; subst.
   rewrite reverse_linelist_cons.
   cut (reverse_linelist ll = l2 ++ l1).
    intro rll; rewrite rll.
    unfold reverse; simpl.
    apply Rot_trans with (l1 ++ [(p2, y)] ++ l2).
     rewrite app_assoc; rewrite <- app_assoc.
     now apply Rot_app.
     simpl; now apply rotSame.
   assert (forall l : Line, In l (l2 ++ l1) -> In l (reverse_linelist ll)) as INll.
   {
   rewrite <- app_nil_l with Line (reverse_linelist ll).
   apply NoDup_remove_In with (p2, y).
    apply Permutation_NoDup with (l1 ++ (p2, y) :: l2).
     now apply (trail_NoDup tr).
     apply Permutation_trans with ((p2, y) :: l1 ++ l2).
      now apply Permutation_sym, Permutation_middle.
      apply Permutation_cons_app.
       now apply Permutation_app_comm.
    intros.
     assert (In x0 (reverse_linelist ((y, p2) :: ll))).
      unfold reverse_linelist.
      apply (proj1 (in_rev _ _)).
      apply (proj2 (map_reverse x0 ((y, p2) :: ll))).
      apply INoc.
      apply in_or_app; simpl; apply in_app_or in H2; destruct H2 as [? | [? | ?]]; now auto.
     rewrite reverse_linelist_cons in H3; apply in_app_or in H3; destruct H3 as [? | [? | []]]; simpl; now auto.
   }
   assert (forall l : Line, In l (reverse_linelist ll) -> In l (l2 ++ l1)) as INll'.
   {
   rewrite <- app_nil_l with Line (reverse_linelist ll).
   apply NoDup_remove_In with (p2, y).
    apply Permutation_NoDup with (reverse_linelist ((y, p2) :: ll)).
     now apply (trail_NoDup (rev_trail CT1)).
     rewrite reverse_linelist_cons.
      unfold reverse; simpl.
      now apply Permutation_sym, Permutation_cons_append.
    intros.
     assert (In x0 (reverse_linelist ((y, p2) :: ll))).
      rewrite reverse_linelist_cons.
       unfold reverse; simpl in *; apply in_or_app; simpl; destruct H2; now auto.
      unfold reverse_linelist in H3.
      apply (proj2 (in_rev _ _)) in H3.
      apply (proj1 (map_reverse _ ((y, p2) :: ll))) in H3.
      apply INco in H3.
      rewrite reverse_reverse in H3.
      apply in_or_app; simpl; apply in_app_or in H3; destruct H3 as [? | [? | ?]]; now auto.
   }
   revert sp INll INll'; generalize (reverse_linelist ll) as ll1; generalize (rev pl) as pl1.
   assert (exists pl2 : list Point, trail y p2 pl2 (l2 ++ l1)) as Hpl2.
   {
   assert (DJ := trail_split_disjoint _ _ tr).
   apply trail_split in tr.
   destruct tr as [pl1 [pl2 [tr1 tr2]]].
   inversion tr1; subst.
    exists pl2; rewrite app_nil_r; exact tr2.
    exists (pl2 ++ pl0).
    apply (app_trail' tr2).
     exact tr1.
     intros l LI1 LI2.
      apply DJ with l.
       exact LI2.
       right; exact LI1.
   }
   revert Hpl2; generalize (l2 ++ l1) as ll2; intros.
   clear -sp INll INll' Hpl2.
   destruct Hpl2 as [pl2 tr].
   revert y pl1 ll1 sp pl2 tr INll INll'; induction ll2; intros.
    inversion sp; subst.
     now auto.
     destruct INll' with (y, p0).
      left; now auto.
    inversion sp; subst.
     destruct INll with a.
      left; now auto.
      inversion tr; subst.
      assert (p0 = p3) as nextp.
      {
       assert (~In (y, p3) ll).
        intro; apply H0.
        apply (path_ll_pl H) with (y, p3).
         simpl; now auto.
         exact H1.
       destruct (INll (y, p3)).
        left; now auto.
        inversion H2; now auto.
        elim (H1 H2).
      }
       subst; f_equal.
       apply IHll2 with p3 pl pl0.
        exact H.
        exact H3.
        rewrite <- app_nil_l with Line ll2.
         rewrite <- app_nil_l with Line ll.
          apply NoDup_remove_In with (y, p3).
           simpl; apply (trail_NoDup tr).
           simpl; exact INll.
        rewrite <- app_nil_l with Line ll2.
         rewrite <- app_nil_l with Line ll.
          apply NoDup_remove_In with (y, p3).
           simpl; apply (trail_NoDup (simplepath_onlyif_trail sp)).
           simpl; exact INll'.
  }
  apply PLCAeq.
   now apply (IPLCA_satisfies_planarity_condition _ _ _ _ _ IPLCA).
   unfold PLCAplanarity_condition; split_n 3.
    split_n 3; now auto.
    split_n 13; now auto.
    now auto.
    unfold PLCAeuler.
     rewrite PLCAe'; rewrite (Permutation_length PermCoa); simpl; now auto.
   now auto.
   now auto.
   intros l INl.
    now apply LIn_reverse_inv, LPerm', INco, INl.
   intros l INL.
    apply LPerm in INL.
    apply LIn_In in INL; destruct INL as [INL | INL].
     now apply LIn_reverse_inv, in_lin, INoc, INL.
     rewrite <- reverse_reverse with l.
      now apply in_lin, INoc, INL.
    intros c INc.
     destruct INc as [? | [? | []]]; subst.
      apply in_rin; apply (Permutation_in _ (Permutation_sym PermCoa)); right; left; now auto.
      apply Permutation_eqin with [o; (y, p2) :: ll].
       apply Permutation_sym; now auto.
       left; exact orot.
    intros c INc.
     apply (Permutation_in _ PermCoa) in INc.
      destruct INc as [? | [? | []]]; subst.
       right; left.
        now apply Rot_sym, orot.
       left; apply rotSame.
    intros; apply in_ain; now auto.
    intros; apply in_ain; now auto.
    exact orot.
  (* inductive step *)
  assert (2 <= length A) by (simpl; rewrite <- Heqn; omega).
  assert (Heq := exists_PLCAeq CSP H).
  clear H.
  destruct Heq as [Pl [Pr [Ll [Lr [Cl [Cr [Al [Ar [ol [or [Heq MA]]]]]]]]]]].
  assert (EqOPLCA := PLCAeq_planar CSP Heq).
  assert (EqP := MA_planar MA EqOPLCA).
  assert (S (S n) = length Al).
   rewrite Heqn.
   eapply PLCAeq_lengthA; eauto.
  rewrite <- (MA_Relation_decrease _ _ _ _ _ _ _ _ _ _ MA) in H.
  inversion H; clear H.
  destruct (IHn _ _ _ _ _ H1 EqP) as [P' [L' [C' [A' [o' [IPLCA Ieq]]]]]].
  assert (PLCAplanarity_condition P' L' C' A' o') as IT.
   now apply (IPLCA_satisfies_planarity_condition _ _ _ _ _ IPLCA).
  assert (PLCAequivalence Pr Lr Cr Ar or P' L' C' A' o') as Ieq_inv.
   apply PLCAeq_sym.
   now auto.
   now auto.
  assert (IHeq' := PLCAeq_inv EqP Ieq_inv).
  clear IHn.
  intuition.
  inversion MA; subst.
  (* ma_addloop *)
  *{assert (AIn a A').
     apply H6.
     left; auto.
    destruct (ain_in _ _ H10) as [a' [Ha1 Ha2]].
    destruct (in_split _ _ Ha2) as [A1 [A2 Ha3]]; subst.
    exists (ap ++ P'), ((y, x) :: al ++ L'),
           (((y, x) :: al) :: reverse_linelist ((y, x) :: al) :: C'),
           ([(y, x) :: al] :: (reverse_linelist ((y, x) :: al) :: a') :: A1 ++ A2),
           o'.
    extractPLCAspec EqOPLCA.
    assert (I (ap ++ P') ((y, x) :: al ++ L')
              (((y, x) :: al) :: reverse_linelist ((y, x) :: al) :: C')
              ([(y, x)  :: al] :: (reverse_linelist ((y, x) :: al) :: a') :: A1 ++ A2)
              o') as IPLCA'.
     assert (A1 ++ A2 = set_remove eq_listcircuit_dec a' (A1 ++ a' :: A2)).
      symmetry; apply ain_distinct_setremove.
       extractPLCAspec IT; intro; apply DA0 with a'.
        apply in_or_app; simpl; now auto.
        now auto.
     rewrite H11 in *.
     apply add_loop; intuition.
      apply LT with (y, x); subst.
       left; now auto.
       simpl; now auto.
      destruct CT with ((y, x) :: al).
       left; now auto.
       simpl in H13; omega.
      apply H2 in H13.
      apply DP with p.
       intuition.
       apply in_split in H12.
        destruct H12 as [l1 [l2 ?]]; subst.
         rewrite <- app_assoc; simpl.
          rewrite (@eqin_distinct_setremove Point eq).
           apply in_or_app; right.
            apply in_or_app; now auto.
           intro; now auto.
           intro; apply DP with p.
            apply in_or_app; now auto.
            rewrite <- app_assoc; apply eq_eqin_in; now auto.
    split.
    +auto.
    +apply PLCAeq_sym; auto.
     eapply PLCAtrans; eauto.
     apply PLCAeq; auto.
     -unfold PLCAplanarity_condition.
      intuition.
      unfold PLCAconstraints; intuition.
      unfold PLCAconsistency; intuition.
     -apply IPLCA_satisfies_planarity_condition; exact IPLCA'.
     -intros.
      apply in_or_app; apply in_app_or in H11.
      destruct H11; eauto.
     -intros.
      apply in_or_app; apply in_app_or in H11.
      destruct H11; eauto.
     -intros.
      destruct H11.
       left; subst; constructor.
      right; apply lin_or_app; apply in_app_or in H11.
      destruct H11.
       left; apply in_lin; auto.
       right; apply H0; auto.
     -intros.
      destruct H11.
       left; subst; constructor.
      right; apply lin_or_app; apply in_app_or in H11.
      destruct H11.
       left; apply in_lin; auto.
       right; apply H3; auto.
     -intros.
      destruct H11.
       left; subst; apply rotSame.
      destruct H11.
       right; left; subst; apply rotSame.
      right; right; apply H4; auto.
     -intros.
      destruct H11.
       left; subst; apply rotSame.
      destruct H11.
       right; left; subst; apply rotSame.
      right; right; apply H5; auto.
     -intros.
      destruct H11.
       left; subst; apply eq_area_refl.
      destruct H11.
       right; left; subst.
       apply eq_area_cons; auto.
      right; right.
      assert (AIn a0 (A1 ++ a' :: A2)).
       apply H6.
       right; auto.
      apply ain_or_app.
      apply ain_app_or in H12.
      destruct H12.
       auto.
       destruct H12.
        extractPLCAspec EqP.
        elim DA0 with a0.
         simpl; auto.
         simpl; destruct eq_listcircuit_dec with a0 a.
          apply in_ain; auto.
          left; apply trans_area with a'; auto.
          apply eq_area_sym; auto.
        auto.
     -intros.
      destruct H11.
       left; subst; apply eq_area_refl.
      destruct H11.
       right; left; subst.
       apply eq_area_cons; apply eq_area_sym; auto.
      right; right.
      assert (AIn a0 (a :: A0)).
       apply H7.
       apply in_or_app; simpl.
       apply in_app_or in H11; destruct H11; auto.
      destruct H12.
       extractPLCAspec IT.
       elim DA0 with a0.
       apply in_or_app; simpl.
        apply in_app_or in H11; destruct H11; auto.
        apply ain_eq_set_remove_permutation with (a' :: A1 ++ A2).
         now apply Permutation_cons_app, Permutation_refl.
         simpl; destruct eq_listcircuit_dec with a0 a'.
          apply in_ain; now auto.
          left; apply trans_area with a; now auto.
        now auto.
   }
  (* ma_inpath *)
  *{inversion H8; subst.
   (* ->add_inline *)
   -simpl in *.
     inversion H10; subst.
      extractPLCAspec EqOPLCA; elim LT with (s, s).
       left; now auto.
       simpl; now auto.
      inversion H11; subst.
       extractPLCAspec EqOPLCA; elim LT with (y, y).
        left; now auto.
        simpl; now auto.
       assert (AIn [((y, p2) :: ll) ++ (s, p0) :: ll0] A') as AIN' by auto.
       apply ain_in in AIN'.
       destruct AIN' as [a' [eqa' Ina']].
       assert (exists c' : Circuit, Rot (((y, p2) :: ll) ++ (s, p0) :: ll0) c' /\ a' = [c']) as eqc'.
       {
        apply eq_area_length_1_inv.
        now auto.
       }
       clear eqa'.
       destruct eqc' as [c' [Rc' ?]]; subst.
       assert (~LIn (y, s) L') as NLIN.
       {
        intro.
        apply LIn_In in H20.
        extractPLCAspec EqOPLCA.
         destruct H20.
          apply H3 in H20.
           elim DL with (s, y).
            left; now auto.
            simpl.
            destruct eq_dline_dec with (s, y) (s, y).
             apply LIn_reverse_inv.
              unfold reverse; simpl; now auto.
             elim n0; now auto.
          apply H3 in H20.
           apply LIn_reverse_inv in H20.
            elim DL with (s, y).
             left; now auto.
             simpl.
             destruct eq_dline_dec with (s, y) (s, y).
              apply LIn_reverse_inv.
               unfold reverse; simpl; now auto.
              elim n0; now auto.
       }
       assert (~LIn (s, y) L') as NLIN'.
       {
        intro; apply NLIN.
        apply LIn_reverse_inv; unfold reverse; simpl; now auto.
       }
       apply Rot_divide in Rc'.
       destruct Rc' as [[ll3 [ll4 [Rc' ?]]] | [ll3 [ll4 [Rc' ?]]]]; subst. 
       *assert (I P'
                  ((y, s)::L')
                  (((s, y) :: (y, p2) :: ll) :: (ll4 ++ (y, s) :: ll3) :: (set_remove eq_listline_dec (ll4 ++ ((y, p2) :: ll) ++ ll3) C'))
                  ([(s, y) :: (y, p2) :: ll] :: [ll4 ++ (y, s) :: ll3] :: (set_remove eq_listcircuit_dec [ll4 ++ ((y, p2) :: ll) ++ ll3] A'))
                  o') as IPLCA'.
        {
         rewrite Rc' in H11.
         apply trail_app_or in H11.
         destruct H11 as [z [pl1 [pl2 [Hll3 Hll4]]]].
         assert (I P'
                   ((y, s) :: L')
                   (((s, y) :: (y, p2) :: ll) :: (ll4 ++ (y, s) :: ll3)
                     :: set_remove eq_listline_dec (ll4 ++ ((y, p2) :: ll) ++ ll3) C')
                   ([(s, y) :: (y, p2) :: ll] :: ((ll4 ++ (y, s) :: ll3)
                     :: set_remove eq_listline_dec (ll4 ++ ((y, p2) :: ll) ++ ll3) [ll4 ++ ((y, p2) :: ll) ++ ll3])
                     :: set_remove eq_listcircuit_dec [ll4 ++ ((y, p2) :: ll) ++ ll3] A')
                 o') as IPLCA''.
         +apply (add_inline P' L' C' A' o' [ll4 ++ ((y, p2) :: ll) ++ ll3] z y s pl2 (y :: pyx) pl1 ll4 ((y, p2) :: ll) ll3); auto.
          -simpl; now auto.
          -intro; subst.
           extractPLCAspec EqOPLCA.
           elim LT with (s, s).
            left; now auto.
            simpl; now auto.
          -destruct ll.
            inversion H10; subst.
             inversion H24; subst.
              extractPLCAspec EqOPLCA.
               elim DL with (s, y).
                left; now auto.
                 simpl.
                  edestruct eq_dline_dec.
                   extractPLCAspec EqP.
                    apply LIn_reverse_inv.
                     unfold reverse; simpl.
                      apply CL0 with ([(y, s)] ++ (s, p0) :: ll0).
                       left; now auto.
                       apply in_or_app; left; simpl; now auto.
                   elim n0; now auto.
            simpl; omega.
          -assert (length ((s, p0) :: ll0) = length (ll3 ++ ll4)).
            f_equal.
            now auto.
           assert (length (ll3 ++ ll4) = length (ll4 ++ ll3)).
            rewrite app_length.
            rewrite app_length.
            apply plus_comm.
           rewrite H20 in H11.
           refine (eq_ind _ (fun x => 2 <= x) _ (length (ll4 ++ ll3)) H11). (* rewrite does not work here *)
           destruct ll0.
            inversion H14; subst.
             extractPLCAspec EqOPLCA.
              elim DL with (s, y).
               left; now auto.
                simpl.
                 edestruct eq_dline_dec.
                  extractPLCAspec EqP.
                   apply CL0 with (((y, p2) :: ll) ++ [(s, y)]).
                    left; now auto.
                    apply in_or_app; right; simpl; now auto.
                  elim n0; now auto.
            simpl; omega.
         +revert IPLCA''.
          simpl.
          edestruct eq_listline_dec.
           simpl; auto.
           elim n0; now auto.
        }
        repeat eexists.
        exact IPLCA'.
        apply PLCAeq_sym; auto.
        eapply PLCAtrans; eauto.
        apply PLCAeq; auto.
        +apply IPLCA_satisfies_planarity_condition; exact IPLCA'.
        +intros.
         destruct H20.
          subst; left; constructor.
           unfold reverse; simpl; now auto.
          right; eauto.
        +intros.
         destruct H20.
          subst; left; constructor.
           unfold reverse; simpl; now auto.
          right; eauto.
        +intros.
         destruct H20 as [? | [? | ?]]; subst.
          left; now apply rotSame.
          right; left.
           apply Rot_app_r.
            simpl; rewrite Rc'; now apply rotSame.
          right; right.
           assert (~ Rot (ll4 ++ ((y, p2) :: ll) ++ ll3) c).
            intro; extractPLCAspec EqP.
             apply DC with c.
              simpl; now auto.
              simpl.
               edestruct eq_listline_dec.
                apply in_rin; now auto.
                left; rewrite Rc'.
                 apply Rot_sym, Rot_app_r in H21.
                  rewrite <- app_assoc in H21; now apply H21.
           assert (RIn c C') by eauto.
           assert (In (ll4 ++ ((y, p2) :: ll) ++ ll3) C').
            apply InductivePLCAACconsistency in IPLCA.
             apply IPLCA with [ll4 ++ ((y, p2) :: ll) ++ ll3].
              now auto.
              simpl; now auto.
            apply in_split in H23; destruct H23 as [C'1 [C'2 ?]]; subst.
             apply rin_setremove_remove.
              apply rin_or_app; apply rin_app_or in H22; destruct H22 as [? | [? | ?]]; auto.
               elim H21.
                apply Rot_sym; now auto.
        +intros.
         destruct H20 as [? | [? | ?]]; subst.
          left; now apply rotSame.
          right; left; apply Rot_sym, Rot_app_r.
           rewrite Rc'; now apply rotSame.
          right; right.
           destruct H5 with c.
            apply set_remove_in in H20; now auto.
            elim (InductivePLCADistinctC IPLCA) with (ll4 ++ ((y, p2) :: ll) ++ ll3).
             apply (InductivePLCAACconsistency IPLCA) with [ll4 ++ ((y, p2) :: ll) ++ll3].
              now auto.
              simpl; now auto.
             apply eq_rin_elem with c.
              apply Rot_app_r.
               rewrite <- app_assoc.
                rewrite Rc' in H21; now auto.
              apply in_rin; now auto.
           now auto.
        +intros.
         destruct H20 as [? | [? | ?]]; subst.
          left; now apply eq_area_refl.
          right; left; apply rotate_c_area.
           rewrite Rc'; apply Rot_app_r.
            simpl; now apply rotSame.
          right; right.
           assert (AIn a A') by eauto.
           apply in_split in Ina'; destruct Ina' as [A'1 [A'2 ?]]; subst.
           apply ain_setremove_remove.
           apply ain_or_app; apply ain_app_or in H21; destruct H21 as [? | [? | ?]]; auto.
           extractPLCAspec EqP.
            elim DA with a.
             right; now auto.
             simpl.
              edestruct eq_listcircuit_dec.
               apply in_ain; now auto.
               left.
                apply trans_area with [ll4 ++ ((y, p2) :: ll) ++ ll3].
                 now auto.
                 apply rotate_c_area.
                  rewrite Rc'.
                   apply Rot_sym, Rot_app_r.
                    rewrite app_assoc; now apply rotSame.
        +intros.
         destruct H20 as [? | [? | ?]]; subst.
          left; now apply eq_area_refl.
          right; left.
           apply rotate_c_area.
            rewrite Rc'; apply Rot_sym, Rot_app_r.
             simpl; now apply rotSame.
          right; right.
           destruct H7 with a.
            apply set_remove_in in H20; now auto.
            elim (InductivePLCADistinctA IPLCA) with [ll4 ++ ((y, p2) :: ll) ++ ll3].
             apply set_remove_in in H20; now auto.
             apply eq_ain_elem with a.
              apply trans_area with [((y, p2) :: ll) ++ (s, p0) :: ll0].
               now auto.
               apply rotate_c_area.
                rewrite Rc'; apply Rot_app_r.
                 rewrite app_assoc; now apply rotSame.
              apply in_ain; now auto.
            now auto.
       *assert (I P'
                  ((s, y)::L')
                  (((y, s) :: (s, p0) :: ll0) :: (ll4 ++ (s, y) :: ll3) :: (set_remove eq_listline_dec (ll4 ++ ((s, p0) :: ll0) ++ ll3) C'))
                  ([(y, s) :: (s, p0) :: ll0] :: [ll4 ++ (s, y) :: ll3] :: (set_remove eq_listcircuit_dec [ll4 ++ ((s, p0) :: ll0) ++ ll3] A'))
                  o') as IPLCA'.
        {
         rewrite Rc' in H10.
         apply trail_app_or in H10.
         destruct H10 as [z [pl1 [pl2 [Hll3 Hll4]]]].
         assert (I P'
                   ((s, y) :: L')
                   (((y, s) :: (s, p0) :: ll0) :: (ll4 ++ (s, y) :: ll3)
                     :: set_remove eq_listline_dec (ll4 ++ ((s, p0) :: ll0) ++ ll3) C')
                   ([(y, s) :: (s, p0) :: ll0] :: ((ll4 ++ (s, y) :: ll3)
                     :: set_remove eq_listline_dec (ll4 ++ ((s, p0) :: ll0) ++ ll3) [ll4 ++ ((s, p0) :: ll0) ++ ll3])
                     :: set_remove eq_listcircuit_dec [ll4 ++ ((s, p0) :: ll0) ++ ll3] A')
                   o') as IPLCA''.
         +apply (add_inline P' L' C' A' o' [ll4 ++ ((s, p0) :: ll0) ++ ll3] z s y pl2 (s :: pl) pl1 ll4 ((s, p0) :: ll0) ll3); auto.
          -simpl; now auto.
          -intro; subst.
           extractPLCAspec EqOPLCA.
           elim LT with (y, y).
            left; now auto.
            simpl; now auto.
          -destruct ll0.
            inversion H11; subst.
             inversion H24; subst.
              extractPLCAspec EqOPLCA.
               elim DL with (s, y).
                left; now auto.
                 simpl.
                  edestruct eq_dline_dec.
                   extractPLCAspec EqP.
                    apply CL0 with (((y, p2) :: ll) ++ [(s, y)]).
                     left; now auto.
                     apply in_or_app; right; simpl; now auto.
                   elim n0; now auto.
            simpl; omega.
          -assert (length ((y, p2) :: ll) = length (ll3 ++ ll4)).
            f_equal.
            now auto.
           assert (length (ll3 ++ ll4) = length (ll4 ++ ll3)).
            rewrite app_length.
            rewrite app_length.
            apply plus_comm.
           rewrite H20 in H10.
           refine (eq_ind _ (fun x => 2 <= x) _ (length (ll4 ++ ll3)) H10). (* rewrite does not work here *)
           destruct ll.
            inversion H15; subst.
             extractPLCAspec EqOPLCA.
              elim DL with (s, y).
               left; now auto.
                simpl.
                 edestruct eq_dline_dec.
                  apply LIn_reverse_inv.
                   unfold reverse; simpl.
                    extractPLCAspec EqP.
                     apply CL0 with ([(y, s)] ++ (s, p0) :: ll0).
                      left; now auto.
                      apply in_or_app; left; simpl; now auto.
                  elim n0; now auto.
            simpl; omega.
         +revert IPLCA''.
          simpl.
          edestruct eq_listline_dec.
           simpl; auto.
           elim n0; now auto.
        }
        repeat eexists.
        exact IPLCA'.
        apply PLCAeq_sym; auto.
        eapply PLCAtrans; eauto.
        apply PLCAeq; auto.
        +apply IPLCA_satisfies_planarity_condition; exact IPLCA'.
        +intros.
         destruct H20.
          subst; left; now constructor.
          right; eauto.
        +intros.
         destruct H20.
          subst; left; now constructor.
          right; eauto.
        +intros.
         destruct H20 as [? | [? | ?]]; subst.
          right; left.
           apply Rot_app_r.
            simpl; rewrite Rc'; now apply rotSame.
          left; now apply rotSame.
          right; right.
           assert (~ Rot (ll4 ++ ((s, p0) :: ll0) ++ ll3) c).
            intro; extractPLCAspec EqP.
             apply DC with c.
              simpl; now auto.
              simpl.
               edestruct eq_listline_dec.
                apply in_rin; now auto.
                left; apply Rot_trans with (ll4 ++ ((s, p0) :: ll0) ++ ll3).
                 apply Rot_sym; now auto.
                 apply Rot_sym, Rot_app_r.
                  rewrite <- app_assoc.
                   rewrite <- Rc'.
                    apply Rot_app_r.
                     now apply rotSame.
           assert (RIn c C') by eauto.
           assert (In (ll4 ++ ((s, p0) :: ll0) ++ ll3) C').
            apply InductivePLCAACconsistency in IPLCA.
             apply IPLCA with [ll4 ++ ((s, p0) :: ll0) ++ ll3].
              now auto.
              simpl; now auto.
            apply in_split in H23; destruct H23 as [C'1 [C'2 ?]]; subst.
             apply rin_setremove_remove.
              apply rin_or_app; apply rin_app_or in H22; destruct H22 as [? | [? | ?]]; auto.
               elim H21.
                apply Rot_sym; now auto.
        +intros.
         destruct H20 as [? | [? | ?]]; subst.
          right; left; now apply rotSame.
          left; apply Rot_sym, Rot_app_r.
           rewrite Rc'; now apply rotSame.
          right; right.
           destruct H5 with c.
            apply set_remove_in in H20; now auto.
            elim (InductivePLCADistinctC IPLCA) with (ll4 ++ ((s, p0) :: ll0) ++ ll3).
             apply (InductivePLCAACconsistency IPLCA) with [ll4 ++ ((s, p0) :: ll0) ++ll3].
              now auto.
              simpl; now auto.
             apply eq_rin_elem with c.
              apply Rot_app_r.
               rewrite <- app_assoc.
                rewrite Rc' in H21; apply Rot_trans with ((ll3 ++ ll4) ++ (s, p0) :: ll0).
                 now auto.
                 apply Rot_app.
              apply in_rin; now auto.
           now auto.
        +intros.
         destruct H20 as [? | [? | ?]]; subst.
          right; left; apply rotate_c_area.
           rewrite Rc'; apply Rot_app_r.
            simpl; now apply rotSame.
          left; now apply eq_area_refl.
          right; right.
           assert (AIn a A') by eauto.
           apply in_split in Ina'; destruct Ina' as [A'1 [A'2 ?]]; subst.
           apply ain_setremove_remove.
           apply ain_or_app; apply ain_app_or in H21; destruct H21 as [? | [? | ?]]; auto.
           extractPLCAspec EqP.
            elim DA with a.
             right; now auto.
             simpl.
              edestruct eq_listcircuit_dec.
               apply in_ain; now auto.
               left.
                apply trans_area with [ll4 ++ ((s, p0) :: ll0) ++ ll3].
                 now auto.
                 apply rotate_c_area.
                  apply Rot_sym, Rot_app_r.
                   rewrite <- app_assoc; rewrite <- Rc'.
                    rewrite app_comm_cons; now apply Rot_app.
        +intros.
         destruct H20 as [? | [? | ?]]; subst.
          right; left.
           apply rotate_c_area.
            apply rotSame.
          left; apply rotate_c_area.
           rewrite Rc', app_comm_cons.
            now apply Rot_app.
          right; right.
           destruct H7 with a.
            apply set_remove_in in H20; now auto.
            elim (InductivePLCADistinctA IPLCA) with [ll4 ++ ((s, p0) :: ll0) ++ ll3].
             apply set_remove_in in H20; now auto.
             apply eq_ain_elem with a.
              apply trans_area with [((y, p2) :: ll) ++ (s, p0) :: ll0].
               now auto.
               apply rotate_c_area.
                rewrite Rc'; apply Rot_app_r.
                 rewrite <- app_assoc with _ _ ll3 _.
                  now apply Rot_app.
              apply in_ain; now auto.
            now auto.
   (* ->add_inpath *)
   -erewrite reverse_linelist_cons in *.
    unfold reverse in *; simpl (fst (x, p2)) in *; simpl (snd (x, p2)) in *.
    assert (AIn [lyx ++ lxy] A').
      apply H6.
      left; auto.
     destruct (ain_in _ _ H14) as [a' [Ha1 Ha2]].
     destruct (in_split _ _ Ha2) as [A1 [A2 Ha3]]; subst.
     assert (RIn (lyx ++ lxy) C').
      apply H4.
      left; auto.
     destruct (rin_in _ _ H16) as [c' [Hc1 Hc2]].
     destruct (in_split _ _ Hc2) as [C1 [C2 Hc3]]; subst.
     destruct (Rot_divide _ _ _ Hc1) as [[c1 [c2 [Hc1' Hc2']]] | [c1 [c2 [Hc1' Hc2']]]]; subst.
     +assert (a' = [c2 ++ lyx ++ c1]).
      {
       apply eq_area_length_1_inv in Ha1.
       destruct Ha1 as [c' [Ha1 ?]]; subst.
       eapply (InductivePLCAACconsistency IPLCA) in Ha2.
       instantiate (1 := c') in Ha2.
       apply ((fun (X Y : Type) (x : X) (xy : X -> Y) => xy x) _ _ (in_eq c' [])) in Ha2.
       apply in_app_or in Ha2; destruct Ha2 as [Ha2 | [Ha2 | Ha2]].
       -elim (InductivePLCADistinctC IPLCA) with (c2 ++ lyx ++ c1).
         apply in_or_app; simpl; now auto.
         apply rin_setremove_remove.
          apply eq_rin_elem with c'.
           apply Rot_app_r.
            rewrite <- app_assoc.
             apply Rot_sym; now auto.
          apply in_rin, in_or_app; now auto.
       -subst; now auto.
       -elim (InductivePLCADistinctC IPLCA) with (c2 ++ lyx ++ c1).
         apply in_or_app; simpl; now auto.
         apply rin_setremove_remove.
          apply eq_rin_elem with c'.
           apply Rot_app_r.
            rewrite <- app_assoc.
             apply Rot_sym; now auto.
          apply in_rin, in_or_app; now auto.
      }
      subst.
      case_eq (length (c1 ++ c2)).
      {
       intro Hc12; destruct c1; destruct c2; simpl in Hc12; inversion Hc12; clear Hc12; subst.
       repeat (rewrite app_nil_l in * );
         repeat (rewrite app_nil_r in * ).
       inversion H11; subst.
       assert (I (ap ++ P') ((y, p2) :: (s, y) :: ll ++ L')
                 (([(p2, y)] ++ (y, s) :: reverse_linelist ll)
                  :: (((y, p2) :: ll) ++ (s, y) :: lyx) :: C1 ++ C2)
                 ([[(p2, y)] ++ (y, s) :: reverse_linelist ll]
                  :: ([((y, p2) :: ll) ++ (s, y) :: lyx]) :: A1 ++ A2) o') as IPLCA'.
       {
        assert (C1 ++ C2 = set_remove eq_listline_dec ([] ++ [] ++ lyx) (C1 ++ ([] ++ [] ++ lyx) :: C2)).
         simpl; symmetry; apply rin_distinct_setremove.
          apply (InductivePLCADistinctC IPLCA).
           apply in_or_app; simpl; now auto.
        rewrite H17; clear H17.
        assert ([((y, p2) :: ll) ++ (s, y) :: lyx] = (((y, p2) :: ll) ++ (s, y) :: lyx) :: set_remove eq_listline_dec ([] ++ [] ++ lyx) ([[] ++ [] ++ lyx] ++ [])).
         simpl; destruct eq_listline_dec with lyx lyx.
          now auto.
          elim n0; now auto.
        rewrite H17; clear H17.
        assert (A1 ++ A2 = set_remove eq_listcircuit_dec [[] ++ [] ++ lyx] (A1 ++ [[] ++ [] ++ lyx] :: A2)).
         simpl; symmetry; apply ain_distinct_setremove.
          apply (InductivePLCADistinctA IPLCA).
           apply in_or_app; simpl; now auto.
        rewrite H17; clear H17.
        rewrite <- (app_nil_l (((y, p2) :: ll) ++ (s, y) :: lyx)).
        apply add_inpath with y [y] [y] (y :: pyx); repeat (rewrite app_nil_l in * ); repeat (rewrite app_nil_r in * ); simpl; auto.
        +destruct ll.
          inversion H15; subst.
           extractPLCAspec EqOPLCA.
            elim DL with (s, y); simpl.
             now auto.
             destruct eq_dline_dec with (s, y) (s, y).
              left; constructor.
               unfold reverse; simpl; now auto.
              elim n0; now auto.
          simpl; omega.
        +omega.
        +destruct eq_point_dec with p2 s.
          subst; extractPLCAspec EqOPLCA.
           elim DL with (s, y); simpl.
            now auto.
            destruct eq_dline_dec with (s, y) (s, y).
             left; constructor.
              unfold reverse; simpl; now auto.
             elim n0; now auto.
          now auto.
        +intros.
          intro.
           extractPLCAspec EqOPLCA.
            apply H2 in H19.
             apply DP with p.
              apply in_or_app; now auto.
              clear -H17 H19; induction ap.
               simpl in H17; inversion H17.
               simpl in *; destruct eq_point_dec with p a.
                apply in_or_app; now auto.
                destruct H17.
                 subst; elim n; now auto.
                 right; apply IHap; now auto.
       }
       repeat eexists; [ exact IPLCA' | ].
       apply PLCAeq_sym; auto.
       eapply PLCAtrans; eauto.
       apply PLCAeq; auto.
       *apply IPLCA_satisfies_planarity_condition; exact IPLCA'.
       *intros.
         apply in_or_app; apply in_app_or in H17.
          destruct H17; [ left | right ]; now auto.
       *intros.
         apply in_or_app; apply in_app_or in H17.
          destruct H17; [ left | right ]; now auto.
       *intros; simpl in *.
         destruct H17 as [? | [? | ?]]; [ subst; right; left; now constructor | subst; left; now constructor | right; right ].
          apply lin_or_app; apply in_app_or in H17; destruct H17.
           left; apply in_lin; now auto.
           right; apply H0; now auto.
       *intros; simpl in *.
         destruct H17 as [? | [? | ?]]; [ subst; right; left; now constructor | subst; left; now constructor | right; right ].
          apply lin_or_app; apply in_app_or in H17; destruct H17.
           left; apply in_lin; now auto.
           right; apply H3; now auto.
       *intros; simpl in *.
         destruct H17 as [? | [? | ?]].
          subst; right; left; now apply rotSame.
          subst; left; now apply Rot_inv_r, rotSame.
          assert (~Rot c lyx).
           intro; extractPLCAspec EqP.
            apply DC with lyx.
             left; now auto.
             simpl; edestruct eq_listline_dec.
              apply eq_rin_elem with c.
               now auto.
               apply in_rin; now auto.
              elim n0; now auto.
          right; right; assert (RIn c (C1 ++ lyx :: C2)).
           apply H4; now auto.
          apply rin_or_app; apply rin_app_or in H20; destruct H20 as [? | [? | ?]]; now auto.
       *intros; simpl in *.
         destruct H17 as [? | [? | ?]].
          subst; right; left; now apply Rot_inv_l, rotSame.
          subst; left; now apply rotSame.
          assert (~Rot c lyx).
           intro; apply (InductivePLCADistinctC IPLCA) with lyx.
            apply in_or_app; simpl; now auto.
            apply rin_setremove_remove.
             apply eq_rin_elem with c.
              now auto.
              apply in_rin; now auto.
          right; right; assert (RIn c (lyx :: C0)).
           apply H5.
            apply in_app_or in H17; apply in_or_app; simpl; destruct H17; now auto.
          destruct H20; now auto.
       *intros; simpl in *.
         destruct H17 as [? | [? | ?]].
          subst; right; left; now apply eq_area_refl.
          subst; left; now apply rotate_c_area, Rot_inv_r, rotSame.
          assert (~eq_area a [lyx]).
           intro; extractPLCAspec EqP.
            apply DA with [lyx].
             left; now auto.
             simpl; edestruct eq_listcircuit_dec.
              apply eq_ain_elem with a.
               now auto.
               apply in_ain; now auto.
              elim n0; now auto.
          right; right; assert (AIn a (A1 ++ [lyx] :: A2)).
           apply H6; now auto.
          apply ain_or_app; apply ain_app_or in H20; destruct H20 as [? | [? | ?]]; now auto.
       *intros; simpl in *.
         destruct H17 as [? | [? | ?]].
          subst; right; left; now apply rotate_c_area, Rot_inv_l, rotSame.
          subst; left; now apply eq_area_refl.
          assert (~eq_area a [lyx]).
           intro; apply (InductivePLCADistinctA IPLCA) with [lyx].
            apply in_or_app; simpl; now auto.
            apply ain_setremove_remove.
             apply eq_ain_elem with a.
              now auto.
              apply in_ain; now auto.
          right; right; assert (AIn a ([lyx] :: A0)).
           apply H7.
            apply in_app_or in H17; apply in_or_app; simpl; destruct H17; now auto.
          destruct H20; now auto.
      }
      {
       intros n' Hc12.
       assert (I (rev ap ++ P') ((y, s) :: (p2, x) :: reverse_linelist ll ++ L')
                 ((((s, y) :: lyx) ++ (x, p2) :: reverse_linelist (reverse_linelist ll))
                   :: (c2 ++ ((y, s) :: reverse_linelist ll) ++ (p2, x) :: c1) :: C1 ++ C2)
                 ([((s, y) :: lyx) ++ (x, p2) :: reverse_linelist (reverse_linelist ll)]
                   :: [c2 ++ ((y, s) :: reverse_linelist ll) ++ (p2, x) :: c1] :: A1 ++ A2)
                 o') as IPLCA'.
       {
        assert (C1 ++ C2 = set_remove eq_listline_dec (c2 ++ lyx ++ c1) (C1 ++ (c2 ++ lyx ++ c1) :: C2)).
         symmetry; apply rin_distinct_setremove.
          apply (InductivePLCADistinctC IPLCA).
           apply in_or_app; simpl; now auto.
        rewrite H17; clear H17.
        assert ([c2 ++ ((y, s) :: reverse_linelist ll) ++ (p2, x) :: c1] = (c2 ++ ((y, s) :: reverse_linelist ll) ++ (p2, x) :: c1) :: set_remove eq_listline_dec (c2 ++ lyx ++ c1) ([c2 ++ lyx ++ c1] ++ [])).
         simpl; destruct eq_listline_dec with (c2 ++ lyx ++ c1) (c2 ++ lyx ++ c1).
          now auto.
          elim n0; now auto.
        rewrite H17; clear H17.
        assert (A1 ++ A2 = set_remove eq_listcircuit_dec [c2 ++ lyx ++ c1] (A1 ++ [c2 ++ lyx ++ c1] :: A2)).
         symmetry; apply ain_distinct_setremove.
          apply (InductivePLCADistinctA IPLCA).
           apply in_or_app; simpl; now auto.
        rewrite H17; clear H17.
        apply trail_app_or in H11.
        destruct H11 as [z [pxz [pzy [Hxz Hzy]]]].
        apply add_inpath with z pzy (y :: pyx) pxz; simpl; auto.
        +apply rev_path; now auto.
        +destruct ll.
          inversion H15; subst.
           destruct lyx; simpl.
            inversion H10; subst.
             simpl in EqOPLCA; extractPLCAspec EqOPLCA.
              elim DL with (s, x); simpl.
               now auto.
               edestruct eq_dline_dec.
                left; constructor.
                 unfold reverse; simpl; now auto.
                 elim n0; now auto.
            omega.
           rewrite reverse_linelist_cons.
            repeat (rewrite app_length).
             simpl; omega.
        +destruct c2, c1; simpl in Hc12; simpl; omega.
        +destruct (eq_point_dec y x).
          destruct (eq_point_dec s p2).
           subst; extractPLCAspec EqOPLCA.
            elim DL with (p2, x); simpl.
             now auto.
             edestruct eq_dline_dec.
              left; constructor.
               unfold reverse; simpl; now auto.
              elim n0; now auto.
           now auto.
          now auto.
        +intros; intro.
          apply H2 in H17.
           extractPLCAspec EqOPLCA.
            elim DP with p.
             apply in_or_app; now auto.
             apply in_rev in H11; clear -H11 H17; induction ap.
              inversion H11.
              simpl in *; edestruct eq_point_dec.
               apply in_or_app; now auto.
               destruct H11; subst; simpl; now auto.
       }
       rewrite reverse_linelist_involutive in IPLCA'.
       repeat eexists; [ exact IPLCA' | ].
       apply PLCAeq_sym; auto.
       eapply PLCAtrans; eauto.
       apply PLCAeq; auto.
       *apply IPLCA_satisfies_planarity_condition; exact IPLCA'.
       *intros.
         apply in_or_app; apply in_app_or in H17; destruct H17.
          left; apply in_rev; rewrite rev_involutive; now auto.
          right; now auto.
       *intros.
         apply in_or_app; apply in_app_or in H17; destruct H17.
          left; apply in_rev; now auto.
          right; now auto.
       *simpl; intros.
         destruct H17 as [? | [? | ?]]; subst.
          left; constructor.
           unfold reverse; simpl; now auto.
          right; left; constructor.
           unfold reverse; simpl; now auto.
          right; right; apply lin_or_app; apply in_app_or in H17; destruct H17.
           left; apply LIn_reverse_linelist.
            apply in_lin; now auto.
           right; apply H0; now auto.
       *simpl; intros.
         destruct H17 as [? | [? | ?]]; subst.
          left; constructor.
           unfold reverse; simpl; now auto.
          right; left; constructor.
           unfold reverse; simpl; now auto.
          right; right; apply lin_or_app; apply in_app_or in H17; destruct H17.
           left; apply LIn_reverse_linelist_inv.
            apply in_lin; now auto.
           right; apply H3; now auto.
       *simpl; intros.
         destruct H17 as [? | [? | ?]]; subst.
          left; repeat (rewrite app_comm_cons); apply Rot_app.
          right; left; apply Rot_app_r.
           simpl; repeat (rewrite <- app_assoc); simpl; now apply rotSame.
          right; right; assert (~Rot c (c2 ++ lyx ++ c1)).
           intro; extractPLCAspec EqP.
            apply DC with (lyx ++ c1 ++ c2).
             left; now auto.
             simpl; edestruct eq_listline_dec.
              apply eq_rin_elem with c.
               apply Rot_trans with (c2 ++ lyx ++ c1).
                now auto.
                apply Rot_sym, Rot_app_r.
                 rewrite app_assoc; now apply rotSame.
               apply in_rin; now auto.
              elim n0; now auto.
          assert (RIn c (C1 ++ (c2 ++ lyx ++ c1) :: C2)).
           apply H4.
            right; now auto.
          apply rin_or_app; apply rin_app_or in H20; destruct H20 as [? | [? | ?]]; auto.
           elim H19; now auto.
       *simpl; intros.
         destruct H17 as [? | [? | ?]]; subst.
          left; repeat (rewrite app_comm_cons); apply Rot_app.
          right; left; apply Rot_sym, Rot_app_r.
           simpl; repeat (rewrite <- app_assoc); simpl; now apply rotSame.
          right; right; assert (~Rot c (lyx ++ c1 ++ c2)).
           intro; elim (InductivePLCADistinctC IPLCA) with (c2 ++ lyx ++ c1).
            apply in_or_app; simpl; now auto.
             apply rin_setremove_remove.
              apply eq_rin_elem with c.
               apply Rot_trans with (lyx ++ c1 ++ c2).
                now auto.
                apply Rot_app_r.
                 rewrite app_assoc; now apply rotSame.
               apply in_rin; now auto.
          assert (RIn c ((lyx ++ c1 ++ c2) :: C0)).
           apply H5.
            apply in_or_app; apply in_app_or in H17; simpl; destruct H17; now auto.
          destruct H20 as [? | ?]; auto.
           elim H19; now auto.
       *simpl; intros.
         destruct H17 as [? | [? | ?]]; subst.
          left; repeat (rewrite app_comm_cons); apply rotate_c_area, Rot_app.
          right; left; apply rotate_c_area, Rot_app_r.
           simpl; repeat (rewrite <- app_assoc); simpl; now apply rotSame.
          right; right; assert (~eq_area a [c2 ++ lyx ++ c1]).
           intro; extractPLCAspec EqP.
            apply DA with [lyx ++ c1 ++ c2].
             left; now auto.
             simpl; edestruct eq_listcircuit_dec.
              apply eq_ain_elem with a.
               apply trans_area with [c2 ++ lyx ++ c1].
                now auto.
                apply rotate_c_area, Rot_sym, Rot_app_r.
                 rewrite app_assoc; now apply rotSame.
               apply in_ain; now auto.
              elim n0; now auto.
          assert (AIn a (A1 ++ [c2 ++ lyx ++ c1] :: A2)).
           apply H6.
            right; now auto.
          apply ain_or_app; apply ain_app_or in H20; destruct H20 as [? | [? | ?]]; auto.
           elim H19; now auto.
       *simpl; intros.
         destruct H17 as [? | [? | ?]]; subst.
          left; repeat (rewrite app_comm_cons); apply rotate_c_area, Rot_app.
          right; left; apply rotate_c_area, Rot_sym, Rot_app_r.
           simpl; repeat (rewrite <- app_assoc); simpl; now apply rotSame.
          right; right; assert (~eq_area a [lyx ++ c1 ++ c2]).
           intro; elim (InductivePLCADistinctA IPLCA) with [c2 ++ lyx ++ c1].
            apply in_or_app; simpl; now auto.
             apply ain_setremove_remove.
              apply eq_ain_elem with a.
               apply trans_area with [lyx ++ c1 ++ c2].
                now auto.
                apply rotate_c_area, Rot_app_r.
                 rewrite app_assoc; now apply rotSame.
               apply in_ain; now auto.
          assert (AIn a ([lyx ++ c1 ++ c2] :: A0)).
           apply H7.
            apply in_or_app; apply in_app_or in H17; simpl; destruct H17; now auto.
          destruct H20 as [? | ?]; auto.
           elim H19; now auto.
      }
     +assert (a' = [c2 ++ lxy ++ c1]).
      {
       apply eq_area_length_1_inv in Ha1.
       destruct Ha1 as [c' [Ha1 ?]]; subst.
       eapply (InductivePLCAACconsistency IPLCA) in Ha2.
       instantiate (1 := c') in Ha2.
       apply ((fun (X Y : Type) (x : X) (xy : X -> Y) => xy x) _ _ (in_eq c' [])) in Ha2.
       apply in_app_or in Ha2; destruct Ha2 as [Ha2 | [Ha2 | Ha2]].
       -elim (InductivePLCADistinctC IPLCA) with (c2 ++ lxy ++ c1).
         apply in_or_app; simpl; now auto.
         apply rin_setremove_remove.
          apply eq_rin_elem with c'.
           rewrite app_assoc; apply Rot_app_r.
            rewrite app_assoc.
             apply Rot_sym; now auto.
          apply in_rin, in_or_app; now auto.
       -subst; now auto.
       -elim (InductivePLCADistinctC IPLCA) with (c2 ++ lxy ++ c1).
         apply in_or_app; simpl; now auto.
         apply rin_setremove_remove.
          apply eq_rin_elem with c'.
           rewrite app_assoc; apply Rot_app_r.
            rewrite app_assoc.
             apply Rot_sym; now auto.
          apply in_rin, in_or_app; now auto.
      }
      subst.
      case_eq (length (c1 ++ c2)).
      {
       intro Hc12; destruct c1; destruct c2; simpl in Hc12; inversion Hc12; clear Hc12; subst.
       repeat (rewrite app_nil_l in * );
         repeat (rewrite app_nil_r in * ).
       inversion H10; subst.
       assert (I (rev ap ++ P') ((x, s) :: (p2, x) :: reverse_linelist ll ++ L')
                 (([(s, x)] ++ (x, p2) :: reverse_linelist (reverse_linelist ll))
                  :: (((x, s) :: reverse_linelist ll) ++ (p2, x) :: lxy) :: C1 ++ C2)
                 ([[(s, x)] ++ (x, p2) :: reverse_linelist (reverse_linelist ll)]
                  :: ([((x, s) :: reverse_linelist ll) ++ (p2, x) :: lxy]) :: A1 ++ A2) o') as IPLCA'.
       {
        assert (C1 ++ C2 = set_remove eq_listline_dec ([] ++ [] ++ lxy) (C1 ++ ([] ++ [] ++ lxy) :: C2)).
         simpl; symmetry; apply rin_distinct_setremove.
          apply (InductivePLCADistinctC IPLCA).
           apply in_or_app; simpl; now auto.
        rewrite H17; clear H17.
        assert ([((x, s) :: reverse_linelist ll) ++ (p2, x) :: lxy] = (((x, s) :: reverse_linelist ll) ++ (p2, x) :: lxy) :: set_remove eq_listline_dec ([] ++ [] ++ lxy) ([[] ++ [] ++ lxy] ++ [])).
         simpl; destruct eq_listline_dec with lxy lxy.
          now auto.
          elim n0; now auto.
        rewrite H17; clear H17.
        assert (A1 ++ A2 = set_remove eq_listcircuit_dec [[] ++ [] ++ lxy] (A1 ++ [[] ++ [] ++ lxy] :: A2)).
         simpl; symmetry; apply ain_distinct_setremove.
          apply (InductivePLCADistinctA IPLCA).
           apply in_or_app; simpl; now auto.
        rewrite H17; clear H17.
        rewrite <- (app_nil_l (((x, s) :: reverse_linelist ll) ++ (p2, x) :: lxy)).
        apply add_inpath with x [x] [x] pxy; repeat (rewrite app_nil_l in * ); repeat (rewrite app_nil_r in * ); simpl; auto.
        +apply rev_path; now auto.
        +destruct ll.
          inversion H15; subst.
           extractPLCAspec EqOPLCA.
            elim DL with (s, x); simpl.
             now auto.
             destruct eq_dline_dec with (s, x) (s, x).
              left; constructor.
               unfold reverse; simpl; now auto.
              elim n0; now auto.
          rewrite reverse_linelist_cons, app_length; simpl; omega.
        +omega.
        +destruct eq_point_dec with p2 s.
          subst; extractPLCAspec EqOPLCA.
           elim DL with (s, x); simpl.
            now auto.
            destruct eq_dline_dec with (s, x) (s, x).
             left; constructor.
              unfold reverse; simpl; now auto.
             elim n0; now auto.
          now auto.
        +intros.
          intro.
           extractPLCAspec EqOPLCA.
            apply H2 in H19; apply in_rev in H17.
             apply DP with p.
              apply in_or_app; now auto.
              clear -H17 H19; induction ap.
               simpl in H17; inversion H17.
               simpl in *; destruct eq_point_dec with p a.
                apply in_or_app; now auto.
                destruct H17.
                 subst; elim n; now auto.
                 right; apply IHap; now auto.
       }
       rewrite reverse_linelist_involutive in IPLCA'.
       repeat eexists; [ exact IPLCA' | ].
       apply PLCAeq_sym; auto.
       eapply PLCAtrans; eauto.
       apply PLCAeq; auto.
       *apply IPLCA_satisfies_planarity_condition; exact IPLCA'.
       *intros.
         apply in_or_app; apply in_app_or in H17.
          destruct H17; [ left; apply in_rev in H17 | right ]; now auto.
       *intros.
         apply in_or_app; apply in_app_or in H17.
          destruct H17; [ left; apply in_rev | right ]; now auto.
       *intros; simpl in *.
         destruct H17 as [? | [? | ?]].
          subst; left; constructor.
           unfold reverse; simpl; now auto.
          subst; right; left; constructor.
           unfold reverse; simpl; now auto.
          right; right; apply lin_or_app; apply in_app_or in H17; destruct H17.
           left; apply LIn_reverse_linelist, in_lin; now auto.
           right; apply H0; now auto.
       *intros; simpl in *.
         destruct H17 as [? | [? | ?]].
          subst; left; constructor.
           unfold reverse; simpl; now auto.
          subst; right; left; constructor.
           unfold reverse; simpl; now auto.
          right; right; apply lin_or_app; apply in_app_or in H17; destruct H17.
           left; apply LIn_reverse_linelist_inv, in_lin; now auto.
           right; apply H3; now auto.
       *intros; simpl in *.
         destruct H17 as [? | [? | ?]].
          subst; left; now apply Rot_inv_r, rotSame.
          rewrite <- app_assoc in H17.
           subst; right; left; now apply rotSame.
          assert (~Rot c lxy).
           intro; extractPLCAspec EqP.
            apply DC with lxy.
             left; now auto.
             simpl; edestruct eq_listline_dec.
              apply eq_rin_elem with c.
               now auto.
               apply in_rin; now auto.
              elim n0; now auto.
          right; right; assert (RIn c (C1 ++ lxy :: C2)).
           apply H4; now auto.
          apply rin_or_app; apply rin_app_or in H20; destruct H20 as [? | [? | ?]]; now auto.
       *intros; simpl in *.
         destruct H17 as [? | [? | ?]].
          subst; left; now apply Rot_inv_l, rotSame.
          subst; right; left; rewrite <- app_assoc; now apply rotSame.
          assert (~Rot c lxy).
           intro; apply (InductivePLCADistinctC IPLCA) with lxy.
            apply in_or_app; simpl; now auto.
            apply rin_setremove_remove.
             apply eq_rin_elem with c.
              now auto.
              apply in_rin; now auto.
          right; right; assert (RIn c (lxy :: C0)).
           apply H5.
            apply in_app_or in H17; apply in_or_app; simpl; destruct H17; now auto.
          destruct H20; now auto.
       *intros; simpl in *.
         destruct H17 as [? | [? | ?]].
          subst; left; now apply rotate_c_area, Rot_inv_r, rotSame.
          subst; right; left; rewrite <- app_assoc; now apply eq_area_refl.
          assert (~eq_area a [lxy]).
           intro; extractPLCAspec EqP.
            apply DA with [lxy].
             left; now auto.
             simpl; edestruct eq_listcircuit_dec.
              apply eq_ain_elem with a.
               now auto.
               apply in_ain; now auto.
              elim n0; now auto.
          right; right; assert (AIn a (A1 ++ [lxy] :: A2)).
           apply H6; now auto.
          apply ain_or_app; apply ain_app_or in H20; destruct H20 as [? | [? | ?]]; now auto.
       *intros; simpl in *.
         destruct H17 as [? | [? | ?]].
          subst; left; now apply rotate_c_area, Rot_inv_l, rotSame.
          subst; right; left; rewrite <- app_assoc; now apply eq_area_refl.
          assert (~eq_area a [lxy]).
           intro; apply (InductivePLCADistinctA IPLCA) with [lxy].
            apply in_or_app; simpl; now auto.
            apply ain_setremove_remove.
             apply eq_ain_elem with a.
              now auto.
              apply in_ain; now auto.
          right; right; assert (AIn a ([lxy] :: A0)).
           apply H7.
            apply in_app_or in H17; apply in_or_app; simpl; destruct H17; now auto.
          destruct H20; now auto.
      }
      {
       intros n' Hc12.
       assert (I (ap ++ P')
                 ((x, p2) :: (s, y) :: ll ++ L')
                 ((((p2, x) :: lxy) ++ (y, s) :: reverse_linelist ll)
                   :: (c2 ++ ((x, p2) :: ll) ++ (s, y) :: c1) :: C1 ++ C2)
                 ([((p2, x) :: lxy) ++ (y, s) :: reverse_linelist ll]
                   :: [c2 ++ ((x, p2) :: ll) ++ (s, y) :: c1] :: A1 ++ A2)
                 o') as IPLCA'.
       {
        assert (C1 ++ C2 = set_remove eq_listline_dec (c2 ++ lxy ++ c1) (C1 ++ (c2 ++ lxy ++ c1) :: C2)).
         symmetry; apply rin_distinct_setremove.
          apply (InductivePLCADistinctC IPLCA).
           apply in_or_app; simpl; now auto.
        rewrite H17; clear H17.
        assert ([c2 ++ ((x, p2) :: ll) ++ (s, y) :: c1] = (c2 ++ ((x, p2) :: ll) ++ (s, y) :: c1) :: set_remove eq_listline_dec (c2 ++ lxy ++ c1) ([c2 ++ lxy ++ c1] ++ [])).
         simpl; destruct eq_listline_dec with (c2 ++ lxy ++ c1) (c2 ++ lxy ++ c1).
          now auto.
          elim n0; now auto.
        rewrite H17; clear H17.
        assert (A1 ++ A2 = set_remove eq_listcircuit_dec [c2 ++ lxy ++ c1] (A1 ++ [c2 ++ lxy ++ c1] :: A2)).
         symmetry; apply ain_distinct_setremove.
          apply (InductivePLCADistinctA IPLCA).
           apply in_or_app; simpl; now auto.
        rewrite H17; clear H17.
        apply trail_app_or in H10.
        destruct H10 as [z [pyz [pzx [Hyz Hzx]]]].
        apply add_inpath with z pzx pxy pyz; simpl; auto.
        +destruct ll.
          inversion H15; subst.
           destruct lxy; simpl.
            inversion H11; subst.
             simpl in EqOPLCA; extractPLCAspec EqOPLCA.
              elim DL with (s, y); simpl.
               now auto.
               edestruct eq_dline_dec.
                left; constructor.
                 unfold reverse; simpl; now auto.
                 elim n0; now auto.
            omega.
          simpl; omega.
        +destruct c2, c1; simpl in Hc12; simpl; omega.
        +destruct (eq_point_dec x y).
          destruct (eq_point_dec p2 s).
           subst; extractPLCAspec EqOPLCA.
            elim DL with (s, y); simpl.
             now auto.
             edestruct eq_dline_dec.
              left; constructor.
               unfold reverse; simpl; now auto.
              elim n0; now auto.
           now auto.
          now auto.
        +intros; intro.
          apply H2 in H17.
           extractPLCAspec EqOPLCA.
            elim DP with p.
             apply in_or_app; now auto.
             clear -H10 H17; induction ap.
              inversion H10.
              simpl in *; edestruct eq_point_dec.
               apply in_or_app; now auto.
               destruct H10; subst; simpl; now auto.
       }
       repeat eexists; [ exact IPLCA' | ].
       apply PLCAeq_sym; auto.
       eapply PLCAtrans; eauto.
       apply PLCAeq; auto.
       *apply IPLCA_satisfies_planarity_condition; exact IPLCA'.
       *intros.
         apply in_or_app; apply in_app_or in H17; destruct H17; now auto.
       *intros.
         apply in_or_app; apply in_app_or in H17; destruct H17; now auto.
       *simpl; intros.
         destruct H17 as [? | [? | ?]]; subst.
          right; left; now constructor.
          left; now constructor.
          right; right; apply lin_or_app; apply in_app_or in H17; destruct H17.
           left; apply in_lin; now auto.
           right; apply H0; now auto.
       *simpl; intros.
         destruct H17 as [? | [? | ?]]; subst.
          right; left; now constructor.
          left; now constructor.
          right; right; apply lin_or_app; apply in_app_or in H17; destruct H17.
           left; apply in_lin; now auto.
           right; apply H3; now auto.
       *simpl; intros.
         destruct H17 as [? | [? | ?]]; subst.
          right; left; apply Rot_app_r.
           simpl; rewrite <- app_assoc; simpl; now apply rotSame.
          left; repeat (rewrite app_comm_cons); apply Rot_app_r.
           simpl; repeat (rewrite <- app_assoc); simpl; now apply rotSame.
          right; right; assert (~Rot c (c2 ++ lxy ++ c1)).
           intro; extractPLCAspec EqP.
            apply DC with ((c1 ++ c2) ++ lxy).
             left; now auto.
             simpl; edestruct eq_listline_dec.
              apply eq_rin_elem with c.
               apply Rot_trans with (c2 ++ lxy ++ c1).
                now auto.
                rewrite <- app_assoc; apply Rot_app_r.
                 rewrite app_assoc; now apply rotSame.
               apply in_rin; now auto.
              elim n0; now auto.
          assert (RIn c (C1 ++ (c2 ++ lxy ++ c1) :: C2)).
           apply H4.
            right; now auto.
          apply rin_or_app; apply rin_app_or in H20; destruct H20 as [? | [? | ?]]; auto.
           elim H19; now auto.
       *simpl; intros.
         destruct H17 as [? | [? | ?]]; subst.
          right; left; rewrite <- app_assoc; simpl; repeat (rewrite app_comm_cons); apply Rot_app.
          left; apply Rot_sym, Rot_app_r.
           simpl; repeat (rewrite <- app_assoc); simpl; now apply rotSame.
          right; right; assert (~Rot c ((c1 ++ c2) ++ lxy)).
           intro; elim (InductivePLCADistinctC IPLCA) with (c2 ++ lxy ++ c1).
            apply in_or_app; simpl; now auto.
             apply rin_setremove_remove.
              apply eq_rin_elem with c.
               apply Rot_trans with ((c1 ++ c2) ++ lxy).
                now auto.
                rewrite app_assoc; apply Rot_app_r.
                 rewrite app_assoc; now apply rotSame.
               apply in_rin; now auto.
          assert (RIn c (((c1 ++ c2) ++ lxy) :: C0)).
           apply H5.
            apply in_or_app; apply in_app_or in H17; simpl; destruct H17; now auto.
          destruct H20 as [? | ?]; auto.
           elim H19; now auto.
       *simpl; intros.
         destruct H17 as [? | [? | ?]]; subst.
          right; left; apply rotate_c_area, Rot_app_r.
           simpl; rewrite <- app_assoc; simpl; now apply rotSame.
          left; repeat (rewrite app_comm_cons); apply rotate_c_area, Rot_app_r.
           simpl; repeat (rewrite <- app_assoc); simpl; now apply rotSame.
          right; right; assert (~eq_area a [c2 ++ lxy ++ c1]).
           intro; extractPLCAspec EqP.
            apply DA with [(c1 ++ c2) ++ lxy].
             left; now auto.
             simpl; edestruct eq_listcircuit_dec.
              apply eq_ain_elem with a.
               apply trans_area with [c2 ++ lxy ++ c1].
                now auto.
                rewrite <- app_assoc; apply rotate_c_area, Rot_app_r.
                 rewrite app_assoc; now apply rotSame.
               apply in_ain; now auto.
              elim n0; now auto.
          assert (AIn a (A1 ++ [c2 ++ lxy ++ c1] :: A2)).
           apply H6.
            right; now auto.
          apply ain_or_app; apply ain_app_or in H20; destruct H20 as [? | [? | ?]]; auto.
           elim H19; now auto.
       *simpl; intros.
         destruct H17 as [? | [? | ?]]; subst.
          right; left; rewrite <- app_assoc; simpl; repeat (rewrite app_comm_cons); apply rotate_c_area, Rot_app.
          left; apply rotate_c_area, Rot_sym, Rot_app_r.
           simpl; repeat (rewrite <- app_assoc); simpl; now apply rotSame.
          right; right; assert (~eq_area a [(c1 ++ c2) ++ lxy]).
           intro; elim (InductivePLCADistinctA IPLCA) with [c2 ++ lxy ++ c1].
            apply in_or_app; simpl; now auto.
             apply ain_setremove_remove.
              apply eq_ain_elem with a.
               apply trans_area with [(c1 ++ c2) ++ lxy].
                now auto.
                rewrite app_assoc; apply rotate_c_area, Rot_app_r.
                 rewrite app_assoc; now apply rotSame.
               apply in_ain; now auto.
          assert (AIn a ([(c1 ++ c2) ++ lxy] :: A0)).
           apply H7.
            apply in_or_app; apply in_app_or in H17; simpl; destruct H17; now auto.
          destruct H20 as [? | ?]; auto.
           elim H19; now auto.
      }
    }
  (* ma_inloop *)
  *{
    assert (AIn (lxx :: a) A').
     apply H6.
      left; now auto.
    apply ain_in in H12; destruct H12 as [a' [Ha' Hina']].
    assert (RIn lxx a').
     apply eq_area_rin with (lxx :: a).
      left; now apply rotSame.
      now auto.
    apply rin_in in H12; destruct H12 as [c' [Hc' Hinc']].
    apply Rot_split in Hc'; destruct Hc' as [lxz [lzx [? ?]]]; subst.
    apply trail_app_or in H10.
    destruct H10 as [z [pxz [pzx [Hxz Hzx]]]].
    inversion H8; subst.
     extractPLCAspec EqOPLCA.
      elim LT with (s, s).
       left; now auto.
       simpl; now auto.
     rewrite reverse_linelist_cons in *.
     unfold reverse in *.
     simpl (snd (x, p2)) in *; simpl (fst (x, p2)) in *.
     assert (I (rev ap ++ P')
               ((x, s) :: (p2, x) :: reverse_linelist ll ++ L')
               (([(s, x)] ++ ((x, p2) :: reverse_linelist (reverse_linelist ll)))
                 :: (lzx ++ ((x, s) :: reverse_linelist ll) ++ ((p2, x) :: lxz))
                 :: (set_remove eq_listline_dec (lzx ++ [] ++ lxz) C'))
               ([[(s, x)] ++ ((x, p2) :: reverse_linelist (reverse_linelist ll))]
                 :: ((lzx ++ ((x, s) :: reverse_linelist ll) ++ ((p2, x) :: lxz))
                      :: (set_remove eq_listline_dec (lzx ++ [] ++ lxz) a'))
                 :: (set_remove eq_listcircuit_dec a' A'))
               o') as IPLCA'.
     {
      apply add_inpath with z pzx [x] pxz; auto.
      +apply rev_path; now auto.
      +inversion H12; subst.
        extractPLCAspec EqOPLCA.
         elim DL with (s, x).
          left; now auto.
          simpl; destruct eq_dline_dec with (s, x) (s, x).
           left; constructor.
            unfold reverse; now auto.
           elim n0; now auto.
        rewrite reverse_linelist_cons, app_length, app_length; simpl; omega.
      +destruct lzx; destruct lxz; simpl; simpl in H11; try omega.
        elim H11; now auto.
      +destruct eq_point_dec with s p2.
        subst; extractPLCAspec EqOPLCA.
         elim DL with (p2, x).
          left; now auto.
          simpl; destruct eq_dline_dec with (p2, x) (p2, x).
           left; constructor.
            unfold reverse; now auto.
           elim n0; now auto.
        now auto.
      +intros; intro.
       apply in_rev in H10.
       apply H2 in H13.
       extractPLCAspec EqOPLCA.
       elim DP with p.
        apply in_or_app; now auto.
        clear -H10 H13; induction ap; simpl in *.
         contradiction.
         edestruct eq_point_dec.
          apply in_or_app; now auto.
          simpl; destruct H10; now auto.
     }
     rewrite reverse_linelist_involutive in IPLCA'.
     rewrite app_nil_l in IPLCA'.
     repeat eexists; [ exact IPLCA' | ].
     apply PLCAeq_sym; auto.
     eapply PLCAtrans; eauto.
     apply PLCAeq; auto.
     *apply IPLCA_satisfies_planarity_condition; exact IPLCA'.
     *intros.
       apply in_app_or in H10; apply in_or_app.
        destruct H10.
         left; apply in_rev in H10; now auto.
         right; now auto.
     *intros.
       apply in_app_or in H10; apply in_or_app.
        destruct H10.
         left; apply in_rev; now auto.
         right; now auto.
     *intros.
      simpl in H10; destruct H10 as [? | [? | ?]]; subst.
       left; constructor.
        unfold reverse; simpl; now auto.
       right; left; constructor.
        unfold reverse; simpl; now auto.
       right; right; apply lin_or_app; apply in_app_or in H10; destruct H10.
        left; apply LIn_reverse_linelist.
         apply in_lin; now auto.
        right; apply H0; now auto.
     *intros.
      simpl in H10; destruct H10 as [? | [? | ?]]; subst.
       left; constructor.
        unfold reverse; simpl; now auto.
       right; left; constructor.
        unfold reverse; simpl; now auto.
       right; right; apply lin_or_app; apply in_app_or in H10; destruct H10.
        left; apply LIn_reverse_linelist_inv.
         apply in_lin; now auto.
        right; apply H3; now auto.
     *intros.
      destruct H10 as [? | [? | ?]]; subst.
       left; now apply Rot_app.
       right; left.
        apply Rot_app_r.
         simpl; repeat rewrite <- app_assoc; simpl; now apply rotSame.
       right; right.
        assert (~Rot c (lzx ++ lxz)).
         intro; extractPLCAspec EqP.
          apply DC with (lxz ++ lzx).
           left; now auto.
           simpl; edestruct eq_listline_dec.
            apply eq_rin_elem with c.
             apply Rot_trans with (lzx ++ lxz).
              now auto.
              now apply Rot_app.
             apply in_rin; now auto.
            elim n0; now auto.
        assert (In (lzx ++ lxz) C').
         extractPLCAspec IT.
          apply AC with a'; now auto.
        assert (RIn c C').
         apply H4.
          right; now auto.
        apply in_split in H14; destruct H14 as [C'1 [C'2 ?]]; subst.
        apply rin_setremove_remove.
        apply rin_app_or in H16; apply rin_or_app; destruct H16 as [? | [? | ?]]; auto.
         elim H13; now auto.
     *intros.
      destruct H10 as [? | [? | ?]]; subst.
       left; now apply Rot_app.
       right; left.
        apply Rot_sym, Rot_app_r.
         simpl; repeat rewrite <- app_assoc; simpl; now apply rotSame.
       right; right.
        assert (~Rot c (lzx ++ lxz)).
         intro; extractPLCAspec IT.
          apply DC with (lzx ++ lxz).
           apply AC with a'; now auto.
           apply eq_rin_elem with c.
            now auto.
            apply in_rin; now auto.
        destruct H5 with c.
         apply set_remove_in in H10; now auto.
         elim H13; apply Rot_trans with (lxz ++ lzx).
          now auto.
          now apply Rot_app.
         now auto.
     *intros.
      destruct H10 as [? | [? | ?]]; subst.
       left; now apply rotate_c_area, Rot_app.
       right; left.
        apply eq_area_cons_rot.
         apply Rot_app_r.
          simpl; repeat rewrite <- app_assoc; simpl; now apply rotSame.
         assert (Heqa := eq_area_set_remove_inv _ _ (lzx ++ lxz) Ha').
         assert (~RIn (lzx ++ lxz) (set_remove eq_listline_dec (lzx ++ lxz) a')) as nrin.
          extractPLCAspec IT.
           apply AT; now auto.
         apply in_split in Hinc'.
         destruct Hinc' as [a'1 [a'2 ?]]; subst.
         rewrite (rin_distinct_setremove _ _ _ nrin).
         erewrite rin_distinct_setremove_eq in Heqa.
          simpl in Heqa.
          edestruct eq_circuit_dec.
           now auto.
           elim n0; now apply Rot_app.
           apply nrin.
       right; right.
        assert (~eq_area a0 a').
         intro; extractPLCAspec EqP.
          apply DA with ((lxz ++ lzx) :: a).
           left; now auto.
           simpl; edestruct eq_listcircuit_dec.
            apply eq_ain_elem with a0.
             apply trans_area with a'.
              now auto.
              apply eq_area_sym; now auto.
             apply in_ain; now auto.
            elim n0; now auto.
        assert (In a' A') by auto.
        assert (AIn a0 A').
         apply H6.
          right; now auto.
        apply in_split in H14; destruct H14 as [C'1 [C'2 ?]]; subst.
        apply ain_setremove_remove.
        apply ain_app_or in H16; apply ain_or_app; destruct H16 as [? | [? | ?]]; auto.
         elim H13; now auto.
     *intros.
      destruct H10 as [? | [? | ?]]; subst.
       left; now apply rotate_c_area, Rot_app.
       right; left.
        apply eq_area_sym, eq_area_cons_rot.
         apply Rot_app_r.
          simpl; repeat rewrite <- app_assoc; simpl; now apply rotSame.
         assert (Heqa := eq_area_set_remove_inv _ _ (lzx ++ lxz) Ha').
         assert (~RIn (lzx ++ lxz) (set_remove eq_listline_dec (lzx ++ lxz) a')) as nrin.
          extractPLCAspec IT.
           apply AT; now auto.
         apply in_split in Hinc'.
         destruct Hinc' as [a'1 [a'2 ?]]; subst.
         rewrite (rin_distinct_setremove _ _ _ nrin).
         erewrite rin_distinct_setremove_eq in Heqa.
          simpl in Heqa.
          edestruct eq_circuit_dec.
           now auto.
           elim n0; now apply Rot_app.
           apply nrin.
       right; right.
        assert (~eq_area a0 a').
         intro; extractPLCAspec IT.
          apply DA with a'.
           now auto.
           apply eq_ain_elem with a0.
            now auto.
            apply in_ain; now auto.
        destruct H7 with a0.
         apply set_remove_in in H10; now auto.
         elim H13; apply trans_area with ((lxz ++ lzx) :: a); now auto.
         now auto.
   }
  (* ma_outloop *)
  *{
    apply Rot_split in H9; destruct H9 as [lxz [lzx [? ?]]]; subst.
    apply trail_app_or in H10.
    destruct H10 as [z [pxz [pzx [Hxz Hzx]]]].
    inversion H8; subst.
     extractPLCAspec EqOPLCA.
      elim LT with (s, s).
       left; now auto.
       simpl; now auto.
     rewrite reverse_linelist_cons in *.
     unfold reverse in *.
     simpl (snd (x, p2)) in *; simpl (fst (x, p2)) in *.
     assert (I (rev ap ++ P')
               ((x, s) :: (p2, x) :: reverse_linelist ll ++ L')
               (([(s, x)] ++ ((x, p2) :: reverse_linelist (reverse_linelist ll)))
                 :: (lzx ++ ((x, s) :: reverse_linelist ll) ++ ((p2, x) :: lxz))
                 :: (set_remove eq_listline_dec (lzx ++ [] ++ lxz) C'))
               ([[(s, x)] ++ ((x, p2) :: reverse_linelist (reverse_linelist ll))] :: A')
               (lzx ++ ((x, s) :: reverse_linelist ll) ++ ((p2, x) :: lxz))) as IPLCA'.
     {
      apply add_outpath with (lzx ++ lxz) z pzx [x] pxz; auto.
      +simpl; extractPLCAspec IT.
        now apply O1.
      +apply rev_path; now auto.
      +inversion H10; subst.
        extractPLCAspec EqOPLCA.
         elim DL with (s, x).
          left; now auto.
          simpl; destruct eq_dline_dec with (s, x) (s, x).
           left; constructor.
            unfold reverse; now auto.
           elim n0; now auto.
        rewrite reverse_linelist_cons, app_length, app_length; simpl; omega.
      +destruct lzx; destruct lxz; simpl; simpl in H11; try omega.
        elim H11; now auto.
      +destruct eq_point_dec with s p2.
        subst; extractPLCAspec EqOPLCA.
         elim DL with (p2, x).
          left; now auto.
          simpl; destruct eq_dline_dec with (p2, x) (p2, x).
           left; constructor.
            unfold reverse; now auto.
           elim n0; now auto.
        now auto.
      +intros; intro.
       apply in_rev in H9.
       apply H2 in H12.
       extractPLCAspec EqOPLCA.
       elim DP with p.
        apply in_or_app; now auto.
        clear -H9 H12; induction ap; simpl in *.
         contradiction.
         edestruct eq_point_dec.
          apply in_or_app; now auto.
          simpl; destruct H9; now auto.
     }
     rewrite reverse_linelist_involutive in IPLCA'.
     rewrite app_nil_l in IPLCA'.
     repeat eexists; [ exact IPLCA' | ].
     apply PLCAeq_sym; auto.
     eapply PLCAtrans; eauto.
     apply PLCAeq; auto.
     *apply IPLCA_satisfies_planarity_condition; exact IPLCA'.
     *intros.
       apply in_app_or in H9; apply in_or_app.
        destruct H9.
         left; apply in_rev in H9; now auto.
         right; now auto.
     *intros.
       apply in_app_or in H9; apply in_or_app.
        destruct H9.
         left; apply in_rev; now auto.
         right; now auto.
     *intros.
      simpl in H9; destruct H9 as [? | [? | ?]]; subst.
       left; constructor.
        unfold reverse; simpl; now auto.
       right; left; constructor.
        unfold reverse; simpl; now auto.
       right; right; apply lin_or_app; apply in_app_or in H9; destruct H9.
        left; apply LIn_reverse_linelist.
         apply in_lin; now auto.
        right; apply H0; now auto.
     *intros.
      simpl in H9; destruct H9 as [? | [? | ?]]; subst.
       left; constructor.
        unfold reverse; simpl; now auto.
       right; left; constructor.
        unfold reverse; simpl; now auto.
       right; right; apply lin_or_app; apply in_app_or in H9; destruct H9.
        left; apply LIn_reverse_linelist_inv.
         apply in_lin; now auto.
        right; apply H3; now auto.
     *intros.
      destruct H9 as [? | [? | ?]]; subst.
       left; now apply Rot_app.
       right; left.
        apply Rot_app_r.
         simpl; repeat rewrite <- app_assoc; simpl; now apply rotSame.
       right; right.
        assert (~Rot c (lzx ++ lxz)).
         intro; extractPLCAspec EqP.
          apply DC with (lxz ++ lzx).
           left; now auto.
           simpl; edestruct eq_listline_dec.
            apply eq_rin_elem with c.
             apply Rot_trans with (lzx ++ lxz).
              now auto.
              now apply Rot_app.
             apply in_rin; now auto.
            elim n0; now auto.
        assert (In (lzx ++ lxz) C').
         extractPLCAspec IT.
          now apply O1.
        assert (RIn c C').
         apply H4.
          right; now auto.
        apply in_split in H13; destruct H13 as [C'1 [C'2 ?]]; subst.
        apply rin_setremove_remove.
        apply rin_app_or in H15; apply rin_or_app; destruct H15 as [? | [? | ?]]; auto.
         elim H12; now auto.
     *intros.
      destruct H9 as [? | [? | ?]]; subst.
       left; now apply Rot_app.
       right; left.
        apply Rot_sym, Rot_app_r.
         simpl; repeat rewrite <- app_assoc; simpl; now apply rotSame.
       right; right.
        assert (~Rot c (lzx ++ lxz)).
         intro; extractPLCAspec IT.
          apply DC with (lzx ++ lxz).
           now apply O1.
           apply eq_rin_elem with c.
            now auto.
            apply in_rin; now auto.
        destruct H5 with c.
         apply set_remove_in in H9; now auto.
         elim H12; apply Rot_trans with (lxz ++ lzx).
          now auto.
          now apply Rot_app.
         now auto.
     *intros.
      destruct H9 as [? | ?]; subst.
       left; now apply rotate_c_area, Rot_app.
       right.
        apply H6; now auto.
     *intros.
      destruct H9 as [? | ?]; subst.
       left; now apply rotate_c_area, Rot_app.
       right.
        apply H7; now auto.
     *apply Rot_app_r.
       simpl; repeat rewrite <- app_assoc; simpl.
        now apply rotSame.
   }
(* <- *)
+intros IPLCA.
 destruct IPLCA as [P' [L' [C' [A' [o' [IPLCA PLCAEQ]]]]]].
 refine (PLCAeq_planar _ PLCAEQ).
 apply IPLCA_satisfies_planarity_condition; exact IPLCA.
Qed.
