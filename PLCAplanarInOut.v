Require Import List Arith.
Require Import EqIn SetRemove PLCAobject.
Require Import PLCAconstraints PLCAconsistency.
Require Import PLCAplanar.
Require Import PLCAconnectpath.
Import ListNotations.


Section ConnectCircuit.

Variable P : list Point.
Variable L : list Line.
Variable C : list Circuit.
Variable A : list Area.
Variable o : Circuit.

Variable planar : PLCAplanarity_condition P L C A o.



Lemma Circuit_ConnectPath' :
 forall (c1 c2 : Circuit),
  In c1 C -> In c2 C -> exists os : list object, connectpath P L C A (o_circuit c1) (o_circuit c2) os.
intros.
extractPLCAspec planar.
apply connectpath_PLCAconnect.
+simpl; now auto.
+simpl; now auto.
+apply PLCAcon.
 -simpl; now auto.
 -simpl; now auto.
Qed.



Lemma Circuit_ConnectPath :
 forall (c1 c2 : Circuit),
  In c1 C -> In c2 C -> c1 <> c2 -> exists os : list object, connectpath P L C A (o_circuit c1) (o_circuit c2) (o_circuit c1 :: os ++ [o_circuit c2]).
intros.
destruct (Circuit_ConnectPath' _ _ H H0).
inversion H2; subst.
+elim H1; now auto.
+apply connectpath_rev in H7.
 inversion H7; subst.
 -exists (rev path0).
  apply pathCA with a; auto.
  rewrite <- (rev_involutive [o_circuit c2]).
  rewrite <- rev_app_distr.
  apply connectpath_rev.
  simpl; rewrite H8.
  now auto.
 -exists (rev path0).
  apply pathCA with a; auto.
  rewrite <- (rev_involutive [o_circuit c2]).
  rewrite <- rev_app_distr.
  apply connectpath_rev.
  simpl; rewrite H8.
  now auto.
+apply connectpath_rev in H7.
 inversion H7; subst.
 -exists (rev path0).
  apply pathCL with l; auto.
  rewrite <- (rev_involutive [o_circuit c2]).
  rewrite <- rev_app_distr.
  apply connectpath_rev.
  simpl; rewrite H8.
  now auto.
 -exists (rev path0).
  apply pathCL with l; auto.
  rewrite <- (rev_involutive [o_circuit c2]).
  rewrite <- rev_app_distr.
  apply connectpath_rev.
  simpl; rewrite H8.
  now auto.
Qed.





Definition ConnectCircuit : Circuit -> Circuit -> Prop :=
 fun c1 c2 =>
   exists (os : list object), (connectpath P L C A (o_circuit c1) (o_circuit c2) os /\
                              (forall a : Area, ~In (o_area a) os)).


Inductive InOut : Set := IN | OUT.


Inductive Circuit_InOut : Circuit -> InOut -> list Area -> Prop :=
| outermost_out : Circuit_InOut o OUT []
| connect_in : forall (c1 c2 : Circuit) (al : list Area),
                 In c1 C ->
                 In c2 C ->
                 Circuit_InOut c1 OUT al ->
                 ConnectCircuit c1 c2 ->
                 c1 <> c2 -> Circuit_InOut c2 IN al
| in_area_out : forall (c1 c2 : Circuit) (a : Area) (al : list Area),
                 In c1 C ->
                 In c2 C ->
                 In a A ->
                 Circuit_InOut c1 IN al ->
                 In c1 a ->
                 In c2 a ->
                 c1 <> c2 -> Circuit_InOut c2 OUT (a :: al).





Lemma Circuit_In :
 forall (c : Circuit) (io : InOut) (al : list Area),
  Circuit_InOut c io al -> In c C.
intros.
extractPLCAspec planar.
induction H.
+now apply O1.
+now auto.
+now auto.
Qed.




Lemma Circuit_OUT_exist_IN :
 forall (c : Circuit) (a : Area) (al : list Area), Circuit_InOut c OUT (a :: al)
 -> exists (c' : Circuit) (a : Area), In a A /\ In c a /\ In c' a /\ Circuit_InOut c' IN al.
intros.
inversion H; subst.
exists c1, a; now auto.
Qed.


Lemma Circuit_OUT_area_dec :
 forall (c1 c2 : Circuit) (a : Area) (al : list Area),
  Circuit_InOut c1 OUT (a :: al)
  -> In c1 a
  -> In c2 a
  -> In a A
  -> Circuit_InOut c2 IN al \/ Circuit_InOut c2 OUT (a :: al).
intros.
inversion H; subst.
destruct eq_listline_dec with c0 c2.
-subst; now auto.
-right; apply in_area_out with c0; auto.
 extractPLCAspec planar.
 apply AC with a.
  now auto.
  now auto.
Qed.



Lemma Circuit_connect_dec :
 forall (c1 c2 : Circuit) (io : InOut) (al : list Area),
  Circuit_InOut c1 io al
  -> ConnectCircuit c1 c2
  -> Circuit_InOut c2 IN al \/ Circuit_InOut c2 OUT al.
intros.
inversion H; subst.
+destruct eq_listline_dec with c1 c2.
 -subst; now auto.
 -left; apply connect_in with c1; auto.
  extractPLCAspec planar.
   exact O1.
   unfold ConnectCircuit in H0.
   destruct H0 as [path [H0 _]].
   assert (in_r := @connectpath_in_r P L C A (o_circuit c1) (o_circuit c2) path H0).
   simpl in in_r; now auto.
+destruct eq_listline_dec with c0 c2.
 -subst; now auto.
 -left; apply connect_in with c0; auto.
  *clear -H0; unfold ConnectCircuit in H0.
   destruct H0 as [path [H0 _]].
   assert (in_r := @connectpath_in_r P L C A (o_circuit c1) (o_circuit c2) path H0).
    simpl in in_r; now auto.
  *unfold ConnectCircuit in *.
   destruct H0 as [os1 [H0 H0']].
   inversion H0; subst.
    now auto.
    destruct H4 as [os0 [H4 H4']].
    exists (os0 ++ path).
    split.
     apply connectpath_trans with (o_circuit c1); now auto.
     intros a0 H11.
     apply in_app_or in H11; destruct H11.
      apply H4' with a0; now auto.
      apply H0' with a0; simpl; now auto.
    destruct H4 as [os0 [H4 H4']].
    exists (os0 ++ path).
    split.
     apply connectpath_trans with (o_circuit c1); now auto.
     intros a0 H11.
     apply in_app_or in H11; destruct H11.
      apply H4' with a0; now auto.
      apply H0' with a0; simpl; now auto.
+destruct eq_listline_dec with c1 c2.
 -subst; now auto.
 -left; apply connect_in with c1; auto.
  destruct H0 as [path [H0 _]].
  assert (in_r := @connectpath_in_r P L C A (o_circuit c1) (o_circuit c2) path H0).
  simpl in in_r; now auto.
Qed.



Lemma path_circuit_exists :
 forall path : list object,
  (exists (c : Circuit) (path1 path2 : list object),
    path = path1 ++ o_circuit c :: path2
    /\ In (o_circuit c) path /\ forall c' : Circuit, ~In (o_circuit c') path2)
  \/ (forall c : Circuit, ~In (o_circuit c) path).
induction path; intros.
+right; simpl; now auto.
+(*destruct IHpath as 

destruct a.
 -destruct IHpath.
  *left; destruct H; eexists; right; now eauto.
  *right; intros c ?.
   destruct H0.
    inversion H0.
    apply H with c; now auto.
 -destruct IHpath.
  *left; destruct H; eexists; right; now eauto.
  *right; intros c ?.
   destruct H0.
    inversion H0.
    apply H with c; now auto.
 -left; exists c; simpl; now auto.
 -destruct IHpath.
  *left; destruct H; eexists; right; now eauto.
  *right; intros c ?.
   destruct H0.
    inversion H0.
    apply H with c; now auto. *)
Admitted.


Lemma path_area_exists :
 forall path : list object,
  (exists a : Area, In (o_area a) path) \/ (forall a : Area, ~In (o_area a) path).
induction path; intros.
+right; simpl; now auto.
+destruct a.
 -destruct IHpath.
  *left; destruct H; eexists; right; now eauto.
  *right; intros a ?.
   destruct H0.
    inversion H0.
    apply H with a; now auto.
 -destruct IHpath.
  *left; destruct H; eexists; right; now eauto.
  *right; intros a ?.
   destruct H0.
    inversion H0.
    apply H with a; now auto.
 -destruct IHpath.
  *left; destruct H; eexists; right; now eauto.
  *right; intros a ?.
   destruct H0.
    inversion H0.
    apply H with a; now auto.
 -left; exists a; simpl; now auto.
Qed.




Lemma Outermost_not_in :
 forall al : list Area, ~Circuit_InOut o IN al.
intros al H.
inversion H; subst.
Admitted.


Require Import Wf_nat.

(*
意味不明なものになったので
Lemma Circuit_InOut_dec' :
 forall (c1 c2 : Circuit) (,
  In c1 C
  -> In c2 C
  -> Circuit_InOut c1 IN \/ Circuit_InOut c1 OUT
  -> Circuit_InOut c2 IN \/ Circuit_InOut c2 OUT.
intros.
destruct eq_listline_dec with c1 c2.
 subst; now auto.
apply Circuit_ConnectPath with c1 c2 in H0; auto.
clear H n.
destruct H0 as [path cp].
remember (length path).
revert c1 c2 path Heqn cp H1.
apply (lt_wf_ind n); clear n; intros.
destruct path_circuit_exists with path as [[c H0] | H0].
+apply in_split in H0; destruct H0 as [path1 [path2 ?]]; subst.
 assert (connectpath P L C A (o_circuit c1) (o_circuit c) ((o_circuit c1 :: path1) ++ [o_circuit c])) as cp1.
 {
  apply connectpath_cut_l with (o_circuit c2) (path2 ++ [o_circuit c2]).
  simpl; rewrite <- app_assoc in cp.
  simpl in cp; now auto.
 }
 simpl in cp1; assert (connectpath P L C A (o_circuit c) (o_circuit c2) (o_circuit c :: (path2 ++ [o_circuit c2]))) as cp2.
 {
  apply connectpath_cut_r with (o_circuit c1) (o_circuit c1 :: path1).
  simpl; rewrite <- app_assoc in cp.
  simpl in cp; now auto.
 }
 assert (Circuit_InOut c IN \/ Circuit_InOut c OUT) as IHc.
 {
  apply H with (length path1) c1 path1; auto.
   rewrite app_length; simpl.
   omega.
 }
 apply H with (length path2) c path2; auto.
  rewrite app_length; simpl.
  omega.
+clear n H Heqn; destruct path_area_exists with path as [[a H2] | H2].
 -apply in_split in H2; destruct H2 as [path1 [path2 ?]]; subst.
  assert (path1 = []).
  {
   assert (connectpath P L C A (o_circuit c1) (o_area a) ((o_circuit c1 :: path1) ++ [o_area a])).
    apply connectpath_cut_l with (o_circuit c2) (path2 ++ [o_circuit c2]).
    simpl.
    rewrite <- app_assoc in cp; simpl in cp; now auto.
   destruct path1.
    auto.
    apply connectpath_rev in H; simpl in H.
    rewrite rev_unit in H; simpl in H.
    inversion H; subst.
    elim H0 with c.
    apply in_or_app; left.
    apply in_rev.
    simpl.
    apply connectpath_head in H8.
    destruct H8.
    destruct (rev path1); simpl in *; inversion H2; subst.
     left; auto.
     left; auto.
  }
  subst; simpl in *.
  inversion cp; subst.
  *inversion H7; subst.
   destruct path2; simpl in *.
    inversion H12; subst.
     destruct eq_listline_dec with c1 c2.
      subst; now auto.
      destruct H1.
       right; apply in_area_out with c1 a; now auto.
       apply Circuit_OUT_area_dec with c1 a; now auto.
     now inversion H16.
     now inversion H16.
    inversion H12; subst.
     destruct path2; now inversion H13.
     elim H0 with c; now auto.
     elim H0 with c; now auto.
  *inversion H7.
 -destruct H1.
  *right; extractPLCAspec planar.
   destruct CA with c1.
    apply Circuit_In in H; now auto.
apply in_area_out with c1 a.  
admit.
Qed.
*)


Lemma Planar_InOut_dec :
 forall c : Circuit, In c C -> exists al : list Area, Circuit_InOut c IN al \/ Circuit_InOut c OUT al.
intros.
Admitted.
(*
apply Circuit_InOut_dec' with o.
+extractPLCAspec planar.
 exact O1.
+now auto.
+right; now constructor.
Qed.
*)




Lemma Planar_InOut_exclusive :
 forall c : Circuit, (exists al : list Area, Circuit_InOut c IN al) -> (exists al : list Area, Circuit_InOut c OUT al) -> False.
Admitted.



End ConnectCircuit.
