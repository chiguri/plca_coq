Require Import List Arith Permutation.
Require Import EqIn Rotate SetRemove PLCAobject.
Require Import PLCAconstraints PLCAconsistency PLCAplanar.
Require Import ReplaceCircuit.
Require Import PLCAconnectpath.
Require Import PLCAequivalence.

Section difference_num.

Context {A : Set}.
Variable eq_dec : forall a1 a2 : A, {a1 = a2} + {a1 <> a2}.


Fixpoint dif_num l1 l2 :=
match l1 with
| nil => O
| a :: l => if (in_dec eq_dec a l2) then dif_num l l2 else S (dif_num l l2)
end.



Lemma dif_num_O_perm :
  forall l1 l2,
   NoDup l1 -> NoDup l2 -> length l1 = length l2 -> dif_num l1 l2 = 0 -> Permutation l1 l2.
induction l1; intros.
 destruct l2; simpl in H1.
  now constructor.
 now inversion H1.
simpl in H1, H2.
destruct (in_dec eq_dec a l2).
 apply in_split in i.
 destruct i as [l2' [l2'' H3]].
 subst.
 apply Permutation_cons_app.
 apply IHl1.
  inversion H; now auto.
  apply NoDup_remove_1 in H0.
   now auto.
  rewrite app_length in H1.
   simpl in H1.
    rewrite app_length.
     rewrite <- plus_n_Sm in H1.
      inversion H1; reflexivity.
  clear -H H2.
   induction l1; simpl.
    now auto.
    destruct (in_dec eq_dec a0 (l2' ++ l2'')).
     apply IHl1.
      inversion H; subst.
       constructor.
        intro; apply H3; right; now auto.
        inversion H4; now auto.
      simpl in H2.
       destruct (in_dec eq_dec a0 (l2' ++ a :: l2'')).
        now auto.
        now inversion H2.
     assert (a = a0).
      simpl in H2.
       destruct (in_dec eq_dec a0 (l2' ++ a :: l2'')).
        apply in_app_or in i; destruct i as [? | [? | ?]].
         elim n; apply in_or_app; left; now auto.
         now auto.
         elim n; apply in_or_app; right; now auto.
        now inversion H2.
      inversion H; subst.
       elim H4; simpl; now auto.
now inversion H2.
Qed.


Lemma dif_num_S_exists :
  forall l1 l2 n,
   NoDup l1 -> dif_num l1 l2 = S n ->
     exists x : A, In x l1 /\ ~ In x l2.
intros.
induction l1.
simpl in H0; inversion H0.
inversion H; subst.
simpl in H0; destruct (in_dec eq_dec a l2).
destruct (IHl1 H4 H0) as [x [Hx Hnx]].
exists x; split.
right; exact Hx.
exact Hnx.
exists a; split; simpl; auto.
Qed.


Lemma dif_num_permutation_same_l :
  forall l1 l2 l3,
   Permutation l1 l2 -> dif_num l1 l3 = dif_num l2 l3.
intros l1 l2 l3 H.
induction H.
 simpl; now auto.
 simpl.
  destruct (in_dec eq_dec x l3); now auto.
 simpl.
  destruct (in_dec eq_dec x l3); destruct (in_dec eq_dec y l3); now auto.
 rewrite IHPermutation1; now auto.
Qed.


Lemma dif_num_permutation_same_r :
  forall l1 l2 l3,
   Permutation l2 l3 -> dif_num l1 l2 = dif_num l1 l3.
induction l1; intros; simpl.
 now auto.
 destruct (in_dec eq_dec a l2); destruct (in_dec eq_dec a l3).
  apply IHl1; now auto.
  elim n; apply Permutation_in with l2; now auto.
  elim n; apply Permutation_in with l3; now auto.
  f_equal; apply IHl1; now auto.
Qed.


Lemma dif_num_remove_object :
  forall l1 l2 l3 a,
   ~In a l1 -> dif_num l1 (l2 ++ a :: l3) = dif_num l1 (l2 ++ l3).
induction l1; intros; simpl.
 now auto.
 destruct (in_dec eq_dec a (l2 ++ a0 :: l3)).
  destruct (in_dec eq_dec a (l2 ++ l3)).
   apply IHl1.
    intro; apply H; right; now auto.
    apply in_app_or in i; destruct i as [? | [? | ?]].
     elim n; apply in_or_app; now auto.
     subst; elim H; simpl; now auto.
     elim n; apply in_or_app; now auto.
  destruct (in_dec eq_dec a (l2 ++ l3)).
   elim n; apply in_or_app; simpl; apply in_app_or in i; destruct i; now auto.
   f_equal; apply IHl1.
    intro; apply H; right; now auto.
Qed.


End difference_num.


(* misc *)
Lemma plus_m_n_O : forall m n, m + n = 0 -> m = 0 /\ n = 0.
intros.
destruct m; simpl in H.
auto.
inversion H.
Qed.


Lemma eq_area_exist_circuit :
  forall (a a' : Area) (c : Circuit),
   eq_area a a' -> In c a -> exists c' : Circuit, In c' a' /\ Rot c c'.
intros a a' c H H0; revert c H0; induction H; intros.
 exists c; split.
  apply Permutation_in with a1; now auto.
  now apply rotSame.
 destruct H0.
  subst; exists c2; split; simpl; now auto.
  exists c; split; simpl; [now auto | now apply rotSame].
 destruct IHeq_area1 with c.
  now auto.
  destruct H2; destruct IHeq_area2 with x.
   now auto.
   exists x0; destruct H4; split.
    now auto.
    apply Rot_trans with x; now auto.
Qed.


Lemma AIn_length :
  forall A1 A2,
   DistinctA A1 ->
   DistinctA A2 ->
   (forall a, In a A1 -> AIn a A2) ->
   (forall a, In a A2 -> AIn a A1) ->
    length A1 = length A2.
induction A1; intros; simpl in *.
 destruct A2.
  now auto.
  elim H2 with a.
   left; now auto.
 assert (AIn a A2) by auto.
  apply ain_in in H3.
   destruct H3 as [a' [? ?]].
    apply in_split in H4; destruct H4 as [A3 [A4 ?]]; subst.
     rewrite app_length; simpl.
      rewrite <- plus_n_Sm; f_equal.
       rewrite <- app_length; apply IHA1.
        unfold DistinctA in *; intros.
         intro; apply H with a0.
          right; now auto.
          simpl.
           destruct eq_listcircuit_dec with a0 a.
            apply in_ain; now auto.
            right; now auto.
        unfold DistinctA in *; intros.
         intro; apply H0 with a0.
          apply in_app_or in H4; apply in_or_app; simpl; destruct H4; now auto.
          clear -H4 H5; induction A3.
           simpl in *; destruct eq_listcircuit_dec with a0 a'.
            apply in_ain; now auto.
            right; now auto.
           simpl in *.
            destruct eq_listcircuit_dec with a0 a.
             apply ain_or_app; simpl; apply ain_app_or in H5; destruct H5; now auto.
             destruct H4.
              elim n; now auto.
              destruct H5.
               left; now auto.
               right; apply IHA3; now auto.
        intros.
         assert (AIn a0 (A3 ++ a' :: A4)) by auto.
          apply ain_or_app; apply ain_app_or in H5; destruct H5 as [? | [? | ?]]; auto.
           assert (eq_area a a0).
            apply trans_area with a'.
             now auto.
             apply eq_area_sym; now auto.
            elim H with a0.
             right; now auto.
             simpl; destruct eq_listcircuit_dec with a0 a.
              apply in_ain; now auto.
              left; apply eq_area_sym; now auto.
        intros.
         destruct H2 with a0.
          apply in_or_app; simpl; apply in_app_or in H4; destruct H4; now auto.
          elim H0 with a'.
           apply in_or_app; simpl; now auto.
           apply ain_setremove_remove.
            apply eq_ain_elem with a0.
             apply trans_area with a.
              now auto.
              now auto.
             apply in_ain; now auto.
          now auto.
Qed.




Lemma eq_area_perm :
  forall (C : list Circuit) (A A' : list Area) (a a' : Area),
    DistinctC C
    -> ACconsistency C A
    -> ACconsistency C A'
    -> Areaconsistency A
    -> Areaconsistency A'
    -> Aconstraint A
    -> Aconstraint A'
    -> In a A
    -> In a' A'
    -> eq_area a a'
    -> Permutation a a'.
intros.
apply NoDup_Permutation.
 apply DistinctC_NoDup.
  unfold Aconstraint in H4; unfold DistinctC.
   unfold Area_constraints in H4.
    destruct (H4 a) as [_ [? _]]; now auto.
 apply DistinctC_NoDup.
  unfold Aconstraint in H5; unfold DistinctC.
   unfold Area_constraints in H5.
    destruct (H5 a') as [_ [? _]]; now auto.
 intro; split; intro.
  assert (In x C).
   unfold ACconsistency in H0.
    apply H0 with a; now auto.
  destruct eq_area_exist_circuit with a a' x.
   now auto.
   now auto.
   assert (In x0 C).
    unfold ACconsistency in H1.
     apply H1 with a'; now auto.
   destruct H11; destruct eq_listline_dec with x x0.
    subst; now auto.
    elim H with x.
     now auto.
     apply in_split in H10.
      destruct H10 as [l1 [l2 H10]]; subst.
       apply rin_setremove_remove.
        apply rin_or_app.
         apply in_app_or in H12; destruct H12 as [? | [? | ?]].
          left; apply eq_rin_elem with x0.
           apply Rot_sym; now auto.
           apply in_rin; now auto.
          elim n; now auto.
          right; apply eq_rin_elem with x0.
           apply Rot_sym; now auto.
           apply in_rin; now auto.
  assert (In x C).
   unfold ACconsistency in H1.
    apply H1 with a'; now auto.
  destruct eq_area_exist_circuit with a' a x.
   apply eq_area_sym; now auto.
   now auto.
   assert (In x0 C).
    unfold ACconsistency in H0.
     apply H0 with a; now auto.
   destruct H11; destruct eq_listline_dec with x x0.
    subst; now auto.
    elim H with x.
     now auto.
     apply in_split in H10.
      destruct H10 as [l1 [l2 H10]]; subst.
       apply rin_setremove_remove.
        apply rin_or_app.
         apply in_app_or in H12; destruct H12 as [? | [? | ?]].
          left; apply eq_rin_elem with x0.
           apply Rot_sym; now auto.
           apply in_rin; now auto.
          elim n; now auto.
          right; apply eq_rin_elem with x0.
           apply Rot_sym; now auto.
           apply in_rin; now auto.
Qed.



Lemma PLCAeq_A :
  forall (P : list Point) (L : list Line) (C : list Circuit) (A A' : list Area) (o : Circuit),
    PLCAplanarity_condition P L C A o
    -> PLCAplanarity_condition P L C A' o
    -> (forall a : Area, In a A  -> AIn a A')
    -> (forall a : Area, In a A' -> AIn a A)
    -> PLCAequivalence P L C A o P L C A' o.
intros P L C A A' o.
remember (dif_num eq_listcircuit_dec A A').
symmetry in Heqn.
revert P L C A A' o Heqn.
induction n;
  intros P L C A A' o Heqn x x' IAA' IA'A.
 apply permutePLCA; try (apply Permutation_refl).
  apply dif_num_O_perm with eq_listcircuit_dec.
   extractPLCAspec x.
    apply DistinctA_NoDup.
     exact DA.
   extractPLCAspec x'.
    apply DistinctA_NoDup.
     exact DA.
   extractPLCAspec x; extractPLCAspec x'; apply AIn_length; now auto.
   exact Heqn.
 destruct (dif_num_S_exists eq_listcircuit_dec A A' n) as [c [IA NIA']].
  apply DistinctA_NoDup; extractPLCAspec x; now auto.
  now auto.
  assert (AIn c A') as AIA'.
   apply IAA'; now auto.
   apply ain_in in AIA'.
    destruct AIA' as [c' [eqc IA']].
  apply in_split in IA'; destruct IA' as [A1' [A2' ?]]; subst.
  apply in_split in IA; destruct IA as [A1 [A2 ?]]; subst.
  assert (PLCAequivalence P L C (A1 ++ c :: A2) o P L C (A1 ++ c' :: A2) o) as eqMid.
   apply PLCAtrans with P L C (c :: A1 ++ A2) o.
    apply permutePLCA; try (apply Permutation_refl).
     apply Permutation_sym; apply Permutation_cons_app; now apply Permutation_refl.
    apply PLCAtrans with P L C (c' :: A1 ++ A2) o.
     apply permuteArea.
      extractPLCAspec x; extractPLCAspec x'; apply eq_area_perm with C (A1 ++ c :: A2) (A1' ++ c' :: A2'); auto.
      apply in_or_app; simpl; now auto.
      apply in_or_app; simpl; now auto.
     apply permutePLCA; try (apply Permutation_refl).
      apply Permutation_cons_app; now apply Permutation_refl.
  apply PLCAtrans with P L C (A1 ++ c' :: A2) o.
   exact eqMid.
   assert (x'' := PLCAeq_planar x eqMid).
    apply IHn; auto.
     rewrite dif_num_permutation_same_l with eq_listcircuit_dec _ (c' :: A1 ++ A2) _.
      rewrite dif_num_permutation_same_l with eq_listcircuit_dec _ (c :: A1 ++ A2) _ in Heqn.
       simpl; simpl in Heqn.
        destruct (in_dec eq_listcircuit_dec c' (A1' ++ c' :: A2')).
         destruct (in_dec eq_listcircuit_dec c(A1' ++ c' :: A2')).
          elim NIA'; now auto.
          inversion Heqn; now auto.
         elim n0; apply in_or_app; simpl; now auto.
       apply Permutation_sym; apply Permutation_cons_app; apply Permutation_refl.
       apply Permutation_sym; apply Permutation_cons_app; apply Permutation_refl.
     intros.
      apply in_app_or in H; destruct H as [? | [? | ?]].
       apply IAA'.
        apply in_or_app; left; now auto.
       apply ain_or_app; right; left; subst; now apply eq_area_refl.
       apply IAA'.
        apply in_or_app; right; right; now auto.
     intros.
      apply IA'A in H.
       apply ain_or_app; simpl; apply ain_app_or in H; destruct H as [? | [? | ?]].
        now auto.
        right; left; apply trans_area with c; now auto.
        now auto.
Qed.



Lemma RIn_length :
  forall C1 C2,
   DistinctC C1 ->
   DistinctC C2 ->
   (forall c, In c C1 -> RIn c C2) ->
   (forall c, In c C2 -> RIn c C1) ->
    length C1 = length C2.
induction C1; intros; simpl in *.
 destruct C2.
  now auto.
  elim H2 with c.
   left; now auto.
 assert (RIn a C2) by auto.
  apply rin_in in H3.
   destruct H3 as [a' [? ?]].
    apply in_split in H4; destruct H4 as [C3 [C4 ?]]; subst.
     rewrite app_length; simpl.
      rewrite <- plus_n_Sm; f_equal.
       rewrite <- app_length; apply IHC1.
        unfold DistinctC in *; intros.
         intro; apply H with c.
          right; now auto.
          simpl.
           destruct eq_listline_dec with c a.
            apply in_rin; now auto.
            right; now auto.
        unfold DistinctC in *; intros.
         intro; apply H0 with c.
          apply in_app_or in H4; apply in_or_app; simpl; destruct H4; now auto.
          clear -H4 H5; induction C3.
           simpl in *; destruct eq_listline_dec with c a'.
            apply in_rin; now auto.
            right; now auto.
           simpl in *.
            destruct eq_listline_dec with c a.
             apply rin_or_app; simpl; apply rin_app_or in H5; destruct H5; now auto.
             destruct H4.
              elim n; now auto.
              destruct H5.
               left; now auto.
               right; apply IHC3; now auto.
        intros.
         assert (RIn c (C3 ++ a' :: C4)) by auto.
          apply rin_or_app; apply rin_app_or in H5; destruct H5 as [? | [? | ?]]; auto.
           elim H with c.
            right; now auto.
            simpl; destruct eq_listline_dec with c a.
             apply in_rin; now auto.
             left; apply Rot_trans with a'.
              now auto.
              apply Rot_sym; now auto.
        intros.
         destruct H2 with c.
          apply in_or_app; simpl; apply in_app_or in H4; destruct H4; now auto.
          elim H0 with a'.
           apply in_or_app; simpl; now auto.
           apply rin_setremove_remove.
            apply eq_rin_elem with c.
             apply Rot_trans with a.
              now auto.
              now auto.
             apply in_rin; now auto.
          now auto.
Qed.





Lemma PLCAeq_C :
  forall (P : list Point) (L : list Line) (C C' : list Circuit) (A A' : list Area) (o : Circuit),
    PLCAplanarity_condition P L C A o
    -> PLCAplanarity_condition P L C' A' o
    -> (forall c : Circuit, In c C  -> RIn c C')
    -> (forall c : Circuit, In c C' -> RIn c C)
    -> (forall a : Area, In a A  -> AIn a A')
    -> (forall a : Area, In a A' -> AIn a A)
    -> PLCAequivalence P L C A o P L C' A' o.
intros P L C C' A A' o.
remember (dif_num eq_listline_dec C C').
symmetry in Heqn.
revert C C' A A' o Heqn.
induction n;
   intros C C' A A' o Heqn x x' ICC' IC'C IAA' IA'A.
 assert (Permutation C C').
  apply dif_num_O_perm with eq_listline_dec.
   extractPLCAspec x.
    apply DistinctC_NoDup.
     exact DC.
   extractPLCAspec x'.
    apply DistinctC_NoDup.
     exact DC.
   extractPLCAspec x; extractPLCAspec x'; apply RIn_length; now auto.
   exact Heqn.
  assert (PLCAequivalence P L C A o P L C' A o).
   apply permutePLCA.
    now apply Permutation_refl.
    now apply Permutation_refl.
    exact H.
    now apply Permutation_refl.
   assert (PLCAplanarity_condition P L C' A o).
    exact (PLCAeq_planar x H0).
    apply PLCAtrans with P L C' A o.
     exact H0.
     apply PLCAeq_A.
      exact H1.
      exact x'.
      exact IAA'.
      exact IA'A.
 destruct (dif_num_S_exists eq_listline_dec C C' n) as [c [IC NIC']].
  apply DistinctC_NoDup; extractPLCAspec x; now auto.
  now auto.
  assert (RIn c C') by auto.
   apply rin_in in H.
    destruct H as [c' [Rc IC']].
     assert (PLCAequivalence P L C A o P L (map (replace_circuit c c') C) (map (map (replace_circuit c c')) A) o) as eqMid.
      apply rotateCircuit; auto.
       intro; subst; apply NIC'.
        extractPLCAspec x'; now auto.
      apply PLCAtrans with P L (map (replace_circuit c c') C) (map (map (replace_circuit c c')) A) o; auto.
       assert (x'' := PLCAeq_planar x eqMid); clear eqMid.
        apply in_split in IC; destruct IC as [C1 [C2 ?]]; subst.
         apply in_split in IC'; destruct IC' as [C1' [C2' ?]]; subst.
          assert (exists a, In c a /\ In a A).
           extractPLCAspec x.
            apply CA.
             apply in_or_app; simpl; now auto.
             intro; subst; apply NIC'.
              extractPLCAspec x'; now auto.
           destruct H as [a [Ha HA]].
            apply in_split in HA; apply in_split in Ha.
             destruct HA as [A1 [A2 ?]]; subst.
              destruct Ha as [a1 [a2 ?]]; subst.
               assert (map (map (replace_circuit c c')) (A1 ++ (a1 ++ c :: a2) :: A2) = A1 ++ (a1 ++ c' :: a2) :: A2).
                extractPLCAspec x; apply ReplaceCircuit_A; now auto.
                rewrite H in *; clear H.
                 assert (map (replace_circuit c c') (C1 ++ c :: C2) = C1 ++ c' :: C2).
                  extractPLCAspec x; apply ReplaceCircuit_C; now auto.
                  rewrite H in *; clear H.
                   apply IHn; auto.
                   +rewrite dif_num_permutation_same_l with eq_listline_dec _ (c' :: C1 ++ C2) _.
                     rewrite dif_num_permutation_same_l with eq_listline_dec _ (c :: C1 ++ C2) _ in Heqn.
                      simpl; simpl in Heqn.
                       destruct (in_dec eq_listline_dec c' (C1' ++ c' :: C2')).
                        destruct (in_dec eq_listline_dec c (C1' ++ c' :: C2')).
                         elim NIC'; now auto.
                         inversion Heqn; now auto.
                        elim n0; apply in_or_app; simpl; now auto.
                      apply Permutation_sym, Permutation_cons_app, Permutation_refl.
                     apply Permutation_sym, Permutation_cons_app, Permutation_refl.
                   +intros.
                     apply in_app_or in H; destruct H as [? | [? | ?]].
                      apply ICC'.
                       apply in_or_app; left; now auto.
                      apply rin_or_app; right; left; subst; now constructor.
                      apply ICC'.
                       apply in_or_app; right; right; now auto.
                   +intros.
                     apply IC'C in H.
                      apply rin_or_app; simpl; apply rin_app_or in H; destruct H as [? | [? | ?]].
                       now auto.
                       right; left; apply Rot_trans with c; now auto.
                       now auto.
                   +intros.
                     apply in_app_or in H; destruct H as [? | [? | ?]].
                      apply IAA'.
                       apply in_or_app; left; now auto.
                      subst; apply eq_ain_elem with (a1 ++ c :: a2).
                       apply eq_area_app_cons; now auto.
                       apply IAA'.
                        apply in_or_app; right; left; now auto.
                      apply IAA'.
                       apply in_or_app; right; right; now auto.
                   +intros.
                     apply IA'A in H.
                      apply ain_or_app; simpl; apply ain_app_or in H; destruct H as [? | [? | ?]].
                       now auto.
                       right; left; apply trans_area with (a1 ++ c :: a2).
                        now auto.
                        apply eq_area_app_cons; now auto.
                       now auto.
Qed.




Lemma LIn_length :
  forall L1 L2,
   DistinctL L1 ->
   DistinctL L2 ->
   (forall l, In l L1 -> LIn l L2) ->
   (forall l, In l L2 -> LIn l L1) ->
    length L1 = length L2.
induction L1; intros; simpl in *.
 destruct L2.
  now auto.
  elim H2 with l.
   left; now auto.
 assert (LIn a L2) by auto.
  apply lin_in in H3.
   destruct H3 as [l [? ?]].
    apply in_split in H4; destruct H4 as [L3 [L4 ?]]; subst.
     rewrite app_length; simpl.
      rewrite <- plus_n_Sm; f_equal.
       rewrite <- app_length; apply IHL1.
        unfold DistinctL in *; intros.
         intro; apply H with l0.
          right; now auto.
          simpl.
           destruct eq_dline_dec with l0 a.
            apply in_lin; now auto.
            right; now auto.
        unfold DistinctL in *; intros.
         intro; apply H0 with l0.
          apply in_app_or in H4; apply in_or_app; simpl; destruct H4; now auto.
          clear -H4 H5; induction L3.
           simpl in *; destruct eq_dline_dec with l0 l.
            apply in_lin; now auto.
            right; now auto.
           simpl in *.
            destruct eq_dline_dec with l0 a.
             apply lin_or_app; simpl; apply lin_app_or in H5; destruct H5; now auto.
             destruct H4.
              elim n; now auto.
              destruct H5.
               left; now auto.
               right; apply IHL3; now auto.
        intros.
         assert (LIn l0 (L3 ++ l :: L4)) by auto.
          apply lin_or_app; apply lin_app_or in H5; destruct H5 as [? | [? | ?]]; auto.
           elim H with l0.
            right; now auto.
            simpl; destruct eq_dline_dec with l0 a.
             apply in_lin; now auto.
             left; apply eq_ul_trans with l.
              now auto.
              apply eq_ul_sym; now auto.
        intros.
         destruct H2 with l0.
          apply in_or_app; simpl; apply in_app_or in H4; destruct H4; now auto.
          elim H0 with l.
           apply in_or_app; simpl; now auto.
           apply lin_setremove_remove.
            apply eq_lin_elem with l0.
             apply eq_ul_trans with a.
              now auto.
              now auto.
             apply in_lin; now auto.
          now auto.
Qed.






Lemma PLCAeq_L :
  forall (P : list Point) (L L' : list Line) (C C' : list Circuit) (A A' : list Area) (o : Circuit),
    PLCAplanarity_condition P L C A o
    -> PLCAplanarity_condition P L' C' A' o
    -> (forall l : Line, In l L  -> LIn l L')
    -> (forall l : Line, In l L' -> LIn l L)
    -> (forall c : Circuit, In c C  -> RIn c C')
    -> (forall c : Circuit, In c C' -> RIn c C)
    -> (forall a : Area, In a A  -> AIn a A')
    -> (forall a : Area, In a A' -> AIn a A)
    -> PLCAequivalence P L C A o P L' C' A' o.
intros P L L' C C' A A' o.
remember (dif_num eq_dline_dec L L').
symmetry in Heqn.
revert L L' C C' A A' o Heqn.
induction n;
   intros L L' C C' A A' o Heqn x x' ILL' IL'L ICC' IC'C IAA' IA'A.
 assert (Permutation L L').
  apply dif_num_O_perm with eq_dline_dec.
   extractPLCAspec x.
    apply DistinctL_NoDup.
     exact DL.
   extractPLCAspec x'.
    apply DistinctL_NoDup.
     exact DL.
   extractPLCAspec x; extractPLCAspec x'; apply LIn_length; now auto.
   exact Heqn.
  assert (PLCAequivalence P L C A o P L' C A o).
   apply permutePLCA.
    now apply Permutation_refl.
    exact H.
    now apply Permutation_refl.
    now apply Permutation_refl.
  assert (PLCAplanarity_condition P L' C A o).
   exact (PLCAeq_planar x H0).
  apply PLCAtrans with P L' C A o.
   exact H0.
  apply PLCAeq_C; now auto.
 destruct (dif_num_S_exists eq_dline_dec L L' n) as [l [IL NIL']].
  apply DistinctL_NoDup; extractPLCAspec x; now auto.
  now auto.
  assert (LIn l L') by auto.
   apply lin_in in H.
    destruct H as [l' [ll IL']].
     apply in_split in IL; destruct IL as [L1 [L2 ?]]; subst.
      apply in_split in IL'; destruct IL' as [L1' [L2' ?]]; subst.
  assert (PLCAequivalence P (L1 ++ l :: L2) C A o P (L1 ++ l' :: L2) C A o) as eqMid.
   apply PLCAtrans with P (l :: L1 ++ L2) C A o.
    apply permutePLCA; try (apply Permutation_refl).
     apply Permutation_sym, Permutation_cons_app, Permutation_refl.
    apply PLCAtrans with P (l' :: L1 ++ L2) C A o.
     apply reverseLine.
      now auto.
     apply permutePLCA; try (apply Permutation_refl).
      apply Permutation_cons_app, Permutation_refl.
  apply PLCAtrans with P (L1 ++ l' :: L2) C A o.
   exact eqMid.
   assert (x'' := PLCAeq_planar x eqMid).
    apply IHn; auto.
     rewrite dif_num_permutation_same_l with eq_dline_dec _ (l' :: L1 ++ L2) _.
      rewrite dif_num_permutation_same_l with eq_dline_dec _ (l :: L1 ++ L2) _ in Heqn.
       simpl; simpl in Heqn.
        destruct (in_dec eq_dline_dec l' (L1' ++ l' :: L2')).
         destruct (in_dec eq_dline_dec l (L1' ++ l' :: L2')).
          elim NIL'; now auto.
          inversion Heqn; now auto.
         elim n0; apply in_or_app; simpl; now auto.
       apply Permutation_sym, Permutation_cons_app, Permutation_refl.
       apply Permutation_sym, Permutation_cons_app, Permutation_refl.
     intros.
      apply in_app_or in H; destruct H as [? | [? | ?]].
       apply ILL'.
        apply in_or_app; left; now auto.
       apply lin_or_app; right; left; subst; now constructor.
       apply ILL'.
        apply in_or_app; right; right; now auto.
     intros.
      apply IL'L in H.
       apply lin_or_app; simpl; apply lin_app_or in H; destruct H as [? | [? | ?]].
        now auto.
        right; left; apply eq_ul_trans with l; now auto.
        now auto.
Qed.








Lemma PLCAeq_P :
  forall (P P' : list Point) (L L' : list Line) (C C' : list Circuit) (A A' : list Area) (o : Circuit),
    PLCAplanarity_condition P L C A o
    -> PLCAplanarity_condition P' L' C' A' o
    -> (forall p : Point, In p P  -> In p P')
    -> (forall p : Point, In p P' -> In p P)
    -> (forall l : Line, In l L  -> LIn l L')
    -> (forall l : Line, In l L' -> LIn l L)
    -> (forall c : Circuit, In c C  -> RIn c C')
    -> (forall c : Circuit, In c C' -> RIn c C)
    -> (forall a : Area, In a A  -> AIn a A')
    -> (forall a : Area, In a A' -> AIn a A)
    -> PLCAequivalence P L C A o P' L' C' A' o.
intros P P' L L' C C' A A' o x x' IPP' IP'P ILL' IL'L ICC' IC'C IAA' IA'A.
assert (Permutation P P').
 apply NoDup_Permutation.
  extractPLCAspec x.
   apply DistinctP_NoDup.
    exact DP.
  extractPLCAspec x'.
   apply DistinctP_NoDup.
    exact DP.
  intro p; split.
   now apply IPP'.
   now apply IP'P.
assert (PLCAequivalence P L C A o P' L C A o).
 apply permutePLCA.
  exact H.
  now apply Permutation_refl.
  now apply Permutation_refl.
  now apply Permutation_refl.
assert (PLCAplanarity_condition P' L C A o).
 exact (PLCAeq_planar x H0).
apply PLCAtrans with P' L C A o.
 exact H0.
apply PLCAeq_L; now auto.
Qed.




Theorem PLCAeq :
  forall {P P' : list Point} {L L' : list Line} {C C' : list Circuit} {A A' : list Area} {o o' : Circuit},
    PLCAplanarity_condition P L C A o
    -> PLCAplanarity_condition P' L' C' A' o'
    -> (forall p : Point, In p P  -> In p P')
    -> (forall p : Point, In p P' -> In p P)
    -> (forall l : Line, In l L  -> LIn l L')
    -> (forall l : Line, In l L' -> LIn l L)
    -> (forall c : Circuit, In c C  -> RIn c C')
    -> (forall c : Circuit, In c C' -> RIn c C)
    -> (forall a : Area, In a A  -> AIn a A')
    -> (forall a : Area, In a A' -> AIn a A)
    -> Rot o o'
    -> PLCAequivalence P L C A o P' L' C' A' o'.
intros P P' L L' C C' A A' o o' x x' IPP' IP'P ILL' IL'L ICC' IC'C IAA' IA'A ROT.
assert (PLCAequivalence P L C A o P L (map (replace_circuit o o') C) A o').
 apply rotateOutermost.
 exact ROT.
assert (PLCAplanarity_condition P L (map (replace_circuit o o') C) A o').
 exact (PLCAeq_planar x H).
apply PLCAtrans with P L (map (replace_circuit o o') C) A o'.
 exact H.
apply PLCAeq_P; auto.
 intros; destruct eq_listline_dec with c o'.
  subst; apply eq_rin_elem with o.
   now auto.
   apply ICC'.
    extractPLCAspec x; now auto.
    apply ICC'.
     clear -H1 n; induction C; simpl in *.
      elim H1.
      destruct H1.
       unfold replace_circuit in H.
        destruct eq_listline_dec with o a.
         elim n; now auto.
         now auto.
        right; now auto.
 intros; assert (RIn c C) by auto.
  clear -H2 ROT; induction C; simpl in *.
   elim H2.
   unfold replace_circuit at 1; destruct H2.
    destruct eq_listline_dec with o a.
     left; apply Rot_trans with o; subst; now auto.
     now auto.
    right; now auto.
Qed.








Lemma PLCAeq_outermost :
forall(P P' : list Point)(L L' : list Line)(C C' : list Circuit)(A A' : list Area)(o o' : Circuit),
   PLCAequivalence P L C A o P' L' C' A' o'
-> Rot o o'.
Proof.
intros P P' L L' C C' A A' o o' PLCAEQ.
induction PLCAEQ.
 exact (rotSame _).
 exact (rotSame _).
 exact (rotSame _).
 exact (rotSame _).
 exact H.
 exact (Rot_trans _ _ _ IHPLCAEQ1 IHPLCAEQ2).
Qed.


Lemma PLCAeq_in :
forall(P P' : list Point)(L L' : list Line)(C C' : list Circuit)(A A' : list Area)(o o' : Circuit),
 PLCAequivalence P L C A o P' L' C' A' o'
 -> forall p : Point,
     In p P
     -> In p P'.
Proof.
intros P P' L L' C C' A A' o o' PLCAEQ p INP.
induction PLCAEQ.
 exact (Permutation_in _ H INP).
 exact INP.
 exact INP.
 exact INP.
 exact INP.
 exact (IHPLCAEQ2 (IHPLCAEQ1 INP)).
Qed.


Lemma PLCAeq_in_inv :
forall(P P' : list Point)(L L' : list Line)(C C' : list Circuit)(A A' : list Area)(o o' : Circuit),
 PLCAequivalence P L C A o P' L' C' A' o'
 -> forall p : Point,
     In p P'
     -> In p P.
Proof.
intros P P' L L' C C' A A' o o' PLCAEQ p INP.
induction PLCAEQ.
 exact (Permutation_in _ (Permutation_sym H) INP).
 exact INP.
 exact INP.
 exact INP.
 exact INP.
 exact (IHPLCAEQ1 (IHPLCAEQ2 INP)).
Qed.




Lemma PLCAeq_lin :
forall(P P' : list Point)(L L' : list Line)(C C' : list Circuit)(A A' : list Area)(o o' : Circuit)(l : Line),
 PLCAequivalence P L C A o P' L' C' A' o'
 -> LIn l L
 -> LIn l L'.
Proof.
intros P P' L L' C C' A A' o o' l PLCAEQ LIN.
induction PLCAEQ.
 destruct (LIn_In _ _ LIN) as [LIN1 | LIN1].
 apply (Permutation_in _ H0) in LIN1.
 exact (in_lin _ _ LIN1).
 apply (Permutation_in _ H0) in LIN1.
 apply LIn_reverse_inv.
 exact (in_lin _ _ LIN1).
 simpl in LIN.
 destruct LIN as [LIN | LIN].
 apply (eq_lin_elem _ _ _ (eq_ul_sym _ _ LIN)).
 apply (eq_lin_elem _ _ _ (eq_ul_sym _ _ H)).
 exact (in_lin _ _ (in_eq _ _)).
 exact (lin_cons _ _ _ LIN).
 exact LIN.
 exact LIN.
 exact LIN.
 exact (IHPLCAEQ2 (IHPLCAEQ1 LIN)).
Qed.


Lemma PLCAeq_Lin :
forall(P P' : list Point)(L L' : list Line)(C C' : list Circuit)(A A' : list Area)(o o' : Circuit),
 PLCAequivalence P L C A o P' L' C' A' o'
 -> forall l : Line,
     In l L
     -> LIn l L'.
Proof.
intros.
apply in_lin in H0.
eapply PLCAeq_lin; eauto.
Qed.



Lemma PLCAeq_lin_inv :
forall(P P' : list Point)(L L' : list Line)(C C' : list Circuit)(A A' : list Area)(o o' : Circuit)(l : Line),
 PLCAequivalence P L C A o P' L' C' A' o'
 -> LIn l L'
 -> LIn l L.
intros P P' L L' C C' A A' o o' l PLCAEQ LIN.
induction PLCAEQ.
 destruct (LIn_In _ _ LIN) as [LIN1 | LIN1].
 apply (Permutation_in _ (Permutation_sym H0)) in LIN1.
 exact (in_lin _ _ LIN1).
 apply (Permutation_in _ (Permutation_sym H0)) in LIN1.
 apply LIn_reverse_inv.
 exact (in_lin _ _ LIN1).
 simpl in LIN.
 destruct LIN as [LIN | LIN].
 apply (eq_lin_elem _ _ _ (eq_ul_sym _ _ LIN)).
 apply (eq_lin_elem _ _ _ (eq_ul_sym _ _ (eq_ul_sym _ _ H))).
 exact (in_lin _ _ (in_eq _ _)).
 exact (lin_cons _ _ _ LIN).
 exact LIN.
 exact LIN.
 exact LIN.
 exact (IHPLCAEQ1 (IHPLCAEQ2 LIN)).
Qed.


Lemma PLCAeq_Lin_inv :
forall(P P' : list Point)(L L' : list Line)(C C' : list Circuit)(A A' : list Area)(o o' : Circuit),
 PLCAequivalence P L C A o P' L' C' A' o'
 -> forall l : Line,
     In l L'
     -> LIn l L.
Proof.
intros.
apply in_lin in H0.
eapply PLCAeq_lin_inv; eauto.
Qed.



Lemma PLCAeq_rin :
forall(P P' : list Point)(L L' : list Line)(C C' : list Circuit)(A A' : list Area)(o o' : Circuit)(c : Circuit),
 PLCAequivalence P L C A o P' L' C' A' o'
 -> RIn c C
 -> RIn c C'.
Proof.
intros P P' L L' C C' A A' o o' c PLCAEQ INC.
induction PLCAEQ.
 exact (permutation_rin _ _ _ H1 INC).
 exact INC.
 exact INC.
 clear H.
 induction C; simpl in *.
  exact INC.
  destruct INC as [INC | INC].
   destruct (eq_listline_dec c0 a) as [EQ | NEQ].
    rewrite (Eq_replace_circuit _ _ _ EQ).
    left.
    apply (Rot_trans _ a _ INC).
    rewrite <- EQ.
    exact H1.
    rewrite (Neq_replace_circuit _ _ _ NEQ).
    left; exact INC.
    right; exact (IHC INC).
 induction C.
  exact INC.
  simpl; destruct INC as [INC | INC].
   unfold replace_circuit at 1.
   destruct (eq_listline_dec o a).
    left.
    apply (Rot_trans _ _ _ INC).
    rewrite <- e.
    exact H.
    left; exact INC.
   right; exact (IHC INC).
 exact (IHPLCAEQ2 (IHPLCAEQ1 INC)).
Qed.


Lemma PLCAeq_Cin :
forall(P P' : list Point)(L L' : list Line)(C C' : list Circuit)(A A' : list Area)(o o' : Circuit),
 PLCAequivalence P L C A o P' L' C' A' o'
 -> forall c : Circuit,
     In c C
     -> RIn c C'.
intros.
apply in_rin in H0.
eapply PLCAeq_rin; eauto.
Qed.



Lemma PLCAeq_rin_inv :
forall(P P' : list Point)(L L' : list Line)(C C' : list Circuit)(A A' : list Area)(o o' : Circuit)(c : Circuit),
 PLCAequivalence P L C A o P' L' C' A' o'
 -> RIn c C'
 -> RIn c C.
Proof.
intros P P' L L' C C' A A' o o' c PLCAEQ INC.
induction PLCAEQ.
 exact (permutation_rin _ _ _ (Permutation_sym H1) INC).
 exact INC.
 exact INC.
 clear H.
 induction C; simpl in *.
  exact INC.
  destruct INC as [INC | INC].
   destruct (eq_listline_dec c0 a) as [EQ | NEQ].
    rewrite (Eq_replace_circuit _ _ _ EQ) in INC.
    left.
    apply (Rot_trans _ _ _ INC).
    rewrite EQ in H1.
    exact (Rot_sym _ _ H1).
    rewrite (Neq_replace_circuit _ _ _ NEQ) in INC.
    left; exact INC.
    right; exact (IHC INC).
 induction C.
  exact INC.
  simpl in *.
  destruct INC as [INC | INC].
   unfold replace_circuit in INC.
    destruct eq_listline_dec with o a.
     left; subst; apply Rot_trans with o'.
      exact INC.
      exact (Rot_sym _ _ H).
     left; exact INC.
   right.
    exact (IHC INC).
 exact (IHPLCAEQ1 (IHPLCAEQ2 INC)).
Qed.



Lemma PLCAeq_Cin_inv :
forall(P P' : list Point)(L L' : list Line)(C C' : list Circuit)(A A' : list Area)(o o' : Circuit),
 PLCAequivalence P L C A o P' L' C' A' o'
 -> forall c : Circuit,
     In c C'
     -> RIn c C.
Proof.
intros.
apply in_rin in H0.
eapply PLCAeq_rin_inv; eauto.
Qed.



Lemma PLCAeq_ain :
forall(P P' : list Point)(L L' : list Line)(C C' : list Circuit)(A A' : list Area)(o o' : Circuit)(a : Area),
 PLCAequivalence P L C A o P' L' C' A' o'
 -> AIn a A
 -> AIn a A'.
Proof.
intros P P' L L' C C' A A' o o' a PLCAEQ INA.
induction PLCAEQ.
 exact (permutation_ain _ _ _ H2 INA).
 exact INA.
 simpl in INA. 
 destruct INA as [INA | INA].
 simpl.
 left; apply (trans_area _ a0 _ INA). 
 apply permute_area.
 exact H.
 right; exact INA.
 clear H.
 induction A; simpl in *.
  exact INA.
  destruct INA as [INA | INA].
   left.
   clear IHA.
   apply (trans_area _ a0 _ INA).
   exact (replace_circuit_eq_area _ _ _ H1).
 right; exact (IHA INA).
 exact INA.
 exact (IHPLCAEQ2 (IHPLCAEQ1 INA)).
Qed.


Lemma PLCAeq_Ain :
forall(P P' : list Point)(L L' : list Line)(C C' : list Circuit)(A A' : list Area)(o o' : Circuit),
 PLCAequivalence P L C A o P' L' C' A' o'
 -> forall a : Area,
     In a A
     -> AIn a A'.
intros.
apply in_ain in H0.
eapply PLCAeq_ain; eauto.
Qed.


Lemma PLCAeq_ain_inv :
forall(P P' : list Point)(L L' : list Line)(C C' : list Circuit)(A A' : list Area)(o o' : Circuit)(a : Area),
 PLCAequivalence P L C A o P' L' C' A' o'
 -> AIn a A'
 -> AIn a A.
Proof.
intros P P' L L' C C' A A' o o' a PLCAEQ INA.
induction PLCAEQ.
 exact (permutation_ain _ _ _ (Permutation_sym H2) INA).
 exact INA.
 simpl in INA.
 destruct INA as [INA | INA].
 simpl.
 left; apply (trans_area _ a' _ INA). 
 apply permute_area in H.
 exact (eq_area_sym _ _ H).
 right; exact INA.
 clear H.
 induction A; simpl in *.
  exact INA.
  destruct INA as [INA | INA].
   left.
   clear IHA.
   apply (trans_area _ (map (replace_circuit c c') a0) _ INA).
   apply eq_area_sym.
   exact (replace_circuit_eq_area _ _ _ H1).
 right; exact (IHA INA).
 exact INA.
 exact (IHPLCAEQ1 (IHPLCAEQ2 INA)).
Qed.



Lemma PLCAeq_Ain_inv :
forall(P P' : list Point)(L L' : list Line)(C C' : list Circuit)(A A' : list Area)(o o' : Circuit),
 PLCAequivalence P L C A o P' L' C' A' o'
 -> forall a : Area,
     In a A'
     -> AIn a A.
intros.
apply in_ain in H0.
eapply PLCAeq_ain_inv; eauto.
Qed.



Theorem PLCAeq_inv :
  forall {P P' : list Point} {L L' : list Line} {C C' : list Circuit} {A A' : list Area} {o o' : Circuit},
    PLCAplanarity_condition P L C A o
    -> PLCAequivalence P L C A o P' L' C' A' o'
    -> (forall p : Point, In p P  -> In p P')
       /\ (forall p : Point, In p P' -> In p P)
       /\ (forall l : Line, In l L  -> LIn l L')
       /\ (forall l : Line, In l L' -> LIn l L)
       /\ (forall c : Circuit, In c C  -> RIn c C')
       /\ (forall c : Circuit, In c C' -> RIn c C)
       /\ (forall a : Area, In a A  -> AIn a A')
       /\ (forall a : Area, In a A' -> AIn a A)
       /\ Rot o o'.
Proof with eauto.
intros.
unfold PLCAplanarity_condition in H.
split_n 8.
+eapply PLCAeq_in...
+eapply PLCAeq_in_inv...
+eapply PLCAeq_Lin...
+eapply PLCAeq_Lin_inv...
+eapply PLCAeq_Cin...
+eapply PLCAeq_Cin_inv...
+eapply PLCAeq_Ain...
+eapply PLCAeq_Ain_inv...
+eapply PLCAeq_outermost...
Qed.


Lemma PLCAeq_sym :
forall {P P' : list Point} {L L' : list Line} {C C' : list Circuit} {A A' : list Area} {o o' : Circuit},
 PLCAplanarity_condition P L C A o
 -> PLCAequivalence P  L  C  A  o  P' L' C' A' o'
 -> PLCAequivalence P' L' C' A' o' P  L  C  A  o.
Proof.
intros.
 assert (H' := PLCAeq_planar H H0).
 apply PLCAeq_inv in H0.
 apply PLCAeq; intuition.
 apply Rot_sym; now auto.
 now auto.
Qed.

