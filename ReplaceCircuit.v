Require Import List Arith Permutation.
Require Import EqIn Rotate SetRemove PLCAobject.
Require Import PLCAconstraints PLCAconsistency.




Definition replace_circuit(x y z : Circuit) : Circuit :=
 match eq_listline_dec x z with
| left  _  => y
| right _  => z
 end.


Lemma Eq_replace_circuit :
forall(x y z : Circuit),
   x = z
-> replace_circuit x y z = y.
Proof.
intros x y z H.
unfold replace_circuit.
destruct (eq_listline_dec x z) as [XZ | NXZ]; [ | elim (NXZ H) ].
reflexivity.
Qed.


Lemma Neq_replace_circuit :
forall(x y z : Circuit),
   x <> z
-> replace_circuit x y z = z.
Proof.
intros x y z NEQ.
unfold replace_circuit.
destruct (eq_listline_dec x z) as [XZ | NXZ]; [ elim (NEQ XZ) | ].
reflexivity.
Qed.


Lemma replace_circuit_eq_area :
forall(c1 c2 : Circuit)(a : Area),
   Rot c1 c2
-> eq_area a (map (replace_circuit c1 c2) a).
Proof.
intros c1 c2 a ROT.
induction a; simpl.
 exact (eq_area_refl _).
 unfold replace_circuit.
 destruct (eq_listline_dec c1 a) as [RCA | NRCA].
  apply (trans_area _ (c2 :: a0)).
  apply rotate_c_area.
  subst.
  exact ROT.
  subst.
  exact (eq_area_cons_rot _ _ _ _ (rotSame _) IHa).
  exact (eq_area_cons_rot _ _ _ _ (rotSame _) IHa).
Qed.



Lemma replace_circuit_eq :
forall(c c' : Circuit)(a : Area),
   ~In c a
-> map (replace_circuit c c') a = a.
Proof.
intros c c' a NIN.
induction a; simpl in *.
 exact (eq_refl _).
 destruct (eq_listline_dec c a).
  subst; elim (NIN (or_introl _ (eq_refl _))).
  rewrite (Neq_replace_circuit _ _ _ n).
  f_equal.
  apply IHa.
  intros D; apply NIN; right; exact D.
Qed.


Lemma replace_circuit_rin :
forall(c c' : Circuit)(C : list Circuit),
   In c C
-> In c' (map (replace_circuit c c') C).
Proof.
intros c c' C INC.
induction C; simpl.
 exact INC.
 simpl in INC.
 destruct INC as [INC | INC].
  symmetry in INC; rewrite (Eq_replace_circuit _ _ _ INC).
  left; exact (eq_refl _).
  right; exact (IHC INC).
Qed.



Lemma replace_circuit_not_in :
forall(c c' c'' : Circuit)(C : list Circuit),
   ~In c C
-> In c'' C
-> In c'' (map (replace_circuit c c') C).
Proof.
intros c c' c'' C IN INC.
apply in_split in INC.
destruct INC as [l1 [l2 INC]]; subst.
rewrite map_app.
apply in_or_app.
right; left.
unfold replace_circuit.
destruct eq_listline_dec.
 subst; elim IN.
  apply in_or_app.
  right; now apply in_eq.
 reflexivity.
Qed.



Lemma replace_circuit_not_ain :
forall(c c' : Circuit)(a : Area)(A : list Area),
   In a A
-> ~In c a
-> In a (map (map (replace_circuit c c')) A).
Proof.
intros c c' a A INA NIN.
induction A; simpl in *.
 exact INA.
 destruct INA as [INA | INA]; subst.
  left; exact (replace_circuit_eq _ _ _ NIN).
  right; exact (IHA INA).
Qed.





(* about map and NoDup *)
Lemma DistinctP_NoDup : forall (P : list Point),
  DistinctP P -> NoDup P.
induction P; unfold DistinctP; intros.
 constructor.
 constructor.
  intro Ia; apply (H a).
   now apply in_eq.
   simpl.
    destruct eq_point_dec with a a.
     exact Ia.
     now apply in_eq.
  apply IHP.
   unfold DistinctP.
    intros p H0 H1.
     apply H with p.
      right; exact H0.
      simpl; destruct eq_point_dec with p a.
       apply set_remove_in in H1; exact H1.
       right; exact H1.
Qed.


Lemma DistinctL_NoDup : forall (L : list Line),
  DistinctL L -> NoDup L.
unfold DistinctL; induction L; intros; constructor.
 intro Ia; apply (H a).
  now apply in_eq.
  simpl.
   destruct eq_dline_dec with a a.
    apply in_lin.
     exact Ia.
    left.
     constructor.
  apply IHL.
   intros l H0 H1.
    apply H with l.
     right; exact H0.
     simpl; destruct eq_dline_dec with l a.
      apply in_lin; exact H0.
      right; exact H1.
Qed.


Lemma DistinctC_NoDup : forall (C : list Circuit),
  DistinctC C -> NoDup C.
unfold DistinctC; induction C; intros; constructor.
 intro Ia; apply (H a).
  now apply in_eq.
  simpl.
   destruct eq_listline_dec with a a.
    apply in_rin.
     exact Ia.
    left.
     constructor.
  apply IHC.
   intros c H0 H1.
    apply H with c.
     right; exact H0.
     simpl; destruct eq_listline_dec with c a.
      apply in_rin; exact H0.
      right; exact H1.
Qed.


Lemma DistinctA_NoDup : forall (A : list Area),
  DistinctA A -> NoDup A.
unfold DistinctA; induction A; intros; constructor.
 intro Ia; apply (H a).
  now apply in_eq.
  simpl.
   destruct eq_listcircuit_dec with a a.
    apply in_ain.
     exact Ia.
    left.
     now apply eq_area_refl.
  apply IHA.
   intros a0 H0 H1.
    apply H with a0.
     right; exact H0.
     simpl; destruct eq_listcircuit_dec with a0 a.
      apply in_ain; exact H0.
      right; exact H1.
Qed.



(* refine "map replace_circuit" *)
Lemma ReplaceCircuit_C :
  forall (C1 C2 : list Circuit) (c1 c2 : Circuit),
    DistinctC (C1 ++ c1 :: C2) -> (map (replace_circuit c1 c2) (C1 ++ c1 :: C2)) = C1 ++ c2 :: C2.
intros C1 C2 c1 c2 H; apply DistinctC_NoDup in H; induction C1.
 inversion H.
  clear -H2 H3; simpl.
   unfold replace_circuit at 1.
    destruct eq_listline_dec with c1 c1.
     f_equal.
      induction C2.
       reflexivity.
       simpl; unfold replace_circuit at 1.
        destruct eq_listline_dec with c1 a.
         elim H2; rewrite e0.
          now apply in_eq.
         f_equal; apply IHC2.
          intro H; apply H2; right; exact H.
          inversion H3; auto.
     elim n; reflexivity.
 simpl.
  inversion H.
   clear -H2 H3 IHC1.
    unfold replace_circuit at 1.
     destruct eq_listline_dec with c1 a.
      subst; elim H2.
       apply in_or_app; right; now apply in_eq.
      f_equal; apply IHC1.
       exact H3.
Qed.



(* refine "map map replace_circuit" *)
Lemma ReplaceCircuit_A :
  forall (A1 A2 : list Area) (cl cr : list Circuit) (c1 c2 : Circuit),
   DistinctA (A1 ++ (cl ++ c1 :: cr) :: A2)
   -> Areaconsistency (A1 ++ (cl ++ c1 :: cr) :: A2)
   -> Aconstraint (A1 ++ (cl ++ c1 :: cr) :: A2)
   -> map (map (replace_circuit c1 c2)) (A1 ++ (cl ++ c1 :: cr) :: A2)
       = A1 ++ (cl ++ c2 :: cr) :: A2.
intros.
apply DistinctA_NoDup in H.
unfold Aconstraint in H1.
unfold Area_constraints in H1.
assert (forall c : Circuit, In c (cl ++ c1 :: cr)
        -> ~ RIn c (set_remove eq_listline_dec c (cl ++ c1 :: cr))).
 destruct (H1 (cl ++ c1 :: cr)) as [_ [H2 _]].
  apply in_or_app; right; now apply in_eq.
 exact H2.
clear H1.
induction A1.
 simpl in *.
 rewrite (ReplaceCircuit_C _ _ _ _ H2).
 f_equal.
 inversion H.
 clear -H0 H4.
 induction A2.
  reflexivity.
  simpl.
  destruct (in_dec eq_listline_dec c1 a).
   elim H4.
   left.
   unfold Areaconsistency in H0.
   apply H0 with c1.
     right; now apply in_eq.
    now apply in_eq.
   exact i.
  apply in_or_app; right; now apply in_eq.
 rewrite (replace_circuit_eq _ c2 _ n).
 f_equal.
 apply IHA2.
  unfold Areaconsistency in *.
  intros c a1 a2 H H1 H2 H3.
  apply H0 with c.
     simpl in H.
     destruct H as [H | H].
      left; exact H.
      right; right; exact H.
    destruct H1 as [H1 | H1].
     left; exact H1.
     right; right; exact H1.
   exact H2.
  exact H3.
 intro H; apply H4.
 right; exact H.
inversion H.
clear -IHA1 H4 H5 H0.
destruct (in_dec eq_listline_dec c1 a).
 elim H4.
 apply in_or_app; right; left.
 unfold Areaconsistency in H0.
 apply H0 with c1.
    apply in_or_app; right; now apply in_eq.
   now apply in_eq.
  apply in_or_app; right; now apply in_eq.
 exact i.
simpl.
rewrite (replace_circuit_eq _ c2 _ n).
f_equal.
apply IHA1.
 exact H5.
 unfold Areaconsistency in *.
 intros c a1 a2 H H1 H2 H3.
 apply H0 with c.
   simpl in H.
   right; exact H.
  right; exact H1.
 exact H2.
exact H3.
Qed.



Lemma eq_area_rot :
 forall (a1 a2 : list Circuit) (c1 c2 : Circuit),
  Rot c1 c2 -> eq_area (a1 ++ c1 :: a2) (a1 ++ c2 :: a2).
intros.
apply trans_area with (c1 :: a1 ++ a2).
 apply permute_area.
 apply Permutation_sym.
 apply Permutation_cons_app.
 apply Permutation_refl.
apply trans_area with (c2 :: a1 ++ a2).
 apply rotate_c_area; auto.
 apply permute_area.
 apply Permutation_cons_app.
 apply Permutation_refl.
Qed.
