Require Import List Arith Omega.
Require Import EqIn PLCAobject.


(* trail (terminal point) (terminal point) (points of trail) (lines of trail) *)
Inductive trail : Point -> Point -> list Point -> list Line -> Prop :=
| nil_trail  : forall(p : Point), trail p p (p::nil) nil
| step_trail : forall(p1 p2 p3 : Point)(pl : list Point)(ll : list Line),
   trail p2 p1 pl ll
-> p3 <> p2
-> ~LIn (p3, p2) ll
-> trail p3 p1 (p3::pl) ((p3, p2)::ll).

Hint Constructors trail.


Lemma trail_head :
forall{x y : Point}{pl : list Point}{ll : list Line},
 trail x y pl ll -> x = fst (hd (y, y) ll).
Proof.
intros x y pl ll pph.
inversion pph; simpl in *; auto.
Qed.


Lemma trail_tail :
forall{x y : Point}{pl : list Point}{ll : list Line},
 trail x y pl ll -> y = snd (last ll (x, x)).
Proof.
intros.
revert x y pl H.
induction ll; intros.
 inversion H; subst; simpl; auto.
 inversion H; subst; simpl in *.
 apply IHll in H2.
 clear -H2.
 induction ll; simpl in *; auto.
 destruct ll; simpl in *; auto.
Qed.


Lemma trail_tail' :
forall{x y z1 z2 : Point}{pl1 pl2 : list Point}{ll1 ll2 : list Line},
 ll2 <> nil ->
   trail x z1 pl1 (ll1 ++ ll2) ->
   trail y z2 pl2 ll2 ->
   z1 = z2.
Proof.
intros.
revert x y pl1 H0 H1; induction ll1; intros.
simpl in H0.
apply trail_tail in H0; apply trail_tail in H1.
induction ll2.
 elim H; auto.
 simpl in *.
  destruct ll2.
   rewrite H0; rewrite H1; auto.
   apply IHll2; auto.
    intro H2; inversion H2.
simpl in H0; inversion H0; subst.
apply IHll1 with p2 y pl; auto.
Qed.



Lemma trail_start_pl :
forall{s e : Point}{pl : list Point}{ll : list Line},
 trail s e pl ll -> In s pl.
Proof.
intros s e pl ll ph.
destruct ph; exact (in_eq _ _).
Qed.


Lemma trail_end_pl :
forall{s e : Point}{pl : list Point}{ll : list Line},
 trail s e pl ll -> In e pl.
Proof.
intros s e pl ll ph.
induction ph; [ exact (in_eq _ _) | exact (in_cons _ _ _ IHph) ].
Qed.


Lemma reverse_step_trail :
forall{p1 p2 p3 : Point}{pl : list Point}{ll : list Line},
 trail p2 p1 pl ll -> ~LIn (p1, p3) ll -> p1 <> p3 -> trail p2 p3 (pl++(p3::nil)) (ll++((p1, p3)::nil)).
Proof.
intros.
induction H; intros.
 simpl in *.
 apply step_trail; auto.
 simpl; apply step_trail; [ | exact H2 | ].
 apply IHtrail; auto.
 intros D; apply H0.
 exact (eqin_cons eq_ul D).
 intros D.
 apply lin_app_or in D.
 destruct D as [D | D]; [ elimtype False; exact (H3 D) | ].
 simpl in D.
 destruct D as [D | D]; [ | exact D].
 inversion D; subst.
 apply H0.
 exact (lin_eq _ _ _ (ul_refl _)).
 inversion H4; subst.
 apply H0; simpl.
 left; exact (reverse_ul _ _ (eq_refl _)).
Qed.


Lemma trail_ll_pl :
forall{p1 p2 : Point}{pl : list Point}{ll : list Line}(tl : trail p1 p2 pl ll)(p : Point)(l : Line),
 InPL p l -> In l ll -> In p pl.
Proof.
intros.
induction tl.
 right; exact H0.
 simpl in H0; destruct H0; subst.
 destruct H; simpl in H; subst.
 left; exact (eq_refl _).
 right; exact (trail_start_pl tl).
 right; exact (IHtl H0).
Qed.


Lemma rev_trail :
forall{x y : Point}{pl : list Point}{ll : list Line},
 trail x y pl ll -> trail y x (rev pl) (reverse_linelist ll).
Proof.
intros x y pl ll tl.
unfold reverse_linelist.
induction tl; simpl.
 exact (nil_trail _).
 apply (reverse_step_trail IHtl).
 intros D.
 simpl in D.
 apply H0.
 destruct (LIn_In _ _ D) as [D1 | D2].
  rewrite <- in_rev, map_reverse in D1.
  unfold reverse in D1; simpl in D1.
  exact (in_lin _ _ D1).
  rewrite <- in_rev, map_reverse, reverse_reverse in D2.
  exact (LIn_reverse _ _ (in_lin _ _ D2)).
  simpl.
  exact (not_eq_sym H).
Qed.


Lemma trail_pl_ll :
forall{x y p : Point}{pl : list Point}{ll : list Line},
   trail x y pl ll
-> 1 <= length ll
-> In p pl
-> exists l : Line, InPL p l /\ In l ll.
Proof.
intros x y p pl ll tl lg PL.
destruct ll.
 elimtype False.
 simpl in lg; omega.
inversion tl; subst.
simpl in PL; destruct PL; subst.
exists (p, p2); split; [ auto | exact (in_eq _ _) ].
clear -H1 H.
induction H1; simpl in *.
 destruct H; [ subst | contradiction ].
 exists (x, p); auto.
 destruct H; [ subst | ].
 exists (x, p); auto.
 apply IHtrail in H.
 destruct H as [l [PL LL]].
 destruct LL; [ subst | ].
 unfold InPL in PL; simpl in PL.
 destruct PL; subst. 
 exists (p, p3); auto.
 exists (p3, p); auto.
 exists l; auto.
Qed.



Lemma app_trail :
forall{p1 p2 p3 p4 : Point}{pl1 pl2 : list Point}{ll1 ll2 : list Line},
   trail p1 p2 pl1 ll1
-> trail p3 p4 pl2 ll2
-> (forall(l : Line), LIn l ll1 -> ~LIn l ll2)
-> p2 <> p3
-> ~LIn (p2, p3) ll1
-> ~LIn (p2, p3) ll2
-> trail p1 p4 (pl1++pl2) (ll1 ++ (p2, p3) :: ll2).
Proof.
intros p1 p2 p3 p4 pl1 pl2 ll1 ll2 tl1 tl2 F NEQ NL1 NL2.
induction tl1; simpl.
 apply (step_trail _ _ _ _ _ tl2 NEQ NL2).
 apply step_trail; [ | exact H | ].
 apply IHtl1; [ | exact NEQ | | exact NL2 ].
 intros.
 exact (F _ (eqin_cons eq_ul H1)).
 intros D.
 exact (NL1 (eqin_cons eq_ul D)).
 intros D.
 destruct (lin_app_or _ _ _ D) as [D1 | D1]; [ exact (H0 D1) | ].
 simpl in D1.
 destruct D1 as [D1 | D1].
 inversion D1; subst.
 apply NL1; simpl; left.
 exact (ul_refl _).
 rewrite <- H1 in NL1.
 apply NL1; simpl; left.
  exact (eq_ul_reverse _ _ (ul_refl _)).
  exact (F _ (lin_eq _ _ _ (ul_refl _)) D1).
Qed.



Lemma app_trail' :
forall{p1 p2 p3 : Point}{pl1 pl2 : list Point}{ll1 ll2 : list Line},
   trail p1 p2 pl1 ll1
-> trail p2 p3 (p2 :: pl2) ll2
-> (forall(l : Line), LIn l ll1 -> ~LIn l ll2)
-> trail p1 p3 (pl1++pl2) (ll1 ++ ll2).
Proof.
intros p1 p2 p3 pl1 pl2 ll1 ll2 tl1 tl2 F.
inversion tl2; subst.
 repeat (rewrite app_nil_r); auto.
 apply app_trail; auto.
  intros; intro; apply F with l; simpl; auto.
  intro H; apply F with (p2, p4).
   auto.
   left; constructor.
Qed.



Lemma U_turn_trail :
forall{x : Point}{pl : list Point}{ll : list Line},
   trail x x pl ll
-> 3 <= length ll
-> ll = reverse_linelist ll
-> False.
Proof.
intros x pl ll TL LG LL.
destruct ll.
simpl in LG; omega.
assert(In (reverse l) (reverse_linelist (l :: ll))).
  unfold reverse_linelist.
  simpl.
  apply in_app_iff.
 right; exact (in_eq _ _ ).
rewrite <- LL in H.
simpl in H.
destruct H.
 inversion TL; subst.
 unfold reverse in H; simpl in H.
 inversion H; subst.
 exact (H6 H3).
inversion TL; subst.
  exact (H7 (In_LIn_r _ _ H)).
Qed.



Lemma trail_app_or :
forall{x y : Point}{pl : list Point}{ll1 ll2 : list Line},
   trail x y pl (ll1++ll2)
-> exists z : Point, exists pl1 pl2 : list Point, trail x z pl1 ll1 /\ trail z y pl2 ll2.
Proof.
intros x y pl ll1 ll2 TL.
revert x pl TL.
induction ll1; intros; simpl in *.
 exists x, (x :: nil), pl.
  split.
   constructor.
   auto.
 inversion TL; subst.
 assert(exists (z : Point) (pl1 pl2 : list Point), trail p2 z pl1 ll1 /\ trail z y pl2 ll2).
  exact (IHll1 _ _ H1).
 destruct H as [z [pl1 [pl2 [TL1 TL2]]]].
 exists z; exists (x::pl1); exists pl2.
 split; [ | exact TL2 ].
 apply step_trail.
 exact TL1.
 exact H5.
 intros D; apply H6; apply lin_or_app; left; exact D.
Qed.



Lemma trail_eq_l :
 forall {x y1 y2 : Point} {pl1 pl2 : list Point} {ll : list Line},
  trail x y1 pl1 ll -> trail x y2 pl2 ll -> y1 = y2 /\ pl1 = pl2.
intros.
revert x pl1 pl2 H H0; induction ll; intros.
 inversion H; subst; inversion H0; subst; auto.
 destruct a; inversion H; subst.
 inversion H0; subst.
 destruct IHll with p0 pl pl0; auto.
  subst; auto.
Qed.


Lemma trail_eq_r :
 forall {x1 x2 y1 y2 : Point} {pl1 pl2 : list Point} {ll : list Line},
  trail x1 y1 pl1 ll -> trail x2 y2 pl2 ll -> ll <> nil -> x1 = x2 /\ y1 = y2 /\ pl1 = pl2.
intros.
destruct ll.
 elim H1; auto.
 destruct l; inversion H; subst.
 inversion H0; subst.
  split; auto.
  apply (trail_eq_l H); auto.
Qed.


Lemma trail_line_dup :
 forall {x y : Point} {pl : list Point},
  forall (l : Line) (ll1 ll2 : list Line), trail x y pl (ll1 ++ ll2) -> In l ll1 -> In l ll2 -> False.
intros x y pl l ll1; revert x pl; induction ll1; intros.
 exact H0.
 destruct H0.
  subst; inversion H; subst.
   apply H8; apply in_lin.
    apply in_or_app; now auto.
  inversion H; subst.
   apply IHll1 with p2 pl0 ll2; now auto.
Qed.


Lemma trail_line_dup' :
 forall {x y : Point} {pl : list Point},
  forall (l : Line) (ll1 ll2 : list Line), trail x y pl (ll1 ++ ll2) -> LIn l ll1 -> LIn l ll2 -> False.
intros x y pl l ll1; revert x pl; induction ll1; intros.
 exact H0.
 inversion H; subst.
  destruct H0.
   inversion H0; subst.
    apply H9.
     apply lin_or_app; now auto.
    apply H9.
     rewrite <- H2.
      apply lin_or_app; right; apply LIn_reverse; now auto.
   apply IHll1 with p2 pl0 ll2; now auto.
Qed.


Lemma trail_NoDup :
 forall {x y : Point} {pl : list Point} {ll : list Line},
  trail x y pl ll -> NoDup ll.
intros; induction H; simpl in *; constructor.
 intro H2; apply H1; apply in_lin; now auto.
 now auto.
Qed.


Lemma trail_pl_ll_len :
 forall {x y : Point} {pl : list Point} {ll : list Line},
  trail x y (x :: pl) ll -> length pl = length ll.
intros x y pl ll; revert x y pl; induction ll; intros; inversion H; subst.
 simpl; now auto.
 inversion H5; subst; simpl.
  now auto.
  rewrite IHll with p2 y pl0; now auto.
Qed.


Lemma trail_l_rev_l :
 forall {x y : Point} {pl : list Point} {ll : list Line} (l : Line),
  trail x y pl ll -> In l ll -> ~In (reverse l) ll.
intros x y pl ll; revert x pl; induction ll; intros.
 intro; exact H0.
 intro; destruct H0.
  subst; inversion H; subst.
   destruct H1.
    unfold reverse in H0; simpl in H0.
     inversion H0; subst; apply H7; now auto.
    apply H8; now apply LIn_reverse_inv, in_lin, H0.
  destruct H1.
   destruct l; unfold reverse in H1; simpl in H1; subst; inversion H; subst.
    apply H9; apply LIn_reverse_inv.
     unfold reverse; simpl; now apply in_lin, H0.
   inversion H; subst.
    apply IHll with p2 pl0 l.
     exact H4.
     exact H0.
     exact H1.
Qed.


Lemma trail_split :
 forall {x y s t : Point} {pl : list Point} (ll1 ll2 : list Line),
  trail x y pl (ll1 ++ (s, t) :: ll2)
  -> exists pl1 pl2 : list Point,
       trail x s pl1 ll1 /\ trail t y pl2 ll2.
intros x y s t pl ll1; revert x pl; induction ll1; intros.
 simpl in H; inversion H; subst.
  exists (s::nil), pl0; split.
   constructor.
   exact H6.
 inversion H; subst.
  destruct (IHll1 _ _ _ H2) as [? [? [? ?]]].
   exists (x::x0), x1; split.
    constructor.
     exact H0.
     exact H6.
     intro lin; apply H7; apply lin_or_app; left; exact lin.
    exact H1.
Qed.


Lemma trail_split_disjoint :
 forall {x y : Point} {pl : list Point} (ll1 ll2 : list Line),
  trail x y pl (ll1 ++ ll2) ->
   forall l : Line, LIn l ll1 -> LIn l ll2 -> False.
intros x y pl ll1; revert x pl; induction ll1; intros.
 exact H0.
 inversion H; subst.
  destruct H0.
   apply H9.
    apply eq_lin_elem with l.
     exact H0.
     apply lin_or_app; right; exact H1.
   apply IHll1 with p2 pl0 ll2 l.
    exact H4.
    exact H0.
    exact H1.
Qed.
