Require Import List.
Require Import EqIn Rotate PLCAobject PLCAtrail PLCApath.
Require Import PLCAconstraints.



(*            MergeArea Relation              *)
(*             Merge(A = S A')                *)
(* (P, L, C, A, o) ----> (P', L', C', A', o') *)
(* arg : MA_Relation P L C A o P' L' C' A' o' *)
(* merge c1 and c2, ph is shared lines in c1, c2  *)
Definition InPC (p : Point) (c : Circuit) : Prop := exists l : Line, In l c /\ InPL p l.

Import ListNotations.

Inductive MA_Relation : list Point -> list Line -> list Circuit -> list Area -> Circuit ->
                        list Point -> list Line -> list Circuit -> list Area -> Circuit -> Prop :=
| ma_addloop : forall (P : list Point) (L : list Line) (C : list Circuit) (A : list Area) (o : Circuit)
               (x y : Point) (ap : list Point) (al : list Line) (a : Area),
               simplepath x y ap al ->
                 MA_Relation (ap ++ P) (((y, x) :: al) ++ L) (((y, x) :: al) :: (reverse_linelist ((y, x) :: al)) :: C) ([(y, x) :: al] :: (reverse_linelist ((y, x) :: al) :: a) :: A) o
                             P L C (a :: A) o

| ma_inpath : forall(P : list Point)(L : list Line)(C : list Circuit)(A : list Area)(o : Circuit)
             (x y s : Point)(ap pxy pyx : list Point)(al lxy lyx : list Line),
             simplepath x s (x :: ap) al ->
               trail y x (y :: pyx) lyx ->
               trail x y pxy lxy ->
               (forall l : Line, LIn l lxy -> ~LIn l lyx) ->
               3 <= length (lxy ++ lyx) ->
               MA_Relation (ap ++ P) ((s,y)::al++L) ((al ++ (s,y) :: lyx) :: ((y,s) :: reverse_linelist al ++ lxy) :: C) ([al ++ (s,y) :: lyx] :: [(y,s) :: reverse_linelist al ++ lxy] :: A)  o
                           P L ((lyx ++ lxy) :: C) ([lyx ++ lxy] :: A) o

| ma_inloop : forall(P : list Point)(L : list Line)(C : list Circuit)(A : list Area)(o : Circuit)
               (a : Area)(x s : Point)(ap pxx : list Point)(al lxx : list Line),
              simplepath x s (x :: ap) al ->
                trail x x pxx lxx ->
                lxx <> nil ->
                MA_Relation (ap ++ P) ((s,x)::al++L) ((al ++ [(s,x)]) :: ((x,s) :: reverse_linelist al ++ lxx) :: C) ([al++[(s,x)]] :: (((x,s) :: reverse_linelist al ++ lxx) :: a) :: A) o
                            P L (lxx :: C) ((lxx :: a) :: A) o

| ma_outloop : forall(P : list Point)(L : list Line)(C : list Circuit)(A : list Area)(o : Circuit)
               (x s : Point)(ap pxx : list Point)(al lxx : list Line),
              simplepath x s (x :: ap) al ->
                trail x x pxx lxx ->
                lxx <> nil ->
               MA_Relation (ap ++ P) ((s,x)::al++L) ((al ++ [(s,x)]) :: ((x,s) :: reverse_linelist al ++ lxx) :: C) ([al++[(s,x)]] :: A) ((x,s) :: reverse_linelist al ++ lxx)
                           P L (lxx :: C) A lxx.


Lemma MA_Relation_decrease :
 forall (Pl Pr : list Point) (Ll Lr : list Line) (Cl Cr : list Circuit) (Al Ar : list Area) (ol or : Circuit),
  MA_Relation Pl Ll Cl Al ol Pr Lr Cr Ar or ->
   S (length Ar) = length Al.
intros.
inversion H; simpl; auto.
Qed.




Lemma Rot_reverse_linelist_line :
forall(l : Line)(c1 c2 : Circuit),
   Circuit_constraints c1
-> In l c1
-> In (reverse l) c2
-> Rot c1 c2
-> False.
Proof.
intros l c1 c2 CC1 LC1 LC2 ROT.
induction ROT.
 destruct CC1 as [[x [pl TL]] CC2].
 inversion TL; subst.
  exact LC1.
 simpl in LC1, LC2.
 destruct LC1 as [LC1 | LC1]; subst.
 destruct LC2 as [LC2 | LC2].
  inversion LC2; subst.
  exact (H0 (eq_refl _)).
  apply H1, LIn_reverse_inv.
  exact (in_lin _ _ LC2).
 destruct LC2 as [LC2 | LC2].
  rewrite LC2 in H1.
  apply H1, LIn_reverse.
  exact (in_lin _ _ LC1).
 clear -H LC1 LC2.
 induction H.
  exact LC1.
  simpl in LC1, LC2.
  destruct LC1 as [LC1 | LC1]; subst.
  destruct LC2 as [LC2 | LC2].
   inversion LC2; subst.
   exact (H0 H4).
   apply H1, LIn_reverse_inv.
   exact (in_lin _ _ LC2).
  destruct LC2 as [LC2 | LC2].
   rewrite LC2 in H1.
   apply H1, LIn_reverse.
   exact (in_lin _ _ LC1).
  exact (IHtrail LC1 LC2).
  refine (IHROT CC1 LC1 _).
  apply in_app_or in LC2.
  destruct LC2 as [LC2 | LC2].
   right; exact LC2.
  simpl in LC2.
  destruct LC2 as [LC2 | LC2]; [ | elim LC2 ].
   left; exact LC2.
Qed.


