Require Import List Permutation Arith.
Require Import EqIn Rotate SetRemove.

(* 4 objects *)
Definition Point := nat.

Definition Line := prod Point Point.

Definition Circuit := list Line.

Definition Area := list Circuit.


(* reverse function *)
Definition reverse(l : Line) := (snd l, fst l).


(* reverse circuit function *)
Definition reverse_linelist(ll : list Line) := rev (map reverse ll).

Hint Unfold reverse.


Lemma map_reverse :
forall(l : Line)(ll : list Line),
 In l (map reverse ll) <-> In (reverse l) ll.
Proof.
split; intros.
 apply in_map_iff in H; destruct H; destruct H.
 destruct x; destruct l; unfold reverse in *; simpl in *.
 inversion H; subst; auto.
 apply in_map_iff.
 destruct l; unfold reverse in *; simpl in *.
 exists (p0,p); split; auto.
Qed.

Lemma reverse_reverse :
forall(l : Line), reverse (reverse l) = l.
Proof.
intros l.
destruct l.
unfold reverse; simpl.
exact (eq_refl _).
Qed.


Lemma reverse_linelist_involutive :
forall(ll : list Line),
 reverse_linelist (reverse_linelist ll) = ll.
Proof.
intros ll.
unfold reverse_linelist.
induction ll; simpl.
 exact (eq_refl _).
 rewrite map_app, rev_app_distr.
 simpl.
 rewrite reverse_reverse.
 f_equal.
 exact IHll.
Qed.


Lemma reverse_linelist_cons :
forall(l : Line)(ll : list Line),
 reverse_linelist (l :: ll) = reverse_linelist ll ++ (reverse l :: nil).
intros l ll; revert l; induction ll; intros.
simpl; now auto.
unfold reverse_linelist; simpl.
auto.
Qed.



Lemma reverse_linelist_app :
forall(ll1 ll2 : list Line),
 reverse_linelist (ll1 ++ ll2) = reverse_linelist ll2 ++ reverse_linelist ll1.
induction ll1.
 intros; simpl.
  rewrite app_nil_r; now auto.
 intros; simpl.
  rewrite reverse_linelist_cons.
   rewrite IHll1.
   rewrite <- app_assoc; now auto.
Qed.


(* Equality, Decidability *)
(* point decidability(unnecessray) *)
Definition eq_point_dec := eq_nat_dec.

(* directed line decidability *)
Lemma eq_dline_dec :
 forall (x y : Line), { x = y } + { ~x = y }.
Proof.
intros.
decide equality.
 exact (eq_nat_dec b p0).
 exact (eq_nat_dec a p).
Qed.

(* predicate of undirected line equality *)
Inductive eq_ul : Line -> Line -> Prop :=
| ul_refl    : forall(l : Line), eq_ul l l
| reverse_ul : forall(l1 l2 : Line), reverse l1 = l2 -> eq_ul l1 l2.


Lemma eq_ul_sym :
forall(l1 l2 : Line),
 eq_ul l1 l2 -> eq_ul l2 l1.
Proof.
intros l1 l2 UL.
destruct UL; subst.
 exact (ul_refl _).
 apply reverse_ul.
 exact (reverse_reverse _).
Qed.


Lemma eq_ul_trans :
forall(l1 l2 l3 : Line),
 eq_ul l1 l2 -> eq_ul l2 l3 -> eq_ul l1 l3.
Proof.
intros l1 l2 l3 UL1 UL2.
destruct UL1; subst.
 exact UL2.
inversion UL2; subst.
 exact (reverse_ul _ _ (eq_refl _)).
 rewrite reverse_reverse.
 exact (ul_refl _).
Qed.


Lemma eq_ul_reverse :
forall(l1 l2 : Line),
 eq_ul l1 l2 -> eq_ul (reverse l1) l2.
Proof.
intros l1 l2 UL.
destruct UL; subst.
 apply reverse_ul.
 rewrite reverse_reverse.
 exact (eq_refl _).
 exact (ul_refl _).
Qed.


Lemma eq_ul_reverse_inv :
forall(l1 l2 : Line),
 eq_ul (reverse l1) l2 -> eq_ul l1 l2.
Proof.
intros l1 l2 UL.
inversion UL; subst.
 apply reverse_ul.
 exact (eq_refl _).
 rewrite reverse_reverse.
 exact (ul_refl _).
Qed.


(* undirected line decidability *)
Lemma eq_uline_dec :
 forall (x y : Line), { eq_ul x y } + { ~eq_ul x y }.
Proof.
intros.
destruct (eq_dline_dec x y); [ subst | ].
 left.
 exact (ul_refl _).
 destruct (eq_dline_dec (reverse x) y); [ subst | ].
  left.
  exact (reverse_ul x (reverse x) (eq_refl (reverse x))).
  right.
  intros D.
  destruct D; [ subst | ].
   exact (n (eq_refl _)).
   rewrite H in n0.
   exact (n0 (eq_refl _)).
Qed.



(* Circuit decidability(Rotate) *)
Definition eq_circuit_dec := Rot_dec eq_dline_dec.


(* not circuit decidability *)
Lemma eq_listline_dec :
 forall (c1 c2 : Circuit), { c1 = c2 } + { ~c1 = c2 }.
Proof.
intros c1 c2.
decide equality.
 decide equality.
  exact (eq_nat_dec b p0).
  exact (eq_nat_dec a0 p).
Qed.



(* predicate of Area equality *)
Inductive eq_area : Area -> Area -> Prop :=
| permute_area : forall(a1 a2 : Area),
   Permutation a1 a2 -> eq_area a1 a2
| rotate_c_area : forall(a : Area)(c1 c2 : Circuit),
   Rot c1 c2 -> eq_area (c1::a) (c2::a)
| trans_area : forall(a1 a2 a3 : Area),
  eq_area a1 a2 -> eq_area a2 a3 -> eq_area a1 a3.


Lemma eq_area_refl :
forall(a : Area),
 eq_area a a.
Proof.
intros a.
exact (permute_area _ _ (Permutation_refl _)).
Qed.


Lemma eq_area_sym :
forall(a1 a2 : Area),
 eq_area a1 a2 -> eq_area a2 a1.
Proof.
intros a1 a2 EQA.
induction EQA.
 apply permute_area.
 exact (Permutation_sym H).
 apply rotate_c_area.
 exact (Rot_sym _ _ H).
 exact (trans_area _ _ _ IHEQA2 IHEQA1).
Qed.
  

Lemma eq_area_cons_nil :
forall(c : Circuit)(cl : list Circuit),
 ~eq_area (c::cl) nil.
Proof.
intros c cl D.
remember (c :: cl) as a1.
remember nil as a2.
revert c cl Heqa1.
induction D; intros; subst; simpl in *.
 elimtype False.
 exact (Permutation_nil_cons (Permutation_sym H)).
 discriminate.
 destruct a2.
  exact (IHD1 (eq_refl _) c cl (eq_refl _)).
  exact (IHD2 (eq_refl _) c0 a2 (eq_refl _)).
Qed.


Lemma eq_area_nil_cons :
forall(c : Circuit)(cl : list Circuit),
 ~eq_area nil (c::cl).
Proof.
intros c cl D.
exact (eq_area_cons_nil _ _ (eq_area_sym _ _ D)).
Qed.


Lemma eq_area_nil_l :
forall(a : Area),
 eq_area a nil -> a = nil.
Proof.
intros a EQA.
destruct a; [exact (eq_refl _) | ].
elimtype False.
exact (eq_area_cons_nil _ _ EQA).
Qed.


Lemma eq_area_nil_r :
forall(a : Area),
 eq_area nil a -> a = nil.
Proof.
intros a EQA.
exact (eq_area_nil_l _ (eq_area_sym _ _ EQA)).
Qed.


Lemma eq_area_cons :
forall(a1 a2 : Area)(c : Circuit),
 eq_area a1 a2 -> eq_area (c :: a1) (c :: a2).
Proof.
intros a1 a2 c EQA.
induction EQA.
 apply permute_area.
 exact (perm_skip c H).
 apply (trans_area _ (c2 :: c :: a)).
 apply (trans_area _ (c1 :: c :: a)).
 apply permute_area.
 exact (perm_swap _ _ _).
 exact (rotate_c_area _ _ _ H).
 apply permute_area.
 exact (perm_swap _ _ _).
 exact (trans_area _ _ _ IHEQA1 IHEQA2).
Qed.


Lemma eq_area_length :
forall (a1 a2 : Area),
 eq_area a1 a2 -> length a1 = length a2.
intros; induction H.
 apply Permutation_length; now auto.
 simpl; now auto.
 rewrite IHeq_area1; now auto.
Qed.

Import ListNotations.

Lemma eq_area_length_1_inv :
forall (c : Circuit) (a : Area),
 eq_area [c] a -> exists c' : Circuit, Rot c c' /\ a = [c'].
intros; remember [c].
revert c Heql; induction H; intros; subst.
 exists c; split.
  now apply rotSame.
  apply Permutation_length_1_inv; now auto.
 inversion Heql; subst.
  exists c2; split; now auto.
 destruct IHeq_area1 with c as [? [? ?]].
  now auto.
  subst.
   destruct IHeq_area2 with x as [? [? ?]].
    now auto.
    subst.
     exists x0; split.
      apply Rot_trans with x; now auto.
      now auto.
Qed.


Lemma eq_area_length_1 :
forall (c1 c2 : Circuit),
 eq_area [c1] [c2] -> Rot c1 c2.
intros.
apply eq_area_length_1_inv in H.
 destruct H as [? [H H0]]; inversion H0; subst; now auto.
Qed.
 


Lemma eq_area_set_remove_inv :
forall(a1 a2 : Area)(c : Circuit),
   eq_area a1 a2 -> eq_area (set_remove eq_circuit_dec c a1) (set_remove eq_circuit_dec c a2).
Proof.
intros a1 a2 c EQA.
induction EQA.
 induction H; simpl.
  apply permute_area.
  exact (perm_nil _).
  destruct (eq_circuit_dec c x).
   exact (permute_area _ _ H).
   exact (eq_area_cons _ _ _ IHPermutation).
  destruct (eq_circuit_dec c y); destruct (eq_circuit_dec c x).
   apply rotate_c_area.
   exact (Rot_trans _ _ _ (Rot_sym _ _ r0) r).
   apply permute_area.
   exact (Permutation_refl _).
   apply permute_area.
   exact (Permutation_refl _).
   apply permute_area, perm_swap.
   exact (trans_area _ _ _ IHPermutation1 IHPermutation2).
 simpl.
 destruct (eq_circuit_dec c c1); destruct (eq_circuit_dec c c2).
  apply permute_area.
  exact (Permutation_refl _).
  elimtype False; exact (n (Rot_trans _ _ _ r H)).
  elimtype False; exact (n (Rot_trans _ _ _ r (Rot_sym _ _ H))).
  exact(rotate_c_area _ _ _ H).
 exact (trans_area _ _ _ IHEQA1 IHEQA2).
Qed.



Lemma eq_area_cons_rot :
forall(a1 a2 : Area)(c1 c2 : Circuit),
 Rot c1 c2 -> eq_area a1 a2 -> eq_area (c1::a1) (c2::a2).
Proof.
intros a1 a2 c1 c2 ROT EQA.
induction ROT.
 exact (eq_area_cons _ _ _ EQA).
 apply (trans_area _ ((a :: l') :: a2) _ IHROT).
 apply rotate_c_area.
 exact (rotNext _ _ _ (rotSame _)).
Qed.


Lemma eq_area_set_remove :
forall(a : Area)(c : Circuit),
   EqIn Rot c a
-> eq_area a (c :: set_remove eq_circuit_dec c a).
Proof.
intros a c RIN.
induction a; simpl in *.
 elimtype False; exact RIN.
 destruct (eq_circuit_dec c a).
  apply rotate_c_area.
  exact (Rot_sym _ _ r).
  destruct RIN.
   elimtype False; exact (n H).
   apply IHa in H.
   apply (trans_area _ (a :: c :: set_remove eq_circuit_dec c a0)).
   exact (eq_area_cons _ _ _ H).
   apply permute_area.
   exact (perm_swap _ _ _).
Qed.


Lemma eq_area_rin :
forall(a1 a2 : Area)(c : Circuit),
   EqIn Rot c a1
-> eq_area a1 a2
-> EqIn Rot c a2.
Proof.
intros a1 a2 c RIN EQA.
induction EQA.
 exact (Permutation_eqin _ _ _ _ H RIN).
 simpl in *.
 destruct RIN.
  left; exact (Rot_trans _ _ _ H0 H).
  right; exact H0.
 exact (IHEQA2 (IHEQA1 RIN)).
Qed.


Lemma eq_area_app_cons :
 forall (a1 a2 : Area) (c1 c2 : Circuit),
   Rot c1 c2
-> eq_area (a1 ++ c1 :: a2) (a1 ++ c2 :: a2).
intros.
apply trans_area with (c1 :: a1 ++ a2).
 apply permute_area.
  now apply Permutation_sym, Permutation_cons_app, Permutation_refl.
 apply trans_area with (c2 :: a1 ++ a2).
  apply rotate_c_area; now auto.
  apply permute_area.
   now apply Permutation_cons_app, Permutation_refl.
Qed.


 
Lemma eq_area_dec :
 forall (x y : Area), { eq_area x y } + { ~eq_area x y }.
Proof.
induction x.
 induction y.
 left.
 exact (eq_area_refl _).
 right.
 intros D.
 exact (eq_area_nil_cons _ _ D).
 intros y.
 destruct (IHx (set_remove eq_circuit_dec a y)).
  destruct ((eqin_dec Rot eq_circuit_dec) a y).
   left.
   clear IHx.
   apply (eq_area_cons _ _ a) in e.
   apply (trans_area _ (a :: set_remove eq_circuit_dec a y) _ e).
   exact (eq_area_sym _ _ (eq_area_set_remove _ _ e0)).
   right.
   intros D; apply n.
   clear -D.
   apply (eq_area_rin (a :: x)); [ | exact D ].
   simpl.
   left; exact (rotSame _).
   right.
   intros D; apply n.
   clear -D.
   remember (a :: x) as a1.
   revert a x Heqa1.
   induction D; intros; subst.
    apply permute_area, (eq_area_set_remove_inv _ _ a) in H.
    simpl in H.
    destruct (eq_circuit_dec a a).
     exact H.
     elimtype False; exact (n (rotSame _)).
    inversion Heqa1; subst.
    simpl.
    destruct (eq_circuit_dec a0 c2).
     exact (eq_area_refl _).
     elimtype False; exact (n H).
     apply (trans_area _ _ (set_remove eq_circuit_dec a a3) (IHD1 a x (eq_refl _))).
    exact (eq_area_set_remove_inv _ _ _ D2).
Qed.


(* not area decidability *)
Lemma eq_listcircuit_dec :
forall(a1 a2 : Area),
 { a1 = a2 } + { a1 <> a2 }.
Proof.
intros a1 a2.
decide equality.
exact (eq_listline_dec a c).
Qed.



(* relation between Point to Line  *)
Definition InPL(p : Point)(l : Line) : Prop := fst l = p \/ snd l = p.

Hint Unfold InPL.

Lemma InPL_fst :
forall(l : Line), InPL (fst l) l.
Proof.
intros l.
destruct l; simpl; exact (or_introl _ (eq_refl _)).
Qed.


Lemma InPL_snd :
forall(l : Line), InPL (snd l) l.
Proof.
intros l.
destruct l; simpl; exact (or_intror _ (eq_refl _)).
Qed.


Lemma InPL_left :
forall(x y : Point), InPL x (x, y).
Proof.
intros x y.
exact (or_introl _ (eq_refl _)).
Qed.


Lemma InPL_right :
forall(x y : Point), InPL y (x, y).
Proof.
intros x y.
exact (or_intror _ (eq_refl _)).
Qed.


Lemma InPL_reverse :
forall(x : Point)(l : Line),
 InPL x l -> InPL x (reverse l).
Proof.
intros x l PL.
destruct l as [lx ly].
unfold InPL in *; simpl in *.
destruct PL as [PL | PL]; [ right; exact PL | left; exact PL ].
Qed.


Lemma InPL_reverse_inv :
forall(x : Point)(l : Line),
 InPL x (reverse l) -> InPL x l.
Proof.
intros x l PL.
apply InPL_reverse in PL.
rewrite reverse_reverse in PL.
exact PL.
Qed.


Lemma InPL_eq_ul :
forall(x : Point)(l l' : Line),
 eq_ul l l' -> InPL x l -> InPL x l'.
intros.
inversion H; subst.
exact H0.
now apply InPL_reverse, H0.
Qed.


Definition LIn (l : Line)(L : list Line) : Prop := EqIn eq_ul l L.

(* EqIn property *)
Definition lin_nil         := @eqin_nil Line eq_ul.
Definition lin_eq          := @eqin_eq Line eq_ul.
Definition lin_cons        := @eqin_cons Line eq_ul.
Definition in_lin          := @In_EqIn Line eq_ul ul_refl.
Definition lin_in          := @EqIn_In Line eq_ul.
Definition permutation_lin := @Permutation_eqin Line eq_ul.
Definition eq_lin_dec      := eqin_dec eq_ul eq_uline_dec.
Definition lin_inv         := @eqin_inv Line eq_ul.
Definition lin_app_or      := @eqin_app_or Line eq_ul.
Definition lin_or_app      := @eqin_or_app Line eq_ul.
Definition lin_app_iff     := @eqin_app_iff Line eq_ul.
Definition eq_lin_elem     := @eq_eqin_elem Line eq_ul eq_ul_sym eq_ul_trans.
(* Set_remove property *)
Definition set_remove_lin        := set_remove_eqin eq_uline_dec.
Definition lin_set_remove        := eqin_set_remove eq_uline_dec eq_ul_sym eq_ul_trans.
Definition lin_set_remove_app_or := eqin_set_remove_app_or eq_uline_dec.
Definition lin_set_remove_permutation := @eqin_set_remove_permutation Line eq_ul eq_uline_dec eq_ul_sym eq_ul_trans.
Definition lin_map_iff := @eqin_map_iff Line eq_ul ul_refl eq_ul_trans.
Definition lin_eq_set_remove_permutation := @eqin_eq_set_remove_permutation Line eq_ul eq_dline_dec.
Definition lin_in_setremove      := @eqin_in_setremove Line eq_ul eq_uline_dec ul_refl eq_ul_sym eq_ul_trans eq_dline_dec.
Definition lin_setremove_remove  := @eqin_setremove_remove Line eq_ul eq_dline_dec.
Definition lin_distinct_setremove := @eqin_distinct_setremove Line eq_ul ul_refl eq_dline_dec.
Definition lin_distinct_setremove' := @eqin_distinct_setremove' Line eq_ul ul_refl eq_dline_dec.
Definition lin_distinct_setremove_eq := @eqin_distinct_setremove_eq Line eq_ul eq_uline_dec ul_refl eq_dline_dec.
Definition set_remove_in_l := @set_remove_in_eq Line eq_ul eq_dline_dec.

Lemma In_LIn_r :
forall(l : Line)(L : list Line),
 In (reverse l) L -> LIn l L.
Proof.
intros l L IL.
induction L; simpl in *.
 exact IL.
 destruct IL; subst.
 left; apply eq_ul_sym, eq_ul_reverse, ul_refl.
 right; exact (IHL H).
Qed.


Lemma LIn_In :
forall(l : Line)(L : list Line),
 LIn l L -> In l L \/ In (reverse l) L.
Proof.
intros l L IL.
induction L; simpl in *.
 left; exact IL.
 destruct IL.
  inversion H; subst.
  left; left; exact (eq_refl _).
  right; left; exact (eq_refl _).
  destruct (IHL H).
   left; right; exact H0.
   right; right; exact H0.
Qed.


Lemma LIn_reverse :
forall(l : Line)(L : list Line),
 LIn l L -> LIn (reverse l) L.
Proof.
intros l L ILL.
induction L; simpl in *.
 exact ILL.
 destruct ILL.
  left; exact (eq_ul_trans _ l _ (eq_ul_reverse l _ (ul_refl l)) H).
  right; exact (IHL H).
Qed.


Lemma LIn_reverse_inv :
forall(l : Line)(L : list Line),
 LIn (reverse l) L -> LIn l L.
Proof.
intros l L IL.
induction L; simpl in *.
 exact IL.
 destruct IL.
  left; refine (eq_ul_trans l (reverse l) a _ H).
  apply eq_ul_sym, eq_ul_reverse, ul_refl.
  right; exact (IHL H).
Qed.


Lemma LIn_reverse_linelist :
forall (l : Line) (L : list Line),
 LIn l L -> LIn l (reverse_linelist L).
induction L; intros.
 elim H.
 rewrite reverse_linelist_cons.
 destruct H.
  apply lin_or_app; right.
   left; apply eq_ul_sym, eq_ul_reverse, eq_ul_sym, H.
  apply lin_or_app; left.
   apply IHL.
    now auto.
Qed.


Lemma LIn_reverse_linelist_inv :
forall (l : Line) (L : list Line),
 LIn l (reverse_linelist L) -> LIn l L.
induction L; intros.
 elim H.
 rewrite reverse_linelist_cons in H.
 apply lin_app_or in H; destruct H.
  right; now auto.
  destruct H as [H | []].
  left; apply eq_ul_sym, eq_ul_reverse_inv, eq_ul_sym.
   now auto.
Qed.



Definition RIn (c : Circuit)(C : list Circuit) := EqIn Rot c C.


(* EqIn property *)
Definition rin_nil         := @eqin_nil Circuit Rot.
Definition rin_eq          := @eqin_eq Circuit Rot.
Definition rin_cons        := @eqin_cons Circuit Rot.
Definition in_rin          := @In_EqIn Circuit Rot rotSame.
Definition rin_in          := @EqIn_In Circuit Rot.
Definition permutation_rin := @Permutation_eqin Circuit Rot.
Definition eq_rin_dec      := eqin_dec Rot eq_circuit_dec.
Definition rin_inv         := @eqin_inv Circuit Rot.
Definition rin_app_or      := @eqin_app_or Circuit Rot.
Definition rin_or_app      := @eqin_or_app Circuit Rot.
Definition rin_app_iff     := @eqin_app_iff Circuit Rot.
Definition eq_rin_elem     := @eq_eqin_elem Circuit Rot Rot_sym Rot_trans.
(* Set_remove property *)
Definition set_remove_rin        := set_remove_eqin eq_circuit_dec.
Definition rin_set_remove        := eqin_set_remove eq_circuit_dec Rot_sym Rot_trans.
Definition rin_set_remove_app_or := eqin_set_remove_app_or eq_circuit_dec.
Definition rin_set_remove_permutation := @eqin_set_remove_permutation Circuit Rot eq_circuit_dec Rot_sym Rot_trans.
Definition rin_map_iff := @eqin_map_iff Circuit Rot rotSame Rot_trans.
Definition rin_eq_set_remove_permutation := @eqin_eq_set_remove_permutation Circuit Rot eq_listline_dec.
Definition rin_in_setremove      := @eqin_in_setremove Circuit Rot eq_circuit_dec rotSame Rot_sym Rot_trans eq_listline_dec.
Definition rin_setremove_remove  := @eqin_setremove_remove Circuit Rot eq_listline_dec.
Definition rin_distinct_setremove := @eqin_distinct_setremove Circuit Rot rotSame eq_listline_dec.
Definition rin_distinct_setremove' := @eqin_distinct_setremove' Circuit Rot rotSame eq_listline_dec.
Definition rin_distinct_setremove_eq := @eqin_distinct_setremove_eq Circuit Rot eq_circuit_dec rotSame eq_listline_dec.
Definition set_remove_in_r := @set_remove_in_eq Circuit Rot eq_listline_dec.


Lemma Not_RIn_Not_In :
forall (a : Circuit) (l : list Circuit),
 ~RIn a l -> ~ In a l.
intros; intro H0.
apply H; apply (In_EqIn Rot rotSame); auto.
Qed. 


Lemma Not_In_elem_Not_Rot :
forall (a : Circuit) (l1 l2 : list Circuit),
 ~ In a l1 -> In a l2 -> ~ Rot l1 l2.
intros.
intro H1.
apply H.
apply Rot_sym in H1; eapply RotIn; eauto.
Qed.



Definition AIn(a : Area)(A : list Area) : Prop := EqIn eq_area a A.


(* EqIn property *)
Definition ain_nil         := @eqin_nil Area eq_area.
Definition ain_eq          := @eqin_eq Area eq_area.
Definition ain_cons        := @eqin_cons Area eq_area.
Definition in_ain          := @In_EqIn Area eq_area eq_area_refl.
Definition ain_in          := @EqIn_In Area eq_area.
Definition permutation_ain := @Permutation_eqin Area eq_area.
Definition eq_ain_dec      := eqin_dec eq_area eq_area_dec.
Definition ain_inv         := @eqin_inv Area eq_area.
Definition ain_app_or      := @eqin_app_or Area eq_area.
Definition ain_or_app      := @eqin_or_app Area eq_area.
Definition ain_app_iff     := @eqin_app_iff Area eq_area.
Definition eq_ain_elem     := @eq_eqin_elem Area eq_area eq_area_sym trans_area.
(* Set_remove property *)
Definition set_remove_ain        := set_remove_eqin eq_area_dec.
Definition ain_set_remove        := eqin_set_remove eq_area_dec eq_area_sym trans_area.
Definition ain_set_remove_app_or := eqin_set_remove_app_or eq_area_dec.
Definition ain_set_remove_permutation := @eqin_set_remove_permutation Area eq_area eq_area_dec eq_area_sym trans_area.
Definition ain_map_iff := @eqin_map_iff Area eq_area eq_area_refl trans_area.
Definition ain_eq_set_remove_permutation := @eqin_eq_set_remove_permutation Area eq_area eq_listcircuit_dec.
Definition ain_in_setremove      := @eqin_in_setremove Area eq_area eq_area_dec eq_area_refl eq_area_sym trans_area eq_listcircuit_dec.
Definition ain_setremove_remove  := @eqin_setremove_remove Area eq_area eq_listcircuit_dec.
Definition ain_distinct_setremove := @eqin_distinct_setremove Area eq_area eq_area_refl eq_listcircuit_dec.
Definition ain_distinct_setremove' := @eqin_distinct_setremove' Area eq_area eq_area_refl eq_listcircuit_dec.
Definition ain_distinct_setremove_eq := @eqin_distinct_setremove_eq Area eq_area eq_area_dec eq_area_refl eq_listcircuit_dec.
Definition set_remove_in_a := @set_remove_in_eq Area eq_area eq_listcircuit_dec.



Definition point_connect(c1 c2 : Circuit) : Prop :=
 exists l1 : Line, exists l2 : Line, exists p : Point,
 In l1 c1 /\ In l2 c2 /\ InPL p l1 /\ InPL p l2.

Definition line_connect(c1 c2 : Circuit) : Prop :=
 exists l : Line, In l c1 /\ In (reverse l) c2.

Lemma lc_only_if_pc :
forall(c1 c2 : Circuit),
 line_connect c1 c2 -> point_connect c1 c2.
Proof.
intros.
unfold point_connect.
unfold line_connect in H.
destruct H as [x [LC LRC]].
exists x; exists (reverse x).
destruct x.
unfold reverse in *; simpl in *.
exists p0.
repeat split; auto.
Qed.


Lemma not_pc_only_if_not_lc_pc :
forall(c1 c2 : Circuit),
 ~point_connect c1 c2 -> ~point_connect c1 c2 /\ ~line_connect c1 c2.
Proof.
intros c1 c2 PC.
split; [ exact PC | ].
intros LC; apply PC.
exact (lc_only_if_pc _ _ LC).
Qed.



Lemma pc_sym :
forall(c1 c2 : Circuit),
 point_connect c1 c2 -> point_connect c2 c1.
Proof.
intros c1 c2 PC.
unfold point_connect in *.
destruct PC as [l1 [l2 [p [LC1 [LC2 [IPL1 IPL2]]]]]].
exists l2; exists l1; exists p.
split; [ exact LC2 | ].
split; [ exact LC1 | ].
split; [ exact IPL2 | exact IPL1 ].
Qed.

Lemma Rot_pc :
forall(c1 c2 c3 : Circuit),
  Rot c1 c2
-> point_connect c1 c3
-> point_connect c2 c3.
Proof.
intros c1 c2 c3 ROT PC.
induction ROT.
 exact PC.
 destruct (IHROT PC) as [l1 [l2 [p [LC1 [LC2 [IPL1 IPL2]]]]]].
 exists l1; exists l2; exists p.
 split.
  simpl in LC1.
  apply in_or_app.
  destruct LC1 as [LC1 | LC1]; [ subst | ].
   right; exact (in_eq _ _).
   left; exact LC1.
 split; [ exact LC2 | ].
 split; [ exact IPL1 | exact IPL2 ].
Qed.
