Require Import List.
Require Import EqIn Rotate SetRemove PLCAobject PLCAtrail PLCApath InductivePLCA.
Require Import PLCAconsistency PLCAplanar IPLCAconsistency.


Lemma IPLCAconnect_add_inpath :
forall{P : list Point}{L : list Line}{C : list Circuit}{A : list Area}{o : Circuit}(i : I P L C A o)
(o1 o2 : object)(c : Circuit)(a : Area)(x y z s e : Point)(ap pxy pyz pzx : list Point)(al lxy lyz lzx : list Line)
(INA : In a A)(IAC : In c a)(ceq : c = lxy ++ lyz ++ lzx),
   DistinctC C
-> DistinctA A
-> PLCAconnect P L C A o1 o2
->
 (PLCAconnect (ap++P) ((y,s)::(e,z)::(al++L))
 ((((s,y)::lyz)++((z,e)::(reverse_linelist al)))::(lxy++((y,s)::al)++((e,z)::lzx))::(set_remove eq_listline_dec c C))
 (((((s,y)::lyz)++((z,e)::(reverse_linelist al)))::nil)::((lxy++((y,s)::al)++((e,z)::lzx))::(set_remove eq_listline_dec c a))::(set_remove eq_listcircuit_dec a A))
 o1 o2
  /\ o1 <> o_circuit c /\ o1 <> o_area a /\ o2 <> o_circuit c /\ o2 <> o_area a)
\/
 (PLCAconnect (ap++P) ((y,s)::(e,z)::(al++L))
 ((((s,y)::lyz)++((z,e)::(reverse_linelist al)))::(lxy++((y,s)::al)++((e,z)::lzx))::(set_remove eq_listline_dec c C))
 (((((s,y)::lyz)++((z,e)::(reverse_linelist al)))::nil)::((lxy++((y,s)::al)++((e,z)::lzx))::(set_remove eq_listline_dec c a))::(set_remove eq_listcircuit_dec a A))
 (o_circuit (lxy++((y,s)::al)++((e,z)::lzx))) o2
  /\ o1 = o_circuit c)
\/
 (PLCAconnect (ap++P) ((y,s)::(e,z)::(al++L))
 ((((s,y)::lyz)++((z,e)::(reverse_linelist al)))::(lxy++((y,s)::al)++((e,z)::lzx))::(set_remove eq_listline_dec c C))
 (((((s,y)::lyz)++((z,e)::(reverse_linelist al)))::nil)::((lxy++((y,s)::al)++((e,z)::lzx))::(set_remove eq_listline_dec c a))::(set_remove eq_listcircuit_dec a A))
 o1 (o_circuit (lxy++((y,s)::al)++((e,z)::lzx)))
  /\ o2 = o_circuit c)             
\/
 (PLCAconnect (ap++P) ((y,s)::(e,z)::(al++L))
 ((((s,y)::lyz)++((z,e)::(reverse_linelist al)))::(lxy++((y,s)::al)++((e,z)::lzx))::(set_remove eq_listline_dec c C))
 (((((s,y)::lyz)++((z,e)::(reverse_linelist al)))::nil)::((lxy++((y,s)::al)++((e,z)::lzx))::(set_remove eq_listline_dec c a))::(set_remove eq_listcircuit_dec a A))
 (o_circuit (lxy++((y,s)::al)++((e,z)::lzx))) (o_circuit (lxy++((y,s)::al)++((e,z)::lzx)))
  /\ o1 = o_circuit c /\ o2 = o_circuit c)
\/
 (PLCAconnect (ap++P) ((y,s)::(e,z)::(al++L))
 ((((s,y)::lyz)++((z,e)::(reverse_linelist al)))::(lxy++((y,s)::al)++((e,z)::lzx))::(set_remove eq_listline_dec c C))
 (((((s,y)::lyz)++((z,e)::(reverse_linelist al)))::nil)::((lxy++((y,s)::al)++((e,z)::lzx))::(set_remove eq_listline_dec c a))::(set_remove eq_listcircuit_dec a A))
 (o_area ((lxy++((y,s)::al)++((e,z)::lzx))::(set_remove eq_listline_dec c a))) o2
  /\ o1 = o_area a)
\/
 (PLCAconnect (ap++P) ((y,s)::(e,z)::(al++L))
 ((((s,y)::lyz)++((z,e)::(reverse_linelist al)))::(lxy++((y,s)::al)++((e,z)::lzx))::(set_remove eq_listline_dec c C))
 (((((s,y)::lyz)++((z,e)::(reverse_linelist al)))::nil)::((lxy++((y,s)::al)++((e,z)::lzx))::(set_remove eq_listline_dec c a))::(set_remove eq_listcircuit_dec a A))
 o1 (o_area ((lxy++((y,s)::al)++((e,z)::lzx))::(set_remove eq_listline_dec c a)))
  /\ o2 = o_area a)
\/
 (PLCAconnect (ap++P) ((y,s)::(e,z)::(al++L))
 ((((s,y)::lyz)++((z,e)::(reverse_linelist al)))::(lxy++((y,s)::al)++((e,z)::lzx))::(set_remove eq_listline_dec c C))
 (((((s,y)::lyz)++((z,e)::(reverse_linelist al)))::nil)::((lxy++((y,s)::al)++((e,z)::lzx))::(set_remove eq_listline_dec c a))::(set_remove eq_listcircuit_dec a A))
 (o_area ((lxy++((y,s)::al)++((e,z)::lzx))::(set_remove eq_listline_dec c a))) (o_area ((lxy++((y,s)::al)++((e,z)::lzx))::(set_remove eq_listline_dec c a)))
  /\ o1 = o_area a /\ o2 = o_area a)
\/
 (PLCAconnect (ap++P) ((y,s)::(e,z)::(al++L))
 ((((s,y)::lyz)++((z,e)::(reverse_linelist al)))::(lxy++((y,s)::al)++((e,z)::lzx))::(set_remove eq_listline_dec c C))
 (((((s,y)::lyz)++((z,e)::(reverse_linelist al)))::nil)::((lxy++((y,s)::al)++((e,z)::lzx))::(set_remove eq_listline_dec c a))::(set_remove eq_listcircuit_dec a A))
 (o_circuit (lxy++((y,s)::al)++((e,z)::lzx))) (o_area ((lxy++((y,s)::al)++((e,z)::lzx))::(set_remove eq_listline_dec c a)))
  /\ o1 = o_circuit c /\ o2 = o_area a)
\/
 (PLCAconnect (ap++P) ((y,s)::(e,z)::(al++L))
 ((((s,y)::lyz)++((z,e)::(reverse_linelist al)))::(lxy++((y,s)::al)++((e,z)::lzx))::(set_remove eq_listline_dec c C))
 (((((s,y)::lyz)++((z,e)::(reverse_linelist al)))::nil)::((lxy++((y,s)::al)++((e,z)::lzx))::(set_remove eq_listline_dec c a))::(set_remove eq_listcircuit_dec a A))
 (o_area ((lxy++((y,s)::al)++((e,z)::lzx))::(set_remove eq_listline_dec c a))) (o_circuit (lxy++((y,s)::al)++((e,z)::lzx)))
  /\ o1 = o_area a /\ o2 = o_circuit c).
Proof.
intros P L C A o i o1 o2 c a x y z s e ap pxy pyz pzx al lxy lyz lzx INA IAC ceq DC DA PC.
induction PC as [ p l INP LIN IPL | | | | ].
 left; split.
  apply PLcon.
   apply in_or_app; right; exact INP.
   right; right; apply in_or_app; right; exact LIN.
   exact IPL.
  now repeat (split; try discriminate).
 destruct (eq_listline_dec c c0) as [EQ | NEQ]; [ subst | ].
  subst.
  do 2 right; left.
  split; [ | exact (eq_refl _) ].
  apply lin_app_or in H1.
  destruct H1.
  apply LCcon.
   right; right; apply in_or_app; right; exact H.
   exact (in_cons _ _ _ (in_eq _ _)).
   apply lin_or_app; left; exact H1.
  apply lin_app_or in H1.
  destruct H1.
  apply (TRANS _ _ _ _ _ (o_circuit (((s, y) :: lyz) ++ (z, e) :: reverse_linelist al))).
  apply LCcon.
   right; right; apply in_or_app; right; exact H.
   exact (in_eq _ _).
   apply lin_or_app; left.
   exact (lin_cons _ _ _ H1).
  apply (TRANS _ _ _ _ _ (o_line (y, s))).
  apply SYMME, LCcon.
   exact (in_eq _ _).
   exact (in_eq _ _).
   apply LIn_reverse_inv.
   exact (in_lin _ _ (in_eq _ _)).
  apply LCcon.
   exact (in_eq _ _).
   exact (in_cons _ _ _ (in_eq _ _)).
   apply lin_or_app; right.
   exact (in_lin _ _ (in_eq _ _)).
  apply LCcon.
   right; right; apply in_or_app; right.
   exact H.
   exact (in_cons _ _ _ (in_eq _ _)).
   apply lin_or_app; right.
   apply lin_or_app; right.
   exact (lin_cons _ _ _ H1).
 destruct (eq_circuit_dec c c0) as [ROT | NROT].
  elimtype False.
  exact (Rot_not_eq _ _ _ DC (InductivePLCAACconsistency i _ _ INA IAC) H0 NEQ ROT).
 left; split.
  apply LCcon.
   right; right; apply in_or_app; right; exact H.
   right; right.
   exact (in_set_remove _ _ _ _ NEQ H0).
   exact H1.
  split; [ discriminate | ].
  split; [ discriminate | ].
  split; [ congruence | discriminate ].
 destruct (eq_listline_dec c c0) as [EQ1 | NEQ1]; [ subst | ].
  subst.
  destruct (eq_circuit_dec (lxy ++ lyz ++ lzx) o) as [ROT | NROT].
  elimtype False.
  apply in_rin, (eq_rin_elem _ _ _ ROT) in IAC.
  exact (InductivePLCAoutermostconsistency2 i _ INA IAC).
  subst.
  assert(a = a0) by exact (InductivePLCAAreaconsistency i _ _ _ INA H0 IAC H1).
  do 7 right; left.
  subst; split.
   apply CAcon.
    exact (in_cons _ _ _ (in_eq _ _)).
    exact (in_cons _ _ _ (in_eq _ _)).
    exact (in_eq _ _).
   split; exact (eq_refl _).
 destruct (eq_circuit_dec c c0) as [ROT | NROT].
  elimtype False.
  exact (Rot_not_eq _ _ _ DC (InductivePLCAACconsistency i _ _ INA IAC) H NEQ1 ROT).
 destruct (eq_listcircuit_dec a a0) as [EQ2 | NEQ2]; [ subst | ].
  do 5 right; left; split.
   apply CAcon.
    right; right; exact (in_set_remove _ _ _ _ NEQ1 H).
    exact (in_cons _ _ _ (in_eq _ _)).
    right; exact (in_set_remove _ _ _ _ NEQ1 H1).
   exact (eq_refl _).
 destruct (eq_area_dec a a0) as [EQA | NEQA].
  elimtype False.
  exact (eq_area_not_eq _ _ _ DA INA H0 NEQ2 EQA).
  left; split.
   apply CAcon; [ | | assumption ].
    right; right; exact (in_set_remove _ _ _ _ NEQ1 H).
    right; right; exact (in_set_remove _ _ _ _ NEQ2 H0).
    repeat (split; try discriminate); now congruence.
 destruct IHPC
as [[IPC [L1 [L2 [L3 L4]]]]
 | [[IPC L1]
 | [[IPC L1]
 | [[IPC [L1 L2]]
 | [[IPC L1]
 | [[IPC L1] 
 | [[IPC [L1 L2]]
 | [[IPC [L1 L2]]
 |  [IPC [L1 L2]] ]]]]]]]].
  left.
   split; [ exact (SYMME _ _ _ _ _ _ IPC) | ].
   split; [ exact L3 | split; [ exact L4 | split; [ exact L1 | exact L2 ]]].
  right; right; left.
   split; [ exact (SYMME _ _ _ _ _ _ IPC) | exact L1 ].
  right; left.
   split; [ exact (SYMME _ _ _ _ _ _ IPC) | exact L1 ].
  do 3 right; left.
   split; [ exact IPC | ].
   split; [ exact L2 | exact L1 ].
  do 5 right; left.
   split; [ exact (SYMME _ _ _ _ _ _ IPC) | exact L1 ].
  do 4 right; left.
   split; [ exact (SYMME _ _ _ _ _ _ IPC) | exact L1 ].
  do 6 right; left.
   split; [ exact (SYMME _ _ _ _ _ _ IPC) | ].
   split; [ exact L2 | exact L1 ].
  do 8 right.
   split; [ exact (SYMME _ _ _ _ _ _ IPC) | ].
   split; [ exact L2 | exact L1 ].
  do 7 right; left.
  split; [ exact (SYMME _ _ _ _ _ _ IPC) | ].
  split; [ exact L2 | exact L1 ].
 destruct IHPC1
as [[IPC1 [L1 [L2 [L3 L4]]]]
 | [[IPC1 L1]
 | [[IPC1 L1]
 | [[IPC1 [L1 L2]]
 | [[IPC1 L1]
 | [[IPC1 L1] 
 | [[IPC1 [L1 L2]]
 | [[IPC1 [L1 L2]]
 |  [IPC1 [L1 L2]] ]]]]]]]];
 destruct IHPC2
as [[IPC2 [R1 [R2 [R3 R4]]]]
 | [[IPC2 R1]
 | [[IPC2 R1]
 | [[IPC2 [R1 R2]]
 | [[IPC2 R1]
 | [[IPC2 R1] 
 | [[IPC2 [R1 R2]]
 | [[IPC2 [R1 R2]]
 |  [IPC2 [R1 R2]] ]]]]]]]].
 left; split.
  exact (TRANS _ _ _ _ _ o2 _ IPC1 IPC2).
  split; [ exact L1 | split; [ exact L2 | split; [ exact R3 | exact R4 ]]].
 elimtype False; exact (L3 R1).
 right; right; left; split.
  exact (TRANS _ _ _ _ _ o2 _ IPC1 IPC2).
  exact R1.
 elimtype False; exact (L3 R1).
 elimtype False; exact (L4 R1).
 do 5 right; left; split.
  exact (TRANS _ _ _ _ _ o2 _ IPC1 IPC2).
  exact R1.
 elimtype False; exact (L4 R1).
 elimtype False; exact (L3 R1).
 elimtype False; exact (L4 R1).
 right; left; split.
  exact (TRANS _ _ _ _ _ o2 _ IPC1 IPC2).
  exact L1.
 right; left; split.
  exact IPC2.
  exact L1.
 do 3 right; left; split.
  exact (TRANS _ _ _ _ _ o2 _ IPC1 IPC2).
  split; [ exact L1 | exact R1 ].
 do 3 right; left; split.
  exact IPC2.
  split; [ exact L1 | exact R2 ].
 right; left; split.
   refine (TRANS _ _ _ _ _ (o_area ((lxy ++ ((y, s) :: al) ++ (e, z) :: lzx) :: set_remove eq_listline_dec c a)) _ _ IPC2).
   apply CAcon.
    exact (in_cons _ _ _ (in_eq _ _)).
    exact (in_cons _ _ _ (in_eq _ _)).
    exact (in_eq _ _).
   exact L1.
 do 7 right; left; split.
  exact (TRANS _ _ _ _ _ o2 _ IPC1 IPC2).
  split; [ exact L1 | exact R1 ].
 do 7 right; left; split.
  apply CAcon.
   exact (in_cons _ _ _ (in_eq _ _)).
   exact (in_cons _ _ _ (in_eq _ _)).
   exact (in_eq _ _).
  split; [ exact L1 | exact R2 ].
 do 7 right; left; split.
  exact IPC2.
  split; [ exact L1 | exact R2 ].
 do 3 right; left; split.
  refine (TRANS _ _ _ _ _ (o_area ((lxy ++ ((y, s) :: al) ++ (e, z) :: lzx) :: set_remove eq_listline_dec c a)) _ _ IPC2).
  apply CAcon.
   exact (in_cons _ _ _ (in_eq _ _)).
   exact (in_cons _ _ _ (in_eq _ _)).
   exact (in_eq _ _).
  split; [ exact L1 | exact R2 ].
 elimtype False; exact (R1 L1).
 subst c.
 assert(EC1:o1 = o_circuit (lxy ++ lyz ++ lzx) \/ o1 <> o_circuit (lxy ++ lyz ++ lzx)).
  destruct o1; auto.
  right; discriminate.
  right; discriminate.
  destruct (eq_listline_dec c (lxy ++ lyz ++ lzx)); subst; auto.
   right; congruence.
   right; discriminate.
 assert(EC2:o3 = o_circuit (lxy ++ lyz ++ lzx) \/ o3 <> o_circuit (lxy ++ lyz ++ lzx)).
  destruct o3; auto.
  right; discriminate.
  right; discriminate.
  destruct (eq_listline_dec c (lxy ++ lyz ++ lzx)); subst; auto.
  right; congruence.
  right; congruence.
 assert(EA1:o1 = o_area a \/ o1 <> o_area a).
  destruct o1; auto.
  right; discriminate.
  right; discriminate.
  right; discriminate.
  destruct (eq_listcircuit_dec a0 a); subst; auto.
  right; congruence.
 assert(EA2:o3 = o_area a \/ o3 <> o_area a).
  destruct o3; auto.
  right; discriminate.
  right; discriminate.
  right; discriminate.
  destruct (eq_listcircuit_dec a0 a); subst; auto.
  right; congruence.
 destruct EC1 as [EC1 | NC1].
  right; left; split.
   exact IPC2.
   exact EC1.
 destruct EC2 as [EC2 | NC2].
  right; right; left; split.
   exact IPC1.
   exact EC2.
 destruct EA1 as [EA1 | NA1].
  do 4 right; left; split.
   refine (TRANS _ _ _ _ _ (o_circuit (lxy ++ ((y, s) :: al) ++ (e, z) :: lzx)) _ _ IPC2).
   apply SYMME, CAcon.
    exact (in_cons _ _ _ (in_eq _ _)).
    exact (in_cons _ _ _ (in_eq _ _)).
    exact (in_eq _ _).
   exact EA1.
 destruct EA2 as [EA2 | NA2].
  do 5 right; left; split.
   apply (TRANS _ _ _ _ _ (o_circuit (lxy ++ ((y, s) :: al) ++ (e, z) :: lzx)) _ IPC1).
   apply CAcon.
    exact (in_cons _ _ _ (in_eq _ _)).
    exact (in_cons _ _ _ (in_eq _ _)).
    exact (in_eq _ _).
   exact EA2.
 left; split.
  exact (TRANS _ _ _ _ _ _ _ IPC1 IPC2).
  split; [ exact NC1 | split; [ exact NA1 | split; [ exact NC2 | exact NA2 ]]].
 do 2 right; left; split.
  exact IPC1.
  exact R1.
 do 2 right; left; split.
  exact IPC1.
  exact R2.
 subst o2; discriminate.
 do 5 right; left; split.
  apply (TRANS _ _ _ _ _ (o_circuit (lxy ++ ((y, s) :: al) ++ (e, z) :: lzx)) _ IPC1).
  apply CAcon.
   exact (in_cons _ _ _ (in_eq _ _)).
   exact (in_cons _ _ _ (in_eq _ _)).
   exact (in_eq _ _).
  exact R1.
 subst o2; discriminate.
 do 5 right; left; split.
  exact (TRANS _ _ _ _ _ (o_circuit (lxy ++ ((y, s) :: al) ++ (e, z) :: lzx)) _ IPC1 IPC2).
  exact R2.
 subst o2; discriminate.
 elimtype False; exact (R1 L2).
 right; left; split.
  exact IPC2.
  exact L1.
 do 3 right; left; split.
  exact IPC1.
  split; [ exact L1 | exact R1 ].
 do 3 right; left; split.
  exact IPC2.
  split; [ exact L1 | exact R2 ].
 subst o2; discriminate.
 do 7 right; left; split.
  apply CAcon.
   exact (in_cons _ _ _ (in_eq _ _)).
   exact (in_cons _ _ _ (in_eq _ _)).
   exact (in_eq _ _).
  split; [ exact L1 | exact R1 ].
 subst o2; discriminate.
 do 7 right; left; split.
  exact IPC2.
  split; [ exact L1 | exact R2 ].
 subst o2; discriminate.
 do 4 right; left; split.
  exact (TRANS _ _ _ _ _ o2 _ IPC1 IPC2). 
  exact L1.
 do 4 right; left; split.
  refine (TRANS _ _ _ _ _ (o_circuit (lxy ++ ((y, s) :: al) ++ (e, z) :: lzx)) _ _ IPC2).
  apply SYMME, CAcon.
   exact (in_cons _ _ _ (in_eq _ _)).
   exact (in_cons _ _ _ (in_eq _ _)).
   exact (in_eq _ _).
  exact L1.
 do 8 right; split.
  exact (TRANS _ _ _ _ _ o2 _ IPC1 IPC2). 
  split; [ exact L1 | exact R1 ].
 do 8 right; split.
  apply SYMME, CAcon.
   exact (in_cons _ _ _ (in_eq _ _)).
   exact (in_cons _ _ _ (in_eq _ _)).
   exact (in_eq _ _).
  split; [exact L1 | exact R2 ].
 do 4 right; left; split.
  exact IPC2.
  exact L1.
 do 6 right; left; split.
  exact (TRANS _ _ _ _ _ o2 _ IPC1 IPC2). 
  split; [ exact L1 | exact R1 ].
 do 6 right; left; split.
  exact IPC2.
  split; [ exact L1 | exact R2 ].
 do 6 right; left; split.
  refine (TRANS _ _ _ _ _ (o_circuit (lxy ++ ((y, s) :: al) ++ (e, z) :: lzx)) _ _ IPC2).
  apply SYMME, CAcon.
   exact (in_cons _ _ _ (in_eq _ _)).
   exact (in_cons _ _ _ (in_eq _ _)).
   exact (in_eq _ _).
  split; [ exact L1 | exact R2 ].
 do 8 right; split.
  exact IPC2.
  split; [ exact L1 | exact R2 ].
 elimtype False; exact (R2 L1).
 subst o2; discriminate.
 do 2 right; left; split.
  apply (TRANS _ _ _ _ _  (o_area ((lxy ++ ((y, s) :: al) ++ (e, z) :: lzx) :: set_remove eq_listline_dec c a)) _ IPC1).
   apply SYMME, CAcon.
    exact (in_cons _ _ _ (in_eq _ _)).
    exact (in_cons _ _ _ (in_eq _ _)).
    exact (in_eq _ _).
  exact R1.
 subst o2; discriminate.
 subst c.
 assert(EC1:o1 = o_circuit (lxy ++ lyz ++ lzx) \/ o1 <> o_circuit (lxy ++ lyz ++ lzx)).
  destruct o1; auto.
  right; discriminate.
  right; discriminate.
  destruct (eq_listline_dec c (lxy ++ lyz ++ lzx)); subst; auto.
   right; congruence.
   right; discriminate.
 assert(EC2:o3 = o_circuit (lxy ++ lyz ++ lzx) \/ o3 <> o_circuit (lxy ++ lyz ++ lzx)).
  destruct o3; auto.
  right; discriminate.
  right; discriminate.
  destruct (eq_listline_dec c (lxy ++ lyz ++ lzx)); subst; auto.
  right; congruence.
  right; congruence.
 assert(EA1:o1 = o_area a \/ o1 <> o_area a).
  destruct o1; auto.
  right; discriminate.
  right; discriminate.
  right; discriminate.
  destruct (eq_listcircuit_dec a0 a); subst; auto.
  right; congruence.
 assert(EA2:o3 = o_area a \/ o3 <> o_area a).
  destruct o3; auto.
  right; discriminate.
  right; discriminate.
  right; discriminate.
  destruct (eq_listcircuit_dec a0 a); subst; auto.
  right; congruence.
 destruct EC1 as [EC1 | NC1].
  right; left; split.
  refine (TRANS _ _ _ _ _  (o_area ((lxy ++ ((y, s) :: al) ++ (e, z) :: lzx) :: set_remove eq_listline_dec (lxy ++ lyz ++ lzx) a)) _ _ IPC2).
  apply CAcon.
   exact (in_cons _ _ _ (in_eq _ _)).
   exact (in_cons _ _ _ (in_eq _ _)).
   exact (in_eq _ _).
  exact EC1.
 destruct EC2 as [EC2 | NC2].
  right; right; left; split.
  apply (TRANS _ _ _ _ _ (o_area ((lxy ++ ((y, s) :: al) ++ (e, z) :: lzx) :: set_remove eq_listline_dec (lxy ++ lyz ++ lzx) a)) _ IPC1).
  apply SYMME, CAcon.
   exact (in_cons _ _ _ (in_eq _ _)).
   exact (in_cons _ _ _ (in_eq _ _)).
   exact (in_eq _ _).
  exact EC2.
 destruct EA1 as [EA1 | NA1].
  do 4 right; left; split.
   exact IPC2.
   exact EA1.
 destruct EA2 as [EA2 | NA2].
  do 5 right; left; split.
   exact IPC1.
   exact EA2.
 left; split.
  exact (TRANS _ _ _ _ _  (o_area ((lxy ++ ((y, s) :: al) ++ (e, z) :: lzx) :: set_remove eq_listline_dec (lxy ++ lyz ++ lzx) a)) _ IPC1 IPC2).
  split; [ exact NC1 | split; [ exact NA1 | split; [ exact NC2 | exact NA2 ]]].
 do 5 right; left; split.
  exact IPC1.
  exact R1.
 do 5 right; left; split.
  exact IPC1.
  exact R2.
 subst o2; discriminate.
 do 2 right; left; split.
  apply (TRANS _ _ _ _ _ (o_area ((lxy ++ ((y, s) :: al) ++ (e, z) :: lzx) :: set_remove eq_listline_dec c a)) _ IPC1).
  apply SYMME, CAcon.
   exact (in_cons _ _ _ (in_eq _ _)). 
   exact (in_cons _ _ _ (in_eq _ _)).
   exact (in_eq _ _).
  exact R2.
 elimtype False; exact (R2 L2).
 subst o2; discriminate.
 do 8 right; split.
  apply SYMME, CAcon.
   exact (in_cons _ _ _ (in_eq _ _)).
   exact (in_cons _ _ _ (in_eq _ _)).
   exact (in_eq _ _).
  split; [ exact L1 | exact R1 ].
 subst o2; discriminate.
 do 4 right; left; split.
  exact IPC2.
  exact L1.
 do 6 right; left; split.
  exact IPC1.
  split; [ exact L1 | exact R1 ].
 do 6 right; left; split.
  exact IPC2.
  split; [ exact L1 | exact R2 ].
 subst o2; discriminate.
 do 8 right; split.
  exact IPC2.
  split; [ exact L1 | exact R2 ].
 elimtype False; exact(R2 L2).
 subst o2; discriminate.
 do 3 right; left; split.
  apply (TRANS _ _ _ _ _ (o_area ((lxy ++ ((y, s) :: al) ++ (e, z) :: lzx) :: set_remove eq_listline_dec c a)) _ IPC1).
  apply SYMME, CAcon.
   exact (in_cons _ _ _ (in_eq _ _)).
   exact (in_cons _ _ _ (in_eq _ _)).
   exact (in_eq _ _).
  split; [ exact L1 | exact R1 ].
 subst o2; discriminate.
 right; left; split.
  exact (TRANS _ _ _ _ _ _ _ IPC1 IPC2).
  exact L1.
 do 7 right; left; split.
  exact IPC1.
  split; [ exact L1 | exact R1 ].
 do 7 right; left; split.
  exact IPC1.
  split; [ exact L1 | exact R2 ].
 subst o2; discriminate.
 do 3 right; left; split.
  exact (TRANS _ _ _ _ _ _ _ IPC1 IPC2).
  split; [ exact L1 | exact R2 ].
 elimtype False; exact (R1 L2).
 do 4 right; left; split.
  exact (TRANS _ _ _ _ _ _ _ IPC1 IPC2).
  exact L1.
 do 8 right; split.
  exact IPC1.
  split; [ exact L1 | exact R1 ].
 do 8 right; split.
  exact IPC1.
  split; [ exact L1 | exact R2 ].
 subst o2; discriminate.
 do 6 right; left; split.
  apply (TRANS _ _ _ _ _ (o_circuit (lxy ++ ((y, s) :: al) ++ (e, z) :: lzx))_ IPC1).
  apply CAcon.
   exact (in_cons _ _ _ (in_eq _ _)).
   exact (in_cons _ _ _ (in_eq _ _)).
   exact (in_eq _ _).
  split; [ exact L1 | exact R1 ].
 subst o2; discriminate.
 do 6 right; left; split.
  exact (TRANS _ _ _ _ _ _ _ IPC1 IPC2).
  split; [ exact L1 | exact R2 ].
 subst o2; discriminate.
Qed.
