Require Import List Permutation.
Require Import EqIn Rotate SetRemove PLCAobject PLCAtrail PLCApath.
Require Import PLCAconstraints PLCAconsistency PLCAplanar.

Require Import MergeArea.

Theorem MAconsistency :
 forall {Pl Pr : list Point} {Ll Lr : list Line} {Cl Cr : list Circuit} {Al Ar : list Area} {ol or : Circuit},
  MA_Relation Pl Ll Cl Al ol Pr Lr Cr Ar or ->
   PLCAconsistency Pl Ll Cl Al ol ->
   PLCAconstraints Ll Cl Al ol ->
    PLCAconsistency Pr Lr Cr Ar or /\
    PLCAconstraints Lr Cr Ar or.
unfold PLCAconsistency;
unfold PLCAconstraints;
intros Pl Pr Ll Lr Cl Cr Al Ar ol or ma consist constraint.
destruct consist as [PL [LP [LC [CL [CA [AC [O1 [O2 [CC [AA [DP [DL [DC DA]]]]]]]]]]]]].
destruct constraint as [LT [CT [AT OT]]].
destruct ma as [P L C A o x y ap al a sp |
                P L C A o x y s ap pxy pyx al lxy lyx sp Hyx Hxy NLin len |
                P L C A o a s x ap pxx al lxx sp Hxx Nnil |
                P L C A o s x ap pxx al lxx sp Hxx Nnil]; split.
(* ma_addloop consistency *)
+repeat split.
 -unfold PLconsistency in *.
  intros p Hp; destruct PL with p.
   apply in_or_app; now auto.
   destruct H as [Hl Hr]; apply in_app_or in Hl; destruct Hl as [[H | H] | H].
   *subst; destruct Hr; simpl in H; subst.
     elim DP with p.
      apply in_or_app; now auto.
      apply path_end_pl, in_split in sp.
       destruct sp as [pl1 [pl2 ?]]; subst.
        rewrite <- app_assoc; simpl.
        apply eq_eqin_in, eqin_setremove_remove.
        apply eq_eqin_in, in_or_app.
        right; apply in_or_app; now auto.
     elim DP with p.
      apply in_or_app; now auto.
      apply path_start_pl, in_split in sp.
       destruct sp as [pl1 [pl2 ?]]; subst.
        rewrite <- app_assoc; simpl.
        apply eq_eqin_in, eqin_setremove_remove.
        apply eq_eqin_in, in_or_app.
        right; apply in_or_app; now auto.
   *elim DP with p.
     apply in_or_app; now auto.
     apply path_ll_pl with p x0 in sp; auto.
      apply in_split in sp; destruct sp as [pl1 [pl2 ?]]; subst.
       rewrite <- app_assoc; simpl.
       apply eq_eqin_in, eqin_setremove_remove.
       apply eq_eqin_in, in_or_app.
       right; apply in_or_app; now auto.
   *exists x0; now auto.
 -unfold LPconsistency in *.
  intros p l Hl Hp; assert (In p (ap ++ P)).
   apply LP with l; auto.
   apply in_or_app; now auto.
   apply in_app_or in H; destruct H; auto.
(* reverse circuitがあるなら、そこからは他に線が現れないはず。だが実際には示せていない。次のma_inpathも同じだろう。 *)
(* inpathの場合は、「外にまるまる含むcircuitがあるなら、その途中からは線が出ない」になる。 *)
admit.
 -admit.
 -admit.
 -admit.
 -admit.
 -admit.
 -admit.
 -admit.
 -admit.
 -admit.
 -admit.
 -admit.
 -admit.
(* ma_addloop constraint *)
+intuition.
 -admit.
 -admit.
 -admit.
(* ma_inpath consistency *)
+repeat split.
 -admit.
 -admit.
 -admit.
 -admit.
 -admit.
 -admit.
 -admit.
 -admit.
 -admit.
 -admit.
 -admit.
 -admit.
 -admit.
 -admit.
(* ma_inpath constraint *)
+intuition.
 -admit.
 -admit.
 -admit.
(* ma_inloop consistency *)
+repeat split.
 -admit.
 -admit.
 -admit.
 -admit.
 -admit.
 -admit.
 -admit.
 -admit.
 -admit.
 -admit.
 -admit.
 -admit.
 -admit.
 -admit.
(* ma_inloop constraint *)
+intuition.
 -admit.
 -admit.
 -admit.
(* ma_outloop consistency *)
+repeat split.
 -admit.
 -admit.
 -admit.
 -admit.
 -admit.
 -admit.
 -admit.
 -admit.
 -admit.
 -admit.
 -admit.
 -admit.
 -admit.
 -admit.
(* ma_outloop constraint *)
+repeat split.
 -admit.
 -admit.
 -admit.
 -admit.
 -admit.
 -admit.
 -admit.
 -admit.
 -admit.
Qed.
