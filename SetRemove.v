Require Import List Permutation Arith.
Require Import EqIn.


Section Set_remove.

Context {A : Type}.
Context {EQ : A -> A -> Prop}. (* equivalence of A *)
Variable eq_dec : forall(x y : A), { EQ x y } + {~EQ x y }.
Variable eq_reflexive  : forall(a : A), EQ a a.
Variable eq_symmetric  : forall(a b : A), EQ a b -> EQ b a.
Variable eq_transitive : forall(a b c : A), EQ a b -> EQ b c -> EQ a c.


Fixpoint set_remove(x : A)(X : list A) : list A :=
 match X with
| nil      => nil
| hd :: tl => if eq_dec x hd then tl else hd :: set_remove x tl
 end.


(* In and set_remove *)
Lemma set_remove_in :
forall(x1 x2 : A)(X : list A),
 In x1 (set_remove x2 X) -> In x1 X.
Proof.
intros x1 x2 X D.
induction X.
 exact D.
 simpl in D.
 destruct (eq_dec x2 a); [ subst | simpl in D ].
  exact(in_cons _ _ _ D).
  destruct D; [ subst | ].
   exact (in_eq _ _).
   exact (in_cons _ _ _ (IHX H)).
Qed.


Lemma in_set_remove :
forall(a b : A)(X : list A),
 ~EQ b a -> In a X -> In a (set_remove b X).
Proof.
intros a b X RAB AX.
induction X; simpl in *.
 exact AX.
 destruct (eq_dec b a0); subst.
  destruct AX; subst; auto.
  elimtype False; exact (RAB e).
  destruct AX; subst; simpl; auto.
Qed.



(* EqIn and set_remove *)
Lemma set_remove_eqin :
forall(a1 a2 : A)(l : list A),
 EqIn EQ a1 (set_remove a2 l) -> EqIn EQ a1 l.
Proof.
intros a1 a2 l RR.
induction l.
 exact RR.
 simpl in RR.
 destruct (eq_dec a2 a).
  exact(eqin_cons EQ RR).
  simpl in *.
  destruct RR.
   left; exact H.
   right; exact (IHl H).
Qed.


Lemma eqin_set_remove :
forall(a1 a2 : A)(l : list A),
 ~EQ a2 a1 -> EqIn EQ a1 l -> EqIn EQ a1 (set_remove a2 l).
Proof.
intros a1 a2 l NROT RIN.
induction l; simpl in *.
 exact RIN.
 destruct (eq_dec a2 a).
  destruct RIN as [RIN1 | RIN2]; [ | exact RIN2 ].
  elimtype False; exact (NROT (eq_transitive _ _ _ e (eq_symmetric _ _ RIN1))).
  destruct RIN; simpl; [ left; exact H | right; exact (IHl H) ].  
Qed.


Lemma eqin_set_remove_app_or :
forall(a b : A)(X Y : list A),
   EqIn EQ a (set_remove b (X ++ Y))
-> EqIn EQ a ((set_remove b X) ++ Y) \/ EqIn EQ a (X ++ (set_remove b Y)).
Proof.
intros a b X Y XY.
induction X; simpl in *.
 right; exact XY.
 destruct (eq_dec b a0).
  left; exact XY.
  simpl in *; destruct XY.
   left; left; exact H.
   destruct (IHX H); auto.
Qed.

    

Lemma eqin_set_remove_permutation :
forall(a b : A)(X Y : list A),
   Permutation X Y
-> EqIn EQ a (set_remove b X)
-> EqIn EQ a (set_remove b Y).
Proof.
intros a b X Y PR IN.
induction PR; simpl in *.
 exact IN.
 destruct (eq_dec b x) as [EQ1 | NEQ1].
  exact (Permutation_eqin _ _ _ _ PR IN).
  simpl in *.
  destruct IN as [IN | IN].
   left; exact IN.
   right; exact (IHPR IN).
  destruct (eq_dec b y) as [EQ1 | NEQ1].
   destruct (eq_dec b x) as [EQ2 | NEQ2].
    simpl in *.
    destruct IN as [IN | IN].
     left.
     apply (eq_transitive _ x _ IN).
     apply (eq_transitive _ b _ (eq_symmetric _ _ EQ2) EQ1).
     right; exact IN.
    exact IN.
   destruct (eq_dec b x) as [EQ2 | NEQ2].
    exact IN.
    simpl in *.
    destruct IN as [IN | [IN | IN]].
     right; left; exact IN.
     left; exact IN.
     right; right; exact IN.
   exact (IHPR2 (IHPR1 IN)).
Qed.


Lemma eqin_map_iff :
forall(f : A -> A)(f_EQ : forall(a b : A), EQ a b -> EQ (f a) (f b) )(a : A)(X : list A),
 EqIn EQ a (map f X) <-> exists b : A, EQ a (f b) /\ EqIn EQ b X.
Proof.
intros f f_EQ a X.
split.
 intros INM.
 induction X; simpl in *.
  elim INM.
  destruct INM as [INM | INM].
  exists a0; split; [ exact INM | left; exact (eq_reflexive _) ].
  destruct (IHX INM) as [b [EQF EQB]].
  exists b; split; [ exact EQF | right; exact EQB ].
 intros INB.
 destruct INB as [b [EQF EQB]].
 induction X; simpl in *.
  exact EQB.
  destruct EQB as [EQB | EQB].
   left; apply (eq_transitive _ (f b)).
   exact EQF.
   apply f_EQ.
   exact EQB.
  right; exact (IHX EQB).
Qed.




End Set_remove.


Section Set_remove'.
(* This section gives some lemmas for EqIn/Set_remove between Leibniz eq and User-defined eq. *)

Context {A : Type}.
Context {EQ : A -> A -> Prop}. (* equivalence on A *)
Variable eq_dec : forall(x y : A), { EQ x y } + {~EQ x y }.
Variable eq_reflexive  : forall(a : A), EQ a a.
Variable eq_symmetric  : forall(a b : A), EQ a b -> EQ b a.
Variable eq_transitive : forall(a b c : A), EQ a b -> EQ b c -> EQ a c.

Variable eq_dec' : forall (x y : A), { x = y } + { x <> y }.



Lemma eqin_eq_set_remove_permutation :
forall(a b : A)(X Y : list A),
   Permutation X Y
-> EqIn EQ a (set_remove eq_dec' b X)
-> EqIn EQ a (set_remove eq_dec' b Y).
Proof.
intros a b X Y PR IN.
induction PR; simpl in *.
 exact IN.
 destruct (eq_dec' b x) as [EQ1 | NEQ1].
  exact (Permutation_eqin _ _ _ _ PR IN).
  simpl in *.
  destruct IN as [IN | IN].
   left; exact IN.
   right; exact (IHPR IN).
  destruct (eq_dec' b y) as [EQ1 | NEQ1].
   destruct (eq_dec' b x) as [EQ2 | NEQ2].
    simpl in *.
    destruct IN as [IN | IN].
     left.
     subst; subst; auto.
     right; exact IN.
    exact IN.
   destruct (eq_dec' b x) as [EQ2 | NEQ2].
    exact IN.
    simpl in *.
    destruct IN as [IN | [IN | IN]].
     right; left; exact IN.
     left; exact IN.
     right; right; exact IN.
   exact (IHPR2 (IHPR1 IN)).
Qed.




Lemma eqin_in_setremove :
forall(a b : A)(X : list A),
 In a (set_remove eq_dec b X) -> EqIn EQ a (set_remove eq_dec' b X).
Proof.
intros a b X IN.
induction X; simpl in *.
 exact IN.
 destruct eq_dec with b a0; subst.
  destruct eq_dec' with b a0.
   exact (In_EqIn _ eq_reflexive _ _ IN).
   destruct eq_dec with a a0.
    left; auto.
    right; apply IHX.
    assert (~EQ a b).
     intro; eauto.
     clear a0 e n n0 IHX.
     induction X.
      exact IN.
      destruct IN.
       simpl; destruct eq_dec with b a0; subst.
        elim H; auto.
        left; auto.
       simpl; destruct eq_dec with b a0; try right; auto.
  destruct eq_dec' with b a0.
   elim n; subst; auto.
   destruct IN.
    left; subst; auto.
    right; auto.
Qed.


Lemma eqin_setremove_remove :
forall (a b : A) (X Y : list A),
 EqIn EQ a (set_remove eq_dec' b (X ++ b :: Y)) <-> EqIn EQ a (X ++ Y).
Proof.
induction X; intros; simpl in *.
 destruct eq_dec' with b b.
  split; auto.
  elim n; auto.
 destruct eq_dec' with b a0.
  subst; split; intro.
   apply eqin_app_or in H; destruct H as [H | [H | H]].
    right; apply eqin_or_app; auto.
    auto.
    right; apply eqin_or_app; auto.
   apply eqin_or_app; destruct H.
    simpl; auto.
    apply eqin_app_or in H; destruct H; simpl; auto.
 split; intro H; destruct H; simpl; auto.
  right; apply IHX; auto.
  right; apply IHX; auto.
Qed.


Lemma eqin_distinct_setremove :
forall (a : A) (X Y : list A),
 ~EqIn EQ a (set_remove eq_dec' a (X ++ a :: Y)) ->
  set_remove eq_dec' a (X ++ a :: Y) = X ++ Y.
Proof.
induction X; intros; simpl in *.
 destruct eq_dec' with a a.
  auto.
  elim n; auto.
 destruct eq_dec' with a a0.
  elim H.
  apply eqin_or_app.
  right; left; auto.
  f_equal.
  apply IHX.
  intro H0.
   apply H.
   right; auto.
Qed.


Lemma eqin_distinct_setremove' :
forall (a : A) (X Y Z : list A),
 ~EqIn EQ a (set_remove eq_dec' a (X ++ a :: Y)) ->
  set_remove eq_dec' a (X ++ a :: Y ++ Z) = X ++ Y ++ Z.
Proof.
induction X; intros; simpl in *.
 destruct eq_dec' with a a.
  auto.
  elim n; auto.
 destruct eq_dec' with a a0.
  elim H.
  apply eqin_or_app.
  right; left; auto.
  f_equal.
  apply IHX.
  intro H0.
   apply H.
   right; auto.
Qed.


Lemma eqin_distinct_setremove_eq :
forall (a : A) (X Y : list A),
 ~EqIn EQ a (set_remove eq_dec' a (X ++ a :: Y)) ->
  set_remove eq_dec a (X ++ a :: Y) = X ++ Y.
Proof.
induction X; intros; simpl in *.
 destruct eq_dec with a a.
  auto.
  elim n; auto.
 destruct eq_dec with a a0.
  elim H.
   destruct eq_dec' with a a0.
    apply eqin_or_app.
     right; left; auto.
    left; now auto.
  f_equal.
  apply IHX.
   intro H0.
    apply H.
     destruct eq_dec' with a a0.
      apply eqin_or_app.
       right; left; auto.
      right; now auto.
Qed.



Lemma eq_eqin_in :
forall (a : A) (X : list A),
  EqIn eq a X <-> In a X.
intros.
induction X; simpl; intuition.
Qed.


Lemma set_remove_in_eq :
forall(x1 x2 : A)(X : list A),
 EqIn EQ x1 (set_remove eq_dec' x2 X) -> EqIn EQ x1 X.
induction X; intro; simpl in *.
 exact H.
 destruct eq_dec' with x2 a.
  right; exact H.
  destruct H.
   left; exact H.
   right; apply IHX.
    exact H.
Qed. 



End Set_remove'.

