Require Import List Arith.
Require Import EqIn Rotate SetRemove PLCAobject.

Section PLCAconsistency.


Variable P : list Point.
Variable L : list Line.
Variable C : list Circuit.
Variable A : list Area.
Variable outermost : Circuit.

(* Definition of consistency *)
Definition PLconsistency : Prop :=
 forall(p : Point), In p P -> (exists l : Line, In l L /\ InPL p l).

Definition LPconsistency : Prop :=
 forall(p : Point)(l : Line), In l L -> InPL p l -> In p P.

Definition LCconsistency : Prop :=
 forall(l : Line), LIn l L -> (exists c : Circuit, In l c /\ In c C).

Definition CLconsistency : Prop :=
 forall(l : Line)(c : Circuit), In c C -> In l c -> LIn l L.

Definition CAconsistency : Prop :=
 forall(c : Circuit), In c C -> c <> outermost -> (exists a : Area, In c a /\ In a A).

Definition ACconsistency : Prop :=
 forall(c : Circuit)(a : Area), In a A -> In c a -> In c C.

Definition Outermostconsistency1 : Prop := In outermost C.

Definition Outermostconsistency2 : Prop := forall(a : Area), In a A -> ~RIn outermost a.

Definition Circuitconsistency : Prop :=
 forall(l : Line)(c1 c2 : Circuit), In c1 C -> In c2 C -> In l c1 -> In l c2 -> c1 = c2.

Definition Areaconsistency : Prop :=
 forall(c : Circuit)(a1 a2 : Area),
 In a1 A -> In a2 A -> In c a1 -> In c a2 -> a1 = a2.

(* unique List (represented set) *)
Definition DistinctP : Prop := forall(p : Point),   In p P -> ~In p  (set_remove eq_point_dec p P).
Definition DistinctL : Prop := forall(l : Line),    In l L -> ~LIn l (set_remove eq_dline_dec l L).
Definition DistinctC : Prop := forall(c : Circuit), In c C -> ~RIn c (set_remove eq_listline_dec c C).
Definition DistinctA : Prop := forall(a : Area),    In a A -> ~AIn a (set_remove eq_listcircuit_dec a A).


Definition PLCAconsistency : Prop :=
 PLconsistency /\ LPconsistency /\
 LCconsistency /\ CLconsistency /\
 CAconsistency /\ ACconsistency /\
 Outermostconsistency1 /\ Outermostconsistency2 /\
 Circuitconsistency /\ Areaconsistency /\
 DistinctP /\ DistinctL /\ DistinctC /\ DistinctA.

End PLCAconsistency.


Lemma CPconsistency :
forall{P : list Point}{L : list Line}{C : list Circuit}(p : Point)(l : Line)(c : Circuit),
   LPconsistency P L
-> CLconsistency L C
-> In c C
-> In l c
-> InPL p l 
-> In p P.
Proof.
intros P L C p l c LP CL IC LC PL.
destruct l; simpl in PL; destruct PL as [peq | peq]; simpl in peq; subst.
 assert (LI := CL _ _ IC LC).
  destruct (LIn_In _ _ LI).
   apply (LP p (p, p1)); auto.
   unfold reverse in H; simpl in H.
    apply (LP p (p1, p)); auto.
 assert (LI := CL _ _ IC LC).
  destruct (LIn_In _ _ LI).
   apply (LP p (p0, p)); auto.
   unfold reverse in H; simpl in H.
    apply (LP p (p, p0)); auto.
Qed.



Lemma Rot_not_eq :
forall(C : list Circuit)(c1 c2 : Circuit),
   DistinctC C
-> In c1 C
-> In c2 C
-> c1 <> c2
-> Rot c1 c2
-> False.
Proof.
intros C c1 c2 DC INC1 INC2 NEQ ROT.
apply (DC c1).
 auto.
 destruct (in_split _ _ INC1) as [ C1 [ C2 H ] ]; subst.
  clear INC1 DC; induction C1.
   simpl in *.
    destruct eq_listline_dec with c1 c1.
     destruct INC2.
      elim NEQ; auto.
      destruct (in_split _ _ H) as [ C3 [ C4 H1 ] ]; subst.
       unfold RIn.
        apply eqin_or_app.
         simpl; auto.
     elim n; auto.
  simpl.
   destruct eq_listline_dec with c1 a.
    apply eqin_or_app.
     right; simpl; left; apply rotSame.
    simpl; destruct eq_listline_dec with c2 a.
     subst; auto.
     right; apply IHC1.
      simpl in INC2; destruct INC2.
       elim n0; symmetry; auto.
       auto.
Qed.


Lemma eq_area_not_eq :
forall(A : list Area)(a1 a2 : Area),
   DistinctA A
-> In a1 A
-> In a2 A
-> a1 <> a2
-> eq_area a1 a2
-> False.
Proof.
intros A a1 a2 DA INA1 INA2 NEQ EQA.
apply (DA a1).
 auto.
 destruct (in_split _ _ INA1) as [ A1 [ A2 H ] ]; subst.
  clear DA INA1; induction A1.
   simpl.
    destruct eq_listcircuit_dec with a1 a1.
     simpl in INA2; destruct INA2.
      elim NEQ; auto.
      destruct (in_split _ _ H) as [ A3 [ A4 H1 ] ]; subst.
       unfold AIn; apply eqin_or_app; simpl; auto.
       elim n; auto.
   simpl.
    destruct eq_listcircuit_dec with a1 a.
     apply eqin_or_app; right; simpl; left.
      apply eq_area_refl.
     simpl in INA2; destruct INA2.
      subst; simpl; auto.
      simpl; right; apply IHA1.
       auto.
Qed.
  

