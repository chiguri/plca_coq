Require Import List Arith.
Require Import EqIn Rotate SetRemove PLCAobject PLCAtrail PLCApath InductivePLCA.
Require Import PLCAconsistency PLCAconstraints PLCAplanar.
Require Import IPLCAconsistency.
Require Import IPLCAconnect_add_inpath IPLCAconnect_add_loop.
Require Import IPLCAconnect_add_inline IPLCAconnect_add_outpath.




Lemma IPLCAconnection :
forall(P : list Point)(L : list Line)(C : list Circuit)(A : list Area)(o : Circuit),
 I P L C A o ->
   (forall(p : Point)(l : Line),   In p P -> In l L -> PLCAconnect P L C A (o_point p) (o_line l))
/\ (forall(l : Line)(c : Circuit), In l L -> In c C -> PLCAconnect P L C A (o_line l) (o_circuit c))
/\ (forall(c : Circuit)(a : Area), In c C -> In a A -> PLCAconnect P L C A (o_circuit c) (o_area a)).
Proof.
intros P L C A o I.
induction I as
[ pl ll x y ph XY lg |
  P L C A o a x y z pxy pyz pzx lxy lyz lzx I IND INA INC tl1 tl2 tl3 F1 F2 lg1 lg2 |
  P L C A o a x y z s e ap pxy pyz pzx al lxy lyz lzx I IND INA INC tl1 tl2 tl3 ph lg1 lg2 NS F |
  P L C A o x y z s e ap pxy pyz pzx al lxy lyz lzx I IND O1 OT tl1 tl2 tl3 ph lg1 lg2 NS F |
  P L C A o a x y ap al I IND INA ph XY lg F ].
(* single_loop *)
+split.
  (* PL single_loop *)
  intros p l INP INL.
  apply (TRANS _ _ _ _ _ (o_circuit ((y, x) :: ll))).
  destruct (path_pl_ll ph (le_Sn_le _ _ lg) INP) as [el [PIL INE]].
  apply (TRANS _ _ _ _ _ (o_line el)).
  apply (PLcon _ _ _ _ _ _ INP); [ right; exact INE | exact PIL ].
  apply LCcon.
   exact (in_cons _ _ _ INE).
   exact (in_eq _ _).
   exact (in_lin _ _ (in_cons _ _ _ INE)).
  apply SYMME, LCcon;[ exact  INL | exact (in_eq _ _) | exact (in_lin _ _ INL) ].
 split.
  (* LC single_loop *)
  intros l c LIN INC.
  simpl in INC.
  destruct INC as [INC | [INC | INC]]; [ subst | subst | elimtype False; exact INC].
  simpl in LIN.
  destruct LIN as [LIN | LIN]; [ subst | ].
   exact (LCcon _ _ _ _ _ _ (in_eq _ _) (in_eq _ _) (in_lin _ _ (in_eq _ _))).
   apply LCcon.
    exact (in_cons _ _ _ LIN).
    exact (in_eq _ _).
    exact (in_lin _ _ (in_cons _ _ _ LIN)).
   simpl in LIN.
   destruct LIN as [LIN | LIN]; [ subst | ].
    apply (TRANS _ _ _ _ _ (o_line (y, x))).
    apply (TRANS _ _ _ _ _ (o_point x)).
    apply SYMME, (PLcon _ _ _ _ _ _ (path_start_pl ph) (in_eq _ _) (InPL_right _ _)).
    refine (PLcon _ _ _ _ _ _ (path_start_pl ph) _ (InPL_right _ _)).
     exact (in_eq _ _).
    apply LCcon.
     exact (in_eq _ _).
     exact (in_cons _ _ _ (in_eq _ _)).
     apply LIn_reverse_inv, in_lin.
     unfold reverse_linelist.
     rewrite <- in_rev, map_reverse.
     exact (in_eq _ _).
    apply (TRANS _ _ _ _ _ (o_line l)).
    destruct l as [lx ly].
    apply (TRANS _ _ _ _ _ (o_point lx)).
    apply SYMME, PLcon.
     exact (path_ll_pl ph lx _ (InPL_left _ _) LIN).
     right; exact LIN.
     exact (InPL_left _ _).
    apply PLcon.
     exact (path_ll_pl ph lx _ (InPL_left _ _) LIN).
     right; exact LIN.
     exact (InPL_left _ _).
    apply LCcon.
     exact (in_cons _ _ _ LIN).
     exact (in_cons _ _ _ (in_eq _ _)).
     apply LIn_reverse_inv, in_lin.
     unfold reverse_linelist.
     rewrite <- in_rev, map_reverse, reverse_reverse.
     exact (in_cons _ _ _ LIN).
  (* CA signle_loop *)
  intros c a INC INA.
  simpl in *.
  destruct INA as [INA | F]; [ subst | elimtype False; exact F].
  destruct INC as [C1 | [C2 | C3]]; [ subst | subst | elimtype False; exact C3].
   apply (CAcon _ _ _ _ _ _ (in_eq _ _) (in_eq _ _) (in_eq _ _)).
   apply (TRANS _ _ _ _ _ (o_line (y, x))).
   apply SYMME, LCcon.
    exact (in_eq _ _).
    exact (in_cons _ _ _ (in_eq _ _)).
    apply LIn_reverse_inv, in_lin.
    unfold reverse_linelist.
    rewrite <- in_rev, map_reverse.
    exact (in_eq _ _).
   apply (TRANS _ _ _ _ _ (o_point x)).
   apply SYMME, PLcon.
    exact (path_start_pl ph).
    exact (in_eq _ _).
    exact (InPL_right _ _).
   apply (TRANS _ _ _ _ _ (o_line (y, x))).
   apply PLcon.
    exact (path_start_pl ph).
    exact (in_eq _ _).
    exact (InPL_right _ _).
   apply (TRANS _ _ _ _ _ (o_circuit ((y, x) :: ll))).
   apply LCcon.
    exact (in_eq _ _).
    exact (in_eq _ _).
    exact (in_lin _ _ (in_eq _ _)).
   apply CAcon.
    exact (in_eq _ _).
    exact (in_eq _ _).
    exact (in_eq _ _).
(* add_inline *)
+destruct IND as [EPL [ELC ECA]].
 destruct (add_pathpoint a x y z pxy pyz pzx lxy lyz lzx I
(InductivePLCAconstraints I) (InductivePLCAconsistency I) INA INC tl1 tl2 tl3) as [IX [IY IZ]].
 split; [ | split].
(* PL add_inline *)
 -intros p l INP LIN.
  destruct LIN as [LIN | LIN]; subst.
  apply SYMME, (TRANS _ _ _ _ _ (o_circuit (lxy ++ (y, z) :: lzx))).
  apply LCcon.
   exact (in_eq _ _).
   exact (in_cons _ _ _ (in_eq _ _)).
   apply in_lin, in_or_app; right.
   exact (in_eq _ _).
  destruct L.
   inversion I.
  assert(PLCAconnect P (l :: L) C A (o_point p) (o_line l)).
   exact (EPL _ _ INP (in_eq _ _)).
  assert(PLCAconnect P (l :: L) C A (o_line l) (o_circuit (lxy ++ lyz ++ lzx))).
   exact (ELC _ _ (in_eq _ _) (InductivePLCAACconsistency I _ _ INA INC)).
  apply (TRANS _ _ _ _ _ _ _ H) in H0.
  apply SYMME in H0.
  destruct (IPLCAconnect_add_inline I _ (o_point p) (lxy ++ lyz ++ lzx)
   a x y z pxy pyz pzx lxy lyz lzx INA INC (eq_refl _)
  (InductivePLCADistinctC I) (InductivePLCADistinctA I) H0) as
  [ [PC DIS] |
  [ [PC DIS] |
  [ [PC DIS] | 
  [ [PC [DIS1 DIS2]] |
  [ [PC DIS] |
  [ [PC DIS] | 
  [ [PC [DIS1 DIS2]] | 
  [ [PC [DIS1 DIS2]] | 
  [PC [DIS1 DIS2]] ]]]]]]]]; try discriminate.
  destruct DIS as [DIS1 DIS2].
  elimtype False; exact (DIS1 (eq_refl _)).
  exact PC.
  assert(PLCAconnect P L C A (o_point p) (o_line l)).
   exact (EPL _ _ INP LIN).
  apply SYMME in H.
  destruct (IPLCAconnect_add_inline I _ (o_point p) (lxy ++ lyz ++ lzx)
   a x y z pxy pyz pzx lxy lyz lzx INA INC (eq_refl _)
  (InductivePLCADistinctC I) (InductivePLCADistinctA I) H) as
  [ [PC DIS] |
  [ [PC DIS] |
  [ [PC DIS] | 
  [ [PC [DIS1 DIS2]] |
  [ [PC DIS] |
  [ [PC DIS] | 
  [ [PC [DIS1 DIS2]] | 
  [ [PC [DIS1 DIS2]] | 
  [PC [DIS1 DIS2]] ]]]]]]]]; try discriminate.
  apply SYMME.
  exact PC.
(* LC add_inline *)
 -intros l c LIN INC'.
  simpl in INC', LIN.
  destruct INC' as [INC' | [INC' | INC']]; destruct LIN as [LIN | LIN]; subst.
   apply LCcon.
    exact (in_eq _ _).
    exact (in_eq _ _).
    apply LIn_reverse_inv.
    exact (in_lin _ _ (in_eq _ _)).
   apply SYMME, (TRANS _ _ _ _ _ (o_line (y, z))).
   apply SYMME, LCcon.
    exact (in_eq _ _).
    exact (in_eq _ _).
    apply LIn_reverse_inv, in_lin.
    exact (in_eq _ _).
   apply (TRANS _ _ _ _ _ (o_circuit (lxy ++ (y, z) :: lzx))).
   apply LCcon.
    exact (in_eq _ _).
    exact (in_cons _ _ _ (in_eq _ _)).
    apply lin_or_app; right.
    exact (in_lin _ _ (in_eq _ _)).
   assert(PLCAconnect P L C A (o_line l) (o_circuit (lxy ++ lyz ++ lzx))).
    exact (ELC _ _ LIN (InductivePLCAACconsistency I _ _ INA INC)).
   destruct (IPLCAconnect_add_inline I _ (o_line l) (lxy ++ lyz ++ lzx)
   a x y z pxy pyz pzx lxy lyz lzx INA INC (eq_refl _)
   (InductivePLCADistinctC I) (InductivePLCADistinctA I) (SYMME _ _ _ _ _ _ H)) as
   [ [PC DIS] |
   [ [PC DIS] |
   [ [PC DIS] | 
   [ [PC [DIS1 DIS2]] |
   [ [PC DIS] |
   [ [PC DIS] | 
   [ [PC [DIS1 DIS2]] | 
   [ [PC [DIS1 DIS2]] | 
   [PC [DIS1 DIS2]] ]]]]]]]]; try discriminate.
   destruct DIS as [DIS1 DIS2].
   elimtype False; exact (DIS1 (eq_refl _)).
   exact PC.
   apply LCcon.
    exact (in_eq _ _).
    exact (in_cons _ _ _ (in_eq _ _)).
    apply lin_or_app; right.
   exact (in_lin _ _ (in_eq _ _)).
   assert(PLCAconnect P L C A (o_line l) (o_circuit (lxy ++ lyz ++ lzx))).
    exact (ELC _ _ LIN (InductivePLCAACconsistency I _ _ INA INC)).
   apply SYMME in H.
   destruct (IPLCAconnect_add_inline I _ (o_line l) (lxy ++ lyz ++ lzx)
   a x y z pxy pyz pzx lxy lyz lzx INA INC (eq_refl _)
   (InductivePLCADistinctC I) (InductivePLCADistinctA I) H) as
   [ [PC DIS] |
   [ [PC DIS] |
   [ [PC DIS] | 
   [ [PC [DIS1 DIS2]] |
   [ [PC DIS] |
   [ [PC DIS] | 
   [ [PC [DIS1 DIS2]] | 
   [ [PC [DIS1 DIS2]] | 
   [PC [DIS1 DIS2]] ]]]]]]]]; try discriminate.
   destruct DIS as [DIS1 DIS2].
   elimtype False; exact (DIS1 (eq_refl _)).
   apply SYMME; exact PC.
   destruct L.
    inversion I.
   destruct (eq_circuit_dec c (lxy ++ lyz ++ lzx)) as [ROT | NROT].
   elimtype False.
   apply (InductivePLCADistinctC I (lxy ++ lyz ++ lzx)).
   exact (InductivePLCAACconsistency I _ _ INA INC).
   apply (eq_rin_elem _ _ _ ROT).
   apply in_rin.
   exact INC'.
   apply set_remove_in in INC'.
   apply (TRANS _ _ _ _ _ (o_point y)).
   apply SYMME, PLcon.
    exact IY.
    exact (in_eq _ _).
    exact (InPL_left _ _).
   assert(PLCAconnect P (l::L) C A (o_point y) (o_circuit c)).
    apply (TRANS _ _ _ _ _ _ _ (EPL _ _ IY (in_eq _ _)) (ELC _ _ (in_eq _ _) INC')).
   destruct (IPLCAconnect_add_inline I (o_point y) (o_circuit c) (lxy ++ lyz ++ lzx)
   a x y z pxy pyz pzx lxy lyz lzx INA INC (eq_refl _)
   (InductivePLCADistinctC I) (InductivePLCADistinctA I) H) as
   [ [PC DIS] |
   [ [PC DIS] |
   [ [PC DIS] | 
   [ [PC [DIS1 DIS2]] |
   [ [PC DIS] |
   [ [PC DIS] | 
   [ [PC [DIS1 DIS2]] | 
   [ [PC [DIS1 DIS2]] | 
   [PC [DIS1 DIS2]] ]]]]]]]]; try discriminate.
   exact PC.
   inversion DIS; subst.
   elimtype False; exact (NROT (rotSame _)).
   destruct (eq_listline_dec c (lxy ++ lyz ++ lzx)) as [EQ | NEQ ]; subst.
   elimtype False.
   apply (InductivePLCADistinctC I _ (set_remove_in _ _ _ _ INC')).
   apply in_rin.
   exact INC'.
   apply set_remove_in in INC'.
   destruct (IPLCAconnect_add_inline I (o_line l) (o_circuit c) (lxy ++ lyz ++ lzx)
   a x y z pxy pyz pzx lxy lyz lzx INA INC (eq_refl _)
   (InductivePLCADistinctC I) (InductivePLCADistinctA I) (ELC _ _ LIN INC')) as
   [ [PC DIS] |
   [ [PC DIS] |
   [ [PC DIS] | 
   [ [PC [DIS1 DIS2]] |
   [ [PC DIS] |
   [ [PC DIS] | 
   [ [PC [DIS1 DIS2]] | 
   [ [PC [DIS1 DIS2]] | 
   [PC [DIS1 DIS2]] ]]]]]]]]; try discriminate.
   exact PC.
   inversion DIS; subst.
   elimtype False.
   exact (NEQ (eq_refl _)).
(* CA add_inline *)
 -intros c a0 INC' IAA.
  simpl in *.
  destruct IAA as [IAA | [IAA | IAA]];
  destruct INC' as [INC' | [INC' | INC']]; subst.
   apply CAcon.
    exact (in_eq _ _).
    exact (in_eq _ _).
    exact (in_eq _ _).
   apply (TRANS _ _ _ _ _ (o_line (y, z))).
   apply SYMME, LCcon.
    exact (in_eq _ _).
    exact (in_cons _ _ _ (in_eq _ _)).
    apply lin_or_app; right.
    exact (in_lin _ _ (in_eq _ _)).
   apply (TRANS _ _ _ _ _ (o_circuit ((z, y) :: lyz))).
   apply LCcon.
    exact (in_eq _ _).
    exact (in_eq _ _).
    apply LIn_reverse_inv, in_lin.
    exact (in_eq _ _).
   apply CAcon.
    exact (in_eq _ _).
    exact (in_eq _ _).
    exact (in_eq _ _).
   destruct (eq_listline_dec c (lxy ++ lyz ++ lzx)) as [EQ | NEQ]; [ subst | ].
   elimtype False.
   apply (InductivePLCADistinctC I _ (set_remove_in _ _ _ _ INC')).
   apply in_rin.
   exact INC'.
   apply set_remove_in in INC'.
   apply SYMME, (TRANS _ _ _ _ _ (o_circuit ((z, y) :: lyz))).
   apply SYMME, CAcon.
    exact (in_eq _ _).
    exact (in_eq _ _).
    exact (in_eq _ _).
   apply (TRANS _ _ _ _ _ (o_line (y, z))).
   apply SYMME, LCcon.
    exact (in_eq _ _).
    exact (in_eq _ _).
    apply LIn_reverse_inv, in_lin.
    exact (in_eq _ _).
   apply (TRANS _ _ _ _ _ (o_point y)).
   apply SYMME, PLcon.
    exact IY.
    exact (in_eq _ _).
    exact (InPL_left _ _).
   destruct L.
    inversion I.
    assert(PLCAconnect P (l :: L) C A (o_point y) (o_circuit c)).
     exact (TRANS _ _ _ _ _ _ _ (EPL _ _ IY (in_eq _ _)) (ELC _ _ (in_eq _ _) INC')).
   destruct (IPLCAconnect_add_inline I (o_point y) (o_circuit c) (lxy ++ lyz ++ lzx)
   a x y z pxy pyz pzx lxy lyz lzx INA INC (eq_refl _)
   (InductivePLCADistinctC I) (InductivePLCADistinctA I) H) as
   [ [PC DIS] |
   [ [PC DIS] |
   [ [PC DIS] | 
   [ [PC [DIS1 DIS2]] |
   [ [PC DIS] |
   [ [PC DIS] | 
   [ [PC [DIS1 DIS2]] | 
   [ [PC [DIS1 DIS2]] | 
   [PC [DIS1 DIS2]] ]]]]]]]]; try discriminate.
   exact PC.
   inversion DIS; subst.
   elimtype False; exact (NEQ (eq_refl _)).
   apply (TRANS _ _ _ _ _ (o_line (y, z))).
   apply SYMME, LCcon.
    exact (in_eq _ _).
    exact (in_eq _ _).
    apply LIn_reverse_inv, in_lin.
    exact (in_eq _ _).
   apply (TRANS _ _ _ _ _ (o_circuit (lxy ++ (y, z) :: lzx))).
   apply LCcon.
    exact (in_eq _ _).
    exact (in_cons _ _ _ (in_eq _ _)).
    apply lin_or_app; right.
    exact (in_lin _ _ (in_eq _ _)).
   apply CAcon.
    exact (in_cons _ _ _ (in_eq _ _)).
    exact (in_cons _ _ _ (in_eq _ _)).
    exact (in_eq _ _).
   apply CAcon.
    exact (in_cons _ _ _ (in_eq _ _)).
    exact (in_cons _ _ _ (in_eq _ _)).
    exact (in_eq _ _).
   destruct (eq_listline_dec c (lxy ++ lyz ++ lzx)) as [EQ | NEQ]; subst.
   elimtype False.
   apply (InductivePLCADistinctC I _ (set_remove_in _ _ _ _ INC')).
   apply in_rin.
   exact INC'.
   apply SYMME, (TRANS _ _ _ _ _ (o_circuit (lxy ++ (y, z) :: lzx))).
   apply SYMME, CAcon.
    exact (in_cons _ _ _ (in_eq _ _)).
    exact (in_cons _ _ _ (in_eq _ _)).
    exact (in_eq _ _).
   apply (TRANS _ _ _ _ _ (o_line (y, z))).
   apply SYMME, LCcon.
    exact (in_eq _ _).
    exact (in_cons _ _ _ (in_eq _ _)).
    apply lin_or_app; right; exact (in_lin _ _ (in_eq _ _)).
   apply (TRANS _ _ _ _ _ (o_point y)).
    apply SYMME, PLcon.
    exact IY.
    exact (in_eq _ _).
    exact (InPL_left _ _).
   destruct L.
    inversion I.
   assert(PLCAconnect P (l :: L) C A (o_point y) (o_circuit c)).
    exact (TRANS _ _ _ _ _ _ _ (EPL _ _ IY (in_eq _ _)) (ELC _ _ (in_eq _ _) (set_remove_in _ _ _ _ INC'))).
   destruct (IPLCAconnect_add_inline I (o_point y) (o_circuit c) (lxy ++ lyz ++ lzx)
   a x y z pxy pyz pzx lxy lyz lzx INA INC (eq_refl _)
   (InductivePLCADistinctC I) (InductivePLCADistinctA I) H) as
   [ [PC DIS] |
   [ [PC DIS] |
   [ [PC DIS] | 
   [ [PC [DIS1 DIS2]] |
   [ [PC DIS] |
   [ [PC DIS] | 
   [ [PC [DIS1 DIS2]] | 
   [ [PC [DIS1 DIS2]] | 
   [PC [DIS1 DIS2]] ]]]]]]]]; try discriminate.
   exact PC.
   inversion DIS; subst.
   elimtype False.
   apply (InductivePLCADistinctC I _ (set_remove_in _ _ _ _ INC')).
   apply in_rin.
   exact INC'.
   destruct (eq_listcircuit_dec a0 a) as [EQ | NEQ]; [ subst | ].
   elim (InductivePLCADistinctA I _ (set_remove_in _ _ _ _ IAA)).
   apply in_ain.
   exact IAA.
   apply set_remove_in in IAA.
   apply (TRANS _ _ _ _ _ (o_line (y, z))).
   apply SYMME, LCcon.
    exact (in_eq _ _).
    exact (in_eq _ _).
    apply LIn_reverse_inv, in_lin.
    exact (in_eq _ _).
   apply (TRANS _ _ _ _ _ (o_point y)).
   apply SYMME, PLcon.
    exact IY.
    exact (in_eq _ _).
    exact (InPL_left _ _).
   destruct L.
    inversion I.
   destruct C.
    inversion I.
   assert(PLCAconnect P (l :: L) (c :: C) A (o_point y) (o_area a0)).
    apply (TRANS _ _ _ _ _ (o_line l)).
    exact (EPL _ _ IY (in_eq _ _)).
    apply (TRANS _ _ _ _ _ (o_circuit c)).
    exact (ELC _ _ (in_eq _ _) (in_eq _ _)).
    exact (ECA _ _ (in_eq _ _) IAA).
   destruct (IPLCAconnect_add_inline I (o_point y) (o_area a0) (lxy ++ lyz ++ lzx)
   a x y z pxy pyz pzx lxy lyz lzx INA INC (eq_refl _)
   (InductivePLCADistinctC I) (InductivePLCADistinctA I) H) as
   [ [PC DIS] |
   [ [PC DIS] |
   [ [PC DIS] | 
   [ [PC [DIS1 DIS2]] |
   [ [PC DIS] |
   [ [PC DIS] | 
   [ [PC [DIS1 DIS2]] | 
   [ [PC [DIS1 DIS2]] | 
   [PC [DIS1 DIS2]] ]]]]]]]]; try discriminate.
   exact PC.
   inversion DIS; subst.
   elim (NEQ (eq_refl _)).
   destruct (eq_listcircuit_dec a0 a) as [EQ | NEQ]; [ subst | ].
   elim (InductivePLCADistinctA I _ (set_remove_in _ _ _ _ IAA)).
   apply in_ain.
   exact IAA.
   apply set_remove_in in IAA.
   apply (TRANS _ _ _ _ _ (o_line (y, z))).
   apply SYMME, LCcon.
    exact (in_eq _ _).
    exact (in_cons _ _ _ (in_eq _ _)).
    apply lin_or_app; right.
    exact (in_lin _ _ (in_eq _ _)).
   apply (TRANS _ _ _ _ _ (o_point y)).
   apply SYMME, PLcon.
    exact IY.
    exact (in_eq _ _).
    exact (InPL_left _ _).
   destruct L.
    inversion I.
   destruct C.
    inversion I.
   assert(PLCAconnect P (l :: L) (c :: C) A (o_point y) (o_area a0)).
    apply (TRANS _ _ _ _ _ (o_line l)).
    exact (EPL _ _ IY (in_eq _ _)).
    apply (TRANS _ _ _ _ _ (o_circuit c)).
    exact (ELC _ _ (in_eq _ _) (in_eq _ _)).
    exact (ECA _ _ (in_eq _ _) IAA).
   destruct (IPLCAconnect_add_inline I (o_point y) (o_area a0) (lxy ++ lyz ++ lzx)
   a x y z pxy pyz pzx lxy lyz lzx INA INC (eq_refl _)
   (InductivePLCADistinctC I) (InductivePLCADistinctA I) H) as
   [ [PC DIS] |
   [ [PC DIS] |
   [ [PC DIS] | 
   [ [PC [DIS1 DIS2]] |
   [ [PC DIS] |
   [ [PC DIS] | 
   [ [PC [DIS1 DIS2]] | 
   [ [PC [DIS1 DIS2]] | 
   [PC [DIS1 DIS2]] ]]]]]]]]; try discriminate.
   exact PC.
   inversion DIS; subst.
   elim (NEQ (eq_refl _)).
   destruct (eq_listcircuit_dec a0 a) as [AEQ | NAEQ]; [ subst | ].
   elim (InductivePLCADistinctA I _ (set_remove_in _ _ _ _ IAA)).
   apply in_ain.
   exact IAA.
   destruct (eq_listline_dec c (lxy ++ lyz ++ lzx)) as [CEQ |NCEQ]; [ subst | ].
   elim (InductivePLCADistinctC I _ (set_remove_in _ _ _ _ INC')).
   apply in_rin.
   exact INC'.
   apply set_remove_in in INC'.
   apply set_remove_in in IAA.
   destruct (IPLCAconnect_add_inline I (o_circuit c) (o_area a0) (lxy ++ lyz ++ lzx)
   a x y z pxy pyz pzx lxy lyz lzx INA INC (eq_refl _)
   (InductivePLCADistinctC I) (InductivePLCADistinctA I) (ECA _ _ INC' IAA)) as
   [ [PC DIS] |
   [ [PC DIS] |
   [ [PC DIS] | 
   [ [PC [DIS1 DIS2]] |
   [ [PC DIS] |
   [ [PC DIS] | 
   [ [PC [DIS1 DIS2]] | 
   [ [PC [DIS1 DIS2]] | 
   [PC [DIS1 DIS2]] ]]]]]]]]; try discriminate.
   exact PC.
   inversion DIS; subst.
   elim (NCEQ (eq_refl _)).
   inversion DIS; subst.
   elim (NAEQ (eq_refl _)).
   inversion DIS1; subst.
   elim (NCEQ (eq_refl _)).
(* add_inpath *)
+destruct IND as [EPL [ELC ECA]].
 destruct (add_pathpoint a x y z pxy pyz pzx lxy lyz lzx I
(InductivePLCAconstraints I) (InductivePLCAconsistency I) INA INC tl1 tl2 tl3) as [IX [IY IZ]].
 split.
(* PL add_inpath *)
  intros p l INP LIN.
  apply in_app_or in INP; destruct INP as [INP | INP].
  inversion ph; subst.
   simpl in INP; destruct INP as [INP | INP]; [subst | elim INP].
   destruct NS as [NS | NS]; [ | elim NS; reflexivity].
   simpl in LIN.
   destruct LIN as [LIN | [LIN | LIN]].
    apply PLcon; subst; simpl; auto.
    apply PLcon; subst; simpl; auto.
    apply (TRANS _ _ _ _ _ (o_line (y, p))).
     apply PLcon; simpl; auto.
     apply (TRANS _ _ _ _ _ (o_point y)).
      apply SYMME, PLcon; simpl; auto.
      apply (EPL y l) in IY; [ | exact LIN].
      destruct (IPLCAconnect_add_inpath I (o_point y) (o_line l) (lxy ++ lyz ++ lzx)
      a x y z p p (p :: nil) pxy pyz pzx nil lxy lyz lzx INA INC (eq_refl _)
     (InductivePLCADistinctC I) (InductivePLCADistinctA I) IY) as
      [ [PC DIS] |
      [ [PC DIS] |
      [ [PC DIS] | 
      [ [PC [DIS1 DIS2]] |
      [ [PC DIS] |
      [ [PC DIS] | 
      [ [PC [DIS1 DIS2]] | 
      [ [PC [DIS1 DIS2]] | 
      [PC [DIS1 DIS2]] ]]]]]]]]; try discriminate.
      exact PC.
  assert (1 <= length ((s, p2) :: ll)) as lg by (simpl; omega).
  destruct (path_pl_ll ph lg INP) as [el [PIL INE]].
  apply (TRANS _ _ _ _ _  (o_line el)).
  apply PLcon; [ apply in_or_app; left; exact INP | | exact PIL ].
  right; right; apply in_or_app; left; exact INE.
  apply (TRANS _ _ _ _ _ (o_circuit (lxy ++ (y, s) :: ((s, p2) :: ll) ++ (e, z) :: lzx))).
  apply LCcon; [ | exact (in_cons _ _ _ (in_eq _ _)) | ].
  right; right; apply in_or_app; left; exact INE.
  apply lin_or_app; right; right; apply lin_or_app; left; exact (in_lin _ _ INE).
  destruct LIN as [LIN | [LIN | LIN]]; [subst | subst | ].
   apply SYMME, LCcon.
    exact (in_eq _ _).
    exact (in_cons _ _ _ (in_eq _ _)).
    apply lin_or_app; right; exact (in_lin _ _ (in_eq _ _)).
   apply SYMME, LCcon.
    exact (in_cons _ _ _ (in_eq _ _)).
    exact (in_cons _ _ _ (in_eq _ _)).
    apply lin_or_app; right; right.
    apply lin_or_app; right.
    exact (in_lin _ _ (in_eq _ _)).
   apply in_app_or in LIN.
   destruct LIN as [LIN | LIN].
    apply SYMME, LCcon.
     right; right; apply in_or_app; left; exact LIN.
     exact (in_cons _ _ _ (in_eq _ _ )).
     apply lin_or_app; right; right.
     apply lin_or_app; left; exact (in_lin _ _ LIN).
  apply (TRANS _ _ _ _ _ (o_line (y, s))).
  apply SYMME, LCcon.
   exact (in_eq _ _).
   exact (in_cons _ _ _ (in_eq _ _)).
   apply lin_or_app; right.
   exact (in_lin _ _ (in_eq _ _)).
  apply (TRANS _ _ _ _ _ (o_point y)).
  apply SYMME, PLcon.
   apply in_or_app; right; exact IY.
   exact (in_eq _ _).
   exact (InPL_left _ _).
  apply (EPL y l) in IY; [ | exact LIN].
  destruct (IPLCAconnect_add_inpath I (o_point y) (o_line l) (lxy ++ lyz ++ lzx)
  a x y z s e (s :: pl) pxy pyz pzx ((s, p2) :: ll) lxy lyz lzx INA INC (eq_refl _)
 (InductivePLCADistinctC I) (InductivePLCADistinctA I) IY) as
  [ [PC DIS] |
  [ [PC DIS] |
  [ [PC DIS] | 
  [ [PC [DIS1 DIS2]] |
  [ [PC DIS] |
  [ [PC DIS] | 
  [ [PC [DIS1 DIS2]] | 
  [ [PC [DIS1 DIS2]] | 
  [PC [DIS1 DIS2]] ]]]]]]]]; try discriminate.
  exact PC.
 simpl in LIN.
 destruct LIN as [LIN | [LIN | LIN]]; subst.
  apply SYMME, (TRANS _ _ _ _ _ (o_circuit (lxy ++ ((y, s) :: al) ++ (e, z) :: lzx))).
  apply LCcon.
   exact (in_eq _ _).
   exact (in_cons _ _ _ (in_eq _ _)).
   apply in_lin, in_or_app; right.
   exact (in_eq _ _).
  destruct L.
   inversion I.
  assert(PLCAconnect P (l :: L) C A (o_point p) (o_line l)).
   exact (EPL _ _ INP (in_eq _ _)).
  assert(PLCAconnect P (l :: L) C A (o_line l) (o_circuit (lxy ++ lyz ++ lzx))).
   exact (ELC _ _ (in_eq _ _) (InductivePLCAACconsistency I _ _ INA INC)).
  apply (TRANS _ _ _ _ _ _ _ H) in H0.
  apply SYMME in H0.
  destruct (IPLCAconnect_add_inpath I _ (o_point p) (lxy ++ lyz ++ lzx)
   a x y z s e ap pxy pyz pzx al lxy lyz lzx INA INC (eq_refl _)
  (InductivePLCADistinctC I) (InductivePLCADistinctA I) H0) as
  [ [PC DIS] |
  [ [PC DIS] |
  [ [PC DIS] | 
  [ [PC [DIS1 DIS2]] |
  [ [PC DIS] |
  [ [PC DIS] | 
  [ [PC [DIS1 DIS2]] | 
  [ [PC [DIS1 DIS2]] | 
  [PC [DIS1 DIS2]] ]]]]]]]]; try discriminate.
  destruct DIS as [DIS1 DIS2].
  elimtype False; exact (DIS1 (eq_refl _)).
  exact PC.
  apply SYMME, (TRANS _ _ _ _ _ (o_circuit (lxy ++ ((y, s) :: al) ++ (e, z) :: lzx))).
  apply LCcon.
   exact (in_cons _ _ _ (in_eq _ _)).
   exact (in_cons _ _ _ (in_eq _ _)).
   apply in_lin, in_or_app; right.
   apply in_or_app; right.
   exact (in_eq _ _).
  destruct L.
   inversion I.
  assert(PLCAconnect P (l :: L) C A (o_point p) (o_line l)).
   exact (EPL _ _ INP (in_eq _ _)).
  assert(PLCAconnect P (l :: L) C A (o_line l) (o_circuit (lxy ++ lyz ++ lzx))).
   exact (ELC _ _ (in_eq _ _) (InductivePLCAACconsistency I _ _ INA INC)).
  apply (TRANS _ _ _ _ _ _ _ H) in H0.
  apply SYMME in H0.
  destruct (IPLCAconnect_add_inpath I _ (o_point p) (lxy ++ lyz ++ lzx)
   a x y z s e ap pxy pyz pzx al lxy lyz lzx INA INC (eq_refl _)
  (InductivePLCADistinctC I) (InductivePLCADistinctA I) H0) as
  [ [PC DIS] |
  [ [PC DIS] |
  [ [PC DIS] | 
  [ [PC [DIS1 DIS2]] |
  [ [PC DIS] |
  [ [PC DIS] | 
  [ [PC [DIS1 DIS2]] | 
  [ [PC [DIS1 DIS2]] | 
  [PC [DIS1 DIS2]] ]]]]]]]]; try discriminate.
  destruct DIS as [DIS1 DIS2].
  elimtype False; exact (DIS1 (eq_refl _)).
  exact PC.
  apply in_app_or in LIN.
  destruct LIN as [LIN | LIN].
   apply SYMME, (TRANS _ _ _ _ _ (o_circuit (lxy ++ ((y, s) :: al) ++ (e, z) :: lzx))).
   apply LCcon.
    right; right; apply in_or_app; left; exact LIN.
    exact (in_cons _ _ _ (in_eq _ _)).
    apply lin_or_app; right.
    apply lin_or_app; left.
    right; exact (in_lin _ _ LIN).
   destruct L.
    inversion I.
   assert(PLCAconnect P (l0 :: L) C A (o_circuit (lxy ++ lyz ++ lzx)) (o_point p)).
    apply SYMME, (TRANS _ _ _ _ _ (o_line l0)).
    exact (EPL _ _ INP (in_eq _ _)).
    exact (ELC _ _ (in_eq _ _) (InductivePLCAACconsistency I _ _ INA INC)).
  destruct (IPLCAconnect_add_inpath I _ (o_point p) (lxy ++ lyz ++ lzx)
   a x y z s e ap pxy pyz pzx al lxy lyz lzx INA INC (eq_refl _)
  (InductivePLCADistinctC I) (InductivePLCADistinctA I) H) as
  [ [PC DIS] |
  [ [PC DIS] |
  [ [PC DIS] | 
  [ [PC [DIS1 DIS2]] |
  [ [PC DIS] |
  [ [PC DIS] | 
  [ [PC [DIS1 DIS2]] | 
  [ [PC [DIS1 DIS2]] | 
  [PC [DIS1 DIS2]] ]]]]]]]]; try discriminate.
  destruct DIS as [DIS1 DIS2].
  elimtype False; exact (DIS1 (eq_refl _)).
  exact PC.
  destruct (IPLCAconnect_add_inpath I (o_point p) (o_line l) (lxy ++ lyz ++ lzx)
   a x y z s e ap pxy pyz pzx al lxy lyz lzx INA INC (eq_refl _)
  (InductivePLCADistinctC I) (InductivePLCADistinctA I) (EPL _ _ INP LIN)) as
  [ [PC DIS] |
  [ [PC DIS] |
  [ [PC DIS] | 
  [ [PC [DIS1 DIS2]] |
  [ [PC DIS] |
  [ [PC DIS] | 
  [ [PC [DIS1 DIS2]] | 
  [ [PC [DIS1 DIS2]] | 
  [PC [DIS1 DIS2]] ]]]]]]]]; try discriminate.
  exact PC.
 split.
  (* LC add_inpath *)
  intros l c LIN INC'.
  simpl in INC', LIN.
  destruct INC' as [INC' | [INC' | INC']]; destruct LIN as [LIN | [LIN | LIN]]; subst.
   apply LCcon.
    exact (in_eq _ _).
    exact (in_eq _ _).
    apply LIn_reverse_inv.
    exact (in_lin _ _ (in_eq _ _)).
   apply LCcon.
    exact (in_cons _ _ _ (in_eq _ _)).
    exact (in_eq _ _).
    right; apply lin_or_app; right.
    apply LIn_reverse_inv.
    exact (in_lin _ _ (in_eq _ _)).
   apply in_app_or in LIN.
   destruct LIN as [LIN | LIN].
    apply LCcon.
     right; right; apply in_or_app; left.
     exact LIN.
     exact (in_eq _ _).
     apply LIn_reverse_inv, in_lin.
     right; apply in_or_app; right; right.
     unfold reverse_linelist.
     rewrite <- in_rev, map_reverse, reverse_reverse.
     exact LIN.
   apply SYMME, (TRANS _ _ _ _ _ (o_line (y, s))).
   apply SYMME, LCcon.
    exact (in_eq _ _).
    exact (in_eq _ _).
    apply LIn_reverse_inv, in_lin.
    exact (in_eq _ _).
   apply (TRANS _ _ _ _ _ (o_circuit (lxy ++ ((y, s) :: al) ++ (e, z) :: lzx))).
   apply LCcon.
    exact (in_eq _ _).
    exact (in_cons _ _ _ (in_eq _ _)).
    apply lin_or_app; right.
    exact (in_lin _ _ (in_eq _ _)).
   assert(PLCAconnect P L C A (o_line l) (o_circuit (lxy ++ lyz ++ lzx))).
    exact (ELC _ _ LIN (InductivePLCAACconsistency I _ _ INA INC)).
   destruct (IPLCAconnect_add_inpath I _ (o_line l) (lxy ++ lyz ++ lzx)
   a x y z s e ap pxy pyz pzx al lxy lyz lzx INA INC (eq_refl _)
   (InductivePLCADistinctC I) (InductivePLCADistinctA I) (SYMME _ _ _ _ _ _ H)) as
   [ [PC DIS] |
   [ [PC DIS] |
   [ [PC DIS] | 
   [ [PC [DIS1 DIS2]] |
   [ [PC DIS] |
   [ [PC DIS] | 
   [ [PC [DIS1 DIS2]] | 
   [ [PC [DIS1 DIS2]] | 
   [PC [DIS1 DIS2]] ]]]]]]]]; try discriminate.
   destruct DIS as [DIS1 DIS2].
   elimtype False; exact (DIS1 (eq_refl _)).
   exact PC.
   apply LCcon.
    exact (in_eq _ _).
    exact (in_cons _ _ _ (in_eq _ _)).
    apply lin_or_app; right.
   exact (in_lin _ _ (in_eq _ _)).
   apply LCcon.
    exact (in_cons _ _ _ (in_eq _ _)).
    exact (in_cons _ _ _ (in_eq _ _)).
    apply lin_or_app; right; right.
   apply lin_or_app; right.
   exact (in_lin _ _ (in_eq _ _)).
   apply in_app_or in LIN.
   destruct LIN as [LIN | LIN].
   apply LCcon.
    right; right; apply in_or_app; left.
    exact LIN.
    exact (in_cons _ _ _ (in_eq _ _)).
    apply lin_or_app; right; right.
    apply lin_or_app; left.
    exact (in_lin _ _ LIN).
   assert(PLCAconnect P L C A (o_line l) (o_circuit (lxy ++ lyz ++ lzx))).
    exact (ELC _ _ LIN (InductivePLCAACconsistency I _ _ INA INC)).
   destruct (IPLCAconnect_add_inpath I (o_line l) _ (lxy ++ lyz ++ lzx)
   a x y z s e ap pxy pyz pzx al lxy lyz lzx INA INC (eq_refl _)
   (InductivePLCADistinctC I) (InductivePLCADistinctA I) H) as
   [ [PC DIS] |
   [ [PC DIS] |
   [ [PC DIS] | 
   [ [PC [DIS1 DIS2]] |
   [ [PC DIS] |
   [ [PC DIS] | 
   [ [PC [DIS1 DIS2]] | 
   [ [PC [DIS1 DIS2]] | 
   [PC [DIS1 DIS2]] ]]]]]]]]; try discriminate.
   destruct DIS as [DIS1 [DIS2 [DIS3 DIS4]]].
   elimtype False; exact (DIS3 (eq_refl _)).
   exact PC.
   destruct L.
    inversion I.
   destruct (eq_circuit_dec c (lxy ++ lyz ++ lzx)) as [ROT | NROT].
   elimtype False.
   apply (InductivePLCADistinctC I (lxy ++ lyz ++ lzx)).
   exact (InductivePLCAACconsistency I _ _ INA INC).
   apply (eq_rin_elem _ _ _ ROT).
   apply in_rin.
   exact INC'.
   apply set_remove_in in INC'.
   apply (TRANS _ _ _ _ _ (o_point y)).
   apply SYMME, PLcon.
    apply in_or_app; right; exact IY.
    exact (in_eq _ _).
    exact (InPL_left _ _).
   assert(PLCAconnect P (l::L) C A (o_point y) (o_circuit c)).
    apply (TRANS _ _ _ _ _ _ _ (EPL _ _ IY (in_eq _ _)) (ELC _ _ (in_eq _ _) INC')).
   destruct (IPLCAconnect_add_inpath I (o_point y) (o_circuit c) (lxy ++ lyz ++ lzx)
   a x y z s e ap pxy pyz pzx al lxy lyz lzx INA INC (eq_refl _)
   (InductivePLCADistinctC I) (InductivePLCADistinctA I) H) as
   [ [PC DIS] |
   [ [PC DIS] |
   [ [PC DIS] | 
   [ [PC [DIS1 DIS2]] |
   [ [PC DIS] |
   [ [PC DIS] | 
   [ [PC [DIS1 DIS2]] | 
   [ [PC [DIS1 DIS2]] | 
   [PC [DIS1 DIS2]] ]]]]]]]]; try discriminate.
   destruct DIS as [DIS1 [DIS2 [DIS3 DIS4]]].
   exact PC.
   inversion DIS; subst.
   elimtype False; exact (NROT (rotSame _)).
   destruct (eq_listline_dec c (lxy ++ lyz ++ lzx)) as [EQ | NEQ ]; subst.
   elimtype False.
   apply (InductivePLCADistinctC I _ (set_remove_in _ _ _ _ INC')).
   apply in_rin.
   exact INC'.
   apply set_remove_in in INC'.
   apply (TRANS _ _ _ _ _ (o_point z)).
   apply SYMME, PLcon.
    apply in_or_app; right; exact IZ.
    right; exact (in_eq _ _).
    exact (InPL_right _ _).
   destruct L.
    inversion I.
   assert(PLCAconnect P (l::L) C A (o_point z) (o_circuit c)).
    apply (TRANS _ _ _ _ _ _ _ (EPL _ _ IZ (in_eq _ _)) (ELC _ _ (in_eq _ _) INC')).
   destruct (IPLCAconnect_add_inpath I (o_point z) (o_circuit c) (lxy ++ lyz ++ lzx)
   a x y z s e ap pxy pyz pzx al lxy lyz lzx INA INC (eq_refl _)
   (InductivePLCADistinctC I) (InductivePLCADistinctA I) H) as
   [ [PC DIS] |
   [ [PC DIS] |
   [ [PC DIS] | 
   [ [PC [DIS1 DIS2]] |
   [ [PC DIS] |
   [ [PC DIS] | 
   [ [PC [DIS1 DIS2]] | 
   [ [PC [DIS1 DIS2]] | 
   [PC [DIS1 DIS2]] ]]]]]]]]; try discriminate.
   destruct DIS as [DIS1 [DIS2 [DIS3 DIS4]]].
   exact PC.
   inversion DIS; subst.
   elimtype False; exact (NEQ (eq_refl _)).
   apply in_app_or in LIN.
   destruct LIN as [LIN | LIN].
   apply (TRANS _ _ _ _ _ (o_circuit (lxy ++ ((y, s) :: al) ++ (e, z) :: lzx))).
   apply LCcon.
    right; right; apply in_or_app; left.
    exact LIN.
    exact (in_cons _ _ _ (in_eq _ _)).
    apply lin_or_app; right.
    apply lin_or_app; left.
    right; exact (in_lin _ _ LIN).
   apply (TRANS _ _ _ _ _ (o_line (y, s))).
   apply SYMME, LCcon.
    exact (in_eq _ _).
    exact (in_cons _ _ _ (in_eq _ _)).
    apply lin_or_app; right.
    exact (in_lin _ _ (in_eq _ _)).
   apply (TRANS _ _ _ _ _ (o_point y)).
   apply SYMME, PLcon.
    apply in_or_app; right; exact IY.
    exact (in_eq _ _).
    exact (InPL_left _ _).
   destruct L.
    inversion I.
   destruct (eq_listline_dec c (lxy ++ lyz ++ lzx)) as [EQ | NEQ]; subst.
   elimtype False.
   apply (InductivePLCADistinctC I _ (set_remove_in _ _ _ _ INC')).
   apply in_rin.
   exact INC'.
   assert(PLCAconnect P (l0::L) C A (o_point y) (o_circuit c)).
    exact (TRANS _ _ _ _ _ _ _ (EPL _ _ IY (in_eq _ _)) (ELC _ _ (in_eq _ _) (set_remove_in _ _ _ _ INC'))).
   destruct (IPLCAconnect_add_inpath I (o_point y) (o_circuit c) (lxy ++ lyz ++ lzx)
   a x y z s e ap pxy pyz pzx al lxy lyz lzx INA INC (eq_refl _)
   (InductivePLCADistinctC I) (InductivePLCADistinctA I) H) as
   [ [PC DIS] |
   [ [PC DIS] |
   [ [PC DIS] | 
   [ [PC [DIS1 DIS2]] |
   [ [PC DIS] |
   [ [PC DIS] | 
   [ [PC [DIS1 DIS2]] | 
   [ [PC [DIS1 DIS2]] | 
   [PC [DIS1 DIS2]] ]]]]]]]]; try discriminate.
   destruct DIS as [DIS1 [DIS2 [DIS3 DIS4]]].
   exact PC.
   inversion DIS; subst.
   elimtype False.
   exact (NEQ (eq_refl _)).
   destruct (eq_listline_dec c (lxy ++ lyz ++ lzx)) as [EQ | NEQ]; subst.
   elimtype False.
   apply (InductivePLCADistinctC I _ (set_remove_in _ _ _ _ INC')).
   apply in_rin.
   exact INC'.
   apply set_remove_in in INC'.
   destruct (IPLCAconnect_add_inpath I (o_line l) (o_circuit c) (lxy ++ lyz ++ lzx)
   a x y z s e ap pxy pyz pzx al lxy lyz lzx INA INC (eq_refl _)
   (InductivePLCADistinctC I) (InductivePLCADistinctA I) (ELC _ _ LIN INC')) as
   [ [PC DIS] |
   [ [PC DIS] |
   [ [PC DIS] | 
   [ [PC [DIS1 DIS2]] |
   [ [PC DIS] |
   [ [PC DIS] | 
   [ [PC [DIS1 DIS2]] | 
   [ [PC [DIS1 DIS2]] | 
   [PC [DIS1 DIS2]] ]]]]]]]]; try discriminate.
   destruct DIS as [DIS1 [DIS2 [DIS3 DIS4]]].
   exact PC.
   inversion DIS; subst.
   elimtype False.
   exact (NEQ (eq_refl _)).
  (* CA add_inpath *)
  intros c a0 INC' IAA.
  simpl in *.
  destruct IAA as [IAA | [IAA | IAA]];
  destruct INC' as [INC' | [INC' | INC']]; subst.
   apply CAcon.
    exact (in_eq _ _).
    exact (in_eq _ _).
    exact (in_eq _ _).
   apply (TRANS _ _ _ _ _ (o_line (y, s))).
   apply SYMME, LCcon.
    exact (in_eq _ _).
    exact (in_cons _ _ _ (in_eq _ _)).
    apply lin_or_app; right.
    exact (in_lin _ _ (in_eq _ _)).
   apply (TRANS _ _ _ _ _ (o_circuit ((s, y) :: lyz ++ (z, e) :: reverse_linelist al))).
   apply LCcon.
    exact (in_eq _ _).
    exact (in_eq _ _).
    apply LIn_reverse_inv, in_lin.
    exact (in_eq _ _).
   apply CAcon.
    exact (in_eq _ _).
    exact (in_eq _ _).
    exact (in_eq _ _).
   destruct (eq_listline_dec c (lxy ++ lyz ++ lzx)) as [EQ | NEQ]; [ subst | ].
   elimtype False.
   apply (InductivePLCADistinctC I _ (set_remove_in _ _ _ _ INC')).
   apply in_rin.
   exact INC'.
   apply set_remove_in in INC'.
   apply SYMME, (TRANS _ _ _ _ _ (o_circuit ((s, y) :: lyz ++ (z, e) :: reverse_linelist al))).
   apply SYMME, CAcon.
    exact (in_eq _ _).
    exact (in_eq _ _).
    exact (in_eq _ _).
   apply (TRANS _ _ _ _ _ (o_line (y, s))).
   apply SYMME, LCcon.
    exact (in_eq _ _).
    exact (in_eq _ _).
    apply LIn_reverse_inv, in_lin.
    exact (in_eq _ _).
   apply (TRANS _ _ _ _ _ (o_point y)).
   apply SYMME, PLcon.
    apply in_or_app; right; exact IY.
    exact (in_eq _ _).
    exact (InPL_left _ _).
   destruct L.
    inversion I.
    assert(PLCAconnect P (l :: L) C A (o_point y) (o_circuit c)).
     exact (TRANS _ _ _ _ _ _ _ (EPL _ _ IY (in_eq _ _)) (ELC _ _ (in_eq _ _) INC')).
   destruct (IPLCAconnect_add_inpath I (o_point y) (o_circuit c) (lxy ++ lyz ++ lzx)
   a x y z s e ap pxy pyz pzx al lxy lyz lzx INA INC (eq_refl _)
   (InductivePLCADistinctC I) (InductivePLCADistinctA I) H) as
   [ [PC DIS] |
   [ [PC DIS] |
   [ [PC DIS] | 
   [ [PC [DIS1 DIS2]] |
   [ [PC DIS] |
   [ [PC DIS] | 
   [ [PC [DIS1 DIS2]] | 
   [ [PC [DIS1 DIS2]] | 
   [PC [DIS1 DIS2]] ]]]]]]]]; try discriminate.
   exact PC.
   inversion DIS; subst.
   elimtype False; exact (NEQ (eq_refl _)).
   apply (TRANS _ _ _ _ _ (o_line (y, s))).
   apply SYMME, LCcon.
    exact (in_eq _ _).
    exact (in_eq _ _).
    apply LIn_reverse_inv, in_lin.
    exact (in_eq _ _).
   apply (TRANS _ _ _ _ _ (o_circuit (lxy ++ (y, s) :: al ++ (e, z) :: lzx))).
   apply LCcon.
    exact (in_eq _ _).
    exact (in_cons _ _ _ (in_eq _ _)).
    apply lin_or_app; right.
    exact (in_lin _ _ (in_eq _ _)).
   apply CAcon.
    exact (in_cons _ _ _ (in_eq _ _)).
    exact (in_cons _ _ _ (in_eq _ _)).
    exact (in_eq _ _).
   apply CAcon.
    exact (in_cons _ _ _ (in_eq _ _)).
    exact (in_cons _ _ _ (in_eq _ _)).
    exact (in_eq _ _).
   destruct (eq_listline_dec c (lxy ++ lyz ++ lzx)) as [EQ | NEQ]; subst.
   elimtype False.
   apply (InductivePLCADistinctC I _ (set_remove_in _ _ _ _ INC')).
   apply in_rin.
   exact INC'.
   apply SYMME, (TRANS _ _ _ _ _ (o_circuit (lxy ++ (y, s) :: al ++ (e, z) :: lzx))).
   apply SYMME, CAcon.
    exact (in_cons _ _ _ (in_eq _ _)).
    exact (in_cons _ _ _ (in_eq _ _)).
    exact (in_eq _ _).
   apply (TRANS _ _ _ _ _ (o_line (y, s))).
   apply SYMME, LCcon.
    exact (in_eq _ _).
    exact (in_cons _ _ _ (in_eq _ _)).
    apply lin_or_app; right; exact (in_lin _ _ (in_eq _ _)).
   apply (TRANS _ _ _ _ _ (o_point y)).
    apply SYMME, PLcon.
    apply in_or_app; right; exact IY.
    exact (in_eq _ _).
    exact (InPL_left _ _).
   destruct L.
    inversion I.
   assert(PLCAconnect P (l :: L) C A (o_point y) (o_circuit c)).
    exact (TRANS _ _ _ _ _ _ _ (EPL _ _ IY (in_eq _ _)) (ELC _ _ (in_eq _ _) (set_remove_in _ _ _ _ INC'))).
   destruct (IPLCAconnect_add_inpath I (o_point y) (o_circuit c) (lxy ++ lyz ++ lzx)
   a x y z s e ap pxy pyz pzx al lxy lyz lzx INA INC (eq_refl _)
   (InductivePLCADistinctC I) (InductivePLCADistinctA I) H) as
   [ [PC DIS] |
   [ [PC DIS] |
   [ [PC DIS] | 
   [ [PC [DIS1 DIS2]] |
   [ [PC DIS] |
   [ [PC DIS] | 
   [ [PC [DIS1 DIS2]] | 
   [ [PC [DIS1 DIS2]] | 
   [PC [DIS1 DIS2]] ]]]]]]]]; try discriminate.
   exact PC.
   inversion DIS; subst.
   elimtype False.
   apply (InductivePLCADistinctC I _ (set_remove_in _ _ _ _ INC')).
   apply in_rin.
   exact INC'.
   destruct (eq_listcircuit_dec a0 a) as [EQ | NEQ]; [ subst | ].
   elim (InductivePLCADistinctA I _ (set_remove_in _ _ _ _ IAA)).
   apply in_ain.
   exact IAA.
   apply set_remove_in in IAA.
   apply (TRANS _ _ _ _ _ (o_line (y, s))).
   apply SYMME, LCcon.
    exact (in_eq _ _).
    exact (in_eq _ _).
    apply LIn_reverse_inv, in_lin.
    exact (in_eq _ _).
   apply (TRANS _ _ _ _ _ (o_point y)).
   apply SYMME, PLcon.
    apply in_or_app; right; exact IY.
    exact (in_eq _ _).
    exact (InPL_left _ _).
   destruct L.
    inversion I.
   destruct C.
    inversion I.
   assert(PLCAconnect P (l :: L) (c :: C) A (o_point y) (o_area a0)).
    apply (TRANS _ _ _ _ _ (o_line l)).
    exact (EPL _ _ IY (in_eq _ _)).
    apply (TRANS _ _ _ _ _ (o_circuit c)).
    exact (ELC _ _ (in_eq _ _) (in_eq _ _)).
    exact (ECA _ _ (in_eq _ _) IAA).
   destruct (IPLCAconnect_add_inpath I (o_point y) (o_area a0) (lxy ++ lyz ++ lzx)
   a x y z s e ap pxy pyz pzx al lxy lyz lzx INA INC (eq_refl _)
   (InductivePLCADistinctC I) (InductivePLCADistinctA I) H) as
   [ [PC DIS] |
   [ [PC DIS] |
   [ [PC DIS] | 
   [ [PC [DIS1 DIS2]] |
   [ [PC DIS] |
   [ [PC DIS] | 
   [ [PC [DIS1 DIS2]] | 
   [ [PC [DIS1 DIS2]] | 
   [PC [DIS1 DIS2]] ]]]]]]]]; try discriminate.
   exact PC.
   inversion DIS; subst.
   elim (NEQ (eq_refl _)).
   destruct (eq_listcircuit_dec a0 a) as [EQ | NEQ]; [ subst | ].
   elim (InductivePLCADistinctA I _ (set_remove_in _ _ _ _ IAA)).
   apply in_ain.
   exact IAA.
   apply set_remove_in in IAA.
   apply (TRANS _ _ _ _ _ (o_line (y, s))).
   apply SYMME, LCcon.
    exact (in_eq _ _).
    exact (in_cons _ _ _ (in_eq _ _)).
    apply lin_or_app; right.
    exact (in_lin _ _ (in_eq _ _)).
   apply (TRANS _ _ _ _ _ (o_point y)).
   apply SYMME, PLcon.
    apply in_or_app; right; exact IY.
    exact (in_eq _ _).
    exact (InPL_left _ _).
   destruct L.
    inversion I.
   destruct C.
    inversion I.
   assert(PLCAconnect P (l :: L) (c :: C) A (o_point y) (o_area a0)).
    apply (TRANS _ _ _ _ _ (o_line l)).
    exact (EPL _ _ IY (in_eq _ _)).
    apply (TRANS _ _ _ _ _ (o_circuit c)).
    exact (ELC _ _ (in_eq _ _) (in_eq _ _)).
    exact (ECA _ _ (in_eq _ _) IAA).
   destruct (IPLCAconnect_add_inpath I (o_point y) (o_area a0) (lxy ++ lyz ++ lzx)
   a x y z s e ap pxy pyz pzx al lxy lyz lzx INA INC (eq_refl _)
   (InductivePLCADistinctC I) (InductivePLCADistinctA I) H) as
   [ [PC DIS] |
   [ [PC DIS] |
   [ [PC DIS] | 
   [ [PC [DIS1 DIS2]] |
   [ [PC DIS] |
   [ [PC DIS] | 
   [ [PC [DIS1 DIS2]] | 
   [ [PC [DIS1 DIS2]] | 
   [PC [DIS1 DIS2]] ]]]]]]]]; try discriminate.
   exact PC.
   inversion DIS; subst.
   elim (NEQ (eq_refl _)).
   destruct (eq_listcircuit_dec a0 a) as [AEQ | NAEQ]; [ subst | ].
   elim (InductivePLCADistinctA I _ (set_remove_in _ _ _ _ IAA)).
   apply in_ain.
   exact IAA.
   destruct (eq_listline_dec c (lxy ++ lyz ++ lzx)) as [CEQ |NCEQ]; [ subst | ].
   elim (InductivePLCADistinctC I _ (set_remove_in _ _ _ _ INC')).
   apply in_rin.
   exact INC'.
   apply set_remove_in in INC'.
   apply set_remove_in in IAA.
   destruct (IPLCAconnect_add_inpath I (o_circuit c) (o_area a0) (lxy ++ lyz ++ lzx)
   a x y z s e ap pxy pyz pzx al lxy lyz lzx INA INC (eq_refl _)
   (InductivePLCADistinctC I) (InductivePLCADistinctA I) (ECA _ _ INC' IAA)) as
   [ [PC DIS] |
   [ [PC DIS] |
   [ [PC DIS] | 
   [ [PC [DIS1 DIS2]] |
   [ [PC DIS] |
   [ [PC DIS] | 
   [ [PC [DIS1 DIS2]] | 
   [ [PC [DIS1 DIS2]] | 
   [PC [DIS1 DIS2]] ]]]]]]]]; try discriminate.
   exact PC.
   inversion DIS; subst.
   elim (NCEQ (eq_refl _)).
   inversion DIS; subst.
   elim (NAEQ (eq_refl _)).
   inversion DIS1; subst.
   elim (NCEQ (eq_refl _)).
(* add_outpath *)
+destruct IND as [EPL [ELC ECA]].
 subst; split; [ | split].
(* PL add_outpath *)
 -intros p l INP LIN.
  simpl in LIN.
  destruct LIN as [LIN | [LIN | LIN]]; [ subst | subst | ].
  apply in_app_or in INP; destruct INP as [INP | INP].
   inversion ph; subst.
    destruct INP as [INP | []]; subst.
     apply PLcon.
      left; now auto.
      left; now auto.
      simpl; now auto.
    assert (1 <= length ((s, p2) :: ll)) as lg by (simpl; omega).
    destruct (path_pl_ll ph lg INP) as [el [INL PIL]].
     apply (TRANS _ _ _ _ _ (o_line el)).
     apply PLcon; [ apply in_app_iff; left;exact INP | right; right; apply in_or_app; left; exact PIL | exact INL ].
     apply (TRANS _ _ _ _ _ (o_circuit (lxy ++ ((y, s) :: (s, p2) :: ll) ++ (e, z) :: lzx))).
     apply LCcon; [ right; right; apply in_or_app; left; exact PIL | right; exact (in_eq _ _) | apply lin_or_app; right; right; apply lin_or_app; left; exact (in_lin _ _ PIL) ].
     apply SYMME, LCcon; [ exact (in_eq _ _) | right; exact (in_eq _ _) | apply lin_or_app; right; exact (in_lin _ _ (in_eq _ _)) ].
   apply SYMME, (TRANS _ _ _ _ _ (o_circuit (lxy++((y,s)::al)++((e,z)::lzx)))).
    apply LCcon.
     left; now auto.
     right; left; now auto.
     apply lin_or_app; right; left; now constructor.
    assert (PLCAconnect P L C A (o_circuit (lxy ++ lyz ++ lzx)) (o_point p)).
     destruct L.
      inversion I.
     apply TRANS with (o_line l).
      apply SYMME; apply ELC.
       left; now auto.
       now auto.
      apply SYMME; apply EPL.
       now auto.
       left; now auto.
    destruct (IPLCAconnect_add_outpath I _ (o_point p)
    x y z s e ap al lxy lyz lzx (eq_refl _) (InductivePLCADistinctC I) (InductivePLCADistinctA I)
    (InductivePLCAoutermostconsistency2 I) H) as
    [ [PC [DIS1 DIS2]] |
    [ [PC [DIS1 DIS2]] |
    [ [PC [DIS1 DIS2]] | 
      [PC [DIS1 DIS2]]]]]; try discriminate.
     elim DIS1; now auto.
     exact PC.
  apply in_app_or in INP; destruct INP as [INP | INP].
   inversion ph; subst.
    destruct INP as [INP | []]; subst.
     apply PLcon.
      left; now auto.
      right; left; now auto.
      simpl; now auto.
    assert (1 <= length ((s, p2) :: ll)) as lg by (simpl; omega).
    destruct (path_pl_ll ph lg INP) as [el [INL PIL]].
     apply (TRANS _ _ _ _ _ (o_line el)).
     apply PLcon; [ apply in_app_iff; left;exact INP | right; right; apply in_or_app; left; exact PIL | exact INL ].
     apply (TRANS _ _ _ _ _ (o_circuit (lxy ++ ((y, s) :: (s, p2) :: ll) ++ (e, z) :: lzx))).
     apply LCcon; [ right; right; apply in_or_app; left; exact PIL | right; exact (in_eq _ _) | apply lin_or_app; right; right; apply lin_or_app; left; exact (in_lin _ _ PIL) ].
     apply SYMME, LCcon; [ right; exact (in_eq _ _) | right; exact (in_eq _ _) | apply lin_or_app; right; apply lin_or_app; right; exact (in_lin _ _ (in_eq _ _)) ].
   apply SYMME, (TRANS _ _ _ _ _ (o_circuit (lxy++((y,s)::al)++((e,z)::lzx)))).
    apply LCcon.
     right; left; now auto.
     right; left; now auto.
     apply lin_or_app; right; apply lin_or_app; right; left; now constructor.
    assert (PLCAconnect P L C A (o_circuit (lxy ++ lyz ++ lzx)) (o_point p)).
     destruct L.
      inversion I.
     apply TRANS with (o_line l).
      apply SYMME; apply ELC.
       left; now auto.
       now auto.
      apply SYMME; apply EPL.
       now auto.
       left; now auto.
    destruct (IPLCAconnect_add_outpath I _ (o_point p)
    x y z s e ap al lxy lyz lzx (eq_refl _) (InductivePLCADistinctC I) (InductivePLCADistinctA I)
    (InductivePLCAoutermostconsistency2 I) H) as
    [ [PC [DIS1 DIS2]] |
    [ [PC [DIS1 DIS2]] |
    [ [PC [DIS1 DIS2]] | 
      [PC [DIS1 DIS2]]]]]; try discriminate.
     elim DIS1; now auto.
     exact PC.
  apply in_app_or in LIN.
  destruct LIN as [LIN | LIN].
   apply in_app_or in INP.
   destruct INP as [INP | INP].
    inversion ph; subst.
     elim LIN.
     assert (1 <= length ((s, p2) :: ll)) as lg by (simpl; omega).
     destruct (path_pl_ll ph lg INP) as [el [INL PIL]].
     apply (TRANS _ _ _ _ _ (o_line el)).
      apply PLcon; [ apply in_app_iff; left;exact INP | right; right; apply in_or_app; left; exact PIL | exact INL ].
      apply (TRANS _ _ _ _ _ (o_circuit (lxy++((y,s)::(s,p2)::ll)++((e,z)::lzx)))).
       apply LCcon; [ right; right; apply in_or_app; left; exact PIL | right; exact (in_eq _ _) | apply lin_or_app; right; apply lin_or_app; left; right; exact (in_lin _ _ PIL) ].
       apply SYMME, LCcon.
        right; right; apply in_or_app; left.
         exact LIN.
        right; exact (in_eq _ _).
        apply lin_or_app; right; apply lin_or_app; left; exact (in_lin _ _ (in_cons _ _ _ LIN)).
    apply SYMME, (TRANS _ _ _ _ _ (o_circuit (lxy++((y,s)::al)++((e,z)::lzx)))).
     apply LCcon; [ right; right; apply in_or_app; left; exact LIN | exact (in_cons _ _ _ (in_eq _ _)) | ].
      apply lin_or_app; right; apply lin_or_app; left; right; exact (in_lin _ _ LIN).
    assert (PLCAconnect P L C A (o_circuit (lxy ++ lyz ++ lzx)) (o_point p)).
     destruct L.
      inversion I.
     apply TRANS with (o_line l0).
      apply SYMME; apply ELC.
       left; now auto.
       now auto.
      apply SYMME; apply EPL.
       now auto.
       left; now auto.
    destruct (IPLCAconnect_add_outpath I _ (o_point p)
    x y z s e ap al lxy lyz lzx (eq_refl _) (InductivePLCADistinctC I) (InductivePLCADistinctA I)
    (InductivePLCAoutermostconsistency2 I) H) as
    [ [PC [DIS1 DIS2]] |
    [ [PC [DIS1 DIS2]] |
    [ [PC [DIS1 DIS2]] | 
      [PC [DIS1 DIS2]]]]]; try discriminate.
     elim DIS1; now auto.
     exact PC.
   apply in_app_or in INP.
   destruct INP as [INP | INP].
    inversion ph; subst.
     destruct INP as [INP | []]; subst.
     apply (TRANS _ _ _ _ _ (o_line (y, p))).
      apply PLcon.
       apply in_app_iff; simpl; now auto.
       left; now auto.
       simpl; now auto.
      apply (TRANS _ _ _ _ _ (o_circuit (lxy++((y,p)::((p,z)::lzx))))).
       apply LCcon.
        left; now auto.
        right; exact (in_eq _ _).
        apply lin_or_app; right; exact (in_lin _ _ (in_eq _ _)).
       assert (PLCAconnect P L C A (o_circuit (lxy ++ lyz ++ lzx)) (o_line l)).
        apply SYMME; apply ELC; now auto.
       subst.
       destruct (IPLCAconnect_add_outpath I _ (o_line l)
       x y z p p (p::nil) nil lxy lyz lzx (eq_refl _) (InductivePLCADistinctC I) (InductivePLCADistinctA I)
       (InductivePLCAoutermostconsistency2 I) H) as
       [ [PC [DIS1 DIS2]] |
       [ [PC [DIS1 DIS2]] |
       [ [PC [DIS1 DIS2]] | 
         [PC [DIS1 DIS2]]]]]; try discriminate.
        elim DIS1; now auto.
        exact PC.
     assert (1 <= length ((s, p2) :: ll)) as lg by (simpl; omega).
     destruct (path_pl_ll ph lg INP) as [el [INL PIL]].
     apply (TRANS _ _ _ _ _ (o_line el)).
      apply PLcon; [ apply in_app_iff; left;exact INP | right; right; apply in_or_app; left; exact PIL | exact INL ].
      apply (TRANS _ _ _ _ _ (o_circuit (lxy++((y,s)::(s,p2)::ll)++((e,z)::lzx)))).
       apply LCcon; [ right; right; apply in_or_app; left; exact PIL | right; exact (in_eq _ _) | apply lin_or_app; right; apply lin_or_app; left; right; exact (in_lin _ _ PIL) ].
       assert (PLCAconnect P L C A (o_circuit (lxy ++ lyz ++ lzx)) (o_line l)).
        apply SYMME; apply ELC; now auto.
       subst.
       destruct (IPLCAconnect_add_outpath I _ (o_line l)
       x y z s e (s::pl) ((s,p2)::ll) lxy lyz lzx (eq_refl _) (InductivePLCADistinctC I) (InductivePLCADistinctA I)
       (InductivePLCAoutermostconsistency2 I) H1) as
       [ [PC [DIS1 DIS2]] |
       [ [PC [DIS1 DIS2]] |
       [ [PC [DIS1 DIS2]] | 
         [PC [DIS1 DIS2]]]]]; try discriminate.
        elim DIS1; now auto.
        exact PC.
    assert (PLCAconnect P L C A (o_point p) (o_line l)) by auto.
    destruct (IPLCAconnect_add_outpath I _ _
    x y z s e ap al lxy lyz lzx (eq_refl _) (InductivePLCADistinctC I) (InductivePLCADistinctA I)
    (InductivePLCAoutermostconsistency2 I) H) as
    [ [PC [DIS1 DIS2]] |
    [ [PC [DIS1 DIS2]] |
    [ [PC [DIS1 DIS2]] | 
      [PC [DIS1 DIS2]]]]]; try discriminate.
     exact PC.
(* LC add_outpath *)
 -subst; intros l c LIN INC.
  simpl in *.
  destruct INC as [INC | [INC | INC]]; [ subst | subst | ].
   destruct LIN as [LIN | [LIN | LIN]]; [ subst | subst | ].
    apply LCcon; [ exact (in_eq _ _) | exact (in_eq _ _) | left; constructor; unfold reverse; simpl; auto ].
    apply LCcon; [ right; exact (in_eq _ _) | exact (in_eq _ _) | right; apply lin_or_app; right; left; constructor; unfold reverse; simpl; auto ].
   apply in_app_or in LIN.
   destruct LIN as [LIN | LIN].
    apply LCcon; [ right; right; apply in_or_app; left; exact LIN | exact (in_eq  _ _) | right ].
     apply lin_or_app; right; right.
     apply LIn_reverse_linelist.
      apply in_lin; now auto.
    apply SYMME, (TRANS _ _ _ _ _ (o_line (y, s))).
     apply SYMME, LCcon.
      exact (in_eq _ _).
      exact (in_eq _ _).
      left; constructor; unfold reverse; now auto.
     apply (TRANS _ _ _ _ _ (o_circuit (lxy ++ (y, s) :: al ++ (e, z) :: lzx))).
      apply LCcon.
       exact (in_eq _  _).
       exact (in_cons _ _ _ (in_eq _ _)).
       apply lin_or_app; right; left; now constructor.
      assert(PLCAconnect P L C A (o_circuit (lxy ++ lyz ++ lzx)) (o_line l)).
       apply SYMME.
       exact (ELC _ _ LIN O1).
      destruct (IPLCAconnect_add_outpath I _ (o_line l)
      x y z s e ap al lxy lyz lzx (eq_refl _)
      (InductivePLCADistinctC I) (InductivePLCADistinctA I)
      (InductivePLCAoutermostconsistency2 I) H) as
      [ [PC [DIS1 DIS2]] |
      [ [PC [DIS1 DIS2]] |
      [ [PC [DIS1 DIS2]] | 
        [PC [DIS1 DIS2]]]]]; try discriminate.
       elim DIS1; now auto.
       exact PC.
   destruct LIN as [LIN | [LIN | LIN]]; [ subst | subst | ].
    apply LCcon; [ exact (in_eq _ _) | right; exact (in_eq _ _) | apply lin_or_app; right; left; constructor ].
    apply LCcon; [ right; exact (in_eq _ _) | right; exact (in_eq _ _) | apply lin_or_app; right; right; apply lin_or_app; right; left; constructor ].
   apply in_app_or in LIN.
   destruct LIN as [LIN | LIN].
    apply LCcon.
     right; right; apply in_or_app; left; exact LIN.
     exact (in_cons _ _ _ (in_eq _ _)).
     apply lin_or_app; right; right; apply lin_or_app; left; apply in_lin; exact LIN.
    apply SYMME.
    assert(PLCAconnect P L C A (o_circuit (lxy ++ lyz ++ lzx)) (o_line l)).
     apply SYMME.
      exact (ELC _ _ LIN O1).
    destruct (IPLCAconnect_add_outpath I _ (o_line l)
    x y z s e ap al lxy lyz lzx (eq_refl _)
    (InductivePLCADistinctC I) (InductivePLCADistinctA I)
    (InductivePLCAoutermostconsistency2 I) H) as
    [ [PC [DIS1 DIS2]] |
    [ [PC [DIS1 DIS2]] |
    [ [PC [DIS1 DIS2]] | 
      [PC [DIS1 DIS2]]]]]; try discriminate.
     elim DIS1; now auto.
     exact PC.
   destruct LIN as [LIN | [LIN | LIN]]; [ subst | subst | ].
    apply (TRANS _ _ _ _ _ (o_circuit (lxy ++ (y, s) :: al ++ (e, z) :: lzx))).
     apply LCcon.
      left; now auto.
      right; left; now auto.
      apply lin_or_app; right; left; now constructor.
     destruct L.
      inversion I.
     apply SYMME, TRANS with (o_line l).
      assert(PLCAconnect P (l :: L) C A (o_circuit c) (o_line l)).
       apply SYMME, ELC.
        left; now auto.
        apply set_remove_in in INC.
         exact INC.
      destruct (IPLCAconnect_add_outpath I _ (o_line l)
      x y z s e ap al lxy lyz lzx (eq_refl _)
      (InductivePLCADistinctC I) (InductivePLCADistinctA I)
      (InductivePLCAoutermostconsistency2 I) H) as
      [ [PC [DIS1 DIS2]] |
      [ [PC [DIS1 DIS2]] |
      [ [PC [DIS1 DIS2]] | 
        [PC [DIS1 DIS2]]]]]; try discriminate.
       exact PC.
       inversion DIS1; subst.
        elim (InductivePLCADistinctC I) with (lxy ++ lyz ++ lzx).
         apply set_remove_in in INC.
          exact INC.
         apply in_rin; exact INC.
      apply SYMME.
      assert(PLCAconnect P (l :: L) C A (o_circuit (lxy ++ lyz ++ lzx)) (o_line l)).
       apply SYMME, ELC.
        left; now auto.
        exact O1.
      destruct (IPLCAconnect_add_outpath I _ (o_line l)
      x y z s e ap al lxy lyz lzx (eq_refl _)
      (InductivePLCADistinctC I) (InductivePLCADistinctA I)
      (InductivePLCAoutermostconsistency2 I) H) as
      [ [PC [DIS1 DIS2]] |
      [ [PC [DIS1 DIS2]] |
      [ [PC [DIS1 DIS2]] | 
        [PC [DIS1 DIS2]]]]]; try discriminate.
       elim DIS1; now auto.
       exact PC.
    apply (TRANS _ _ _ _ _ (o_circuit (lxy ++ (y, s) :: al ++ (e, z) :: lzx))).
     apply LCcon.
      right; left; now auto.
      right; left; now auto.
      apply lin_or_app; right; right; apply lin_or_app; right; left; now constructor.
     destruct L.
      inversion I.
     apply SYMME, TRANS with (o_line l).
      assert(PLCAconnect P (l :: L) C A (o_circuit c) (o_line l)).
       apply SYMME, ELC.
        left; now auto.
        apply set_remove_in in INC.
         exact INC.
      destruct (IPLCAconnect_add_outpath I _ (o_line l)
      x y z s e ap al lxy lyz lzx (eq_refl _)
      (InductivePLCADistinctC I) (InductivePLCADistinctA I)
      (InductivePLCAoutermostconsistency2 I) H) as
      [ [PC [DIS1 DIS2]] |
      [ [PC [DIS1 DIS2]] |
      [ [PC [DIS1 DIS2]] | 
        [PC [DIS1 DIS2]]]]]; try discriminate.
       exact PC.
       inversion DIS1; subst.
        elim (InductivePLCADistinctC I) with (lxy ++ lyz ++ lzx).
         apply set_remove_in in INC.
          exact INC.
         apply in_rin; exact INC.
      apply SYMME.
      assert(PLCAconnect P (l :: L) C A (o_circuit (lxy ++ lyz ++ lzx)) (o_line l)).
       apply SYMME, ELC.
        left; now auto.
        exact O1.
      destruct (IPLCAconnect_add_outpath I _ (o_line l)
      x y z s e ap al lxy lyz lzx (eq_refl _)
      (InductivePLCADistinctC I) (InductivePLCADistinctA I)
      (InductivePLCAoutermostconsistency2 I) H) as
      [ [PC [DIS1 DIS2]] |
      [ [PC [DIS1 DIS2]] |
      [ [PC [DIS1 DIS2]] | 
        [PC [DIS1 DIS2]]]]]; try discriminate.
       elim DIS1; now auto.
       exact PC.
   apply in_app_or in LIN.
   destruct LIN as [LIN | LIN].
    apply (TRANS _ _ _ _ _ (o_circuit (lxy ++ (y, s) :: al ++ (e, z) :: lzx))).
     apply LCcon.
      right; right; apply in_or_app; left; exact LIN.
      exact (in_cons _ _ _ (in_eq _ _)).
      apply lin_or_app; right; right; apply lin_or_app; left; exact (in_lin _ _ LIN).
     clear l LIN.
     destruct L.
      inversion I.
     apply SYMME, TRANS with (o_line l).
      assert(PLCAconnect P (l :: L) C A (o_circuit c) (o_line l)).
       apply SYMME, ELC.
        left; now auto.
        apply set_remove_in in INC.
         exact INC.
      destruct (IPLCAconnect_add_outpath I _ (o_line l)
      x y z s e ap al lxy lyz lzx (eq_refl _)
      (InductivePLCADistinctC I) (InductivePLCADistinctA I)
      (InductivePLCAoutermostconsistency2 I) H) as
      [ [PC [DIS1 DIS2]] |
      [ [PC [DIS1 DIS2]] |
      [ [PC [DIS1 DIS2]] | 
        [PC [DIS1 DIS2]]]]]; try discriminate.
       exact PC.
       inversion DIS1; subst.
        elim (InductivePLCADistinctC I) with (lxy ++ lyz ++ lzx).
         apply set_remove_in in INC.
          exact INC.
         apply in_rin; exact INC.
      apply SYMME.
      assert(PLCAconnect P (l :: L) C A (o_circuit (lxy ++ lyz ++ lzx)) (o_line l)).
       apply SYMME, ELC.
        left; now auto.
        exact O1.
      destruct (IPLCAconnect_add_outpath I _ (o_line l)
      x y z s e ap al lxy lyz lzx (eq_refl _)
      (InductivePLCADistinctC I) (InductivePLCADistinctA I)
      (InductivePLCAoutermostconsistency2 I) H) as
      [ [PC [DIS1 DIS2]] |
      [ [PC [DIS1 DIS2]] |
      [ [PC [DIS1 DIS2]] | 
        [PC [DIS1 DIS2]]]]]; try discriminate.
       elim DIS1; now auto.
       exact PC.
    assert (PLCAconnect P L C A (o_line l) (o_circuit c)).
     apply ELC.
      exact LIN.
      apply set_remove_in in INC; exact INC.
     destruct (IPLCAconnect_add_outpath I _ _
     x y z s e ap al lxy lyz lzx (eq_refl _)
     (InductivePLCADistinctC I) (InductivePLCADistinctA I)
     (InductivePLCAoutermostconsistency2 I) H) as
     [ [PC [DIS1 DIS2]] |
     [ [PC [DIS1 DIS2]] |
     [ [PC [DIS1 DIS2]] | 
       [PC [DIS1 DIS2]]]]]; try discriminate.
      exact PC.
      inversion DIS2; subst.
       elim (InductivePLCADistinctC I) with (lxy ++ lyz ++ lzx).
        apply set_remove_in in INC; exact INC.
        apply in_rin; exact INC.
(* CA add_outpath *)
 -intros c a0 INC IAA.
  simpl in IAA, INC.
  destruct IAA as [IAA | IAA]; [ subst | ].
   destruct INC as [INC | [INC | INC]]; [ subst | subst | ].
    apply CAcon; [ exact (in_eq _ _) | exact (in_eq _ _) | exact (in_eq _ _) ].
    apply (TRANS _ _ _ _ _ (o_circuit (((s, y) :: lyz) ++ (z, e) :: reverse_linelist al))).
     apply (TRANS _ _ _ _ _ (o_line (y, s))).
      apply SYMME, LCcon.
       exact (in_eq _ _).
       exact (in_cons _ _ _ (in_eq _ _)).
       apply lin_or_app; right; left; now constructor.
      apply LCcon.
       exact (in_eq _ _).
       exact (in_eq _ _).
       left; constructor; unfold reverse; now auto.
     apply CAcon.
      exact (in_eq _ _).
      exact (in_eq _ _).
      exact (in_eq _ _).
    apply SYMME, (TRANS _ _ _ _ _ (o_circuit (((s, y) :: lyz) ++ (z, e) :: reverse_linelist al))).
     apply SYMME, CAcon.
      exact (in_eq _ _).
      exact (in_eq _ _).
      exact (in_eq _ _).
     apply (TRANS _ _ _ _ _ (o_line (y, s))).
      apply SYMME, LCcon.
       exact (in_eq _ _).
       exact (in_eq _ _).
       left; constructor; unfold reverse; now auto.
      apply (TRANS _ _ _ _ _ (o_circuit (lxy ++ ((y, s) :: al) ++ (e, z) :: lzx))).
       apply LCcon.
        exact (in_eq _ _).
        exact (in_cons _ _ _ (in_eq _ _)).
        apply lin_or_app; right; left; now constructor.
       destruct L.
        inversion I.
       apply (TRANS _ _ _ _ _ (o_line l)).
        assert (PLCAconnect P (l::L) C A (o_circuit (lxy ++ lyz ++ lzx)) (o_line l)).
         apply SYMME, ELC.
          exact (in_eq _ _).
          exact O1.
         destruct (IPLCAconnect_add_outpath I _ _
         x y z s e ap al lxy lyz lzx (eq_refl _)
         (InductivePLCADistinctC I) (InductivePLCADistinctA I)
         (InductivePLCAoutermostconsistency2 I) H) as
         [ [PC [DIS1 DIS2]] |
         [ [PC [DIS1 DIS2]] |
         [ [PC [DIS1 DIS2]] | 
           [PC [DIS1 DIS2]]]]]; try discriminate.
          elim DIS1; now auto.
          exact PC.
        assert (PLCAconnect P (l::L) C A (o_line l) (o_circuit c)).
         apply ELC.
          exact (in_eq _ _).
          apply set_remove_in in INC; exact INC.
         destruct (IPLCAconnect_add_outpath I _ _
         x y z s e ap al lxy lyz lzx (eq_refl _)
         (InductivePLCADistinctC I) (InductivePLCADistinctA I)
         (InductivePLCAoutermostconsistency2 I) H) as
         [ [PC [DIS1 DIS2]] |
         [ [PC [DIS1 DIS2]] |
         [ [PC [DIS1 DIS2]] | 
           [PC [DIS1 DIS2]]]]]; try discriminate.
          exact PC.
          inversion DIS2; subst.
           elim (InductivePLCADistinctC I) with (lxy ++ lyz ++ lzx).
            apply set_remove_in in INC; exact INC.
            apply in_rin; exact INC.
   destruct INC as [INC | [INC | INC]]; [ subst | subst | ].
    apply (TRANS _ _ _ _ _ (o_line (y, s))).
     apply SYMME, LCcon.
      exact (in_eq _ _).
      exact (in_eq _ _).
      left; constructor; unfold reverse; now auto.
     apply (TRANS _ _ _ _ _ (o_circuit (lxy ++ ((y, s) :: al) ++ (e, z) :: lzx))).
      apply LCcon.
       exact (in_eq _ _).
       exact (in_cons _ _ _ (in_eq _ _)).
       apply lin_or_app; right; left; now constructor.
      assert (PLCAconnect P L C A (o_circuit (lxy ++ lyz ++ lzx)) (o_area a0)).
       apply ECA.
        exact O1.
        exact IAA.
       destruct (IPLCAconnect_add_outpath I _ _
       x y z s e ap al lxy lyz lzx (eq_refl _)
       (InductivePLCADistinctC I) (InductivePLCADistinctA I)
       (InductivePLCAoutermostconsistency2 I) H) as
       [ [PC [DIS1 DIS2]] |
       [ [PC [DIS1 DIS2]] |
       [ [PC [DIS1 DIS2]] | 
         [PC [DIS1 DIS2]]]]]; try discriminate.
        elim DIS1; now auto.
        exact PC.
    assert (PLCAconnect P L C A (o_circuit (lxy ++ lyz ++ lzx)) (o_area a0)).
     apply ECA.
      exact O1.
      exact IAA.
     destruct (IPLCAconnect_add_outpath I _ _
       x y z s e ap al lxy lyz lzx (eq_refl _)
       (InductivePLCADistinctC I) (InductivePLCADistinctA I)
       (InductivePLCAoutermostconsistency2 I) H) as
       [ [PC [DIS1 DIS2]] |
       [ [PC [DIS1 DIS2]] |
       [ [PC [DIS1 DIS2]] | 
         [PC [DIS1 DIS2]]]]]; try discriminate.
      elim DIS1; now auto.
      exact PC.
    assert (PLCAconnect P L C A (o_circuit c) (o_area a0)).
     apply ECA.
      apply set_remove_in in INC; exact INC.
      exact IAA.
     destruct (IPLCAconnect_add_outpath I _ _
       x y z s e ap al lxy lyz lzx (eq_refl _)
       (InductivePLCADistinctC I) (InductivePLCADistinctA I)
       (InductivePLCAoutermostconsistency2 I) H) as
       [ [PC [DIS1 DIS2]] |
       [ [PC [DIS1 DIS2]] |
       [ [PC [DIS1 DIS2]] | 
         [PC [DIS1 DIS2]]]]]; try discriminate.
      exact PC.
      inversion DIS1; subst.
       elim (InductivePLCADistinctC I) with (lxy ++ lyz ++ lzx).
        apply set_remove_in in INC; exact INC.
        apply in_rin; exact INC.
(* add_loop *)
+destruct IND as [EPL [ELC ECA]].
 split.
 (* PL add_loop *)
 intros p l INP LIN.
 simpl in LIN.
 destruct LIN as [LIN | LIN]; [ subst | ].
 apply in_app_or in INP; destruct INP as [INP | INP].
  destruct (path_pl_ll ph (le_Sn_le _ _ lg) INP) as [el [INL PIL]].
   apply (TRANS _ _ _ _ _ (o_line el)).
   apply PLcon; [ apply in_app_iff; left;exact INP | right; apply in_or_app; left; exact PIL | exact INL ].
   apply (TRANS _ _ _ _ _ (o_circuit ((y, x) :: al))).
   apply LCcon; [ right; apply in_or_app; left; exact PIL | exact (in_eq _ _) | right; exact (in_lin _ _ PIL) ].
   apply SYMME, LCcon; [ exact (in_eq _ _) | exact (in_eq _ _) | exact (in_lin _ _ (in_eq _ _)) ].
   apply SYMME, (TRANS _ _ _ _ _ (o_circuit (reverse_linelist ((y, x) :: al)))).
   apply LCcon; [ exact (in_eq _ _) | exact (in_cons _ _ _ (in_eq _ _)) | ].
   unfold reverse_linelist.
   apply LIn_reverse_inv, in_lin.
   rewrite <- in_rev, map_reverse, reverse_reverse.
   exact (in_eq _ _).
   apply (TRANS _ _ _ _ _ (o_area (reverse_linelist ((y, x) :: al) :: a))).
   apply CAcon.
    exact(in_cons _ _ _ (in_eq _ _)).
      exact(in_cons _ _ _ (in_eq _ _)).
      exact(in_eq _ _).
     destruct L.
      inversion I.
     destruct C.
      inversion I.
     assert(PLCAconnect P (l :: L) (c :: C) A (o_area a) (o_point p)).
      apply SYMME, (TRANS _ _ _ _ _ (o_line l)).
      exact (EPL _ _ INP (in_eq _ _)).
      apply (TRANS _ _ _ _ _ (o_circuit c)).
      exact (ELC _ _ (in_eq _ _) (in_eq _ _)).
      exact (ECA _ _ (in_eq _ _) INA).
     destruct (IPLCAconnect_add_loop I _ (o_point p)
     a x y ap al INA (InductivePLCADistinctA I) H) as
     [ [PC [DIS1 DIS2]] |
     [ [PC [DIS1 DIS2]] |
     [ [PC [DIS1 DIS2]] | 
       [PC [DIS1 DIS2]]]]]; try discriminate.
     elim (DIS1 (eq_refl _)).
     exact PC.
     apply in_app_or in LIN.
     destruct LIN as [LIN | LIN].
     apply in_app_or in INP.
     destruct INP as [INP | INP].
     destruct (path_pl_ll ph (le_Sn_le _ _ lg) INP) as [el [INL PIL]].
     apply (TRANS _ _ _ _ _ (o_line el)).
     apply PLcon; [ apply in_app_iff; left;exact INP | right; apply in_or_app; left; exact PIL | exact INL ].
     apply (TRANS _ _ _ _ _ (o_circuit ((y, x) :: al))).
     apply LCcon; [ right; apply in_or_app; left; exact PIL | exact (in_eq _ _) | right; exact (in_lin _ _ PIL) ].
     apply SYMME, LCcon.
      right; apply in_or_app; left.
      exact LIN.
      exact (in_eq _ _).
      exact (in_lin _ _ (in_cons _ _ _ LIN)).
     apply SYMME, (TRANS _ _ _ _ _ (o_circuit (reverse_linelist ((y, x) :: al)))).
     apply LCcon; [ right; apply in_or_app; left; exact LIN | exact (in_cons _ _ _ (in_eq _ _)) | ].
     unfold reverse_linelist.
     apply LIn_reverse_inv, in_lin.
     rewrite <- in_rev, map_reverse, reverse_reverse.
     exact (in_cons _ _ _ LIN).
     apply (TRANS _ _ _ _ _ (o_area (reverse_linelist ((y, x) :: al) :: a))).
     apply CAcon.
      exact (in_cons _ _ _ (in_eq _ _)).
      exact (in_cons _ _ _ (in_eq _ _)).
      exact (in_eq _ _).
     destruct L.
      inversion I.
     destruct C.
      inversion I.
     assert(PLCAconnect P (l0 :: L) (c :: C) A (o_area a) (o_point p)).
      apply SYMME, (TRANS _ _ _ _ _ (o_line l0)).
      exact (EPL _ _ INP (in_eq _ _)).
      apply (TRANS _ _ _ _ _ (o_circuit c)).
      exact (ELC _ _ (in_eq _ _) (in_eq _ _)).
      exact (ECA _ _ (in_eq _ _) INA).
     destruct (IPLCAconnect_add_loop I _ (o_point p)
     a x y ap al INA (InductivePLCADistinctA I) H) as
     [ [PC [DIS1 DIS2]] |
     [ [PC [DIS1 DIS2]] |
     [ [PC [DIS1 DIS2]] | 
       [PC [DIS1 DIS2]]]]]; try discriminate.
     elim (DIS1 (eq_refl _)).
     exact PC.
     apply in_app_or in INP.
     destruct INP as [INP | INP].
     destruct (path_pl_ll ph (le_Sn_le _ _ lg) INP) as [el [INL PIL]].
     apply (TRANS _ _ _ _ _ (o_line el)).
     apply PLcon.
      apply in_or_app; left; exact INP.
      right; apply in_or_app; left; exact PIL.
      exact INL.
     apply (TRANS _ _ _ _ _ (o_circuit (reverse_linelist ((y, x) :: al)))).
     apply LCcon; [ right; apply in_or_app; left; exact PIL | exact (in_cons _ _ _ (in_eq _ _)) | ].
     unfold reverse_linelist.
     apply LIn_reverse_inv, in_lin.
     rewrite <- in_rev, map_reverse, reverse_reverse.
     exact (in_cons _ _ _ PIL).
     apply (TRANS _ _ _ _ _ (o_area (reverse_linelist ((y, x) :: al) :: a))).
     apply CAcon.
      exact (in_cons _ _ _ (in_eq _ _)).
      exact (in_cons _ _ _ (in_eq _ _)).
      exact (in_eq _ _).
     destruct C.
      inversion I.
     assert(PLCAconnect P L (c :: C) A (o_area a) (o_line l)).
      apply SYMME, (TRANS _ _ _ _ _ (o_circuit c)).
      exact (ELC _ _ LIN (in_eq _ _)).
      exact (ECA _ _ (in_eq _ _) INA).
     destruct (IPLCAconnect_add_loop I _ (o_line l)
     a x y ap al INA (InductivePLCADistinctA I) H) as
     [ [PC [DIS1 DIS2]] |
     [ [PC [DIS1 DIS2]] |
     [ [PC [DIS1 DIS2]] | 
       [PC [DIS1 DIS2]]]]]; try discriminate.
     elim (DIS1 (eq_refl _)).
     exact PC.
     destruct (IPLCAconnect_add_loop I _ (o_line l)
     a x y ap al INA (InductivePLCADistinctA I) (EPL _ _ INP LIN)) as
     [ [PC [DIS1 DIS2]] |
     [ [PC [DIS1 DIS2]] |
     [ [PC [DIS1 DIS2]] | 
       [PC [DIS1 DIS2]]]]]; try discriminate.
     exact PC.
 split.
  (* LC add_loop *)
  intros l c LIN INC.
  simpl in *.
  destruct INC as [INC | [INC | INC]]; [ subst | subst | ].
   destruct LIN as [LIN | LIN]; [ subst | ].
    apply LCcon; [ exact (in_eq _ _) | exact (in_eq _ _) | exact (in_lin _ _ (in_eq _ _)) ].
   apply in_app_or in LIN.
   destruct LIN as [LIN | LIN].
    apply LCcon; [ right; apply in_or_app; left; exact LIN | exact (in_eq  _ _) | right; exact (in_lin _ _ LIN)].
    apply SYMME, (TRANS _ _ _ _ _ (o_line (y ,x))).
    apply SYMME, LCcon.
     exact (in_eq _ _).
     exact (in_eq _ _).
     exact (in_lin _ _ (in_eq _ _)).
    apply (TRANS _ _ _ _ _ (o_circuit (reverse_linelist ((y, x) :: al)))).
    apply LCcon.
     exact (in_eq _  _).
     exact (in_cons _ _ _ (in_eq _ _)).
     unfold reverse_linelist.
     apply LIn_reverse_inv, in_lin.
     rewrite <- in_rev, map_reverse, reverse_reverse.
     exact (in_eq _ _).
    apply (TRANS _ _ _ _ _ (o_area (reverse_linelist ((y, x) :: al) :: a))).
    apply CAcon.
     exact (in_cons _ _ _  (in_eq _ _)).
     exact (in_cons _ _ _  (in_eq _ _)).
     exact (in_eq _ _).
    destruct C.
     inversion I.
    assert(PLCAconnect P L (c :: C) A (o_area a) (o_line l)).
     apply SYMME, (TRANS _ _ _ _ _ (o_circuit c)).
     exact (ELC _ _ LIN (in_eq _ _)).
     exact (ECA _ _ (in_eq _ _) INA).
    destruct (IPLCAconnect_add_loop I _ (o_line l)
    a x y ap al INA (InductivePLCADistinctA I) H) as
    [ [PC [DIS1 DIS2]] |
    [ [PC [DIS1 DIS2]] |
    [ [PC [DIS1 DIS2]] | 
      [PC [DIS1 DIS2]]]]]; try discriminate.
    elim (DIS1 (eq_refl _)).
    exact PC.
   destruct LIN as [LIN | LIN]; [ subst | ].
    apply LCcon.
     exact (in_eq _ _).
     exact (in_cons _ _ _ (in_eq _ _)).
     unfold reverse_linelist.
     apply LIn_reverse_inv, in_lin.
     rewrite <- in_rev, map_reverse, reverse_reverse.
     exact (in_eq _ _).
   apply in_app_or in LIN.
   destruct LIN as [LIN | LIN].
    apply LCcon.
     right; apply in_or_app; left; exact LIN.
     exact (in_cons _ _ _ (in_eq _ _)).
     unfold reverse_linelist.
     apply LIn_reverse_inv, in_lin.
     rewrite <- in_rev, map_reverse, reverse_reverse.
     exact (in_cons _ _ _ LIN).
    apply SYMME, (TRANS _ _ _ _ _ (o_area (reverse_linelist ((y, x) :: al) :: a))).
    apply CAcon.
     exact (in_cons _ _ _ (in_eq _ _)).
     exact (in_cons _ _ _ (in_eq _ _)).
     exact (in_eq _ _).
    destruct C.
     inversion I.
    assert(PLCAconnect P L (c :: C) A (o_area a) (o_line l)).
     apply SYMME, (TRANS _ _ _ _ _ (o_circuit c)).
     exact (ELC _ _ LIN (in_eq _ _)).
     exact (ECA _ _ (in_eq _ _) INA).
    destruct (IPLCAconnect_add_loop I _ (o_line l)
    a x y ap al INA (InductivePLCADistinctA I) H) as
    [ [PC [DIS1 DIS2]] |
    [ [PC [DIS1 DIS2]] |
    [ [PC [DIS1 DIS2]] | 
      [PC [DIS1 DIS2]]]]]; try discriminate.
    elim (DIS1 (eq_refl _)).
    exact PC.
   destruct LIN as [LIN | LIN]; [ subst | ].
    apply (TRANS _ _ _ _ _ (o_circuit (reverse_linelist ((y, x) :: al)))).
    apply LCcon.
     exact (in_eq _ _).
     exact (in_cons _ _ _ (in_eq _ _)).
     unfold reverse_linelist.
     apply LIn_reverse_inv, in_lin.
     rewrite <- in_rev, map_reverse, reverse_reverse.
     exact (in_eq _ _).
    apply (TRANS _ _ _ _ _ (o_area (reverse_linelist ((y, x) :: al) :: a))).
    apply CAcon.
     exact (in_cons _ _ _ (in_eq _ _)).
     exact (in_cons _ _ _ (in_eq _ _)).
     exact (in_eq _ _).
    destruct (IPLCAconnect_add_loop I _ (o_circuit c)
    a x y ap al INA (InductivePLCADistinctA I) (SYMME _ _ _ _ _ _ (ECA _ _ INC INA))) as
    [ [PC [DIS1 DIS2]] |
    [ [PC [DIS1 DIS2]] |
    [ [PC [DIS1 DIS2]] | 
      [PC [DIS1 DIS2]]]]]; try discriminate.
    elim (DIS1 (eq_refl _)).
    exact PC.
   apply in_app_or in LIN.
   destruct LIN as [LIN | LIN].
    apply (TRANS _ _ _ _ _ (o_circuit (reverse_linelist ((y, x) :: al)))).
    apply LCcon.
     right; apply in_or_app; left; exact LIN.
     exact (in_cons _ _ _ (in_eq _ _)).
     unfold reverse_linelist.
     apply LIn_reverse_inv, in_lin.
      rewrite <- in_rev, map_reverse, reverse_reverse.
      exact (in_cons _ _ _ LIN).
     apply (TRANS _ _ _ _ _ (o_area (reverse_linelist ((y, x) :: al) :: a))).
     apply CAcon.
      exact (in_cons _ _ _ (in_eq _ _)).
      exact (in_cons _ _ _ (in_eq _ _)).
      exact (in_eq _ _).
     destruct (IPLCAconnect_add_loop I _ (o_circuit c)
     a x y ap al INA (InductivePLCADistinctA I) (SYMME _ _ _ _ _ _ (ECA _ _ INC INA))) as
     [ [PC [DIS1 DIS2]] |
     [ [PC [DIS1 DIS2]] |
     [ [PC [DIS1 DIS2]] | 
       [PC [DIS1 DIS2]]]]]; try discriminate.
     elim (DIS1 (eq_refl _)).
     exact PC.
     destruct (IPLCAconnect_add_loop I _ (o_circuit c)
     a x y ap al INA (InductivePLCADistinctA I) (ELC _ _ LIN INC)) as
     [ [PC [DIS1 DIS2]] |
     [ [PC [DIS1 DIS2]] |
     [ [PC [DIS1 DIS2]] | 
       [PC [DIS1 DIS2]]]]]; try discriminate.
     exact PC.
  (* CA add_loop *)
   intros c a0 INC IAA.
   simpl in IAA, INC.
   destruct IAA as [IAA | [IAA | IAA]]; [ subst | subst | ].
    destruct INC as [INC | [INC | INC]]; [ subst | subst | ].
     apply CAcon; [ exact (in_eq _ _) | exact (in_eq _ _) | exact (in_eq _ _) ].
     apply (TRANS _ _ _ _ _ (o_circuit ((y, x) :: al))).
     apply (TRANS _ _ _ _ _ (o_line (y, x))).
     apply SYMME, LCcon.
      exact (in_eq _ _).
      exact (in_cons _ _ _ (in_eq _ _)).
      unfold reverse_linelist.
      apply LIn_reverse_inv, in_lin.
      rewrite <- in_rev, map_reverse, reverse_reverse.
      exact (in_eq _ _).
     apply LCcon.
      exact (in_eq _ _).
      exact (in_eq _ _).
      exact (in_lin _ _ (in_eq _ _)).
     apply CAcon.
      exact (in_eq _ _).
      exact (in_eq _ _).
      exact (in_eq _ _).
     apply SYMME, (TRANS _ _ _ _ _ (o_circuit ((y, x) :: al))).
     apply SYMME, CAcon.
      exact (in_eq _ _).
      exact (in_eq _ _).
      exact (in_eq _ _).
     apply (TRANS _ _ _ _ _ (o_line (y, x))).
     apply SYMME, LCcon.
      exact (in_eq _ _).
      exact (in_eq _ _).
      exact (in_lin _ _ (in_eq _ _ )).
     apply (TRANS _ _ _ _ _ (o_circuit (reverse_linelist ((y, x) :: al)))).
     apply LCcon.
      exact (in_eq _ _).
      exact (in_cons _ _ _ (in_eq _ _)).
      unfold reverse_linelist.
      apply LIn_reverse_inv, in_lin.
      rewrite <- in_rev, map_reverse, reverse_reverse.
      exact (in_eq _ _).
     apply (TRANS _ _ _ _ _ (o_area (reverse_linelist ((y, x) :: al) :: a))).
     apply CAcon.
      exact (in_cons _ _ _ (in_eq _ _)).
      exact (in_cons _ _ _ (in_eq _ _)).
      exact (in_eq _ _).
     destruct (IPLCAconnect_add_loop I (o_area a) (o_circuit c)
     a x y ap al INA (InductivePLCADistinctA I) (SYMME _ _ _ _ _ _ (ECA _ _ INC INA))) as
     [ [PC [DIS1 DIS2]] |
     [ [PC [DIS1 DIS2]] |
     [ [PC [DIS1 DIS2]] | 
       [PC [DIS1 DIS2]]]]]; try discriminate.
     elim (DIS1 (eq_refl _)).
     exact PC.
    destruct INC as [INC | [INC | INC]]; [ subst | subst | ].
     apply (TRANS _ _ _ _ _ (o_line (y, x))).
     apply SYMME, LCcon.
      exact (in_eq _ _).
      exact (in_eq _ _).
      exact (in_lin _ _ (in_eq _ _)).
     apply (TRANS _ _ _ _ _ (o_circuit (reverse_linelist ((y, x) :: al)))).
     apply LCcon.
      exact (in_eq _ _).
      exact (in_cons _ _ _ (in_eq _ _)).
      unfold reverse_linelist.
      apply LIn_reverse_inv, in_lin.
      rewrite <- in_rev, map_reverse, reverse_reverse.
      exact (in_eq _ _).
     apply CAcon.
      exact (in_cons _ _ _ (in_eq _ _)).
      exact (in_cons _ _ _ (in_eq _ _)).
      exact (in_eq _ _).
     apply CAcon.
      exact (in_cons _ _ _ (in_eq _ _)).
      exact (in_cons _ _ _ (in_eq _ _)).
      exact (in_eq _ _).
     destruct (IPLCAconnect_add_loop I (o_circuit c) (o_area a)
     a x y ap al INA (InductivePLCADistinctA I)(ECA _ _ INC INA)) as
     [ [PC [DIS1 DIS2]] |
     [ [PC [DIS1 DIS2]] |
     [ [PC [DIS1 DIS2]] | 
       [PC [DIS1 DIS2]]]]]; try discriminate.
     elim (DIS2 (eq_refl _)).
     exact PC.
    destruct (eq_listcircuit_dec a0 a) as [EQA |NEQA]; [ subst | ].
     elim (InductivePLCADistinctA I _ (set_remove_in _ _ _ _ IAA)).
    apply in_ain.
    exact IAA.
    apply set_remove_in in IAA.
    destruct INC as [INC | [INC | INC]]; [ subst | subst | ].
     apply (TRANS _ _ _ _ _ (o_line (y, x))).
     apply SYMME, LCcon.
      exact (in_eq _ _).
      exact (in_eq _ _).
      exact (in_lin _ _ (in_eq _ _)).
     apply (TRANS _ _ _ _ _ (o_circuit (reverse_linelist ((y, x) :: al)))).
     apply LCcon.
      exact (in_eq _ _).
      exact (in_cons _ _ _ (in_eq _ _)).
      unfold reverse_linelist.
      apply LIn_reverse_inv, in_lin.
      rewrite <- in_rev, map_reverse, reverse_reverse.
      exact (in_eq _ _).
     apply (TRANS _ _ _ _ _ (o_area (reverse_linelist ((y, x) :: al) :: a))).
     apply CAcon.
      exact (in_cons _ _ _ (in_eq _ _)).
      exact (in_cons _ _ _ (in_eq _ _)).
      exact (in_eq _ _).
     destruct C.
      inversion I.
     assert(PLCAconnect P L (c :: C) A (o_area a) (o_area a0)).
      apply (TRANS _ _ _ _ _ (o_circuit c)).
      exact (SYMME _ _ _ _ _ _ (ECA _ _ (in_eq _ _) INA)).
      exact (ECA _ _ (in_eq _ _) IAA).
     destruct (IPLCAconnect_add_loop I (o_area a) (o_area a0)
     a x y ap al INA (InductivePLCADistinctA I) H) as
     [ [PC [DIS1 DIS2]] |
     [ [PC [DIS1 DIS2]] |
     [ [PC [DIS1 DIS2]] | 
       [PC [DIS1 DIS2]]]]]; try discriminate.
     elim (DIS1 (eq_refl _)).
     exact PC.
     inversion DIS2; subst.
     elim (NEQA (eq_refl _)).
     inversion DIS2; subst.
     elim (NEQA (eq_refl _)).
     apply (TRANS _ _ _ _ _ (o_area (reverse_linelist ((y, x) :: al) :: a))).
     apply CAcon.
      exact (in_cons _ _ _ (in_eq _ _)).
      exact (in_cons _ _ _ (in_eq _ _)).
      exact (in_eq _ _).
     destruct C.
      inversion I.
     assert(PLCAconnect P L (c :: C) A (o_area a) (o_area a0)).
      apply (TRANS _ _ _ _ _ (o_circuit c)).
      exact (SYMME _ _ _ _ _ _ (ECA _ _ (in_eq _ _) INA)).
      exact (ECA _ _ (in_eq _ _) IAA).
     destruct (IPLCAconnect_add_loop I (o_area a) (o_area a0)
     a x y ap al INA (InductivePLCADistinctA I) H) as
     [ [PC [DIS1 DIS2]] |
     [ [PC [DIS1 DIS2]] |
     [ [PC [DIS1 DIS2]] | 
       [PC [DIS1 DIS2]]]]]; try discriminate.
     elim (DIS1 (eq_refl _)).
     exact PC.
     inversion DIS2; subst.
     elim (NEQA (eq_refl _)).
     inversion DIS2; subst.
     elim (NEQA (eq_refl _)).
     destruct (IPLCAconnect_add_loop I (o_circuit c) (o_area a0)
     a x y ap al INA (InductivePLCADistinctA I) (ECA _ _ INC IAA)) as
     [ [PC [DIS1 DIS2]] |
     [ [PC [DIS1 DIS2]] |
     [ [PC [DIS1 DIS2]] | 
       [PC [DIS1 DIS2]]]]]; try discriminate.
     exact PC.
     inversion DIS2; subst.
     elim (NEQA (eq_refl _)).
Qed.




Theorem IPLCA_is_satisfied_PLCAconnected :
forall(P : list Point)(L : list Line)(C : list Circuit)(A : list Area)(o : Circuit),
 I P L C A o -> PLCAconnected P L C A.
Proof.
unfold PLCAconnected.
intros P L C A o I o1 o2 IO1 IO2.
destruct o1; destruct o2; simpl in IO1, IO2.
destruct L.
 inversion I.
apply (TRANS _ _ _ _ _ (o_line l)).
 apply (IPLCAconnection _ _ _ _ _ I); [ exact IO1 | exact (in_eq _ _) ].
 apply SYMME, (IPLCAconnection _ _ _ _ _ I); [ exact IO2 | exact (in_eq _ _) ].
apply (IPLCAconnection _ _ _ _ _ I); [ exact IO1 | exact IO2 ].
destruct L.
 inversion I.
apply SYMME, (TRANS _ _ _ _ _ (o_line l)).
apply SYMME, (IPLCAconnection _ _ _ _ _ I); [ exact (in_eq _ _) | exact IO2 ].
apply SYMME, (IPLCAconnection _ _ _ _ _ I); [ exact IO1 | exact (in_eq _ _) ].
destruct L.
 inversion I.
destruct C.
 inversion I.
apply (TRANS _ _ _ _ _ (o_line l)).
apply (IPLCAconnection _ _ _ _ _ I); [ exact IO1 | exact (in_eq _ _) ].
apply (TRANS _ _ _ _ _ (o_circuit c)).
apply (IPLCAconnection _ _ _ _ _ I); [ exact (in_eq _ _) | exact (in_eq _ _) ].
apply (IPLCAconnection _ _ _ _ _ I); [ exact (in_eq _ _) | exact IO2 ].
apply SYMME, (IPLCAconnection _ _ _ _ _ I); [ exact IO2 | exact IO1 ].
destruct C.
 inversion I.
apply (TRANS _ _ _ _ _ (o_circuit c)).
apply (IPLCAconnection _ _ _ _ _ I); [ exact IO1 | exact (in_eq _ _) ].
apply SYMME, (IPLCAconnection _ _ _ _ _ I); [ exact IO2 | exact (in_eq _ _) ].
apply (IPLCAconnection _ _ _ _ _ I); [ exact IO1 | exact IO2 ].
destruct C.
 inversion I.
apply (TRANS _ _ _ _ _ (o_circuit c)).
apply (IPLCAconnection _ _ _ _ _ I); [ exact IO1 | exact (in_eq _ _) ].
apply (IPLCAconnection _ _ _ _ _ I); [ exact (in_eq _ _) | exact IO2 ].
destruct L.
 inversion I.
apply (TRANS _ _ _ _ _ (o_line l)).
apply SYMME, (IPLCAconnection _ _ _ _ _ I); [ exact (in_eq _ _) | exact IO1 ].
apply SYMME, (IPLCAconnection _ _ _ _ _ I); [ exact IO2 | exact (in_eq _ _) ].
apply SYMME, (IPLCAconnection _ _ _ _ _ I); [ exact IO2 | exact IO1 ].
destruct L.
 inversion I.
apply (TRANS _ _ _ _ _ (o_line l)).
apply SYMME, (IPLCAconnection _ _ _ _ _ I); [ exact (in_eq _ _) | exact IO1 ].
apply (IPLCAconnection _ _ _ _ _ I); [ exact (in_eq _ _) | exact IO2 ].
apply (IPLCAconnection _ _ _ _ _ I); [ exact IO1 | exact IO2 ].
destruct L.
 inversion I.
destruct C.
 inversion I.
apply (TRANS _ _ _ _ _ (o_circuit c)).
apply SYMME, (IPLCAconnection _ _ _ _ _ I); [ exact (in_eq _ _) | exact IO1 ].
apply (TRANS _ _ _ _ _ (o_line l)).
apply SYMME, (IPLCAconnection _ _ _ _ _ I); [ exact (in_eq _ _) | exact (in_eq _ _) ].
apply SYMME, (IPLCAconnection _ _ _ _ _ I); [ exact IO2 | exact (in_eq _ _) ].
destruct C.
 inversion I.
apply (TRANS _ _ _ _ _ (o_circuit c)).
apply SYMME, (IPLCAconnection _ _ _ _ _ I); [ exact (in_eq _ _) | exact IO1 ].
apply SYMME, (IPLCAconnection _ _ _ _ _ I); [ exact IO2 | exact (in_eq _ _) ].
apply SYMME, (IPLCAconnection _ _ _ _ _ I); [ exact IO2 | exact IO1 ].
destruct C.
 inversion I.
apply (TRANS _ _ _ _ _ (o_circuit c)).
apply SYMME, (IPLCAconnection _ _ _ _ _ I); [ exact (in_eq _ _) | exact IO1 ].
apply (IPLCAconnection _ _ _ _ _ I); [ exact (in_eq _ _) | exact IO2 ].
Qed.
